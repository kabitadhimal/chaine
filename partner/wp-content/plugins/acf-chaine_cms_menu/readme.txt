=== Advanced Custom Fields: Chaine CMS Menu Field ===
Contributors: Procab Studio
Tags: menu, chaine, procab, cms, custom menu
Requires at least: 3.5
Tested up to: 3.8.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

SHORT_Custom Menu for CMS Pages in Chaine Du Bonheur

== Custom Menu for CMS Pages in Chaine Du Bonheur ==

Custom Menu for CMS Pages in Chaine Du Bonheur

= Compatibility =

This ACF field type is compatible with:
* ACF 5
* ACF 4

== Installation ==

1. Copy the `acf-chaine_cms_menu` folder into your `wp-content/plugins` folder
2. Activate the Chaine CMS Menu plugin via the plugins admin page
3. Create a new field via ACF and select the Chaine CMS Menu type
4. Please refer to the Custom Menu for CMS Pages in Chaine Du Bonheur for more info regarding the field type settings

== Changelog ==

= 1.0.0 =
* Initial Release.