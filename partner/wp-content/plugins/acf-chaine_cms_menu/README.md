# ACF Field Type Template

Welcome to the Advanced Custom Fields field type template repository.
Here you will find a starter-kit for creating a new ACF field type. This start-kit will work as a normal WP plugin.

For more information about creating a new field type, please read the following article:
http://www.advancedcustomfields.com/resources/tutorials/creating-a-new-field-type/

### Structure

* `/assets`:  folder for all asset files.
* `/assets/css`:  folder for .css files.
* `/assets/images`: folder for image files
* `/assets/js`: folder for .js files
* `/fields`:  folder for all field class files.
* `/fields/chaine_cms_menu-v5.php`: Field class compatible with ACF version 5 
* `/fields/chaine_cms_menu-v4.php`: Field class compatible with ACF version 4
* `/lang`: folder for .pot, .po and .mo files
* `acf-chaine_cms_menu.php`: Main plugin file that includes the correct field file based on the ACF version
* `readme.txt`: WordPress readme file to be used by the WordPress repository

### step 1.

This template uses `PLACEHOLDERS` such as `chaine_cms_menu` throughout the file names and code. Use the following list of placeholders to do a 'find and replace':

* `chaine_cms_menu`: Single word, no spaces. Underscores allowed. eg. donate_button
* `Chaine CMS Menu`: Multiple words, can include spaces, visible when selecting a field type. eg. Donate Button
* `http://procab.ch`: Url to the github or WordPress repository
* `menu, chaine, procab, cms, custom menu`: Comma separated list of relevant tags
* `SHORT_Custom Menu for CMS Pages in Chaine Du Bonheur`: Brief Custom Menu for CMS Pages in Chaine Du Bonheur of the field type, no longer than 2 lines
* `EXTENDED_Custom Menu for CMS Pages in Chaine Du Bonheur`: Extended Custom Menu for CMS Pages in Chaine Du Bonheur of the field type
* `Procab Studio`: Name of field type author
* `http://procab.ch`: URL to author's website

### step 2.

Edit the `chaine_cms_menu-v5.php` and `chaine_cms_menu-v4.php` files (now renamed using your field name) and include your custom code in the appropriate functions. 
Please note that v4 and v5 field classes have slightly different functions. For more information, please read:
* http://www.advancedcustomfields.com/resources/tutorials/creating-a-new-field-type/

### step 3.

Edit this `README.md` file with the appropriate information and delete all content above and including the following line.

-----------------------

# ACF Chaine CMS Menu Field

SHORT_Custom Menu for CMS Pages in Chaine Du Bonheur

-----------------------

### Custom Menu for CMS Pages in Chaine Du Bonheur

Custom Menu for CMS Pages in Chaine Du Bonheur

### Compatibility

This ACF field type is compatible with:
* ACF 5
* ACF 4

### Installation

1. Copy the `acf-chaine_cms_menu` folder into your `wp-content/plugins` folder
2. Activate the Chaine CMS Menu plugin via the plugins admin page
3. Create a new field via ACF and select the Chaine CMS Menu type
4. Please refer to the Custom Menu for CMS Pages in Chaine Du Bonheur for more info regarding the field type settings

### Changelog
Please see `readme.txt` for changelog
