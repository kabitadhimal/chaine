<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Chaîne_du_Bonheur
 */

get_header(); ?>
	
	<section class="section-backlink">
        <div class="container">
           <div class="btnback">     
           <p><?php esc_html_e( 'Nous n\'avons malheureusement rien trouvé à cette adresse. Veuillez cliquer sur l\'un des liens suivants ou utiliser le moteur de recherche.', 'ch_du_bo' ); ?></p>
           </div>
      	</div>
  	</section>

	<section class="section-two-col">
        <div class="container">
            <div class="row">
          
                	<div class="col-md-8">
                    	<div class="media-body">
                    	<div class="post-Listing">

							<?php
								get_search_form();
							?>
                        </div>
                    
                    </div>
                    </div>
                    <div class="col-md-4">
                    
                    	<aside class="widget-area" role="complementary">
                        
                        	<section class="widget widget_contact">
                        		<?php
                        			$img = get_field('contact_image', 5);
                        			$name = get_field('name', 5);
                        			$job_title = get_field('job_title', 5);
                        			$phone_number = get_field('phone_number', 5);
                        			$email_address = get_field('email_address', 5);
                        		?>
                        		<?php if($img): ?>
	                            	<img src="<?php echo $img['sizes']['fundraising-thumb']; ?>" alt="">
	                            <?php endif; ?>
                                <address>
                                	<?php echo $name; ?> <br>
                                	<?php echo $job_title; ?> <br>
                                    Tel: <a href="tel:<?php echo $phone_number; ?>"><?php echo $phone_number; ?></a> <br>
                                    E-Mail: <a href="mailto:<?php echo $email_address; ?>"><?php echo $email_address; ?></a>
                                	
                                </address>
                            
                            </section>
                            
                            
                            
                                   <section id="search-2" class="widget widget_email">
                                   <h4 class="widget-title">Recevez nos communiqués</h4>
                                   <form role="search" method="get" class="email-form" action="#">
                                          
                                                <input class="search-field" placeholder="Votre adresse E-mail" value="" name="s" type="text">
                                                <input class="search-submit" value="go" type="submit">
                                        </form></section>

                            <section  class="widget widget_search">
                            	<h4 class="widget-title">Recherche</h4>
                            	<?php get_search_form(true); ?>
                        	</section>
                                
                            <section  class="widget widget_search">
                            	<h4 class="widget-title">Archives</h4>
                            	<form  class="search-form" action="#">
                              	  	<select class="select" onchange="document.location.href=this.options[this.selectedIndex].value;">
                                  		<?php wp_get_archives( array( 'type' => 'yearly', 'format'	=> 'option' ) ); ?>
                                  	</select>
                                </form>

                            </section>   
                                <?php
                                include_once(ABSPATH.WPINC.'/feed.php');
							    $rss = fetch_feed('http://local.procab.com/chaine/feed/');
                                if( ! is_wp_error( $rss ) ) {
    							    $maxitems = $rss->get_item_quantity(3);

    							    $rss_items = $rss->get_items(0, $maxitems);
                                }
                                ?>
                                <section  class="widget widget_archive">
                                  <h4 class="widget-title">Articles recents</h4>
                                  
                                  	<ul>
                                  		<?php 
											if(!empty($rss_items)){
												foreach ( $rss_items as $item ) : ?>
													<li>
														<a href="<?php echo $item->get_permalink(); ?>"><span class="date"><?php echo $item->get_date('d.m.Y'); ?></span>
														<p><?php echo $item->get_title(); ?></p>
														</a>
													</li>
												<?php endforeach;
											}
										?>
                                    </ul>
                                  
                                
                                </section> 
                                
                                
                                 <section  class="widget widget_tags">
                                 <h4 class="widget-title">Tags</h4>
                                 	<?php wp_tag_cloud( 'smallest=16&largest=16&format=list&link=edit' ); ?>
                                 </section>
                                
                                <section  class="widget widget_social"> 
                                <h4 class="widget-title">Suivez-nous</h4>
                                <ul class="social-share">
                                <li class="fb"><a href="<?php echo get_theme_mod('ch_du_bo_fb_link_'.CH_LANG_CODE); ?>"><i class="fa fa-facebook"></i></a></li>
                                <li class="tw"><a href="<?php echo get_theme_mod('ch_du_bo_tw_link_'.CH_LANG_CODE); ?>"><i class="fa fa-twitter"></i></a></li>
                                <li class="insta"><a href="<?php echo get_theme_mod('ch_du_bo_insta_link_'.CH_LANG_CODE); ?>"><i class="fa fa-instagram"></i></a></li>
                                <li class="youtube"><a href="<?php echo get_theme_mod('ch_du_bo_youtube_link_'.CH_LANG_CODE); ?>"><i class="fa fa-youtube-square"></i></a></li>
                                <li class="pint"><a href="<?php echo get_theme_mod('ch_du_bo_pin_link_'.CH_LANG_CODE); ?>"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                                
                                </section>
                        </aside>
                    
                    </div>
                
            </div><!--/.row-->
        </div><!--/.container-->
        
   </section><!--/.section news-->

<?php
get_footer();

