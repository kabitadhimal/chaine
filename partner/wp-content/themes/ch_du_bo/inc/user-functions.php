<?php

// Hide admin bar for other users than admin
if(is_user_logged_in() && !current_user_can( 'create_users', $user->ID )){
	add_filter('show_admin_bar', '__return_false');
}

function chaine_get_partner_menu($user){
	?>
	<nav>
		<?php
			$navArgs =array(
				'menu_id' 			=>	'primary-menu',
				'container' 		=>	'ul',
				'menu_class'		=>	'nav navbar-nav',
				'walker'			=>	new Primary_Nav_Walker()
			);
			$hideMenu = false;
			if( in_array( 'administrator', $user->roles ) ){
				$navArgs = array_merge($navArgs, array('theme_location' 	=>	'primary'));
			}else if( in_array( 'ong-partenaires', $user->roles ) ){
				$navArgs = array_merge($navArgs, array('menu' 	=>	'PARTNER NGO'));
			}else if( in_array( 'conseil-de-fondation', $user->roles ) ){
				$navArgs = array_merge($navArgs, array('menu' 	=>	'Conseil de Fondation'));
			}else if( in_array( 'ong-actives-en-suisse', $user->roles ) ){
				$navArgs = array_merge($navArgs, array('menu' 	=>	'COPRO'));
			}else{
				$hideMenu = true;
			}
			if(!$hideMenu){
				wp_nav_menu( $navArgs );
			}								
		?>
	</nav>
	<?php
}

//page access
add_action('wp',function(){
	global $post;
	$user = wp_get_current_user();
	if(!is_user_logged_in() && $post->post_name != 'login'){
		//wp_redirect(site_url('/login/').'?redirect_to='.urlencode(get_permalink($post->ID)));
		if(!is_404()){
			wp_redirect(site_url('/login/'));
			exit();
		}		
	}else{
		if(!current_user_can( 'create_users', $user->ID )){
			$accessible_by = get_post_meta($post->ID, 'accessible_by', true);			
			$access = array_intersect($user->roles, (array)$accessible_by);
			if(!is_front_page() && empty($access)){			
				if($post->post_name != 'login'){
					global $wp_query;
					$wp_query->set_404();
					status_header( 404 );
					get_template_part( 404 ); exit();
				}			
			}
		}
	}
});

//access backoffice only for admin
if(is_admin()){
	if ( !current_user_can( 'create_users', $user_id ) ){
		if(! ( defined( 'DOING_AJAX' ) && DOING_AJAX )){
			wp_redirect(site_url('/'));
			exit();
		}		
	}
}

//redirect to login page after logout
add_action('logout_redirect', function(){
	//return site_url('/login/').'?redirect_to='.$_SERVER['HTTP_REFERER'];
	return site_url('/login/');
});

//handle after login
add_action('partner_login_head',function(){
	$creds = array();
	$creds['user_login'] = $_POST['log'];
	$creds['user_password'] = $_POST['pwd'];
	$creds['remember'] = isset($_POST['rememberme']);
	$user = wp_signon( $creds, false );	
	if ( is_wp_error($user) ){
		$error = $user->get_error_message();
		return $error;
	}else{
		wp_set_current_user($user->ID);
		wp_set_auth_cookie( $user->ID, ( bool ) $creds['remember'] );
		if($_POST['redirect_to'] && $_POST['redirect_to'] != site_url('/login/')){
			wp_redirect($_POST['redirect_to']);
		}else{
			if( in_array('ong-partenaires', $user->roles)){
				//ong-partenaires
				wp_redirect(site_url('/partner-ngo/'));
			}else if(in_array('conseil-de-fondation', $user->roles)){
				//conseil-de-fondation
				wp_redirect(site_url('/conseil-de-fondation/'));
			}else if(in_array('ong-actives-en-suisse', $user->roles)){
				//ong-actives-en-suisse
				wp_redirect(site_url('/copro/'));
			}else if(in_array('administrator', $user->roles)){
				//admin
				wp_redirect(admin_url());
			}else{
				//others
				wp_redirect(site_url());
			}
			exit();
		}
	}
});

/*
add_action ( 'show_user_profile', 'chaine_show_extra_profile_fields' );
add_action ( 'edit_user_profile', 'chaine_show_extra_profile_fields' );

function chaine_show_extra_profile_fields($user){
	?>
	<h3>Additonal Fields</h3>
	<table class="form-table">
	
		<tr>
			<th><label for="redirect_to">Login Redirect (URL)</label></th>
			<td>
				<input type="text" name="redirect_to" id="redirect_to" value="<?php echo esc_attr( get_the_author_meta( 'redirect_to', $user->ID ) ); ?>" class="regular-text" /><br />
				<p class="description">If kept blank, user will be redirected to homepage.</p>
			</td>
		</tr>
		
		<tr>
			<th><label for="accessposts">Accessable Posts</label></th>
			<td>
				<?php 
					$accessposts = get_the_author_meta( 'accessposts', $user->ID );
					$args = array(
						'post_type'	=>	array('post'),
						'posts_per_page'	=>	'-1'
					);
					$query = new WP_Query($args);
					if($query->have_posts()){
						?><select name="accessposts[]" id="accessposts" multiple style="height:200px">
						<option>Select Posts</option>
						<?php
						while($query->have_posts()){
							$query->the_post();
							?><option value="<?php the_ID(); ?>"
							<?php
								if(in_array(get_the_ID(), $accessposts)){
									echo ' SELECTED="SELECTED"';
								}
							?>><?php the_title(); ?></option><?php
						}
						?></select><br>
						<p class="description">We can select multiple posts. Only the selected posts can be viewed by user.</p>
						<?php
					}
				?>
			</td>
		</tr>
		
		<tr>
			<th><label for="accesspages">Accessable Pages</label></th>
			<td>
				<?php 
					$accesspages = get_the_author_meta( 'accesspages', $user->ID );
					$args = array(
						'post_type'	=>	array('page'),
						'posts_per_page'	=>	'-1'
					);
					$query = new WP_Query($args);
					if($query->have_posts()){
						?><select name="accesspages[]" id="accesspages" multiple style="height:200px">
						<option>Select Pages</option>
						<?php
						while($query->have_posts()){
							$query->the_post();
							?><option value="<?php the_ID(); ?>"
							<?php
								if(in_array(get_the_ID(), $accesspages)){
									echo ' SELECTED="SELECTED"';
								}
							?>><?php the_title(); ?></option><?php
						}
						?></select><br>
						<p class="description">We can select multiple Pages. Only the selected posts can be viewed by user.</p>
						<?php
					}
				?>
			</td>
		</tr>
	</table>
	<?php
}

add_action ( 'personal_options_update', 'chaine_save_extra_profile_fields' );
add_action ( 'edit_user_profile_update', 'chaine_save_extra_profile_fields' );

function chaine_save_extra_profile_fields( $user_id ){
	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;
	update_usermeta( $user_id, 'redirect_to', $_POST['redirect_to'] );
	update_usermeta( $user_id, 'accessposts', $_POST['accessposts'] );
	update_usermeta( $user_id, 'accesspages', $_POST['accesspages'] );

}

add_action('user_register', 'register_extra_fields');
function register_extra_fields ( $user_id, $password = "", $meta = array() ){
	update_user_meta( $user_id, 'redirect_to', $_POST['redirect_to'] );
	update_user_meta( $user_id, 'accessposts', $_POST['accessposts'] );
	update_user_meta( $user_id, 'accesspages', $_POST['accesspages'] );
}
*/

// Add extra query paramerts if any
function apply_restriction_query($args){
	$user = wp_get_current_user();
	if(!current_user_can( 'create_users', $user->ID )){		
		$accesspages = get_the_author_meta( 'accesspages', $user->ID );
		$accessposts = get_the_author_meta( 'accessposts', $user->ID );
		$accesspages = array_merge($accesspages, $accessposts);
		if(isset($args['post__in'])){
			$args['post__in'] = array_merge($args['post__in'], $accesspages);
		}else{
			$postIn = array(
				'post__in'	=>	$accesspages
			);
			$args = array_merge($args, $postIn);
		}
		
	}
	return $args;
}

/* Meta Box for access control */
/**
 * Register meta box(es).
 */
function chaine_register_meta_boxes() {
    add_meta_box( 'post-accessible-by', __( 'Access Permission', 'ch_du_bo' ), 'chaine_my_display_callback', array('post','page'), 'side' );
}
add_action( 'add_meta_boxes', 'chaine_register_meta_boxes' );
 
/**
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 */
function chaine_my_display_callback( $post ) {
    // Display code/markup goes here. Don't forget to include nonces!
    $roles = get_editable_roles();    
    $accessible_by = get_post_meta($post->ID, 'accessible_by', true);
    echo "<fieldset>";
    ?>
    <input type="checkbox" name="accessible_by[]" class="accessible-by" id="accessible-by-everyone" value="everyone"> <label for="accessible-by-everyone" class="accessible-by-icon accessible-by-everyone"
    <?php
    	if(in_array('everyone', $accessible_by)){
    		echo ' checked="CHECKED"';
    	}
    ?>
    ><?php _e('Everyone','ch_du_bo'); ?></label><br>
    <?php
    foreach ($roles as $role => $value) {
    	# code...
    	?>
    	<input type="checkbox" name="accessible_by[]" class="accessible-by" id="accessible-by-<?php echo $role; ?>" value="<?php echo $role; ?>"
    	<?php
	    	if(in_array($role, $accessible_by)){
	    		echo ' checked="CHECKED"';
	    	}	    
    	?>
    	> <label for="accessible-by-<?php echo $role; ?>" class="accessible-by-icon accessible-by-<?php echo $role; ?>"><?php echo $value['name']; ?></label><br>
    	<?php
    }
    echo "</fieldset>";
}
 
/**
 * Save meta box content.
 *
 * @param int $post_id Post ID
 */
function chaine_save_meta_box( $post_id ) {
    // Save logic goes here. Don't forget to include nonce checks!
    // If this is just a revision, don't send the email.
	if ( wp_is_post_revision( $post_id ) )
		return;
	//delete_post_meta($post_id,'accessible_by');
	$accessible_by = get_post_meta($post_id, 'accessible_by');
	if(!$accessible_by){
		if($_POST['accessible_by']){
			add_post_meta($post_id, 'accessible_by', $_POST['accessible_by']);
		}else{
			add_post_meta($post_id, 'accessible_by', array());
		}
	}else{
		if($_POST['accessible_by']){
			update_post_meta($post_id, 'accessible_by', $_POST['accessible_by']);	
		}else{
			update_post_meta($post_id, 'accessible_by', array());
		}		
	}    
}
add_action( 'save_post', 'chaine_save_meta_box' );