<?php
foreach($langs as $lang){
    $wp_customize->add_setting('ch_du_bo_mailchimp_list_id_'.$lang['language_code'],array(
            'type'	=>	'theme_mod',
            'default'	=>	'',
            'capability'	=>	'manage_options',
            'transport'	=>	'refresh',
            'sanitize_callback'	=>	'sanitize_text_field'
        )
    );
    $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'ch_du_bo_mailchimp_list_id_'.$lang['language_code'],
            array(
                'label'    => 'MailChimp List ID '.$lang['language_code'],
                'section'  => 'ch_du_bo_section_api',
                'settings' => 'ch_du_bo_mailchimp_list_id_'.$lang['language_code'],
                'priority' => 11,
                'type'	=>	'text'
            )
    ) );
}
?>
