<?php
	$wp_customize->add_section( 'custom_link_setting', array(
		'title'    => __( 'Links' ),
		'priority' => 15,
	) );
	foreach($langs as $lang){
		$wp_customize->add_setting( 'donate_link_'.$lang['language_code'], array(
			'default'           => '',
			'sanitize_callback' => 'esc_url',
		) );
		$wp_customize->add_control( 'ch_du_bo_donate_'.$lang['language_code'], array(
			'settings' => 'donate_link_'.$lang['language_code'],
			'label'    => __( 'Donate '.$lang['language_code'] ),
			'section'  => 'custom_link_setting',
			'type'     => 'text',
			'priority' => 10,
		) );
	}
	
	$wp_customize->add_setting( 'media_link', array(
		'default'           => '',
		'sanitize_callback' => 'esc_url',
	) );
	$wp_customize->add_control( 'ch_du_bo_media_link', array(
		'settings' => 'media_link',
		'label'    => __( 'Media', 'ch_du_bo' ),
		'section'  => 'custom_link_setting',
		'type'     => 'text',
		'priority' => 10,
	) );
	
	$wp_customize->add_setting( 'login_sao', array(
		'default'           => '',
		'sanitize_callback' => 'esc_url',
	) );
	$wp_customize->add_control( 'ch_du_bo_login_sao_link', array(
		'settings' => 'login_sao',
		'label'    => __( 'Login Services sociaux', 'ch_du_bo' ),
		'section'  => 'custom_link_setting',
		'type'     => 'text',
		'priority' => 11,
	) );

	$wp_customize->add_setting( 'login_partnerweb', array(
		'default'           => '',
		'sanitize_callback' => 'esc_url',
	) );
	$wp_customize->add_control( 'ch_du_bo_login_partnerweb_link', array(
		'settings' => 'login_partnerweb',
		'label'    => __( 'Login Partnerweb', 'ch_du_bo' ),
		'section'  => 'custom_link_setting',
		'type'     => 'text',
		'priority' => 12,
	) );