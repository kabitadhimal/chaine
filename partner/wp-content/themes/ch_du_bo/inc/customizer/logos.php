<?php
	$wp_customize->add_panel( 'ch_du_bo_logo_setting_panel',
	    array(
	        'title'          => 'Logos',
	        'description'    => 'Logos for '.get_bloginfo().'',
	        'capability'     => 'manage_options',
	        'theme-supports' => '',
	        'priority'       => 12,
	    )
	);
	foreach($langs as $lang){

		$wp_customize->add_section( 'ch_du_bo_section_logo_scheme_'.$lang['language_code'],
		    array(
		        'title'          => __(sprintf('Logo (%s)',strtoupper($lang['language_code'])),'ch_du_bo'),
		        'description'    => 'Color Scheme for '.get_bloginfo(),
		        'capability'     => 'manage_options',
		        'theme-supports' => '',
		        'priority'       => 10,
		        'panel'          => 'ch_du_bo_logo_setting_panel',
		    )
		);

		$wp_customize->add_setting('ch_du_bo_custom_logo_'.$lang['language_code'],array(
				'type'	=>	'theme_mod',
				'default'	=>	'',
				'capability'	=>	'manage_options',
				'transport'	=>	'refresh',
				'sanitize_callback'	=>	'',
			)
		);
		$wp_customize->add_control(
	       new WP_Customize_Image_Control(
	           $wp_customize,
	           'ch_du_bo_custom_logo_'.$lang['language_code'],
	           array(
	               'label'      => __( sprintf("Laden Sie ein Logo (%s)",strtoupper($lang['language_code'])), 'ch_du_bo' ),
	               'section'    => 'ch_du_bo_section_logo_scheme_'.$lang['language_code'],
	               'settings'   => 'ch_du_bo_custom_logo_'.$lang['language_code']
	           )
	       )
	   	);
		/* $wp_customize->add_setting('ch_du_bo_custom_emergency_logo_'.$lang['language_code'],array(
				'type'	=>	'theme_mod',
				'default'	=>	'',
				'capability'	=>	'manage_options',
				'transport'	=>	'refresh',
				'sanitize_callback'	=>	'',
			)
		);
		$wp_customize->add_control(
	       new WP_Customize_Image_Control(
	           $wp_customize,
	           'ch_du_bo_custom_emergency_logo_'.$lang['language_code'],
	           array(
	               'label'      => __( sprintf("Laden Sie Notfall-Logo (%s)",strtoupper($lang['language_code'])), 'ch_du_bo' ),
	               'section'    => 'ch_du_bo_section_logo_scheme_'.$lang['language_code'],
	               'settings'   => 'ch_du_bo_custom_emergency_logo_'.$lang['language_code']
	           )
	       )
	   	); */
	}