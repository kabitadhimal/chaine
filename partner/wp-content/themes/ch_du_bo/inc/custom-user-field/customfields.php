<?php
/*
 * Custom user Field
*/

/**
 * Add additional custom field
 */

add_action ( 'show_user_profile', 'kd_show_extra_profile_fields' );
add_action ( 'edit_user_profile', 'kd_show_extra_profile_fields' );

function kd_show_extra_profile_fields ( $user )
{
?>
	<h3>Additonal Fields</h3>
	<table class="form-table">
	
		<tr>
			<th><label for="store">Store</label></th>
			<td>
				<input type="text" name="store" id="store" value="<?php echo esc_attr( get_the_author_meta( 'store', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		
		
		<tr>
			<th><label for="phone">Phone number</label></th>
			<td>
				<input type="text" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		
		
		<tr>
			<th><label for="street">Street</label></th>
			<td>
				<input type="text" name="street" id="street" value="<?php echo esc_attr( get_the_author_meta( 'street', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		
		
		<tr>
			<th><label for="additional">Additional</label></th>
			<td>
				<input type="text" name="additional" id="additional" value="<?php echo esc_attr( get_the_author_meta( 'additional', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		
		
		<tr>
			<th><label for="city">City</label></th>
			<td>
				<input type="text" name="city" id="city" value="<?php echo esc_attr( get_the_author_meta( 'city', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		
		
		<tr>
			<th><label for="postalcode">Postal code</label></th>
			<td>
				<input type="text" name="postalcode" id="postalcode" value="<?php echo esc_attr( get_the_author_meta( 'postalcode', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		
		
		<tr>
			<th><label for="country">Country</label></th>
			<td>
				<input type="text" name="country" id="country" value="<?php echo esc_attr( get_the_author_meta( 'country', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		
			
		
		
	</table>
<?php
}

add_action ( 'personal_options_update', 'kd_save_extra_profile_fields' );
add_action ( 'edit_user_profile_update', 'kd_save_extra_profile_fields' );

function kd_save_extra_profile_fields( $user_id )
{
	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;
	/* Copy and paste this line for additional fields. Make sure to change 'twitter' to the field ID. */
	update_usermeta( $user_id, 'phone', $_POST['phone'] );
	update_usermeta( $user_id, 'street', $_POST['street'] );
	update_usermeta( $user_id, 'additional', $_POST['additional'] );
	update_usermeta( $user_id, 'city', $_POST['city'] );
	update_usermeta( $user_id, 'postalcode', $_POST['postalcode'] );
	update_usermeta( $user_id, 'country', $_POST['country'] );

}

/**
 * Add cutom field to registration form
 */


//add_action('register_form','show_first_name_field');
//add_action('register_post','check_fields',10,3);
add_action('user_register', 'register_extra_fields');
/*

function show_first_name_field()
{

?>

	<p>
		<label>Magazine</label>
		<input type="text" name="magazine" id="magazine" value="<?php echo $_POST['magazine']; ?>" class="regular-text" /><br />
	</p>
	
	
	<p>
		<label>Store</label>
		<input type="text" name="store" id="store" value="<?php echo $_POST['store']; ?>" class="regular-text" /><br />
	</p>
	
	
	
	<p>
	<label>Location<br/>
	<input id="location" type="text" tabindex="30" size="25" value="<?php echo $_POST['location']; ?>" name="location" />
	</label>
	</p>
	
	<p>
	<label>Phone<br/>
	<input id="phone" type="text" tabindex="30" size="25" value="<?php echo $_POST['phone']; ?>" name="phone" />
	</label>
	</p>
	
	<p>
	<label>Street<br/>
	<input id="street" type="text" tabindex="30" size="25" value="<?php echo $_POST['street']; ?>" name="street" />
	</label>
	</p>
	
	<p>
	<label>Additional<br/>
	<input id="additional" type="text" tabindex="30" size="25" value="<?php echo $_POST['additional']; ?>" name="additional" />
	</label>
	</p>
	
	<p>
	<label>City<br/>
	<input id="city" type="text" tabindex="30" size="25" value="<?php echo $_POST['city']; ?>" name="city" />
	</label>
	</p>
	
	<p>
	<label>Postalcode<br/>
	<input id="postalcode" type="text" tabindex="30" size="25" value="<?php echo $_POST['postalcode']; ?>" name="postalcode" />
	</label>
	</p>
	
	<p>
	<label>Country<br/>
	<input id="country" type="text" tabindex="30" size="25" value="<?php echo $_POST['country']; ?>" name="country" />
	</label>
	</p>
	
	
	<p>
	<table class="form-table">
			<tbody><tr>
			<td>What you want to..<td>
			<td>
	
	<select id="new_user_role" name="new_user_role">
		<option value="press" <?=($_POST( 'new_user_role' ) == "press")? "selected":""?> >Press</option>
		<option value="retailer" <?=($_POST( 'new_user_role' ) == "retailer")? "selected":""?>>Retailer</option>
		<option value="distributor" <?=($_POST( 'new_user_role' ) == "distributor")? "selected":""?> >Distributor</option>
	</select>
	</td>
	</tr>
	</tbody>
	</table>
	</p>
	
	
	
	
<?php
}

function check_fields ( $login, $email, $errors )
{
	global $magazine, $store, $location,$phone,$street,$additional,$city,$postalcode,$country,$new_user_role;
	
	
	//Magazine
	if ( $_POST['magazine'] == '' )
	{
		$errors->add( 'empty_realname', "<strong>ERROR</strong>: Please Enter Magazine" );
	}
	else
	{
		$magazine = $_POST['magazine'];
	
	}
	
	//Store
	if ( $_POST['store'] == '' )
	{
		$errors->add( 'empty_realname', "<strong>ERROR</strong>: Please Enter Store" );
	}
	else
	{
		$store = $_POST['store'];
	
	}
	
	//Location
	if ( $_POST['location'] == '' )
	{
		$errors->add( 'empty_realname', "<strong>ERROR</strong>: Please Enter your location" );
	}
	else
	{
		$location = $_POST['location'];
	}
	
	//Phone
	if ( $_POST['phone'] == '' )
	{
		$errors->add( 'empty_realname', "<strong>ERROR</strong>: Please Enter your phonenumber" );
	}
	else
	{
		$phone = $_POST['phone'];
	}
	
	
	//Street
	
	if ( $_POST['street'] == '' )
	{
		$errors->add( 'empty_realname', "<strong>ERROR</strong>: Please Enter your street" );
	}
	else
	{
		$street = $_POST['street'];
	}
	
	//Additional
	if ( $_POST['additional'] == '' )
	{
		$errors->add( 'empty_realname', "<strong>ERROR</strong>: Please Enter your additional information" );
	}
	else
	{
		$additional = $_POST['additional'];
	}
	
	//City
	if ( $_POST['city'] == '' )
	{
		$errors->add( 'empty_realname', "<strong>ERROR</strong>: Please Enter your city" );
	}
	else
	{
		$city = $_POST['city'];
	}
	
	//Postalcode
	if ( $_POST['postalcode'] == '' )
	{
		$errors->add( 'empty_realname', "<strong>ERROR</strong>: Please Enter your postalcode" );
	}
	else
	{
		$postalcode = $_POST['postalcode'];
	}
	
	//Country
	if ( $_POST['country'] == '' )
	{
		$errors->add( 'empty_realname', "<strong>ERROR</strong>: Please Enter your country name" );
	}
	else
	{
		$country = $_POST['country'];
	}
	
	if ( $_POST['new_user_role'] == '' )
	{
		$errors->add( 'empty_realname', "<strong>ERROR</strong>: Please select what you want to be" );
	}
	else
	{
		$country = $_POST['new_user_role'];
	}
	
}

*/
function register_extra_fields ( $user_id, $password = "", $meta = array() )
{
	update_user_meta( $user_id, 'magazine', $_POST['magazine'] );
	update_user_meta( $user_id, 'store', $_POST['store'] );
	update_user_meta( $user_id, 'location', $_POST['location'] );
	update_user_meta( $user_id, 'phone', $_POST['phone'] );
	update_user_meta( $user_id, 'street', $_POST['street'] );
	update_user_meta( $user_id, 'additional', $_POST['additional'] );
	update_user_meta( $user_id, 'city', $_POST['city'] );
	update_user_meta( $user_id, 'postalcode', $_POST['postalcode'] );
	update_user_meta( $user_id, 'country', $_POST['country'] );
	update_user_meta( $user_id, 'new_user_role', $_POST['new_user_role'] );
	
	
}


?>