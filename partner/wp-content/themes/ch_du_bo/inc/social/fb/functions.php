<?php
require 'vendor/autoload.php';
class FB_Page_Manager{
	const APP_ID = '1688352878158363';
	const APP_SECRET_ID = '62fcbbccfa9e9c668ef6791492cc3fe7';
	private $fb,$fbProfileId;

	function __construct($fbProfileId = ''){
		$this->fb = new Facebook\Facebook(array(
		'app_id' => self::APP_ID,
		'app_secret' => self::APP_SECRET_ID,
		'default_graph_version' => 'v2.2',
		));	
		if($fbProfileId){
			$this->fbProfileId = $fbProfileId;	
		}		
	}

	public function get_latest_posts($limit = 3){
		$fields = 'fields=name,full_picture,link,created_time,message,description&limit='.$limit;
		try {
			$response = $this->fb->get('/'.$this->fbProfileId.'/posts?'.$fields.'', self::APP_ID.'|'.self::APP_SECRET_ID);
			$nextUrl = $response->getDecodedBody()['paging']['next'];
			$feedObj['next'] = parse_url($nextUrl)['query'];
			$posts = $response->getDecodedBody()['data'];
			return $posts;
		}catch(Facebook\Exceptions\FacebookSDKException $e) {
			//
		}
	}

	public function get_post_by_url($url){
		$re = '/(?:https?:\/\/)?(?:www\.)?facebook\.com\/([\w\-\.]*)(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-\.]*)/';
		
		$str = $url;
		$str = preg_replace('{/$}', '', $str);
		preg_match($re, $str, $matches);

		$fbProfileName = $matches[1];

		$fbPostId = $matches[2];
		//fields bhaneko fields=full_picture,link,created_time,message,description&limit=2
		$fields = 'fields=name,full_picture,link,created_time,message,description';
		try {
			$response = $this->fb->get('/'.$fbProfileName, self::APP_ID.'|'.self::APP_SECRET_ID);
			$fbProfileName = $response->getDecodedBody()['id'];
			$response = $this->fb->get('/'.$fbProfileName.'_'.$fbPostId.'?'.$fields.'', self::APP_ID.'|'.self::APP_SECRET_ID);

			$data = $response->getDecodedBody();
			return $data;
			}catch(Facebook\Exceptions\FacebookSDKException $e) {
			//
		}
	}
}