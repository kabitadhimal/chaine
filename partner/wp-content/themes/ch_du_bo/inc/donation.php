<?php
function ch_du_bo_get_donation_data($id){
	$data = [];
	$data['devise'] = get_field('devise', $id);
	$donation = get_field('value', $id);

	if($donation / (int)$donation != 1){
		$decimal = 2;
	}else{
		$decimal = 0;
	}
	$figures = $donation/10000;
	if(CH_LANG_CODE === 'en'){
	    $donation = number_format($donation, $decimal, '.', ',');
	}else{
	   if((int)$figures === 0){
		   $donation = number_format($donation, $decimal, '.', '');
	   }else{
		   $donation = number_format($donation, $decimal, '.', '\'');
	   }
	}
	$data['donation'] = $donation;
	return $data;
}

add_shortcode( 'donation', 'ch_du_bo_donation_shortcode' );
function ch_du_bo_donation_shortcode( $atts ) {		
    $atts = shortcode_atts( array(
        'id' => NULL,
        'hidedevise' => NULL,
    ), $atts, 'donation' );    
	if(!$atts['id']){
		return;
	}

    $donation = ch_du_bo_get_donation_data($atts['id']);
    ob_start();

    echo '<span class="amount">';
    if(!$atts['hidedevise'])
		echo $donation['devise'].'&nbsp;';
	echo '<strong>'.$donation['donation'].'</strong></span>';
	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}

add_filter( 'manage_donation_posts_columns', 'set_custom_edit_donation_columns' );
add_action( 'manage_donation_posts_custom_column' , 'custom_donation_column', 10, 2 );

function set_custom_edit_donation_columns($columns) {
    unset( $columns['author'] );
    $columns['shortcode'] = __( 'Shortcode', 'ch_du_bo' );
    $columns['value'] = __( 'Valeur', 'ch_du_bo' );

    return $columns;
}

function custom_donation_column( $column, $post_id ) {
    switch ( $column ) {

        case 'shortcode' :
            echo 'With Devise :<br><strong>[donation id="'.$post_id.'"]</strong>';
            echo '<br><br>';
            echo 'Without Devise :<br><strong>[donation id="'.$post_id.'" hidedevise=1]</strong>';
            break;
		case 'value' :
			$data = ch_du_bo_get_donation_data($post_id);
			echo 'With Devise :<br><span class="amount">'.$data['devise'].'&nbsp;<strong>'.$data['donation'].'</strong></span>';
			echo '<br><br>';
			echo 'Without Devise :<br><span class="amount"><strong>'.$data['donation'].'</strong></span>';
			break;
    }
}
?>
