<?php
/*
  Name: Chaine Du Bonheur Custom Registration
  Description: Registration and Forget Password
  Version: 1.0
  Author: Kabita Dhimal
 */

require 'KD_Registration.php';
require 'KD_Forget_Pass.php';

function set_html_content_type(){
	return 'text/html';
}

add_shortcode( 'kd_registration_form', 'kd_registration_form_callback' );
function kd_registration_form_callback() {
    $kd_registration = new KD_Registration();
    $kd_registration->getForm();
}

add_shortcode( 'kd_forget_pass', 'kd_forget_pass_callback' );
function kd_forget_pass_callback() {
    $kd_registration = new KD_Forget_Pass();
    $kd_registration->getForm();
}



function confirm_user_callbck($message, $user){
    $userPass = '';
	
	$roles = KD_Registration::getRoles();
	$userRole = $user->roles[0];
	
	if(empty($userRole) || !array_key_exists($userRole, $roles)){
		return $message;
	}
	
	$userPass = $roles[$userRole];
	
	add_filter( 'wp_mail_content_type', 'set_html_content_type' );
	
	$message = 'Thank you for registering to the Chaine Du Bonheur Database.<br/><br/>';
	$message .= 'Here are your personal username and password. <br/><br/>';
	$message .= 'Username: '.$user->user_login. '<br/>Password: '.$userPass.'<br/>';
	$message .= "May you not find the requested information or need further assistance, please contact us at info@bonheur.ch.<br/>With our kind regards.<br/><br/>
	
	Chaine Du Bonheur";
	
	add_filter( 'wp_mail_content_type', 'set_html_content_type' );
	
	return $message;
}

add_filter( 'confirm-user-registration-notification-message', 'confirm_user_callbck', 10, 2 );
//$subject = apply_filters( 'confirm-user-registration-notification-subject', $options['subject'], $user );
