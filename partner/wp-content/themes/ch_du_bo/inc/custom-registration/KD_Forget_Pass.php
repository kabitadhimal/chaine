<?php
class KD_Forget_Pass{
    /**
     *
     * @var WP_Error
     */
    protected $errors;
    
   public function getForm(){
       $email = '';
       //if post, update the values form post data
       if(isset($_POST['kd_forget_submit'])){
            if(isset($_POST['user_email'])) $email = sanitize_email($_POST['user_email']);
            
            //validate
            $this->errors = $this->validate($email);
			$countErr = $this->errors->get_error_messages();
            if(empty($countErr)){
                $this->inform($email);
				wp_redirect(site_url().'/forgetpass-success');
            }
        }
        include 'include/forget_form.php';
    }
    
    protected function inform($email){
        $user = get_user_by('email', $email);
        $userRole = $user->roles[0];
        
        $roles = KD_Registration::getRoles();
        
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: Chaine Du Bonheur <noreply@bonheur.com>' . "\r\n";
        
      
        $message = "Hello ".$user->data->user_login.",";
        
        $message .='<p>Someone requested that the password be reset for the following account:</p>';
        $message .='<p><a href="'.site_url().'">'.site_url().'</a><br />';
        $message .='Username: '.$email.'</p>';
        
        if(array_key_exists($userRole, $roles)){
            $pass = $roles[$userRole];
            $message.='<p>Your password for Chaine Du Bonheur is: '.$pass.'</p>';
        }else{
            $lostPasswordUrl = wp_lostpassword_url();
            $message.='<p>To reset your password, visit the following address: <a href="'.$lostPasswordUrl.'">'.$lostPasswordUrl.'</a></p>';            
        }
        
        $message.='<p>If you may not find the requested information or need further assistance, please contact us at <a href="mailto:info@bonheur.ch">info@bonheur.ch</a>.</p>';
        $message.='<p></p><p>Best regards<br />';
        $message.='Chaine Du Bonheur</p>';
        
        //echo $message;
        add_filter( 'wp_mail_content_type', 'set_html_content_type' );
        wp_mail($email, 'Chaine Du Bonheur - Password Request', $message,$headers);
    }
    
    /**
     *
     * @return WP_Error
     */
    protected function validate($data){
        $wpErrors = new WP_Error();
        
        if(
            !isset($_POST['kd_forget_nonce']) 
            || !wp_verify_nonce($_POST['kd_forget_nonce'], 'kd_forget_pass'))
        {
            $wpErrors->add('nonce', 'sorry, something went wrong, please try again!');
        }
    
        //validate
        if(isset($_POST['user_email']))  $email = sanitize_email($_POST['user_email']);
        if(empty($email)){
            $wpErrors->add('email', 'email is required');
        }else if(!is_email($email)){
            $wpErrors->add('email','invalid email address');
        }else if(!email_exists($email)){
            $wpErrors->add('email','user doesnot exit');
        }
        
        return $wpErrors;
    }
    
}
