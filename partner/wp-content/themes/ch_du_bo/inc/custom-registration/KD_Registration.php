<?php
//https://developers.google.com/recaptcha/docs/display



use ReCaptcha\ReCaptcha;
class KD_Registration{
	
    static protected $roles = [
        'ong-partenaires'=>'ong-partenaires',
        'ong-actives-en-suisse'=>'ong-actives-en-suisse',
        'conseil-de-fondation'=>'conseil-de-fondation',
    ];

	/*
	 * Live Server
	 */

	const CAPTCHA_SITE_KEY = '6LcCyxkUAAAAAOVkCAKea1uToGmmT8QZhmVNg04F';
	const CAPTCHA_SECRET_KEY = '6LcCyxkUAAAAAHaAEUSwuXh1vIxir5uof9mFirto';

    /**
     *
     * @var WP_Error
     */
    protected $errors;

    //set default form values
    protected $formData = [
        'name'=>null, 'magazine'=>null, 'store'=>null,'email'=>null, 'fname'=>null,  'lname'=>null,
        'phone'=>null,'street'=>null,'additional'=>null,'city'=>null,
        'postalCode'=>null,'country'=>null,'subscribed'=>null
    ];


    static public function getRoles(){
        return self::$roles;
    }
    
    /**
     * 
     * @param WP_Error $formErrors
     * @param string $inputName
     * @return string
     */
    static public function getErrorHtml($formErrors, $inputName){
        if($formErrors){
            $err = $formErrors->get_error_message($inputName);
            if($err!='') return '<div class="error-block"><div class="error"><strong>'.$err.'</strong></div></div>';
        }
        return '';
    }

    /**
     * callback for registration shortcode
     */
    public function getForm(){
    	
        //if post, update the values form post data
        if(isset($_POST['kd_register_submit'])){
            $this->formData = array_merge($this->formData, $_POST);

            //sanitize
            foreach ($this->formData as $key=>&$value){
                if($key == 'email'){
                    $value = sanitize_email($value);
                }else{
                    $value = sanitize_title($value);
                }
            }

            //validate
            $this->errors = $this->validate($this->formData);
			$countErr = $this->errors->get_error_messages();
            if(empty($countErr)){
                //$this->insertUser($this->userData);

                $pass = self::$roles[$this->formData['role']];
				//var_dump($pass);
                $userdata = [
                    'user_login'    =>  $this->formData['email'],
                    'user_email'    =>  $this->formData['email'],
                    'user_pass'     =>  $pass,
                    'first_name'    =>  $this->formData['fname'],
                    'last_name'     =>  $this->formData['lname'],
                    'phone'			=>	$this->formData['phone'],
                    'street'		=>  $this->formData['street'],
                    'additional'	=>	$this->formData['additional'],
                    'city'			=>	$this->formData['city'],
                    'postalcode'	=>	$this->formData['postalcode'],
                    'country'		=>	$this->formData['country'],
                    'role'          =>	$this->formData['role'],
                	'store'          =>	$this->formData['store'],
                	'magazine'          =>	$this->formData['magazine']
                ];
                
                wp_insert_user($userdata);
                
                if($this->formData['subscribed']=='true'):
                $this->sendAddressToMailChimp($this->formData['email']);
                endif;
                
                $this->sendAddressToMailChimp($this->formData['email']);
                $this->inform($this->formData['email'],$this->formData['magazine'],$this->formData['store']);

                wp_redirect(site_url().'/registration-success');
                ?>
                <script type="text/javascript">
                    window.location = "<?php echo site_url().'/registration-success'; ?>";
                </script>
                <?php
                exit;
            }
        }
        include 'include/registration_form.php';
    }

    protected function inform($email,$magazine,$store){
		$toEmail = get_option( 'admin_email');
		  
  		$storeType = (!empty($magazine)) ? $magazine: $store;
  	
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: Chaine Du Bonheur <no-reply@bonheur.ch>' . "\r\n";
       
        $subject = 'New Registration';
        $message = "You have received new registration request from " .$email . " and ".$storeType." from Chaine Du Bonheur. <br/> Approve user for activation ";
        

                
        add_filter( 'wp_mail_content_type', 'set_html_content_type' );
        wp_mail($toEmail, $subject, $message, $headers);
    }
    
    protected function sendAddressToMailChimp($email) {
    		//$properties = $wpcf7->get_properties();
    		//var_dump($properties);
    		$userEmail = $email;
    		//ues mailchimp api
    		if($userEmail != '' && is_email($userEmail))  {
    			
    			require_once(__DIR__.'/../libs/mailchimp/MCAPI.class.php');
    			define('MC_API_KEY', '1e7fe41d3cf13341986c2fe7dafd5125-us10');
    			if($lang == 'en'){
    				$listID = '2152f34cb6';
    			}else{
    				$listID = '2152f34cb6';
    			}
    			$api = new MCAPI(MC_API_KEY); 			
    			return ($api->listSubscribe($listID, $userEmail, '',null,'html',false));
    			
    		}
    	
    }

    /**
     *
     * @return WP_Error
     */
    protected function validate($data){
        $wpErrors = new WP_Error();

        //dynamically load captcha library
		if(!function_exists('classEdoxAutoLoader')){
			function classEdoxAutoLoader($class_name){
				$isCaptchaClass = strstr($class_name, 'ReCaptcha');
				if($isCaptchaClass){
					$class_name = explode("\\", $class_name);
					$class_name = implode(DIRECTORY_SEPARATOR , $class_name);
					require __DIR__.DIRECTORY_SEPARATOR.$class_name . '.php';
				}
			}
		}
		spl_autoload_register('classEdoxAutoLoader');
		/*
        function __autoload($class_name) {

            $isCaptchaClass = strstr($class_name, 'ReCaptcha');
            if($isCaptchaClass){
				$class_name = explode("\\", $class_name);
				$class_name = implode(DIRECTORY_SEPARATOR , $class_name);
				require __DIR__.DIRECTORY_SEPARATOR.$class_name . '.php';
			}
        }
		*/
        $captcha = new ReCaptcha(self::CAPTCHA_SECRET_KEY);
        
        $captchaVal = '';
        if(isset($_POST['g-recaptcha-response'])) $captchaVal =  $_POST['g-recaptcha-response'];
        
        $resp = $captcha->verify($captchaVal, $_SERVER['REMOTE_ADDR']);
        if (!$resp->isSuccess()) {
            $wpErrors->add('g-recaptcha', 'captcha is invalid or missing');
        }

        if(empty($data['role'])){
        	$wpErrors->add('role','role is required!');
        }else{
        	if(!array_key_exists($data['role'], self::$roles)){
        		$wpErrors->add('role','role should be '. implode(', ', array_keys(self::$roles)));
        	}else{
        		//roles exits, now check for stores or magazine
        		//if role is clients, check for store
        		if($data['role'] == array_keys(self::$roles)[0]){
        			if(empty($data['store'])){
        				$wpErrors->add('store', 'Store field is required!');
        			}
        		}
        		
        		//if role is clients, check for store
        		if($data['role'] == array_keys(self::$roles)[1]){
        			if(empty($data['magazine'])){
        				$wpErrors->add('magazine', 'Magazine field is required!');
        			}
        		}
        	}
        }
        
        //check email
        if(empty($data['email'])){
            $wpErrors->add('email', 'email is required!');
        }else if(!is_email($data['email'])){
            $wpErrors->add('email','invalid email address!');
        }else if(email_exists($data['email'])){
            $wpErrors->add('email','user with the email already exits');
        }
        
        if (!empty($data['phone']) && !(is_numeric($data['phone']))) {
        	$wpErrors->add('phone','Phone must be number');
        }
        
        if (!empty($data['postalcode']) && !(is_numeric($data['postalcode']))) {
        	$wpErrors->add('postalcode','Postalcode must be number');
        }

        if(empty($data['country'])){
            $wpErrors->add('country','country is required!');
        }

       
        return $wpErrors;
    }
}