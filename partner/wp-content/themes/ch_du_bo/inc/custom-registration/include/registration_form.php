<?php 
$countryLists = ["Afghanistan" , "Albania" , "Algeria" , "American Samoa" , "Andorra" , "Angola" , "Anguilla" , "Antarctica" , "Antigua and Barbuda" , "Argentina" , "Armenia" , "Arctic Ocean" , "Aruba" , "Ashmore and Cartier Islands" , "Atlantic Ocean" , "Australia" , "Austria" , "Azerbaijan" , "Bahamas" , "Bahrain" , "Baker Island" , "Bangladesh" , "Barbados" , "Bassas da India" , "Belarus" , "Belgium" , "Belize" , "Benin" , "Bermuda" , "Bhutan" , "Bolivia" , "Bosnia and Herzegovina" , "Botswana" , "Bouvet Island" , "Brazil" , "British Virgin Islands" , "Brunei" , "Bulgaria" , "Burkina Faso" , "Burundi" , "Cambodia" , "Cameroon" , "Canada" , "Cape Verde" , "Cayman Islands" , "Central African Republic" , "Chad" , "Chile" , "China" , "Christmas Island" , "Clipperton Island" , "Cocos Islands" , "Colombia" , "Comoros" , "Cook Islands" , "Coral Sea Islands" , "Costa Rica" , "Cote d'Ivoire" , "Croatia" , "Cuba" , "Cyprus" , "Czech Republic" , "Denmark" , "Democratic Republic of the Congo" , "Djibouti" , "Dominica" , "Dominican Republic" , "East Timor" , "Ecuador" , "Egypt" , "El Salvador" , "Equatorial Guinea" , "Eritrea" , "Estonia" , "Ethiopia" , "Europa Island" , "Falkland Islands (Islas Malvinas)" , "Faroe Islands" , "Fiji" , "Finland" , "France" , "French Guiana" , "French Polynesia" , "French Southern and Antarctic Lands" , "Gabon" , "Gambia" , "Gaza Strip" , "Georgia" , "Germany" , "Ghana" , "Gibraltar" , "Glorioso Islands" , "Greece" , "Greenland" , "Grenada" , "Guadeloupe" , "Guam" , "Guatemala" , "Guernsey" , "Guinea" , "Guinea-Bissau" , "Guyana" , "Haiti" , "Heard Island and McDonald Islands" , "Honduras" , "Hong Kong" , "Howland Island" , "Hungary" , "Iceland" , "India" , "Indian Ocean" , "Indonesia" , "Iran" , "Iraq" , "Ireland" , "Isle of Man" , "Israel" , "Italy" , "Jamaica" , "Jan Mayen" , "Japan" , "Jarvis Island" , "Jersey" , "Johnston Atoll" , "Jordan" , "Juan de Nova Island" , "Kazakhstan" , "Kenya" , "Kingman Reef" , "Kiribati" , "Kerguelen Archipelago" , "Kosovo" , "Kuwait" , "Kyrgyzstan" , "Laos" , "Latvia" , "Lebanon" , "Lesotho" , "Liberia" , "Libya" , "Liechtenstein" , "Lithuania" , "Luxembourg" , "Macau" , "Macedonia" , "Madagascar" , "Malawi" , "Malaysia" , "Maldives" , "Mali" , "Malta" , "Marshall Islands" , "Martinique" , "Mauritania" , "Mauritius" , "Mayotte" , "Mexico" , "Micronesia" , "Midway Islands" , "Moldova" , "Monaco" , "Mongolia" , "Montenegro" , "Montserrat" , "Morocco" , "Mozambique" , "Myanmar" , "Namibia" , "Nauru" , "Navassa Island" , "Nepal" , "Netherlands" , "Netherlands Antilles" , "New Caledonia" , "New Zealand" , "Nicaragua" , "Niger" , "Nigeria" , "Niue" , "Norfolk Island" , "North Korea" , "North Sea" , "Northern Mariana Islands" , "Norway" , "Oman" , "Pacific Ocean" , "Pakistan" , "Palau" , "Palmyra Atoll" , "Panama" , "Papua New Guinea" , "Paracel Islands" , "Paraguay" , "Peru" , "Philippines" , "Pitcairn Islands" , "Poland" , "Portugal" , "Puerto Rico" , "Qatar" , "Reunion" , "Republic of the Congo" , "Romania" , "Russia" , "Rwanda" , "Saint Helena" , "Saint Kitts and Nevis" , "Saint Lucia" , "Saint Pierre and Miquelon" , "Saint Vincent and the Grenadines" , "Samoa" , "San Marino" , "Sao Tome and Principe" , "Saudi Arabia" , "Senegal" , "Serbia" , "Seychelles" , "Sierra Leone" , "Singapore" , "Slovakia" , "Slovenia" , "Solomon Islands" , "Somalia" , "South Africa" , "South Georgia and the South Sandwich Islands" , "South Korea" , "Spain" , "Spratly Islands" , "Sri Lanka" , "Sudan" , "Suriname" , "Svalbard" , "Swaziland" , "Sweden" , "Switzerland" , "Syria" , "Taiwan" , "Tajikistan" , "Tanzania" , "Thailand" , "Togo" , "Tokelau" , "Tonga" , "Trinidad and Tobago" , "Tromelin Island" , "Tunisia" , "Turkey" , "Turkmenistan" , "Turks and Caicos Islands" , "Tuvalu" , "Uganda" , "Ukraine" , "United Arab Emirates" , "United Kingdom" , "USA" , "Uruguay" , "Uzbekistan" , "Vanuatu" , "Venezuela" , "Viet Nam" , "Virgin Islands" , "Wake Island" , "Wallis and Futuna" , "West Bank" , "Western Sahara" , "Yemen" , "Yugoslavia" , "Zambia" , "Zimbabwe" ];

?>
<script type="text/javascript">

	jQuery(document).ready(function($) {
	   var x = location.hash;
	   	var value = x.replace("#", "");

	   	$('#role').change(function(){
        	if ($(this).val() == "journalist") {
                $('#magazine').show();
                $('#store').hide();
        	}else{
        		$('#store').show();
      	  	 	$('#magazine').hide();
        	}
		});
		<?php if(!isset($this->formData['role'])):?>
	   	
        $('#role option').filter(function() { 	
           return this.value && this.value == value;
        }).prop('selected', true);
        
        <?php endif; ?>

        $('#role').change();        
});



</script>



<style>.err input, .err select{	border:1px solid red !important;}</style>
<?php
$errorCodes = [];
if($this->errors):
$errorCodes = $this->errors->get_error_codes();
foreach ( $this->errors->get_error_messages() as $error ):?>
    <ul class="error-block">
	   <li class="error"><strong>ERROR</strong><?=$error?></li>
    </ul>
<?php
    endforeach;
    endif;
?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<form method="post" id="registrationform">
	<div class="col-xs-12 col-sm-6 text-center form-login register-form">
	
    <div <?php if(in_array('role', $errorCodes)) echo 'class="err"'; ?> >
    
    
    <label class="reg-label" for="role"> </label>
    
    <?php /*?>
        <select id="role" class="select" name="role"><option value="">Select Role*</option>
		<?php 
		foreach (self::$roles as $key=>$value):?>
			<option value="<?=$key?>" <?=($this->formData['role'])==$key ? 'selected' : '' ?>><?=$key?></option>
	    <?php endforeach; ?>
		</select>
		<?php */ ?>
		
		<select id="role" class="select" name="role"><option value="">Select Role*</option>

			<option value="ong-partenaires" <?=($this->formData['role'] == 'ong-partenaires')? 'selected': ''?>>ONG partenaires</option>
            <option value="ong-actives-en-suisse" <?=($this->formData['role'] == 'ong-actives-en-suisse')? 'selected': ''?>>ONG actives en Suisse</option>
            <option value="conseil-de-fondation" <?=($this->formData['role'] == 'conseil-de-fondation')? 'selected': ''?>>Conseil de fondation</option>
			
		</select>
		
    </div>
    
    <div <?php if(in_array('magazine', $errorCodes)) echo 'class="err"'; ?> id="magazine" style="display:none" >
    
    	<input placeholder="Magazine*" type="text" name="magazine" value="<?=$this->formData['magazine']?>">
    
    </div>
	
	<div <?php if(in_array('store', $errorCodes)) echo 'class="err"'; ?> id="store" style="display:none" >
    
    	<input placeholder="Store*" type="text" name="store" value="<?=$this->formData['store']?>">
    
    </div>
    
    	
    <div <?php if(in_array('email', $errorCodes)) echo 'class="err"'; ?>>
    <label class="reg-label" for="email">Email <strong>*</strong></label>
    <input placeholder="Email*" type="text" name="email" value="<?=$this->formData['email']?>">
    
    </div>
 
   
    <div>
    <label class="reg-label" for="firstname">First Name</label>
    <input placeholder="First Name" type="text" name="fname" value="<?=$this->formData['fname']?>">
    </div>
   
    <div>
    <label class="reg-label" for="lastname">Last Name</label>
    <input placeholder="Last Name" type="text" name="lname" value="<?=$this->formData['lname']?>">
    </div>
	<br>
    <h2>Additional Information</h2>
    	
    <div>
    <label class="reg-label" for="phone">Phone</label>
    <input placeholder="Phone" type="text" name="phone" value="<?=$this->formData['phone']?>">
    </div>
    		
    <div>
    <label class="reg-label" for="street">Street</label>
    <input placeholder="Street" type="text" name="street" value="<?=$this->formData['street']?>">
    </div>
   
    <div>
    <label class="reg-label" for="additional">Additional</label>
    <input placeholder="Additional" type="text" name="additional" value="<?=$this->formData['additional']?>">
    </div>
    		
    <div>
    <label class="reg-label" for="city">City</label>
    <input placeholder="City" type="text" name="city" value="<?=$this->formData['city']?>">
    </div>
    		
     <div>
    <label class="reg-label" for="postalcode">Postalcode</label>
    <input placeholder="Postalcode" type="text" name="postalcode" value="<?=$this->formData['postalCode']?>">
    </div>
    		
    <div <?php if(in_array('country', $errorCodes)) echo 'class="err"'; ?>>
	  <label class="reg-label" for="country">Country*</label>
	
	<select id="country" name="country" >
		<option>Select Country*</option>
		<?php foreach ($countryLists as $countryItem):
			$countryVal = strtolower($countryItem);
		?>
		 <option value="<?=$countryVal?>" <?=($this->formData['country'] == $countryVal)? 'selected="selected"' : '' ?> ><?=$countryItem?></option>
	  <?php endforeach; ?>
	</select>

  
    
    </div>
    
    <div class="newsl">
    	<label>I want to subscribe to the newsletter</label>
    	<input type="checkbox" name="subscribed" value="true" <?=($this->formData['subscribed'] == 'true')? 'checked': ''?> >
    
    </div>
    
    <?=KD_Registration::getErrorHtml($this->errors, 'g-recaptcha');?>
    <div class="g-recaptcha" name="g-recaptcha" data-sitekey="<?=Kd_Registration::CAPTCHA_SITE_KEY?>"></div>
	</div>
	
	<div class="col-xs-12 col-sm-6 text-center form-login register-form register-btn"><input type="submit" name="kd_register_submit" value="Register" class="submit_button_create"/></div><div>
    
</form>