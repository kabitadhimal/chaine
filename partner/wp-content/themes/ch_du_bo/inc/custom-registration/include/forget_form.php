<?php
$errorCodes = [];
if($this->errors):
$errorCodes = $this->errors->get_error_codes();
foreach ( $this->errors->get_error_messages() as $error ):?>
    <ul class="error-block">
	   <li class="error"><strong>ERROR</strong><?=$error?></li>
    </ul>
<?php
    endforeach;
    endif;
?>

<div id="login">
	<p class="message">Please enter your username or email address. You will receive a link to create a new password via email.</p>
	<form method="post">
	<?php wp_nonce_field('kd_forget_pass', 'kd_forget_nonce') ?>
		<p>
    		<label for="user_email" >e-mail:<br />
			<input type="text" name="user_email" id="user_email" class="input" value="<?=$email?>" size="20" /></label>
		</p>
		<p class="submit"><input type="submit" name="kd_forget_submit" id="wp-submit" class="button button-primary button-large" value="submit" /></p>
	</form>
	<p id="nav"><a href="<?php echo site_url();?>/wp-login.php">Log in</a></p>
	<!--  <p id="backtoblog"><a href="http://192.168.0.213/testwordpress/" title="Are you lost?">&larr; Back to Testwordpress</a></p>-->
</div>
<div class="clear"></div>