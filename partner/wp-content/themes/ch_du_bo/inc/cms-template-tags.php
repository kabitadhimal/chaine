<?php

class Ch_du_bo_Fund_Content{
	public function ch_du_bo_get_flexible_content(){
		
		// check if the flexible content field has rows of data
		if( have_rows('flexible_content') ):

			ob_start();

				while ( have_rows('flexible_content') ) : the_row();

					if( get_row_layout() == 'banner' ):

						$this->ch_du_bo_get_banner();

					elseif( get_row_layout() == 'cms_menu' ):

						$this->ch_du_bo_get_cms_menu();

					elseif( get_row_layout() == 'status_bar' ):

						$this->ch_du_bo_get_status_bar();

					elseif( get_row_layout() == 'description_section' ):

						$this->ch_du_bo_get_description_section();

					elseif( get_row_layout() == 'quote_section' ):

						$this->ch_du_bo_get_quote_section();

					elseif( get_row_layout() == 'key_figure_section' ):

						$this->ch_du_bo_get_key_figure_section();

					elseif( get_row_layout() == 'conclusion_section' ):

						$this->ch_du_bo_get_conclusion_section();

					elseif( get_row_layout() == 'testimonial_section' ):

						$this->ch_du_bo_get_testimonial_section();

					elseif( get_row_layout() == 'photo_gallery_section' ):

						$this->ch_du_bo_get_photo_gallery_section();

					elseif( get_row_layout() == 'latest_news_section' ):

						$this->ch_du_bo_get_latest_news_section();

					elseif( get_row_layout() == 'social_media_section' ):

						$this->ch_du_bo_get_social_media_section();

					elseif( get_row_layout() == 'video_block' ):

						$this->ch_du_bo_get_video_block();

					elseif( get_row_layout() == 'partners_section' ):

						$this->ch_du_bo_get_partners_section();

					elseif( get_row_layout() == 'two_column' ):

						$this->ch_du_bo_get_two_column();

					elseif( get_row_layout() == 'three_column' ):

						$this->ch_du_bo_get_three_column();

					elseif( get_row_layout() == 'reportage_news_or_social_posts' ):

						$this->ch_du_bo_get_reportage_news_or_social_posts();

					elseif( get_row_layout() == 'html_free_area_section' ):

						$this->ch_du_bo_get_html_free_area_section();

					elseif( get_row_layout() == 'simple_block_new' ):

						$this->ch_du_bo_get_simple_block_new();

					elseif( get_row_layout() == 'team_member_partners_logos' ):

						$this->ch_du_bo_get_team_member_partners_logos();

					elseif( get_row_layout() == 'small_expandable_area' ):

						$this->ch_du_bo_get_small_expandable_area();

					elseif( get_row_layout() == 'big_expandable_area' ):

						$this->ch_du_bo_get_big_expandable_area();

					elseif( get_row_layout() == 'simple_text_area' ):

						$this->ch_du_bo_get_simple_text_area();

					elseif( get_row_layout() == 'tabs_section' ):

						$this->ch_du_bo_get_tabs_section();

					endif;

				endwhile;

				$content = ob_get_contents();
				ob_end_clean();
				return $content;

		else:

			return false;

		endif;
	}

	private function ch_du_bo_get_banner(){
		$bgImage = get_sub_field('background_image');
		if($bgImage){
			$img = $bgImage['sizes']['slider-img'];
		}else{
			$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'slider-img');
			if($img){
				$img = $img[0];
			}else{
				$img = get_template_directory_uri().'/images/cms-img-landscape.jpg';
			}
		}
		$displayBanner = false;
		$hideDonate = get_sub_field('hide_faire_un_don_button');
		$title = get_sub_field('title');
		if($title != ""){ $displayBanner = true; }
		$shortDescription = get_sub_field('short_description');
		if($shortDescription != ""){ $displayBanner = true; }
		$youtubeVideoLink = get_sub_field('youtube_video_link');
		if($youtubeVideoLink != ""){ $displayBanner = true; }
		$faire_un_don_button_text = get_sub_field('faire_un_don_button_text');
		if($faire_un_don_button_text != ""){ $displayBanner = true; }

		$copyright_text = get_sub_field('copyright_text');

		if($displayBanner):
		?>
			<section class="main-banner no-margin">
	           <div class="item fund-banner" style="background:url(<?php echo $img; ?>) no-repeat center center">
	            <div class="sliderCaption">
	            	<div class="container">
		                <div class="row">
							<?php if($title): ?>
								<h1 class="fund-title"><?php echo do_shortcode($title); ?></h1>
							<?php endif; ?>
		                	<?php if($shortDescription != ''): ?>
			                    <p><?php echo do_shortcode($shortDescription); ?></p>
			                <?php endif; ?>
			                	<?php if($youtubeVideoLink != ''): ?>
									<a href="<?php echo $youtubeVideoLink; ?>" class="btn underline video"><span class="video"></span><?php _e('Voir la vidéo','ch_du_bo'); ?></a>
								<?php endif; ?>
								<?php if($hideDonate[0] != "Yes" || $faire_un_don_button_text !== ''): ?>
								 <a class="btnDonate" href="<?php the_sub_field('faire_un_don_button_link'); ?>"><?php echo $faire_un_don_button_text; ?></a>
								<?php endif; ?>
		                </div>
					</div>
	            </div>
	           </div>
			   <?php if($copyright_text != ''): ?>
				   <div class="copyright"><?php echo $copyright_text; ?></div>
			   <?php endif; ?>
	    	</section><!--/.main-banner-->
			<?php
		endif;
	}

	private function ch_du_bo_get_cms_menu(){

		//debug(wp_get_nav_menu_object(get_sub_field('menu')));
		?>
		<section class="status_bar section-filter section-filter-mobile">
    	<div class="container">
        <div class="panel panel-default">
			<div class="xs-hidden panel-heading">
			  <h4 class="panel-title">
			  <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#filter" href="#cms-filter"><?php _e('Voir aussi', 'ch_du_bo'); ?></a>
			  </h4>
			</div>
            <div id="cms-filter" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                <div class="row">
                    <div class="col-md-2"><h4><?php _e('Voir aussi', 'ch_du_bo'); ?></h4></div>
                    <div class="col-md-10">
                        <?php
                            wp_nav_menu(array(
                                'menu'	=>	get_sub_field('menu'),
                                'container_class' => '',
                                'menu_class'	=> 'filter-dropdown',
                                'menu_id'	=>	'',
                                'container_id'	=>	'',
                                'walker'	=> new Custom_Walker()
                            ));
                        ?>
                    </div>
                </div>
                </div>
            </div>
        </div>

        </div>
    </section><!--/.status bar-->

		<?php
	}

	private function ch_du_bo_get_status_bar(){
		$status_title = get_sub_field('status_title');
		$progress_bar = get_sub_field('progress_bar');
		$date_title = get_sub_field('date_title');
		$date_of_the_disaster = get_sub_field('date_of_the_disaster');
		$donation_title = get_sub_field('donation_title');
		//[donation id="2605"] $donationID = get_sub_field('total_of_donation');
		$donationID = get_field('funds_received');
		$useShortcode = false;
		if(!$donationID || $donationID == ''){
			$donationID = get_sub_field('total_of_donation');
			$useShortcode = true;
		}
		$status = get_sub_field('status');
		/* if($status == 'En cours'){
			$status = __('En cours','ch_du_bo');
		}else{
			$status = __('Terminé','ch_du_bo');
		} */
		$active = get_sub_field('active');
		?>
		<section class="status_bar fund-status">
	    	<div class="container">
	        	<div class="row">
	            	<div class="col-md-5 col-sm-12">
	                	<div class="status"><span class="itemhide"><?php printf(__('%s : %s','ch_du_bo'),$status_title, $status); ?></span>
	                    <div class="progress">
	                          <div class="progress-bar progress-bar-urgence <?php echo $active == 'Step 1' ? 'active' : ''; ?>" role="progressbar" style="width:25%">
	                            <?php the_sub_field('step_1'); ?>
	                          </div>
	                          <div class="progress-bar <?php echo ( $active == 'Step 2' || $active == 'Step 3' ) ? ( $active == 'Step 2' ? 'progress-bar-urgence active' : 'progress-bar-urgence' ) : 'progress-bar-reconstruction'; ?>" role="progressbar" style="width:40%">
	                            <?php the_sub_field('step_2'); ?>
	                          </div>
	                          <div class="progress-bar <?php echo $active == 'Step 3' ? 'progress-bar-urgence active' : 'progress-bar-consolidation'; ?>" role="progressbar" style="width:35%">
	                           <?php the_sub_field('step_3'); ?>
	                          </div>
	                        </div></div>

	                </div>

	                <div class="col-md-5 col-xs-6">
	                	<?php if($date_of_the_disaster): ?>
	                		<div class="middle-status-fund">
			                	<div class="dateDisaster">
									<?php if($date_title): ?>
										<span class="donate itemhide"><?php echo $date_title; ?></span>
									<?php endif; ?>
			                        <span class="date"><?php the_sub_field('date_of_the_disaster'); ?></span>
			                    </div>
			                </div>
	                    <?php endif; ?>
					</div>
					<div class="col-md-2 col-xs-6">
						<?php if($donation_title != '' || $donationID != ''): ?>
							<?php if($useShortcode): ?>
								<div class="totalDonation">
									<?php if($donation_title): ?>
										<span class="total itemhide"><?php echo $donation_title; ?></span>
									<?php endif; ?>
									<?php echo do_shortcode($donationID); ?>
								</div>
							<?php else: ?>
								<?php $donationData = ch_du_bo_get_donation_data($donationID); ?>
								<div class="totalDonation">
									<?php if($donation_title): ?>
										<span class="total itemhide"><?php echo $donation_title; ?></span>
									<?php endif; ?>
									<span class="amount"><?php echo $donationData['devise']; ?>&nbsp;<strong><?php echo $donationData['donation']; ?></strong></span>
								</div>
							<?php endif; ?>
						<?php endif; ?>
	                </div>


	            </div>
	        </div>

	    </section><!--/.status bar-->
	    <?php if(!$stickyUsed): static $stickyUsed = true; ?>
	    <script type="text/javascript">
	    	jQuery(document).ready(function($) {
				// grab the initial top offset of the navigation
			   	var stickyNavTop = $('.status_bar').offset().top;

			   	// our function that decides weather the navigation bar should have "fixed" css position or not.
			   	var stickyNav = function(){
				    var scrollTop = $(window).scrollTop(); // our current vertical position from the top

				    // if we've scrolled more than the navigation, change its position to fixed to stick to top,
				    // otherwise change it back to relative
				    if (scrollTop > stickyNavTop) {
				        $('.status_bar').addClass('sticky');
				    } else {
				        $('.status_bar').removeClass('sticky');
				    }
				};

				stickyNav();
				// and run it again every time you scroll
				$(window).scroll(function() {
					stickyNav();
				});
			});
	    </script>
		<?php endif;?>
		<?php
	}

	private function ch_du_bo_get_description_section(){
		$hideShare = get_sub_field('hide_share');
		$pdfLink = get_sub_field('pdf_link');
		$backgroundColor = get_sub_field('background_color');
		$bgColor = '#fff';
		static $shared = false;
		if($backgroundColor == 'Grey'){
			$bgColor = '#f5f5f5';
		}
		?>
		<section class="fund_desc wow fadeInDown animated" style="background: <?php echo $bgColor; ?>">
	    	<div class="container">
	        	<div class="row">

	                 <div class="col-md-6">
	                		<?php echo do_shortcode(get_sub_field('left_column_text')); ?>
	                </div>

	                <div class="col-md-6">
	                <div class="content-right-fund">
	                	<figure>
	                    	<?php echo do_shortcode(get_sub_field('right_column_text')); ?>
	                    </figure>
	                    <div class="downloadlnk row">
	                    	<?php if($pdfLink != ''): ?>
		                    	<li class="col-md-6 col-sm-12 col-xs-12"><a download class="download" href="<?php the_sub_field('pdf_link'); ?>">		<?php the_sub_field('pdf_title'); ?></a></li>
		                    <?php endif; ?>
	                    	<?php if(!$hideShare): ?>
		                        <li class="col-md-6 col-sm-12 col-xs-12 pull-right">
                                
                                <div class="share_block">
                                
                                <a class="share" href="#"><?php _e('Partager', 'ch_du_bo'); ?></a>
		                        <p>
		                        	<!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox"></div>
		                        	<?php if(!$shared):
		                        		$shared = true;
		                        	?>
			                        	<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5819b0844233f71a"></script>
			                        <?php endif; ?>
		                        </p>
                                
                                </div>
                                </li>
                                
                                
		                    <?php endif; ?>
	                    </div>

	                </div>
	                </div>

	            </div>
	        </div>

	    </section><!--/.description section-->
		<?php
	}

	private function ch_du_bo_get_quote_section(){
		$authorImage = get_sub_field('image_of_author');
		$background_color = get_sub_field('background_color');
		if($background_color == 'White'){
			$bgClass = 'quote-bg-white';
		}else if($background_color == 'Grey'){
			$bgClass = 'quote-bg-grey';
		}else{
			$bgClass = 'quote-bg-red';
		}
		?>
		<?php /*?><section class="secquote wow fadeInDown animated <?php echo $bgClass; ?>">
    	<div class="container">

    	<div class="row">
			<?php $col = 12; ?>
    		<?php if($authorImage != ''): ?>
    			<div class="col-md-4 col-sm-4">
	    			<div class="quote_left">
		       			<img src="<?php echo $authorImage['sizes']['author-img']; ?>" class="author-img">
		       		</div>
			    </div>
			    <?php $col = 8; ?>
	       	<?php endif; ?>

			<div class="col-md-<?php echo $col; ?> col-sm-<?php echo $col; ?>">
				<blockquote><?php echo get_sub_field('text'); ?></blockquote>
				<em><?php the_sub_field('author_name'); ?></em>
			</div>




           </div>
        </div>

    </section><!--/.section quote--><?php */?>
    
    <section class="secquote wow fadeInDown animated <?php echo $bgClass; ?>">
    	<div class="container">

    	<div class="row">
        <?php $col = 'col-md-12'; ?>			
    		<?php if($authorImage != ''): ?>    			
	    			<div class="quote_left">
		       			<img src="<?php echo $authorImage['sizes']['author-img']; ?>" class="author-img">
		       		</div>			   
			   	
	       	<?php 
				$col = ''; 
				endif; ?>

			<div class="quote-right <?php echo $col; ?>">
				<blockquote><?php echo get_sub_field('text'); ?></blockquote>
				<em><?php the_sub_field('author_name'); ?></em>
			</div>




           </div>
        </div>

    </section><!--/.section quote-->
    
		<?php
	}

	private function ch_du_bo_get_key_figure_section(){
		$bgImage = get_sub_field('background_image');
		$copyright_text = get_sub_field('copyright_text');
		?>
		<section class="keyfigure" style="background:url(<?php echo $bgImage['sizes']['slider-img']; ?>) no-repeat center center">
	        <div class="container">

	            <div class="center wow fadeInDown">
	            	<h4><?php echo do_shortcode(get_sub_field('sub_title')); ?></h4>
	                <h2><?php echo do_shortcode(get_sub_field('title')); ?></h2>
	            </div>

	            <?php
				$keyCount = count(get_sub_field('figures'));
				$col = 4;
				if($keyCount === 2){
					$col = 6;
				}
				if( have_rows('figures') ): ?>
	            	<div class="partners">
	               		<div class="row">
	                		<ul>
	                		<?php while ( have_rows('figures') ) : the_row();
							$icon = get_sub_field('icon');
							?>
	                			<li class="col-md-<?php echo $col; ?> col-xs-<?php echo $col; ?>">
									<?php if($icon): ?>
										<i class="icon-<?php echo $icon; ?>"></i>
									<?php endif; ?>
								<h4><?php the_sub_field('title'); ?></h4><p><?php the_sub_field('sub_title'); ?></p></li>
	                		<?php endwhile; ?>
	                		</ul>
	                	</div>
	                </div>
	            <?php endif; ?>
				<?php if($copyright_text != ''): ?>
					<div class="copyright"><?php echo $copyright_text; ?></div>
				<?php endif; ?>
	        </div><!--/.container-->
	    </section><!--/.swiss solidarity-->
		<?php
	}

	private function ch_du_bo_get_conclusion_section(){
		?>
		<section class="secquote quote-bg-red secquote-fund">
	    	<div class="container">
	    		<div class="col-md-12">
	    			<blockquote><?php the_sub_field('text'); ?></blockquote>
	    		</div>
    		</div>
    	</section><!--/.section quote-->
		<?php
	}

	private function ch_du_bo_get_testimonial_section(){
		$background_color = get_sub_field('background_color');
		if($background_color == 'Grey'){
			$background_color = '#f5f5f5';
		}else{
			$background_color = '#fff';
		}
		$lTitle = get_sub_field('l_title');
		$rTitle = get_sub_field('r_title');
		$lImage = get_sub_field('l_picture_of_witness');
		$rImage = get_sub_field('r_picture_of_witness');
		$lDescription = get_sub_field('l_description');
		$rDescription = get_sub_field('r_description');
		$title = get_sub_field('title');
		?>
		<section class="sec-testimonail" style="background-color: <?php echo $background_color; ?>">
    		<div class="container">
				<?php if($title): ?>
					<div class="title center">
						<h3><?php echo $title; ?></h3>
					</div>
				<?php endif; ?>
             	<div class="row">
                    <div class="content">

                    	<div class="col-md-6">
                        	<figure>
                            	<img src="<?php echo $lImage['sizes']['testimonail-thumb']; ?>" alt="">
                                <figcaption>
                                	<?php if($lTitle): ?>
	                                	<h3><?php echo $lTitle; ?></h3>
	                                <?php endif; ?>
                                    <?php echo $lDescription; ?>

                                </figcaption>

                            </figure>

                        </div>
                    	<div class="col-md-6">
                        	<figure>
                            	<img src="<?php echo $rImage['sizes']['testimonail-thumb']; ?>" alt="">
                                <figcaption>
                                	<?php if($rTitle): ?>
	                                	<h3><?php echo $rTitle; ?></h3>
	                                <?php endif; ?>
                                    <?php echo $rDescription; ?>
                                </figcaption>

                            </figure>

                        </div>
                    </div>
              	</div>
        	</div>
        </section> <!--/.end of testimonial-->
		<?php
	}

	private function ch_du_bo_get_photo_gallery_section(){

		$images = get_sub_field('images');
		if( $images ): ?>
		    <section class="photo-gallery-section">
				<div id="example2" class="slider-pro">
					<div class="sp-slides">
		        <?php foreach( $images as $image ): ?>
					<div class="sp-slide">
						<a href="<?php echo $image['sizes']['slider-img']; ?>" class="open-here">
							<img class="sp-image" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"
								data-src="<?php echo $image['sizes']['slider-img']; ?>"
								data-retina="<?php echo $image['sizes']['slider-img']; ?>"/>
						</a>
						<?php if($image['caption']): ?>
							<p class="sp-caption"><?php echo $image['caption']; ?></p>
						<?php endif; ?>						
					</div>
		        <?php endforeach; ?>
					</div>
				</div>
		    </section><!--/.secondary-slider-->
		<?php endif;
	}

	private function ch_du_bo_get_latest_news_section(){
		$ids = get_sub_field('choose_news');
		static $blockID = 1;
		$title = get_sub_field('title');
		?>
		<section class="section-news section-news-fund no-padding-btm" data-ids='<?php echo json_encode($ids); ?>'>
	        <div class="container">
	            <div class="row">
					<?php if($title):?>
		            <div class="center title wow fadeInDown animated">
			            <h3><?php echo $title; ?></h3>
			        </div>
					<?php endif; ?>
			        <?php
			        	$args = array(
			        		'tag__in' => $ids,
			        		//'ordeby'	=> 'post__in',
			        		'posts_per_page'	=>	3
			        	);
//debug($args, 1);
						// The Query
						$query = new WP_Query( $args );

						// The Loop
						if ( $query->have_posts() ) {
							?>
							<div class="news-listing">
	                  			<ul class="mobile-carousel news-list-ul">
							<?php
							while ( $query->have_posts() ) {
								$query->the_post();
								if(has_post_thumbnail()){
									$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'fundraising-thumb');
								}else{
									$img = array(get_template_directory_uri().'/images/img-col3.jpg');
								}

								/* switch(get_post_format()){
									case 'standard':
										$icon = 'icon-news.png';
										break;
									case 'image':
										$icon = 'icon-camera.png';
										break;
									default:
										$icon = 'icon-news.png';
								} */

								$icon = get_template_directory_uri().'/images/icon-news.png';
								$terms = wp_get_post_terms($post['id'],'category');
								foreach($terms as $term){
									if($term->parent == 0){
										continue;
									}
									$icon = get_field('icon',"category_".$term->term_id);
									break;
								}
								?>
								<li id="news-<?php the_ID(); ?>">
			                     	<div class="feature_block col-md-4 item wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
										<a  href="<?php the_permalink(); ?>">
				                    	<div class="features center">
					                        <figure>
												<div class="image_crop">
					                        	<img src="<?php echo $img[0]; ?>" alt="image 4">
												</div>
					                            <figcaption><span class="icon_news"><img src="<?php echo $icon; ?>" alt="#"></span></figcaption>
					                        </figure>
					                        <div class="content-wrap">
						                        <span class="disasterdate"><?php the_time('d.m.Y'); ?></span>
						                        <h4><?php the_title(); ?></h4>
					                            <?php the_excerpt(); ?>
				                        	</div>
				                        </div><!--/.fundraising-->
										</a>
				                    </div><!--/.col-md-4-->
		                    	</li> <!--/.col-md-4-->
								<?php
							}
							?>
								</ul>
							</div>
							<?php
						}
						// Restore original Post Data
						wp_reset_postdata();
			        ?>
	                <div class="center newsAll hidden-xs">
	                	<a class="btn border more-news-ajax" href="#"><?php _e('Voir plus','ch_du_bo'); ?></a>
	                	<p style="display:none" class="news-loader"><img src="<?php echo get_template_directory_uri().'/images/ajax-loader.gif'; ?>" alt="Loader"></p>
	                </div>
	                <?php if($blockID == 1): ?>
	                <script type="text/javascript">
	                	/*var paged = 2;
	                	jQuery('.more-news-ajax').click(function(e){
	                		e.preventDefault();
	                		var thisBlock = jQuery(this).parents('.section-news').find('.news-list-ul');
	                		paged = thisBlock.data('paged');
	                		if(typeof paged === 'undefined'){
	                			paged = 2;
	                			thisBlock.attr('data-paged',2);
	                		}
	                		var ids = thisBlock.parents('.section-news.no-padding-btm').data('ids');
	                		console.log(ids);
	                		jQuery.ajax({
							  method: "POST",
							  url: ch.ajax_url,
							  data: { action: "ajax_more_news", data: ids, paged: paged }
							}).done(function( msg ) {
								thisBlock.append(msg);
							  	paged++;
							  	thisBlock.attr('data-paged',paged);
						  	});
	                	});*/
	                </script>
	                <?php endif; $blockID++; ?>
	            </div><!--/.row-->
	        </div><!--/.container-->

	   </section><!--/.section news-->
		<?php
	}

	public function ch_du_bo_get_social_media_section(){
		$title = get_sub_field('title');
		?>
		<section class="section-news section-news-fund">
	        <div class="container">
	            <div class="row">
				<?php if($title): ?>
	            <div class="center title wow fadeInDown animated">
		            <h3><?php echo $title; ?></h3>
		        </div>
				<?php endif; ?>
	            	<?php if(have_rows('social_links')): ?>
	            		<div class="news-listing social-News">
			                <ul class="mobile-carousel">
		            		<?php
		            		$count = 0;
		            		$rowCount = 1;
		            		while ( have_rows('social_links') ) : the_row();
								if(get_sub_field('social_media') == 'Instagram'){
									$content = file_get_contents('https://api.instagram.com/oembed/?url='.get_sub_field('link'));
									$content =json_decode($content);
									$data = array(
										'full_picture'	=>	get_sub_field('link').'media/?size=m',
										'link'	=>	get_sub_field('link'),
										'created_time'	=>	'',
										'message'	=>	$content->title,
										'id'	=>	'',
										'date'	=>	'',
									);
									$class="icon_news icon_insta";
								}else{
									if(!get_sub_field('link')){ continue; }
									$fb = new FB_Page_Manager();
									$data = $fb->get_post_by_url(get_sub_field('link'));
									$data['date'] = date('d.m.Y',strtotime($data['created_time']));
									$class ="icon_fb";
								}
								$rowClass = '';
								if($count%3==0 && $count!=0){
									$rowCount++;
								}
								if($rowCount > 1){
									$rowClass .= 'hidden ';
								}
								$rowClass .= 'row-'.$rowCount;

		            		?>
		            			<li class="<?php echo $rowClass; ?>">
		                      		<div class="col-md-4 item wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
										<a href="<?php echo $data['link']; ?>">
				                    	<div class="features center">
				                         	<figure>
				                         	<div class="image_crop">
					                        	<img src="<?php echo $data['full_picture']; ?>" alt="image 7">
					                        	</div>
					                            <figcaption>
													<span class="<?php echo $class; ?>" >
					                            <?php if(get_sub_field('social_media') == 'Instagram'){ ?>
					                            	<img src="<?php echo get_template_directory_uri(); ?>/images/icon-insta.png" alt="<?php echo $data['title']; ?>"></span>
					                            <?php }else{ ?>
					                            	<i class="fa fa-facebook"></i>
					                            <?php } ?>
												</figcaption>
					                        </figure>
					                        <div class="content-wrap">
						                        <span class="disasterdate"><?php echo $data['date']; ?></span>
						                        <p><?php echo ch_du_bo_truncate($data['message'],150); ?></p>
					                        </div>
				                        </div><!--/.fundraising-->
										</a>
				                    </div><!--/.col-md-4-->
			                    </li>
		            		<?php $count++; endwhile; ?>
		            		</ul>
	            		</div>
            		<?php endif; ?>
	                <?php if(have_rows('social_links') & !empty(get_sub_field('link'))): ?>
						<div class="center newsAll hidden-xs">
							<a class="btn border show-more-social" href="#"><?php _e('Voir plus','ch_du_bo'); ?></a>
						</div>   <!--end of link all-->
					<?php endif; ?>
	                <?php

						$twitterSearch = get_sub_field('twitter_hashtag_or_search');
						if(!$twitter_hashtag_or_search){
							$twitterSearch = get_post_meta(get_the_ID(), 'flexible_content_0_twitter_hashtag_or_search',true);
						}

						if($twitterSearch){
							$statuses = get_twitter_search_feed($twitterSearch);
							if(!empty($statuses)):
					?>
							<div class="col-md-12 slider-twitter">
								<figure><figcaption><a class="icon_tw" href="#"><i class="fa fa-twitter"></i></a></figcaption></figure>
								<div class="secondary-slider secondary-twitter-slider">
									<?php
										if( count($statuses) == 1 ){
											$statuses[] = $statuses[0];
											$statuses[] = $statuses[0];
										}
									?>
									<?php foreach($statuses as $status): ?>
										<div class="item">
											<p>"<?php echo $status->text; ?>"</p>
											<a href="https://twitter.com/<?php echo $status->user->screen_name; ?>" class="btn underline"><?php echo '@'.$status->user->screen_name; ?></a>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif; } ?>

	            </div><!--/.row-->
	        </div><!--/.container-->
	    </section><!--/#section news social-->
		<?php
	}

	public function ch_du_bo_get_video_block(){
		$imageID = get_sub_field('background_image');
		$img = wp_get_attachment_image_src($imageID,'slider-img');
		if(!$img){
			$img = get_template_directory_uri().'/images/video-placeholder.jpg';
		}else{
			$img = $img[0];
		}
		$title = get_sub_field('video_title');
		?>
		<section class="section-video-cms" style="background:url(<?php echo $img; ?>) no-repeat center center">
	        <div class="container">
	            <div class="center wow fadeInDown">
					<?php if($title): ?>
                    <h1><?php echo $title; ?></h1>
					<?php endif; ?>
                   	<div class="embed-responsive embed-responsive-16by9">
	                   	<a class="play_btn video" href="<?php the_sub_field('video_link'); ?>">
	                   		<span><?php echo get_sub_field('regarder_la_video_text'); ?></span>
	                   	</a>
                   	</div>
	            </div>
	        </div><!--/.container-->
	    </section><!--/.section videos-->

		<?php
	}

	public function ch_du_bo_get_partners_section(){
		$bgColor = get_sub_field('background_color');
		if($bgColor == 'Grey'){
			$bgColor = '#f5f5f5';
		}else{
			$bgColor = '#fff';
		}
		$title = get_sub_field('title');
		?>
		<section class="section-partners" style="background-color: <?php echo $bgColor; ?>">
		   	<div class="container">
	         	<div class="row">
					<?php if($title): ?>
		        	<div class="center title wow fadeInDown animated">
			            <h3><?php echo $title; ?></h3>
			        </div>
					<?php endif; ?>
			        <?php if(have_rows('partners')): ?>
			        	<ul class="center wow fadeInDown animated">
			        	<?php while(have_rows('partners')): the_row(); ?>
		        			<?php
		        				$img = get_sub_field('logo');
		        				$link = get_sub_field('link');
		        				if($link == ''){ $link = '#'; }
		        			?>
				        	<li class="col-md-4 col-sm-6"><span class="partners-section-block"><a href="<?php echo $link; ?>"><img src="<?php echo $img['url']; ?>" alt="heks eper"></a></span></li>
				        <?php endwhile; ?>
			        	</ul>
			        <?php endif; ?>
             	</div>
	        </div>
	   	</section> <!--/.section partners-->
		<?php
	}

	public function ch_du_bo_get_two_column(){
		$backgroundColor = get_sub_field('background_color');
		$bgColor = '#fff';
		$title = get_sub_field('title');
		if($backgroundColor == 'Grey'){
			$bgColor = '#f5f5f5';
		}
		?>
		<section class="section-imagetext two-col-imagetext wow fadeInDown animated" style="background-color:<?php echo $bgColor; ?>">
		    <div class="container">
		        <div class="row">
					<?php if($title): ?>
						<div class="center title hidden-xs wow fadeInDown animated animated" style="visibility: visible; animation-name: fadeInDown;">
							<h3><?php echo $title; ?></h3>
						</div>
					<?php endif; ?>
		        	<div class="col-md-6">
		            	<?php the_sub_field('left_column'); ?>
		            </div>
		            <div class="col-md-6">
		            <div class="right_cms_content">
			            <?php the_sub_field('right_column'); ?>
			        </div>
		            </div>
		        </div>
	        </div>
        </section> <!--end of left image right text-->
		<?php
	}

	public function ch_du_bo_get_three_column(){
		$backgroundColor = get_sub_field('background_color');
		$bgColor = '#fff';
		$title = get_sub_field('title');
		if($backgroundColor == 'Grey'){
			$bgColor = '#f5f5f5';
		}
		?>
		<section class="section-imagetext three-col-imagetext  wow fadeInDown animated" style="background-color:<?php echo $bgColor; ?>">
		    <div class="container">
		        <div class="row">
					<?php if($title): ?>
						<div class="center title hidden-xs wow fadeInDown animated animated" style="visibility: visible; animation-name: fadeInDown;">
							<h3><?php echo $title; ?></h3>
						</div>
					<?php endif; ?>
		        	<div class="col-md-4">
		            	<?php the_sub_field('left_column'); ?>
		            </div>
		            <div class="col-md-4">
			            <?php the_sub_field('middle_column'); ?>
		            </div>
		            <div class="col-md-4">
			            <?php the_sub_field('right_column'); ?>
		            </div>
		        </div>
	        </div>
        </section> <!--end of left image right text-->
		<?php
	}

	public function ch_du_bo_get_reportage_news_or_social_posts(){
		$title = get_sub_field('title');
		$columns = get_sub_field('select_columns');
		$leftPost = get_sub_field('left_post');
		$midPost = get_sub_field('mid_post');
		$rightPost = get_sub_field('right_post');
		$backgroundColor = get_sub_field('background_color');
		$bgColor = '#fff';
		if($backgroundColor == 'Grey'){
			$bgColor = '#f5f5f5';
		}
		?>
		<section class="section-news contact-list" style="background-color:<?php echo $bgColor; ?>">
	        <div class="container">
	            <div class="row">
	                <div class="center title wow fadeInDown animated">
	                	<?php if($title != ''): ?>
				            <h3><?php echo $title; ?></h3>
				        <?php endif; ?>
		        	</div>

	                <div class="contact-listing">
                 	<ul class="mobile-carousel">
                 		<?php
	                 		// WP_Query arguments
							$args = array (
								'posts_per_page'         => '1',
								'post__in'	=> $leftPost
							);

							// The Query
							$query = new WP_Query( $args );

							// The Loop
							if ( $query->have_posts() ) {
								while ( $query->have_posts() ) {
									$query->the_post();
									if($hero == 'Yes'){
										$colClass = 8;
									}else{
										$colClass = 4;
									}
									?>
									<div class="grid-item col-md-<?php echo $colClass; ?> item wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
										<div class="features">
											<?php get_template_part('template-parts/content','reportage'); ?>
										</div>
									</div>
									<?php
								}
							}

							// Restore original Post Data
							wp_reset_postdata();
						?>

	                    <?php
	                 		// WP_Query arguments
							$args = array (
								'posts_per_page'         => '1',
								'post__in'	=> $midPost
							);

							// The Query
							$query = new WP_Query( $args );

							// The Loop
							if ( $query->have_posts() ) {
								while ( $query->have_posts() ) {
									$query->the_post();
									if($hero == 'Yes'){
										$colClass = 8;
									}else{
										$colClass = 4;
									}
									if($columns == 2){
										$colClass = 8;
									}
									?><div class="grid-item col-md-<?php echo $colClass; ?> item wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
										<div class="features">
										<?php
										if($colClass == 8){
											get_template_part('template-parts/content','reportage8');
										}else{
											get_template_part('template-parts/content','reportage');
										}

										?>
										</div>
									</div>
									<?php
								}
							}

							// Restore original Post Data
							wp_reset_postdata();
						?>
	                    <?php if($columns == 3): ?>
	                    	<?php
		                 		// WP_Query arguments
								$args = array (
									'posts_per_page'         => '1',
									'post__in'	=> $rightPost
								);

								// The Query
								$query = new WP_Query( $args );

								// The Loop
								if ( $query->have_posts() ) {
									while ( $query->have_posts() ) {
										$query->the_post();
										if($hero == 'Yes'){
											$colClass = 8;
										}else{
											$colClass = 4;
										}
										?><div class="grid-item col-md-<?php echo $colClass; ?> item wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
											<div class="features">
											<?php
											get_template_part('template-parts/content','reportage');
											?>
											</div>
										</div>
										<?php
									}
								}

								// Restore original Post Data
								wp_reset_postdata();
							?>
	                    <?php endif; ?>
	                      <!--/.col-md-4-->


	                </ul>
	                 </div> <!--/.news-listing-->



	            </div><!--/.row-->
	        </div><!--/.container-->

	   </section> <!--section news-->
		<?php
	}

	private function ch_du_bo_get_html_free_area_section(){
		$total = (int)get_sub_field('columns');
		$title = get_sub_field('title');
		$col_1_bg_color = get_sub_field('col_1_mobile_background_color');
		if($col_1_bg_color == ''){
			$col_1_bg_color = '#e5222f';
		}
		$col_2_bg_color = get_sub_field('col_2_mobile_background_color');
		if($col_2_bg_color == ''){
			$col_2_bg_color = '#3a3a3b';
		}
		$col_3_bg_color = get_sub_field('col_3_mobile_background_color');
		if($col_3_bg_color == ''){
			$col_3_bg_color = '#3a3a3b';
		}

		$col_1_link_url = get_sub_field('col_1_link_url');
		$col_1_open_new_tab = get_sub_field('col_1_link_open_in_new_tab');
		$col_1_class = '';
		if($col_1_open_new_tab == 'No'){
			$col_1_class = 'open-here';
		}

		$col_2_link_url = get_sub_field('col_2_link_url');
		$col_2_open_new_tab = get_sub_field('col_2_link_open_in_new_tab');
		$col_2_class = '';
		if($col_2_open_new_tab == 'No'){
			$col_2_class = 'open-here';
		}
		if($total == 2){
			$col = '6';
		}else{
			$col = '4';
		}
		?>
		<section class="section-htmlfree">
	        <div class="container">
	            <div class="row">
					<?php if($title): ?>
		            <div class="center title hidden-xs wow fadeInDown animated">
			            <h3><?php echo $title; ?></h3>
			        </div>
					<?php endif; ?>
		        	<div class="wow fadeInDown">
                     	<div class="col-md-4">
                     		<figure>
                            	<div class="hidden-xs"><a href="<?php echo $col_1_link_url; ?>"><img src="<?php the_sub_field('col_1_bg_image'); ?>" alt=""></a></div>
                            	<?php if($col_1_link_url != ''): ?>
	                            	<figcaption>
	                            		<a class="btn underline itemred <?php echo $col_1_class; ?>" href="<?php echo $col_1_link_url; ?>" style="background:<?php echo $col_1_bg_color; ?>"><span><?php the_sub_field('col_1_link_title'); ?></span></a></figcaption>
	                            <?php endif;?>
                            </figure>
                     	</div>
                 	 	<div class="col-md-4">
                     		<figure>
                            	<div class="hidden-xs"><a href="<?php echo $col_2_link_url; ?>" class="video"><img src="<?php the_sub_field('col_2_bg_image'); ?>" alt=""></a></div>
                            	<figcaption>
                            		<a class="btn underline itemblk <?php echo $col_2_class; ?>" href="<?php echo $col_2_link_url; ?>" style="background:<?php echo $col_2_bg_color; ?>"><span><?php the_sub_field('col_2_link_title'); ?></span></a></figcaption>
                            </figure>
                     	</div>
                     	<?php if($total == 3){ ?>
                     	<div class="col-md-4">
                     		<figure>
                            	<div class="hidden-xs"><a class="video" href="<?php the_sub_field('col_3_video_url'); ?>"><img src="<?php the_sub_field('col_3_bg_image'); ?>" alt=""></a></div>
                            	<figcaption>
                                	<a class="btn underline itemblk video" href="<?php the_sub_field('col_3_video_url'); ?>" style="background:<?php echo $col_3_bg_color; ?>"><span class="video"></span><span><?php the_sub_field('col_3_title'); ?></span></a>
                                </figcaption>
                            </figure>
                     	</div>
                     	<?php } ?>
	                </div><!--/.-->
				</div>
			</div>
		</section>
		<?php
	}

	private function ch_du_bo_get_simple_block_new(){
		$backgroundColor = get_sub_field('background_color');
		$bgColor = '#fff';
		if($backgroundColor == 'Grey'){
			$bgColor = '#f5f5f5';
		}
		$title = get_sub_field('title')
		?>
		<section class="section-htmlfree">
	        <div class="container">
	            <div class="row">
					<?php if($title): ?>
						<div class="center title hidden-xs wow fadeInDown animated">
							<h3><?php echo $title; ?></h3>
						</div>
					<?php endif; ?>
		        	<div class="wow fadeInDown">
					<?php
						$rows = get_sub_field('blocks');
						
						$row_count = count($rows);
						if( have_rows('blocks') ):

								while ( have_rows('blocks') ) : the_row();

									if(get_row_layout() == 'video_block'):

										$bg = get_sub_field('background_color');
										$img = get_sub_field('background_image');
										if(!$img['url']){
											$img['url'] = get_template_directory_uri().'/images/img-col3-360X253.jpg';
										}
										?>
										<div class="col-md-4">
											<figure>
												<div class="hidden-xs"><img src="<?php echo $img['url']; ?>" alt=""></div>
												<figcaption>
													<a class="btn underline itemblk video" href="<?php the_sub_field('video_url'); ?>" style="background:<?php echo $bg; ?>">
													<span class="video"></span><span><?php the_sub_field('title'); ?></span></a>
												</figcaption>
											</figure>
										</div>
										<?php

									elseif(get_row_layout() == 'text_block'):
										$bg = get_sub_field('background_color');
										$img = get_sub_field('background_image');
										if(!$img['url']){
											$img['url'] = get_template_directory_uri().'/images/img-col3.jpg';
										}
										$link_url = get_sub_field('link_url');
										$open_new_tab = get_sub_field('open_in_new_tab');
										$col_class = '';
										if($open_new_tab == 'No'){
											$col_class = 'open-here';
										}
										?>
										<div class="col-md-4">
											<figure>
												<div class="hidden-xs"><img src="<?php echo $img['url']; ?>" alt=""></div>
												<?php if($link_url != ''): ?>
													<figcaption>
														<a class="btn underline itemred <?php echo $col_class; ?>" href="<?php echo $link_url; ?>" style="background:<?php echo $bg; ?>"><span><?php the_sub_field('link_text'); ?></span></a></figcaption>
												<?php endif;?>
											</figure>
										</div>
										<?php

									endif;

								endwhile;

						endif;
					?>
					</div>
				</div>
			</div>
		</section>
		<?php
	}

	private function ch_du_bo_get_team_member_partners_logos(){
		$backgroundColor = get_sub_field('background_color');
		$bgColor = '#fff';
		if($backgroundColor == 'Grey'){
			$bgColor = '#f5f5f5';
		}
		if( have_rows('logos') ):
		?>
		<section class="section-contact" style="background: <?php echo $bgColor; ?>">
	    	<div class="container">
	        	<div class="row">
                
	            	<?php while ( have_rows('logos') ) : the_row();
	            	$img = get_sub_field('image');
	            	$description = get_sub_field('description');
	            	$link = get_sub_field('url_on_image');
	            	$open_link_in_new_tab = get_sub_field('open_link_in_new_tab');
	            	$option = get_sub_field('option');
	            	$class = '';
	            	if($open_link_in_new_tab == 'No'){
	            		$class = ' class="open-here" ';
	            	}
	            	?>
                    
		                <div class="col-md-4">
		                	<div class="address">
									<?php if($option === 'Team Member'): ?>
                                    <figure>
										<img src="<?php echo $img['sizes']['fundraising-thumb']; ?>" alt="">
                                    </figure>
									<?php else: ?>
										<figure>
											<a href="<?php echo $link; ?>" <?php echo $class; ?>><img src="<?php echo $img['sizes']['fundraising-thumb']; ?>" alt=""></a>
										</figure>
									<?php endif; ?>
								<?php if($description): ?>
									<address><?php echo $description; ?></address>
								<?php endif; ?>

		                    </div>
		                </div>
					<?php endwhile; ?>
	            </div>
	        </div>
	    </section>
		<?php
		endif;
	}

	private function ch_du_bo_get_small_expandable_area(){
		static $panelCount = 1;
		$sub_title = get_sub_field('sub_title');
		$title = get_sub_field('title');
		$left_content = get_sub_field('left_content');
		$right_content = get_sub_field('right_content');
		if(have_rows('contents')): ?>
		<section class="section-accrodion">
	    	<div class="container">
				<div class="center wow fadeInDown">
					<?php if($title): ?>
					<h2><?php echo $title; ?></h2>
					<?php endif; ?>
					<?php if($sub_title): ?>
					<h3><?php echo $sub_title; ?></h3>
					<?php endif; ?>
				</div>
				<div class="row accordion-two-col">
					<div class="col-md-6 col-xs-12">
						<?php echo $left_content; ?>
					</div>
					<div class="col-md-6 col-xs-12">
						<?php echo $right_content; ?>
					</div>
				</div>
	        	<div class="panel-group" id="accordion<?php echo $panelCount; ?>">
		        	<?php
		        	static $counter = 0;
		        	while( have_rows('contents') ): the_row(); ?>
		        		<div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion<?php echo $panelCount; ?>" href="#collapse<?php echo $counter; ?>">
						          <?php the_sub_field('title'); ?>
						        </a>
						      </h4>
						    </div>
						    <div id="collapse<?php echo $counter; ?>" class="panel-collapse collapse">
						      <div class="panel-body">
						        <?php the_sub_field('description'); ?>
						      </div>
						    </div>
					  </div>
		        	<?php $counter++; endwhile; $panelCount++; ?>
	        	</div>
	        </div>
	    </section>
		<?php endif;
	}

	private function ch_du_bo_get_big_expandable_area(){
		$backgroundColor = get_sub_field('background_color');
		$bgColor = '#fff';
		if($backgroundColor == 'Grey'){
			$bgColor = '#f5f5f5';
		}
		if(have_rows('contents')):
		static $panelCount = 100;
		?>
		<section class="section-accordion-conter" style="background-color:<?php echo $bgColor; ?>">
	    	<div class="panel-group  panel-group-center" id="accordion<?php echo $panelCount; ?>">
		        	<?php
		        	static $counter = 100;
		        	while( have_rows('contents') ): the_row(); ?>
		        		<div class="panel panel-default">
						    <div class="panel-heading">
						    	<div class="container">
							      	<h4 class="panel-title">
								        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion<?php echo $panelCount; ?>" href="#big-collapse<?php echo $counter; ?>">
								          <?php the_sub_field('title'); ?>
                                          <span><i class="fa fa-chevron-down"></i></span>
								        </a>
                                        
                                        
							      	</h4>
						      	</div>
						    </div>
						    <div id="big-collapse<?php echo $counter; ?>" class="panel-collapse collapse">
						      <div class="panel-body">
						      	<div class="container">
							      	<div class="accList">
								        	<?php the_sub_field('description'); ?>
								        	<h4 class="panel-title">
											<a class=" collapsed" data-toggle="collapse" data-parent="#accordion<?php echo $panelCount; ?>" href="#big-collapse<?php echo $counter; ?>">
											<i class="fa  fa-chevron-up"></i>
											</a>
											</h4>

							        </div>
						        </div>
						      </div>
						    </div>
					  	</div>
		        	<?php $counter++; endwhile; $panelCount++;?>
	        </div>
	    </section>
		<?php endif;
	}






	private function ch_du_bo_get_simple_text_area(){
		$backgroundColor = get_sub_field('background_color');
		$bgColor = '#fff';
		if($backgroundColor == 'Grey'){
			$bgColor = '#f5f5f5';
		}
		?>
		<section class="section-simple-text" style="background-color:<?php echo $bgColor; ?>">
	    	<div class="container">
	    		<?php echo do_shortcode(get_sub_field('content')); ?>
	    	</div>
	    </section>
		<?php
	}



	private function ch_du_bo_get_tabs_section(){
		if(have_rows('tabs')): ?>
		<section class="section-tabs">
	    	<div class="container">
	        	<div class="row">
	            	<div>
					  <!-- Nav tabs -->
					  <ul class="nav nav-tabs" role="tablist">
					  	<?php
					  	$counter = 0;
					  	while( have_rows('tabs') ): the_row();
					  	$class = '';
					  	if($counter == 0){
					  		$class = 'active';
					  	}
					  	?>
						    <li role="presentation" class="<?php echo $class; ?>"><a href="#tab-<?php echo $counter; ?>" aria-controls="tab-<?php echo $counter; ?>" role="tab" data-toggle="tab"><?php the_sub_field('title'); ?></a></li>
						<?php
						$counter++;
						endwhile;?>
					  </ul>

					  <!-- Tab panes -->
					  <div class="tab-content">
					  	<?php
					  	$counter = 0;
					  	while( have_rows('tabs') ): the_row();
					  	$class = '';
					  	if($counter == 0){
					  		$class = 'active';
					  	}
					  	?>
						    <div role="tabpanel" class="tab-pane <?php echo $class; ?>" id="tab-<?php echo $counter; ?>"><?php the_sub_field('content'); ?></div>
						<?php
						$counter++;
						endwhile; ?>
					  </div>

					</div>
	            </div>
	         </div>
        </section>
		<?php
		endif;
	}
}
