<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Chaîne_du_Bonheur
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/png">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>
<?php 
	$logo = ch_du_bo_get_logo(CH_LANG_CODE,false); 
	$media_link = esc_url( get_theme_mod( 'media_link' ) );
	if(!$media_link){ $media_link = '#'; }
	$login_sao = esc_url( get_theme_mod( 'login_sao' ) );
	if(!$login_sao){ $login_sao = '#'; }
	$login_partnerweb = esc_url( get_theme_mod( 'login_partnerweb' ) );
	if(!$login_partnerweb){ $login_partnerweb = '#'; }
	$user = wp_get_current_user();
?>
<body <?php body_class(); ?>>
<header id="header">
	<nav class="navbar">
		<div class="container">
			<div class="row">
				<div class="col-md-3 navbar-header">
					<a class="navbar-brand open-here" href="javascript:void(0)" rel="home"><img src="<?php echo $logo; ?>" alt="<?php bloginfo(); ?>"></a>
				</div>
				
				<div class="col-md-1 navbar-right">
					<div class="top-bar clearfix hidden-xs">
						<div class="accRt">
							<ul class="account">
								
                                <?php if(is_user_logged_in()): ?>
									<li><a href="<?php echo wp_logout_url(); ?>"  class="logout" title="Logout"><span class="logout">logout</span><i class="fa fa-sign-out"></i></a></li>
								<?php endif; ?>
							</ul>
						</div>
					</div><!--/.top-bar-->					
				</div>
                
                <div class="col-md-8 navbar-right">					
					<div class="navbar-collapse collapse">
						<?php chaine_get_partner_menu($user); ?>
					</div>
				</div>  
                
                
                   
			</div>   
		</div><!--/.container-->
	</nav><!--/nav-->
</header>
<?php
if(!is_page_template('page-templates/cms-template.php') && !is_page_template('page-templates/login-template.php')):
	$backgroundImage = get_field('background_image','option');
	$title = get_field('title','option');
	$subTitle = get_field('subtitle','option');
	?>
	<section class="main-banner main-banner-media no-margin">
		   <div class="item" style="background:url(<?php echo $backgroundImage['url']; ?>) no-repeat center center">
			<div class="sliderCaption">
				<div class="container">
					<div class="row">
						 <h1><?php echo $title; ?></h1>
						 <?php if($subTitle != ''): ?>
							<h4><?php echo $subTitle; ?></h4>
						 <?php endif; ?>
					</div>
				</div>
			
			</div>		
		   
		   </div>
	</section><!--/.main-slider-->
<?php
endif;