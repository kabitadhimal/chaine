<?php
	/*
	Template Name: News
	*/
	get_header(); the_post();

	$banner_img = get_field('banner_image');
	$title_top = get_field('title_top');
	$title_bottom = get_field('title_bottom');
	$short_description = get_field('short_description');
	?>
	<section class="main-banner no-margin news-banner">           
       <div class="item" style="background:url(<?php echo $banner_img['url']; ?>) no-repeat center center">
            <div class="sliderCaption">
            	<div class="container">
	                <div class="row">
	                	<div class="col-md-4 pull-left">	
	                		<?php if($title_top != '' || $title_bottom != ''): ?>	
		                    	<h1><?php echo $title_top.' '.$title_bottom; ?></h1>
		                    <?php endif; ?>
	                      	<h4><?php echo $short_description; ?></h4>						
	                   </div>                	
	                </div>
                </div>            
            </div>		           
       </div>   
    </section><!--/.main-banner-->
    <section class="status_bar section-filter">
    	<div class="container">
        	<div class="row">
			  	<div class="col-md-2"><h4>Filtrer par</h4></div>          	
			 	<div class="pull-right">   
			 		<?php 
			 		$terms = get_terms( 'category', array(
					    'hide_empty' => false,
					    'parent'	 => 0
					) );
					if(!empty($terms)){
						foreach ($terms as $term) {
							?>
							<select class="filtersel" multiple="multiple" data-title="<?php echo $term->name; ?>">
							<?php $children = get_term_children( $term->term_id, 'category' ); ?>
							<?php if(!empty($children)): ?>
								<?php 
									foreach ( $children as $child ) {
										$term = get_term_by( 'id', $child, 'category' );
										echo '<option value="'.$term->term_id.'">'.$term->name . '</option>';
									}
								?>
							<?php endif; ?>
							</select>
							<?php
						}
					}
			 		?>      
			   	</div>             
	        </div>
        </div>  	
    </section><!--/.status bar-->
    <section class="section-news">
        <div class="container">
            <div class="row">
                
            
                <div class="news-listing">
                 <ul class="mobile-carousel">
                 	<?php
                 	// WP_Query arguments
					$args = array (
						'posts_per_page'         => '1',
						'meta_query'	=>	array(
							array(
								'key'     => 'hero',
								'value'   => 'Yes',
								'compare' => '!=',
							)
						)
					);

					// The Query
					$query = new WP_Query( $args );

					// The Loop
					if ( $query->have_posts() ) {
						while ( $query->have_posts() ) {
							$query->the_post();
							get_template_part('template-parts/content','news-post');
						}
					}

					// Restore original Post Data
					wp_reset_postdata();
                 	?>
                     
                     <?php
                     	// WP_Query arguments
						$args = array (
							'posts_per_page'         => '1',
							'meta_query'	=>	array(
								array(
									'key'     => 'hero',
									'value'   => 'Yes',
									'compare' => '=',
								)
							)
						);

						// The Query
						$query = new WP_Query( $args );

						// The Loop
						if ( $query->have_posts() ) {
							while ( $query->have_posts() ) {
								$query->the_post();
								get_template_part('template-parts/content','news-post');
							}
						}

						// Restore original Post Data
						wp_reset_postdata();
                     ?>
                    
                     <?php
                     	// WP_Query arguments
						$args = array (
							//'paged'                  => '1',
							'posts_per_page'         => '6',
							'offset'                 => '1',
							'meta_query'	=>	array(
								array(
									'key'     => 'hero',
									'value'   => 'Yes',
									'compare' => '!=',
								)
							)
						);

						// The Query
						$query = new WP_Query( $args );

						// The Loop
						if ( $query->have_posts() ) {
							while ( $query->have_posts() ) {
								$query->the_post();
								get_template_part('template-parts/content','news-post');
							}
						}

						// Restore original Post Data
						wp_reset_postdata();
                     ?>
                </ul>  
                 </div> <!--/.news-listing-->
                <div class="center newsAll hidden-xs">
                	<a class="btn rounded black more-news-btn" href="#"><?php _e("Plus d'actualités", 'ch_du_bo'); ?></a>
                	<p style="display:none" class="news-loader"><img src="<?php echo get_template_directory_uri().'/images/ajax-loader.gif'; ?>" alt="Loader"></p>
                </div>
                
                
            </div><!--/.row-->
        </div><!--/.container-->
        
   </section>
	<?php
	get_footer();
?>