<?php 
/*
	Template Name: Login
*/

$error = apply_filters('partner_login_head', '');
get_header(); the_post();
$username = isset($_POST['log']) ? $_POST['log'] : '';
?>   
	<section class="section-login">
		<div class="container">
        <div class="align-center">
        
        <h1>PARTNERWEB</h1>
		<?php if($error != ''): ?>
			<div id="login_error"><?php echo $error; ?></div>
		<?php endif; ?>
                <form name="loginform" id="loginform" action="<?php echo site_url('/login'); ?>" method="post">
				   <p class="login-username">
					  <!--<label for="user_login">Username or Email</label>-->
					  <input type="text" name="log" id="user_login" class="input" value="<?php echo $username; ?>" size="20" placeholder="Username or Email">
				   </p>
				   <p class="login-password">
					  <!--<label for="user_pass">Password</label>-->
					  <input type="password" name="pwd" id="user_pass" class="input" value="Password" size="20" placeholder="Password">
				   </p>
				   <p class="login-remember">
                       <span class="login-remember">
                       <input name="rememberme" type="checkbox" id="rememberme" value="forever" <?php echo isset($_POST['rememberme']) ? 'checked' : ''; ?>> <label for="rememberme">Remember Me</label>
                       </span>
                       <span class="forgot-pass">
                       <a href="#">forgot your password</a>
                       </span>
                   </p>
                   
                   
				   <p class="login-submit">
					  <input type="submit" name="wp-submit" id="wp-submit" class="button-primary" value="Submit">
					  <input type="hidden" name="redirect_to" value="<?php echo $_GET['redirect_to']; ?>">
				   </p>
				</form>
                <?php /* <a href="" class="forgot-pass"> Forgot your password</a>	 */ ?>
                
               <!-- <span class="wpcf7-list-item first last">
                <input type="checkbox" name="sendToMailchimp[]" value="Je m'inscris à la newsletter" id="c1">&nbsp;
                <label for="c1" class="wpcf7-list-item-label">Je m'inscris à la newsletter</label></span>-->
                
                	
		</div>
        </div>
	</section><!--/.section news-->
<?php
get_footer();