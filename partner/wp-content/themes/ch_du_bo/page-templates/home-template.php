<?php 
/*
	Template Name: Homepage
*/
get_header(); the_post();
$displayHero = true;
$hero = get_field('select_hero_post','option');
$user = wp_get_current_user();
if(!current_user_can( 'create_users', $user->ID )){
	$accesspages = get_the_author_meta( 'accesspages', $user->ID );
	if(in_array($hero->ID, $accesspages)){
		$displayHero = true;
	}else{
		$displayHero = false;
	}
}

$terms = wp_get_post_terms( $hero->ID, 'category' );
if(!empty($terms)){
	$catIcon = get_field('icon',"category_".$term->term_id);
	if(!$catIcon){
		$catIcon = get_template_directory_uri().'/images/btn-donate.png';
	}
}else{
	$catIcon = get_template_directory_uri().'/images/btn-donate.png';
}

wp_enqueue_script( 'ch_du_bo-masonary');
wp_enqueue_script( 'ch_du_bo-masonary2');


?>
    
    <section class="status_bar section-filter section-filter-mobile">
    	<div class="container">
        <div class="panel panel-default">
			<div class="xs-hidden panel-heading">
			  <h4 class="panel-title">
			  <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#filter" href="#cms-filter"><?php _e('Filtrer par','ch_du_bo'); ?></a>
			  </h4>
			</div>
            <div id="cms-filter" class="panel-collapse collapse" style="height: 0px;">
                <div class="panel-body">
                <div class="row">
                    <div class="col-md-2"><h4><?php _e('Voir aussi', 'ch_du_bo'); ?></h4></div>
                    <div class="col-md-10">
                    <?php 
			 		$terms = get_terms( 'category', array(
					    'hide_empty' => false,
					    'parent'	 => 0,
						'exclude'	=> array(1)
					) );
					if(!empty($terms)){
						$activeCat = NULL;
						if( isset($_GET['category']) ){
							$activeCat = $_GET['category'];
						}
						foreach ($terms as $term) {
							?>
							<select class="filtersel" multiple="multiple" data-title="<?php echo $term->name; ?>">
								<?php
								$args = array(
									'hide_empty' => false,
									'parent'	 => $term->term_id
								);
								$children = get_terms( 'category', $args );
								if(!empty($children)): ?>
									<?php
									foreach ( $children as $child ) {
										echo '<option value="'.$child->term_id.'" ';
										if($activeCat == $child->slug){
											echo "SELECTED ";
										}
										echo '>'.$child->name . '</option>';
									}
									?>
								<?php endif; ?>
							</select>
							<?php
						}
					}
			 		?>  
                    </div>
                </div>
                </div>
            </div>
        </div>

        </div>
    </section><!--/.status bar-->
    
    <section class="section-two-col">
        <div class="container">
            <div class="row">
          
                	<div class="col-md-8 col-sm-8">
                    	<div class="media-body">
						<?php if($displayHero): ?>
                        <div class="features center hero">
                         <figure class="center">
                         	<?php 
								if(has_post_thumbnail($hero->ID)){
									$img = wp_get_attachment_image( get_post_thumbnail_id( $hero->ID ), full );
								}else{
									$img = '<img src="'.get_template_directory_uri().'/images/img-col3-hero.jpg">';
								}
                         		
								
                         		echo '<a href="'.get_permalink( $hero->ID ).'">'.$img.'</a>';
                         	?>
                            <figcaption><a href="<?php echo get_permalink($hero->ID); ?>"><img src="<?php echo $catIcon; ?>" alt="#"></a></figcaption>
                        </figure>
                        <div class="content-wrap">
                            <span class="fundCollected center">
                            <?php 
                            	
                            	if(!empty($terms)){
                            		echo $terms[0]->name.' / ';
                            	}
                            ?>
                        	<?php the_time('d.m.Y'); ?>
                            </span>
                            <h3><?php echo $hero->post_title; ?></h3>
                            <?php echo ch_du_bo_truncate(strip_tags($hero->post_content),350); ?>

                          
                        </div>
                        
                        
                      
                        <div class="media-footer clearfix">
                    	<div class="col-md-12">
                        	<a class="btn rounded black" href="<?php the_permalink($hero->ID); ?>"><?php _e('En savoir plus','ch_du_bo'); ?></a>                        
                        </div>
                        
                        
                        <div class="col-md-12">
                        	<?php
                        		$telLink = get_field('telecharger_le_communique_link', $hero->ID);
                        		if( $telLink != ''):
                        	?>
	                            <a class="btn underline" href="<?php echo $telLink['url']; ?>"><?php _e('Télécharger le communiqué','ch_du_bo'); ?></a>
	                        <?php endif; ?>
                        </div>
                    </div>  <!--end of share-->
                      </div>  <!--end of features-->
					  <?php endif; ?>
                      <div class="slider-twitter hidden"> 
			             <figure><figcaption><a class="icon_tw" href="#"><i class="fa fa-twitter"></i></a></figcaption></figure>
			              <div class="secondary-slider" data-tw-id="<?php echo get_field('twitter_account_name'); ?>">
			              			              
			              </div>
			              </div>
                    	<div class="post-Listing">
                        	
                        <!-- <h3>Communiqués Associes</h3> -->
                        <?php
                        	// WP_Query arguments
							$args = array (
								'post_type'              => array( 'post' ),
								'posts_per_page'         => '4',
							);
							
							$args = apply_restriction_query($args);
							
							// The Query
							$query = new WP_Query( $args );

							// The Loop
							if ( $query->have_posts() ) {
								?>

                                <ul class="postList grid row"><?php
								while ( $query->have_posts() ) {
									$query->the_post();
									$terms = wp_get_post_terms( get_the_ID(), 'category' );
									get_template_part('template-parts/content','media-post');
								}
								?></ul><?php
							} 

							// Restore original Post Data
							wp_reset_postdata();
                        ?>
                        </div> 

                         <div class="center mediaAll">
                            <a class="btn rounded black more-media-btn" href="#"><?php _e("Plus d'actualités", 'ch_du_bo'); ?></a>
                            <p style="display:none" class="news-loader"><img src="<?php echo get_template_directory_uri().'/images/ajax-loader.gif'; ?>" alt="Loader"></p>
                        </div>                       
                    
                    </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                    
                    	<?php get_sidebar(); ?>
                    
                    </div>
                     </div><!--/.row-->                   
                
           
        </div><!--/.container-->
        
   </section><!--/.section news-->



<?php
get_footer();