<?php
/*
	Template Name: Contact
*/
	get_header(); the_post();

	$banner_img = get_field('banner_image');
	$title_top = get_field('title_top');
	$title_bottom = get_field('title_bottom');
	$short_description = get_field('short_description');
	?>
	<section class="main-banner no-margin news-banner">           
       <div class="item" style="background:url(<?php echo $banner_img['url']; ?>) no-repeat center center">
            <div class="sliderCaption">
            	<div class="container">
	                <div class="row">
	                	<div class="col-md-4 pull-left">	
	                		<?php if($title_top != '' || $title_bottom != ''): ?>	
		                    	<h1><?php echo $title_top.' '.$title_bottom; ?></h1>
		                    <?php endif; ?>
	                      	<h4><?php echo $short_description; ?></h4>						
	                   </div>                	
	                </div>
                </div>            
            </div>		           
       </div>   
    </section><!--/.main-banner-->
    <section class="status_bar contact-filter">
    	<div class="container">
        	<div class="row">
	 			<div class="formsubj"><h4><?php _e('Votre demande concerne','ch_du_bo'); ?> :</h4> 
	             	<div class="form-group form-control-sel">
	             		<select class="form-control" id="contact-subject">
	             			<option value=""><?php _e('Objet','ch_du_bo'); ?></option>
	             			<option value="aide.sociale@bonheur.ch" data-subject="<?php _e('Aide sociale','ch_du_bo'); ?>"><?php _e('Aide sociale','ch_du_bo'); ?></option>
	             			<option value="gschwendtner@bonheur.ch" data-subject="<?php _e('Attestation de don','ch_du_bo'); ?>"><?php _e('Attestation de don','ch_du_bo'); ?></option>
	             			<?php if(CH_LANG_CODE == 'fr' || CH_LANG_CODE == 'de'): ?>
		             			<option value="freiwillige@glueckskette.ch" data-subject="<?php _e('Bénévolat','ch_du_bo'); ?>"><?php _e('Bénévolat','ch_du_bo'); ?></option>
		             		<?php endif; ?>
	             			<option value="info@bonheur.ch" data-subject="<?php _e('Autre','ch_du_bo'); ?>"><?php _e('Autre','ch_du_bo'); ?></option>
	             		</select>
	             	</div>
	         	</div>              
            </div>
        </div>
    	
    </section><!--/.status bar-->
    
        
	<section class="section-contactform">
	    <div class="container">
	        <div class="row">
	            <div class="contact-form">
	            
	            <?php echo do_shortcode('[contact-form-7 id="480" title="Contact"]'); ?>
	            
	          </div>
	            
	    
	        </div>
	    </div>
		
	</section><!--/.contact form-->

	<section class="section-news contact-list">
        <div class="container">
            <div class="row">
                <div class="center title wow fadeInDown animated">
		            <h2><?php the_field('title'); ?></h2>          
		        </div>
            	<?php if( have_rows('places') ): ?>
                <div class="contact-listing">
                 <ul class="mobile-carousel">
                 <?php while ( have_rows('places') ) : the_row(); 
                 $address = get_sub_field('address');
                 $tel = get_sub_field('tel');
                 $fax = get_sub_field('fax');
                 $email = get_sub_field('email');
                 $email_text = get_sub_field('email_text');
                 ?>
                     <li> <div class="col-md-4 item wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    	<div class="features center">
                         <figure>
                        	<img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $address['lat'].','.$address['lng']; ?>&size=370x260&zoom=12&markers=color:red%7C<?php echo $address['lat'].','.$address['lng']; ?>" alt="image 1">
                            <figcaption><a class="icon_news" href="#" ><img src="<?php echo get_template_directory_uri(); ?>/images/icon-pin.png" alt="#"></a></figcaption>
                        </figure>
                        <div class="content-wrap">
                           <h4><?php the_sub_field('place_title'); ?></h4>
                           <address>
                           	<?php
                           	$addressName = str_replace(',','<br>',$address['address']);
                           	echo $addressName;
                           	?><br><br>
                           	<?php if($tel != ''): ?>
	                            Tél.: <?php echo $tel; ?><br>
	                        <?php endif; ?>
	                        <?php if($fax != ''): ?>
                            	Fax: <?php echo $fax; ?><br>
                           	<?php endif; ?>
                           </address>
                            <a class="btn underline open-here" href="mailto:<?php echo $email; ?>"><?php echo $email_text; ?></a>
                        </div>
                       
                        </div><!--/.fundraising-->
                    </div><!--/.col-md-4-->
                    </li> <!--/.col-md-4-->
                 <?php endwhile; ?>
                  
                    
                </ul>  
                 </div> <!--/.news-listing-->
             <?php endif; ?>
                
                
                
            </div><!--/.row-->
        </div><!--/.container-->
        
   </section>
<?php get_footer(); ?>