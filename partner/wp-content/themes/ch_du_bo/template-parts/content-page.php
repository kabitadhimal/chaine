<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Chaîne_du_Bonheur
 */

$ch_du_bo_content = new Ch_du_bo_Fund_Content();
if(is_emergency() && (is_home() || is_front_page())){
	$background_image =  get_field('background_image');
	$banner_title_small =  get_field('banner_title_small');
	$banner_title_big =  get_field('banner_title_big');
	$banner_link =  get_field('banner_link');
	$banner_link_text =  get_field('banner_link_text');
	$top_text =  get_field('top_text');
	$bottom_text =  get_field('bottom_text');
	$description =  get_field('description');
	$more_link =  get_field('more_link');
	$more_text =  get_field('more_text');
	$position = get_field('position');
	if($position == 'Left'){
		$class = 'content-left';
	}else if($position == 'Middle'){
		$class = 'pull-middle';
	}else if($position == 'Right'){
		$class = 'content-right';
	}
	$hide =  get_field('hide');
	?>
	<section class="main-banner no-margin">           
           <div class="item" style="background:url(<?php echo $background_image['url']; ?>) no-repeat center center">
            <div class="sliderCaption">
            	<div class="container">
                <div class="row <?php echo $class; ?>">
                		<?php if($banner_title_small): ?>
                    	 <h2><?php echo $banner_title_small; ?></h2>
                    	<?php endif; ?>
                    	<?php if($banner_title_big): ?>
                    	 <h1><?php echo $banner_title_big; ?></h1>
                    	 <?php endif; ?>
                      
						 <a class="btnDonate open-here" href="<?php echo $banner_link; ?>"><?php echo $banner_link_text; ?></a>  
						 <?php if($hide == 'No'): ?>
	                   		 <h4><?php echo $top_text; ?></h4>
	                   		 <?php $days = get_emergency_activated_days(); ?>
							 <?php if($days <= 10 && $bottom_text != ''): ?>
	                         	<span class="nopart"><?php echo $bottom_text; ?></span>
	                         <?php endif; ?>
                         <?php endif; ?>
                	
                </div></div>
            
            </div>		
           
           </div>
      <div class="itemCaption">
     
      <div class="container">
      	<?php echo $description; ?>
        <a href="<?php echo $more_link; ?>" class="btn underline"><?php echo $more_text; ?></a>
        </div>
       
      </div>
    </section><!--/.main-slider-->
	<?php
}
$content = $ch_du_bo_content->ch_du_bo_get_flexible_content();
if($content):
	echo $content;
else:
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'ch_du_bo' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'ch_du_bo' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->
<?php
endif;
?>
