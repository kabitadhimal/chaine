<?php
$socialLink = get_field('social_link');

$hero = get_field('hero');
if($hero == 'Yes'){
	$colClass = 8;
	$imgSize = 'hero-thumb';
	$charLimit = 250;
}else{
	$colClass = 4;
	$imgSize = 'fundraising-thumb';
	$charLimit = 150;
}

if($socialLink != ''){
	$socialMedia = get_field('social_media');
	if($socialMedia == 'Facebook'){
		$fb = new FB_Page_Manager();
		$data = $fb->get_post_by_url($socialLink);
		$data['created_time'] = date('d.m.Y',strtotime($data['created_time']));
		$data['src']	=	'Facebook';
		$class ="icon_fb";
	}else{
		$content = file_get_contents('https://api.instagram.com/oembed/?url='.$socialLink);
		$content =json_decode($content);
		$data = array(
			'full_picture'	=>	$socialLink.'media/?size=m',
			'link'	=>	$socialLink,
			'created_time'	=>	'',
			'message'	=>	$content->title,
			'id'	=>	'',
			'date'	=>	'',
			'name'	=>	'',
			'src'	=>	'Instagram',
		);
		$class="icon_news icon_insta";
	}
}else{
	$data['name'] = get_the_title();
	$data['link'] = get_permalink();
	if(has_post_thumbnail()){
		$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),$imgSize);
		$data['full_picture'] = $img[0];
	}else{
		$data['full_picture'] = get_template_directory_uri().'/images/img-col3-hero.jpg';
	}
	$data['created_time'] = get_the_time('d.m.Y');
	$data['message'] = get_the_excerpt();
	$data['id']	= get_the_ID();
	$data['src']	='WP';
	$class="icon_news";
}
?>
 
	
    	<div class="features center"><a  href="<?php echo $data['link']; ?>">
         <figure>
        	<?php if($data['full_picture']): ?>
            	<img src="<?php echo $data['full_picture']; ?>" alt="<?php echo $data['name']; ?>">
            <?php endif; ?>
        	<?php 
        		$icon = get_post_icon($data['id'],$data['src']);
        	?>
            <figcaption><span class="<?php echo $class; ?>" ><?php echo $icon; ?></span></figcaption>
        </figure>
        <div class="content-wrap">
             <span class="disasterdate"><?php echo $data['created_time']; ?></span>
            <h4><?php echo $data['name']; ?></h4>
            <?php echo ch_du_bo_truncate($data['message'],$charLimit); ?>
        </div></a>
       
        </div><!--/.fundraising-->
