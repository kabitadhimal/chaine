<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Chaîne_du_Bonheur
 */
$terms = wp_get_post_terms( get_the_ID(), 'category' );
if(!empty($terms)){
	$catIcon = z_taxonomy_image_url($terms[0]->term_id);
	if(!$catIcon){
		$catIcon = get_template_directory_uri().'/images/btn-donate.png';
	}
}else{
	$catIcon = get_template_directory_uri().'/images/btn-donate.png';
}

?>

<figure class="center">
	<?php 
 		$img = wp_get_attachment_image( get_post_thumbnail_id(  ), full );
		if(is_single()){
			echo $img;
		}else{
			echo '<a href="'.get_permalink(  ).'">'.$img.'</a>';
		}
 		
 	?>
    <figcaption><a href="<?php echo get_permalink(); ?>"><img src="<?php echo $catIcon; ?>" alt="#"></a></figcaption>
</figure>
<div class="content-wrap">
    <?php
        $donation = get_field('donation_amount');
        if($donation != ''):
    ?>
        <span class="fundCollected center"><?php _e('CHF','ch_du_bo'); ?> <?php echo $donation; ?> <?php _e('Dons collectés','ch_du_bo'); ?></span>
    <?php endif; ?>
    <h3><?php the_title(); ?></h3>
    <?php the_content(); ?>
</div>

<div class="media-footer clearfix">
	<div class="col-md-6">
        <?php
    		$telLink = get_field('telecharger_le_communique_link');
    		if( $telLink != ''):
    	?>
            <a class="btn underline" href="<?php echo $telLink['url']; ?>"><?php _e('Télécharger le communiqué','ch_du_bo'); ?></a>
        <?php endif; ?>
    </div>
    <div class="col-md-6 share_block">
        <a class="share pull-right" href="#"><?php _e('Partager','ch_du_bo'); ?></a>
        <div class="addthis_inline_share_toolbox"></div>
    </div>
</div>  <!--end of share-->