<?php
$terms = wp_get_post_terms( get_the_ID(), 'category' );
?> 
<li class="col-md-6 grid-item">
	<div class="features">
		<?php
			if(has_post_thumbnail()){
				$img = wp_get_attachment_image( get_post_thumbnail_id( get_the_ID() ), 'fundraising-thumb' );
			}else{
				$img = '<img src="'.get_template_directory_uri().'/images/img-col3.jpg">';
			}
     		echo "<figure>";
     		echo '<a href="'.get_permalink( get_the_ID() ).'">'.$img.'</a>';
     		echo "</figure>";
     	?>
        <div class="content-wrap">
            <span class="disasterdate">
            <?php
            	if(!empty($terms)){
            		echo $terms[0]->name.' / ';
            	}
            ?>
        	<?php the_time('d.m.Y'); ?>
            </span>
            <h4><?php the_title(); ?></h4>
            <p><?php echo ch_du_bo_truncate(strip_tags(get_the_content()),150); ?></p>
            <a class="btn rounded black" href="<?php the_permalink(); ?>"><?php _e('En savoir plus','ch_du_bo'); ?></a>
            <?php
        		$telLink = get_field('telecharger_le_communique_link', get_the_ID());
        		if( $telLink != ''):
        	?>
                <a class="btn underline" href="<?php echo $telLink['url']; ?>"><?php _e('Télécharger le communiqué','ch_du_bo'); ?></a>
            <?php endif; ?>
        </div>
    </div>
</li>

