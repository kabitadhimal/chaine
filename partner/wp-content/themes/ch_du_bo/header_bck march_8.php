<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Chaîne_du_Bonheur
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>
<?php 
	$logo = ch_du_bo_get_logo(CH_LANG_CODE,$emergency); 
	$media_link = esc_url( get_theme_mod( 'media_link' ) );
	if(!$media_link){ $media_link = '#'; }
	$login_sao = esc_url( get_theme_mod( 'login_sao' ) );
	if(!$login_sao){ $login_sao = '#'; }
	$login_partnerweb = esc_url( get_theme_mod( 'login_partnerweb' ) );
	if(!$login_partnerweb){ $login_partnerweb = '#'; }
?>
<body <?php body_class(); ?>>
<header id="header">
	<nav class="navbar">
		<div class="container">
			<div class="row">
				<div class="col-md-2-remove header-left">
					<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo $logo; ?>" alt="<?php bloginfo(); ?>"></a>
				</div>
				
				<div class="col-md-9-remove header-right">
					<div class="top-bar clearfix hidden-xs navbar-right">
						<div class="accRt">
							<ul class="lang">
								<li><a target="_blank" href="<?php echo $media_link; ?>"><?php _e('Médias','ch_du_bo'); ?></a></li>
							</ul>
							<ul class="account">
								<li>
									<div class="dropdown">
										<a class="dropdown-toggle acc" type="button" data-toggle="dropdown" href="#"><i class="fa fa-angle-down"></i></a>
										<ul class="dropdown-menu">
											<li><a href="<?php echo $login_sao; ?>"><?php _e('Social Assistance','ch_du_bo'); ?></a></li>


											<li><a target="_blank" href="<?php echo $login_partnerweb; ?>"><?php _e('Partnerweb','ch_du_bo'); ?></a></li>
										</ul>
									</div>
								</li>
								<li><a target="_blank" class="mail" href="<?php echo $contact; ?>"></a></li>
							</ul>
						</div>
					</div><!--/.top-bar-->
					<div class="navbar-collapse collapse">
						<nav>
							<?php
								wp_nav_menu( array(
									'theme_location' 	=>	'primary',
									'menu_id' 			=>	'primary-menu',
									'container' 		=>	'ul',
									'menu_class'		=>	'nav navbar-nav',
									'walker'			=>	new Primary_Nav_Walker()
							) );
							?>
						</nav>
					</div>
				</div>   
			</div>   
		</div><!--/.container-->
	</nav><!--/nav-->
</header>
<?php
if(!is_page_template('page-templates/cms-template.php')):
	$backgroundImage = get_field('background_image','option');
	$title = get_field('title','option');
	$subTitle = get_field('subtitle','option');
	?>
	<section class="main-banner main-banner-media no-margin">
		   <div class="item" style="background:url(<?php echo $backgroundImage['url']; ?>) no-repeat center center">
			<div class="sliderCaption">
				<div class="container">
					<div class="row">
						 <h1><?php echo $title; ?></h1>
						 <?php if($subTitle != ''): ?>
							<h4><?php echo $subTitle; ?></h4>
						 <?php endif; ?>
					</div>
				</div>
			
			</div>		
		   
		   </div>
	</section><!--/.main-slider-->
<?php
endif;