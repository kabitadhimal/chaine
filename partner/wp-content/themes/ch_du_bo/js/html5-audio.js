/**
 * Created by bobaminions on 10/21/2016.
 */

/**
 * Custom html5 audio player
 */

/* ============================================================= */
/*      Initializing Function/Methods before document load       */
/* ============================================================= */

(function() {
	var $ = jQuery.noConflict();

	// Init minionsAudioVideo
	minionsAudioVideo($('.js-audio'));

	// Function to build custom audio-video
	function minionsAudioVideo($media) {
		if($media.length === 0) return;
		$media.each(function() {
			var $self = $(this);
			$self.mediaelementplayer();
		});
	}
})(jQuery);