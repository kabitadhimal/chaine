jQuery(function($) {'use strict',	

	//Initiat WOW JS
	new WOW().init();

	
	$(window).scroll(function() {
    if ($(this).scrollTop() > 1){  
        $('#header').addClass("sticky");
    }
    else{
        $('#header').removeClass("sticky");
    }
});

	
$( document ).ready( function() {
	 if(jQuery('header .collapse nav').length > 0){
		jQuery('header .collapse nav').meanmenu({
			meanMenuClose: '<span></span><span></span><span></span>',
		});
	}
	  
});

var secFil = jQuery('.section-filter').length;
	if(secFil >= 1){
		jQuery('.fund-status').addClass('secFil-present');
}


//goto top
$('.gototop').click(function(event) {
	event.preventDefault();
	$('html, body').animate({
		scrollTop: $("body").offset().top
	}, 500);
});	
	
	

	
});

// Init placeholderHideSeek
placeholderHideSeek(jQuery('#loginform input'));

// Function to hide and show form input field placeholder values
function placeholderHideSeek(input) {
    if(jQuery(input).length === 0) return;
    input.each( function(){
        var meInput = jQuery(this);
        var placeHolder = jQuery(meInput).attr('placeholder');
        jQuery(meInput).focusin(function(){
            jQuery(meInput).attr('placeholder','');
        });
        jQuery(meInput).focusout(function(){
            jQuery(meInput).attr('placeholder', placeHolder);
        });
    });
}


jQuery('#mailchilpCheckbox input[type="checkbox"]').attr('id','c1');

jQuery(window).scroll(function() {
	if (jQuery(this).scrollTop() > 0){
		jQuery('.scroll-top').fadeIn();
	}
	else{
		   jQuery('.scroll-top').fadeOut();
	}
});

jQuery(function(){
jQuery("a[href='#top']").click(function() {
	jQuery("html, body").animate({ scrollTop: 0 }, "slow");
	return false;
});

});




jQuery(window).load(function() {
	jQuery( '.slider-pro' ).sliderPro({
		width: 300,
		height:700,
		visibleSize: '100%',
		forceSize: 'fullWidth',
		autoSlideSize: true,
		arrows: true,
		buttons: false,
		breakpoints: {
			760: {
				width: 270,
				height: 300,
			}
		}
	});

	// instantiate fancybox when a link is clicked
	jQuery( '.slider-pro').each(function() {
		var self = jQuery(this);
		jQuery( '.sp-image', self).parent( 'a' ).on( 'click', function( event ) {
			event.preventDefault();
			event.stopImmediatePropagation();
			
			

			// check if the clicked link is also used in swiping the slider
			// by checking if the link has the 'sp-swiping' class attached.
			// if the slider is not being swiped, open the lightbox programmatically,
			// at the correct index
			
			self.sliderPro( 'autoplay', false );
			
			if ( self.hasClass( 'sp-swiping' ) === false ) {
				jQuery.fancybox.open( 
					jQuery( '.sp-image', self).parent( 'a' ), 
					{ 
						index: jQuery( this ).parents( '.sp-slide' ).index(),
						afterClose : function() {
							self.sliderPro( 'autoplay', true );
						},
						helpers: {
							overlay: {
								locked: false
							}
						}
					}
				);
			}
		});	
	});
	
	
});