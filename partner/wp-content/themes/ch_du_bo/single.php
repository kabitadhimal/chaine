<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Chaîne_du_Bonheur
 */

get_header(); 

$back = isset($_SERVER['HTTP_REFERRER']) ? $_SERVER['HTTP_REFERRER'] : site_url();

?>
	
    <section class="section-backlink">
        <div class="container">
           <div class="btnback">
           		<i class="fa fa-chevron-left"></i><a href="<?php echo $back; ?>"><?php _e("Retour à l’espace media","ch_du_bo"); ?></a>
           </div>
      	</div>
  	</section>
	<section class="section-two-col">
        <div class="container">
            <div class="row">          
            	<div class="col-md-8">
                	<div class="media-body">

		<?php
		while ( have_posts() ) : the_post();

			
			?>
			<div class="features">
				<?php get_template_part( 'template-parts/content', get_post_format() ); ?>
          	</div>
          	<?php
			//the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			

		endwhile; // End of the loop.

    $terms = wp_get_post_terms( get_the_ID(), 'category' );
    $childTerms = array();
    if(!empty($terms)){
      foreach($terms as $term){
        if($term->parent != 0){
          $childTerms[] = $term->term_id;
        }
      }
    }

    if(!empty($childTerms)){
      $args = array(
          'post_type' => array('post'),
          'category__in'  => $childTerms,
          'posts_per_page'  => 2,
          'post__not_in'  => array(get_the_ID()),
          'orderby'       => 'rand'
      );
	  $args = apply_restriction_query($args);
      $query = new WP_Query($args);

      if($query->have_posts()): ?>
        <div class="post-Listing">                          
          <h3>Communiqués Associes</h3>            
          <ul class="postList row">
            <?php while($query->have_posts()): $query->the_post(); 
            get_template_part('template-parts/content','media-post');
            endwhile; ?>
          </ul>
        </div>
        <?php
      endif;
    }
		?>
					</div>
				</div>
				<div class="col-md-4">
          <?php get_sidebar(); ?>                    
        </div>
			</div>
		</div>
	</section>

      <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5819b0844233f71a"></script> 
<?php
get_footer();
