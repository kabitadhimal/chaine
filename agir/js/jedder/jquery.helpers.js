/*
** Add class to body once window is completely loaded
*/

var $win = $(window),
	$pageRoot = $('html'),
	$doc = $(document),
	$page = $('body'),
	winResize = null,
	winResizeDelay = 10;
	
$win.on('load', function() {
	$page.addClass('page-loaded');
});

/*
** Add class to body once DOM is ready
*/

$(function() {
	$page.addClass('content-loaded');    
});

/* Build lightbox */
;(function($) {
	/* Placeholder behavior */
	placeholderEffect($('.form-control'));

	function placeholderEffect(input) {
	    if($(input).length === 0) return;
	    input.each( function(){
	        var meInput = $(this);
	        var placeHolder = $(meInput).attr('placeholder');
	        $(meInput).focusin(function(){
	            $(meInput).attr('placeholder','');
	        });
	        $(meInput).focusout(function(){
	            $(meInput).attr('placeholder', placeHolder);
	        });
	    });
	}

	/*
	** Wow plugin initialization
	*/
	wow = new WOW({
		boxClass:     'wow',      // default
		animateClass: 'animated', // default
		offset:       0,          // default
		mobile:       false,       // default
		live:         true        // default
	});
	wow.init();

	// Script to scroll to the bottom of the page on click on chat box
	$('#chat-box-open').on('click', function(e) {
	  $('html, body').animate({
	    scrollTop: $(document).height() - $(window).height()
	  }, {
	      duration: 500,
	      easing: 'easeInOutExpo'
	  });
	  e.preventDefault();
	});

	// Init activateResizeHandler()
	activateResizeHandler();

	// Add heloper class on DOM element on window resize
	function activateResizeHandler() {	    
	    var resizeClass = 'resize-active',
	    flag, timer;

	    var removeClassHandler = function() {
	      flag = false;
	      $pageRoot.removeClass(resizeClass);

	    };

	    var resizeHandler = function() {
	      if(!flag) {
	        flag = true;
	        if(!$page.hasClass('page-template-news') && !$page.hasClass('page-template-press')) {
	        	$pageRoot.addClass(resizeClass);
	        }
	      }
	      clearTimeout(timer);
	      timer = setTimeout(removeClassHandler, 500);
	    };
	    $win.on('resize orientationchange', resizeHandler);
  	};

  	// Contact form 7 
  	// Remove span with label to match design\  	
  	$('.wpcf7-list-item-label').each(function() {
  		$(this).prev().attr('id', 'js-checbox-' + ($(this).closest('.main').find('.wpcf7-list-item-label').index($(this))) * 1);
  		$(this).replaceWith("<label for="+ 'js-checbox-' + $(this).closest('.main').find('.wpcf7-list-item-label').index($(this)) * 1 +" class="+ $(this).attr('class') +">" + $(this).text() + "</label>");
  	});  	
	
})(jQuery);	