/**
 * Simple js collapsible UI
 */

/* ============================================================= */
/*     Initializing Function/Methods before page/window load     */
/* ============================================================= */

// https://stackoverflow.com/questions/12661797/jquery-click-anywhere-in-the-page-except-on-1-div

;(function($) {
    // Caching DOM elements into varaibles
    var $doc = $('body'),
        $pageRoot = $('html, body'),
        $target = $('.js-accordion__toggler'),
        $blockNavigator = $('.block-navigator');

    // Initialize simpleCollapsibleUI
    simpleCollapsibleUI($($target));

    // Caching DOM elements into varaibles
    var $header = $('#masthead'),
        headerHeight,
        timer = null,
        timerDelay = 380;

    // Storing different value of header height depending upon screen resolution
    var stickyHeightParams = {
        xs: 58,
        sm: 70,
        xmd: 72,
        md: 85,
        lg: 96,
    };

    var finalStickyHeightParams = {
        xs: 60,
        sm: 61,
        md: 98,
        lg: 110 //~ 150
    };

    if($blockNavigator.length === 0) {
        headerHeight = $header.outerHeight(true);

    }else {
        headerHeight = $header.outerHeight(true) + finalStickyHeightParams.lg;
    }    

    // Update height of header in window resize 
    $(window).on('resize', function() {
        if (timer) clearTimeout(timer);
        timer = setTimeout(function() {
            //headerHeight = $header.outerHeight(true);
            var _thisWidth = $doc.prop('clientWidth'); 

            if($blockNavigator.length === 0) {
                if(_thisWidth > 1440) {
                    headerHeight = stickyHeightParams.lg;
                }else if(_thisWidth <= 1440 && _thisWidth >= 1024) {
                    headerHeight = stickyHeightParams.md;
                }else if(_thisWidth < 1024 && _thisWidth > 767) {
                    headerHeight = stickyHeightParams.sm;
                }else if(_thisWidth <= 767 && _thisWidth > 480) {
                    headerHeight = stickyHeightParams.xmd;
                }else if(_thisWidth <= 480) {
                    headerHeight = stickyHeightParams.xs;
                }
            }else {
                if(_thisWidth > 1440) {
                    headerHeight = stickyHeightParams.lg + finalStickyHeightParams.lg;
                }else if(_thisWidth <= 1440 && _thisWidth >= 1024) {
                    headerHeight = stickyHeightParams.md + finalStickyHeightParams.md;
                }else if(_thisWidth < 1024 && _thisWidth > 767) {
                    headerHeight = stickyHeightParams.sm + finalStickyHeightParams.sm;
                }else if(_thisWidth <= 767 && _thisWidth > 480) {
                    headerHeight = stickyHeightParams.xmd + finalStickyHeightParams.xs;
                }else if(_thisWidth <= 480) {
                    headerHeight = stickyHeightParams.xs + finalStickyHeightParams.xs;
                }
            }           
        }, timerDelay);
    }).trigger('resize');    

	// Function to create simple collapsible ui
    function simpleCollapsibleUI($item) {
        if($item.length === 0) return;    
        
        $item.each(function() {             
            var $self = $(this),
                dataScrollActive = ((($self.closest('.js-accordion').attr('data-scroll-active')) !== undefined || ($self.closest('.js-accordion').attr('data-scroll-active')) !== '') && ($self.closest('.js-accordion').attr('data-scroll-active')) === 'true') ? true : false;

            // Prepare accordion before initialization
            $('.js-accordion__panel:not(.active)').find('.js-accordion__panel-collapse').hide();
            $('.js-accordion__panel.active').find('.js-accordion__panel-collapse').css('display', 'block');

            //Attch click event
            $self.on('click', function(e) {
                var $target, $selfParent;

                $target = $self.closest('.js-accordion__panel').find($('.' + $self.attr('data-toggle-target')));
                $selfParent = $self.closest('.js-accordion__panel');

                // Reset all other panels
                $self.closest('.js-accordion').find('.js-accordion__toggler').not($self).removeClass('expanded');
                $self.closest('.js-accordion').find('.js-accordion__panel-collapse').not($target).slideUp({
                    //duration: 500,
                    //easing: 'easeInOutExpo'
                });
                $self.closest('.js-accordion').find('.js-accordion__panel.active').not($selfParent).removeClass('active');                

                // Expand current panel
                $self.toggleClass('expanded');
                $target.stop(true, true).slideToggle({
                    //duration: 700,
                    //easing: 'easeInOutExpo'
                });
                $selfParent.toggleClass('active');

                // Move page to the top offset of currently opened panel
                if($self.hasClass('expanded') && dataScrollActive === true) {
                    setTimeout(function() {
                        $pageRoot.animate({
                            //scrollTop: $selfParent.offset().top - (headerHeight) + 1
                        }, {
                            duration: 1200,
                            easing: 'easeInOutExpo'
                        });
                    }, 400);                    
                }
                
                // Prevent default behavior of anchor tag
                e.preventDefault();
            });
        });        
    }
})(jQuery);