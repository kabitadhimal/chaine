;(function($) {
	// Caching variables
	var $win = $(window),
		$page = $('html, body'),
		winScrollTop, footerHeight,
		winHeight = $win.height(),
		footerHeight = $('#colophon').outerHeight();

	// Init back to top function
	scrollPageTop($('.back-top-trigger'), $('.back-top-wrap'));
	
	// Build a function to scroll at the top of the page
	function scrollPageTop($elem, $parent) {
	    if($elem.length === 0) return;
	    $win.on('scroll', function() {
	        	var pageTop = $win.scrollTop(),
	        		pageBottom = $(document).height() - $(this).height() - $(this).scrollTop();

	        if (pageTop <= 150 || pageBottom <= 50) {
	        	$elem.closest($parent).removeClass('animate');
	        }else {
	        	$elem.closest($parent).addClass('animate');
	        }
	    });

	    //Attach click event on current object
        $elem.on('click', function(e) {
            $page.animate({scrollTop: 0}, {duration: 700,easing: 'easeInOutExpo'});
            // Prevent the default behavior of the anchor tag
            e.preventDefault();
        });
	}
})(jQuery);