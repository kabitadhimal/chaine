/* ============================================================= */
/*        Initializing Function/Methods on document load         */
/* ============================================================= */

//Parse video host name and vide id from the given url
function parseVideoURL(url) {
    function getParm(url, base) {
        var re = new RegExp("(\\?|&)" + base + "\\=([^&]*)(&|$)");
        var matches = url.match(re);
        if (matches) {
            return(matches[2]);
        } else {
            return("");
        }
    }
    var retVal = {};
    var matches;
    if (url.indexOf("youtube.com/watch") != -1) {
        retVal.provider = "youtube";
        retVal.id = getParm(url, "v");
    } else if (matches = url.match(/vimeo.com\/(\d+)/)) {
        retVal.provider = "vimeo";
        retVal.id = matches[1];
    }else if(matches = url.split('?')[0].split("/")) {
        retVal.provider = "vimeo";
        retVal.id = matches[matches.length - 1];
    }
    return(retVal);
}

;(function() {
    // Init video player
    videoPlayer($('.js-video-play'));

    //Function to play youtube video on inline place
    function videoPlayer($elem) {
        if($elem.length === 0) return;
        $elem.each(function() {
            var $self = $(this),
                $selfParent = $self.closest('.promo-video'),
                $bkgHolder = $selfParent.find('.promo-video-preload-wrap'),
                $iframeWrapper = $selfParent.find('.promo-video__holder'),
                $targetIframe = $selfParent.find('iframe'),
                selfDataUrl = $self.attr('data-video-src'),                
                youtubeMainString = 'https://www.youtube.com/embed/',
                youTubeVideoPlayParams = '?modestbranding=1&rel=0&showinfo=0&autoplay=1&autohide=1&controls=1&loop=1',
                vimeoMainString = 'https://player.vimeo.com/video/',
                vimeoVideoPlayParams = 'autoplay=1&fullscreen=1&title=0&byline=0;';

            // Caching values extracted via parseVideourl function
            var videoSource = parseVideoURL(selfDataUrl),
                videoProvider= videoSource.provider,
                loadedVideoId = videoSource.id;
            //console.log(loadedVideoId)
            $self.on('click', function(e) {
                //Hide video banner image and play button
                $iframeWrapper.addClass('video-playing');
                $bkgHolder.hide();
                if( videoProvider == 'youtube' ) {
                    //Assign youtube url to the iframe on modal
                    $targetIframe.attr({'src' : youtubeMainString + loadedVideoId + '?' + youTubeVideoPlayParams + '&playlist=' + loadedVideoId + ''});
                    //Display current youtube url on //console
                    console.log("<=======" + ' ' + videoProvider + ' ' + 'video is playing!!!' + ' ' + "=======>" );
                }else if( videoProvider == 'vimeo' ) {
                    //Assign youtube url to the iframe on modal
                    $targetIframe.attr({'src' : vimeoMainString + loadedVideoId + '?' + vimeoVideoPlayParams});
                    //Display current vimeo url on //console
                    console.log("<=======" + ' ' + videoProvider + ' ' + 'video is playing!!!' + ' ' + "=======>" );
                }
                e.preventDefault();
            });
        });
    }
})(jQuery);