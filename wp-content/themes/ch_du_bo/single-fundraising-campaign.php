<?php
get_header(); the_post();
	$banner_img = get_field('banner_image');
	if($banner_img){
		$img = $banner_img['sizes']['slider-img'];
	}else{
		$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'slider-img');
		if($img){
			$blurClass = 'featured-banner';
			$img = $img[0];
		}else{
			$img = get_template_directory_uri().'/images/cms-img-landscape.jpg';
		}
	}
	$displayBanner = false;
	$title_top = get_field('title_top');
	if($title_top != ""){ $displayBanner = true; }
	$title_bottom = get_field('title_bottom');
	if($title_bottom != ""){ $displayBanner = true; }
	$short_description = get_field('short_description');
	if($short_description != ""){ $displayBanner = true; }
	if($img && $displayBanner):
		if($blurClass == 'featured-banner'):
	?>
			<section class="main-banner no-margin featured-banner">
		       <div class="item" style="background:url(<?php echo $img; ?>) no-repeat center center"></div>
	            <div class="sliderCaption">
	            	<div class="container">
		                <div class="row">
		                	<div class="col-md-8 pull-left">
		                		<?php if($title_top != '' || $title_bottom != ''): ?>
			                    	<h1><?php echo $title_top.' '.$title_bottom; ?></h1>
			                    <?php endif; ?>
		                      	<h4><?php echo $short_description; ?></h4>
		                   </div>
		                </div>
	                </div>
	            </div>
		    </section><!--/.main-banner-->
		<?php else: ?>
			<section class="main-banner no-margin">
		       <div class="item" style="background:url(<?php echo $img; ?>) no-repeat center center">
		            <div class="sliderCaption">
		            	<div class="container">
			                <div class="row">
			                	<div class="col-md-8 pull-left">
			                		<?php if($title_top != '' || $title_bottom != ''): ?>
				                    	<h1><?php echo $title_top.' '.$title_bottom; ?></h1>
				                    <?php endif; ?>
			                      	<h4><?php echo $short_description; ?></h4>
			                   </div>
			                </div>
		                </div>
		            </div>
		       </div>
		    </section><!--/.main-banner-->
		<?php endif;?>
<?php
endif;
$ch_du_bo_content = new Ch_du_bo_Fund_Content();
$content = $ch_du_bo_content->ch_du_bo_get_flexible_content();
echo $content;
get_footer();
