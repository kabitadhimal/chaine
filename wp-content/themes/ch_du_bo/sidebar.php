<aside class="widget-area" role="complementary">
                        
                        	
                                
                            <section  class="widget widget_search">
                            	<h4 class="widget-title">Archives</h4>
                            	<form  class="search-form" action="#">
                              	  	<select class="select" onchange="document.location.href=this.options[this.selectedIndex].value;">
                                  		<?php wp_get_archives( array( 'type' => 'yearly', 'format'	=> 'option' ) ); ?>
                                  	</select>
                                </form>

                            </section>   
                                <?php
                                $args = array(
									'posts_per_page'	=> 5
								);
								$query = new WP_Query($args);
								if($query->have_posts()):
                                ?>
                                <section  class="widget widget_archive">
                                  <h4 class="widget-title">Articles recents</h4>
                                  
                                  	<ul>
                                  		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
	                                    	<li>
	                                        	<a href="<?php echo get_permalink(); ?>"><span class="date"><?php echo get_the_time('d.m.Y'); ?></span>
												<p><?php echo get_the_title(); ?></p>
	                                            </a>
	                                        </li>

                                    	<?php endwhile; wp_reset_postdata(); ?>
                                    
                                    </ul>
                                  
                                
                                </section> 
                                <?php 
									endif;
								?>
                                
                                
                                 	<?php
                                    $tagCloud = wp_tag_cloud( 'smallest=16&largest=16&format=list&link=edit&echo=0' ); 
                                    if($tagCloud){
                                      ?>
                                        <section  class="widget widget_tags">
                                          <h4 class="widget-title">Tags</h4>
                                      <?php echo $tagCloud; ?>
                                        </section>
                                    <?php } ?>
                                
                                <section  class="widget widget_social"> 
                                <h4 class="widget-title">Suivez-nous</h4>
                                <ul class="social-share">
                                <li class="fb"><a href="<?php echo get_theme_mod('ch_du_bo_fb_link_'.CH_LANG_CODE); ?>"><i class="fa fa-facebook"></i></a></li>
                                <li class="tw"><a href="<?php echo get_theme_mod('ch_du_bo_tw_link_'.CH_LANG_CODE); ?>"><i class="fa fa-twitter"></i></a></li>
                                <li class="insta"><a href="<?php echo get_theme_mod('ch_du_bo_insta_link_'.CH_LANG_CODE); ?>"><i class="fa fa-instagram"></i></a></li>
                                <li class="youtube"><a href="<?php echo get_theme_mod('ch_du_bo_youtube_link_'.CH_LANG_CODE); ?>"><i class="fa fa-youtube-square"></i></a></li>
                                <li class="pint"><a href="<?php echo get_theme_mod('ch_du_bo_pin_link_'.CH_LANG_CODE); ?>"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                                
                                </section>
                        </aside>