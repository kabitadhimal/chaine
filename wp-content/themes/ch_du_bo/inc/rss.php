<?php
/* Add the feed. */
function chaine_rss_init(){
	add_feed('a_la_une', 'chaine_rss_a_la_une');

	global $wp_rewrite;
$wp_rewrite->flush_rules();
}
add_action('init', 'chaine_rss_init');

/* Filter the type, this hook wil set the correct HTTP header for Content-type. */
function my_custom_rss_content_type( $content_type, $type ) {
	if ( 'a_la_une' === $type ) {
		return feed_content_type( 'rss2' );
	}
	return $content_type;
}
add_filter( 'feed_content_type', 'my_custom_rss_content_type', 10, 2 );

/* Show the RSS Feed on domain.com/?feed=a_la_une or domain.com/feed/a_la_une. */
function chaine_rss_a_la_une() {
	// Do your magic, with XML for example.
	get_template_part('template-parts/rss', 'a-la-une');
}
