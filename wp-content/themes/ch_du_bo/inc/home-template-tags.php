<?php
class Ch_du_bo_Content{
	public function ch_du_bo_get_flexible_content(){
		// check if the flexible content field has rows of data
		if( have_rows('flexible_content') ):

			ob_start();

			// loop through the rows of data
			while ( have_rows('flexible_content') ) : the_row();

				if( get_row_layout() == 'slider_block' ):

					$this->ch_du_bo_get_slider();

				elseif( get_row_layout() == 'fundraising_campaign_block' ):

					$this->ch_du_bo_get_fundraising_campaign_block();

				elseif( get_row_layout() == 'video_block' ):

					$this->ch_du_bo_get_video_block();

				elseif( get_row_layout() == 'simple_block' ):

					$this->ch_du_bo_get_simple_block();

				elseif( get_row_layout() == 'how_it_works' ):

					$this->ch_du_bo_get_how_it_works();

				elseif( get_row_layout() == 'news_section' ):

					$this->ch_du_bo_get_news_section();

				elseif( get_row_layout() == 'html_free_area_section' ):

					$this->ch_du_bo_get_html_free_area_section();

				elseif( get_row_layout() == 'swiss_solidarity_section' ):

					$this->ch_du_bo_get_swiss_solidarity_section();

				endif;

			endwhile;

			$content = ob_get_contents();
			ob_end_clean();
			return $content;

		else:

			return false;

		endif;
	}



	private function ch_du_bo_get_slider(){
        $hideEmergency = get_sub_field('hide_during_emergency');
       if(!empty($hideEmergency)) {
            if(is_emergency()){ return; }
        };
		if( have_rows('slider') ):
		?>
		<section class="main-slider no-margin"  role="banner">
			<?php while ( have_rows('slider') ) : the_row();
				$imageID = get_sub_field('image');
				$image = wp_get_attachment_image_src($imageID,'slider-img');
				$title = get_sub_field('title');
				$content = get_sub_field('content');
				$link_text = get_sub_field('link_text');
				$url = get_sub_field('link') ? get_sub_field('link') : '#';
				$link_open_in_new_tab = get_sub_field('link_open_in_new_tab');
				$link_color = get_sub_field('link_color');
				$link_text_2 = get_sub_field('link_text_2');
				$url_2 = get_sub_field('link_2') ? get_sub_field('link_2') : '#';
				$link_open_in_new_tab_2 = get_sub_field('link_open_in_new_tab_2');
				$link_color_2 = get_sub_field('link_color_2');
				$copyright_text = get_sub_field('copyright_text');
				$aClass = '';
				if($link_open_in_new_tab == 'No'){
					$aClass = 'open-here';
				}
				$aClass_2 = '';
				if($link_open_in_new_tab_2 == 'No'){
					$aClass_2 = 'open-here';
				}
				$position = get_sub_field('position');
				if($position == 'Left'){
					$class = 'content-left';
				}else if($position == 'Middle'){
					$class = 'pull-middle';
				}else if($position == 'Right'){
					$class = 'content-right';
				}
				?>
				<div class="item" style="background:url(<?php echo $image[0]; ?>) no-repeat center center">
		            <div class="sliderCaption">
		            	<div class="container">
			                <div class="row">
			                	<div class="col-xs-12 slider_thumb_col">
			                		<figure class="slider_thumb">
			                			<img src="<?php echo $image[0]; ?>" />
			                			<?php if($copyright_text != ''): ?>
											<figcaption class="copyright"><?php echo $copyright_text; ?></figcaption>
										<?php endif; ?>
			                		</figure>
			                	</div>
			                	<div class="col-md-6 <?php echo $class; ?>">
			                		<?php if($title): ?>
				                    	<span class="coutry"><?php echo do_shortcode($title); ?></span>
				                    <?php endif; ?>
			                    	<?php echo do_shortcode($content); ?>
									<?php if($link_text != '' && $url != '#'): ?>
										<a href="<?php echo $url; ?>" class="btn rounded <?php echo $link_color.' '.$aClass; ?>"><?php echo $link_text; ?></a>
									<?php endif; ?>
									<?php if($link_text_2 != '' && $url_2 != '#'): ?>
										<a href="<?php echo $url_2; ?>" class="btn rounded <?php echo $link_color_2.' '.$aClass_2; ?>"><?php echo $link_text_2; ?></a>
									<?php endif; ?>

			                    </div>
			                </div>
		                </div>
		            </div>
					<?php if($copyright_text != ''): ?>
						<div class="copyright hidden-xs"><?php echo $copyright_text; ?></div>
					<?php endif; ?>
	           	</div>
           	<?php endwhile; ?>
		</section>
		<?php
		endif;
	}





	private function ch_du_bo_get_fundraising_campaign_block(){
        $hideEmergency = get_sub_field('hide_during_emergency');
        if(!empty($hideEmergency)) {
            if(is_emergency()){ return; }
        };
		$fundIds = get_sub_field('fund_raising_posts');
		$query = new WP_Query( array( 'post_type' => 'fundraising-campaign', 'post__in' => $fundIds, 'orderby'	=>	'post__in' ) );
		if ( $query->have_posts() ) {
			$fundTitle = get_sub_field('fund_raising_title');
			?>
			<section class="section-threeColumn section-fundraising">
    			<div class="container">
    				<div class="row">
    				<?php if($fundTitle !=''): ?>
	    				<div class="center title wow fadeInDown animated hidden-xs">
				            <h3><?php echo $fundTitle; ?></h3>
				        </div>
				    <?php endif; ?>
            			<ul class="mobile-carousel">
			<?php
			while ( $query->have_posts() ) {
				$query->the_post();
				$donationID = get_field('funds_received');
				?>
				<li>
					<div class="col-md-4 col-sm-4 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    	<div class="features center asd">
                         <figure>
                         	<?php
								if(has_post_thumbnail()){
                         			$img = wp_get_attachment_image(get_post_thumbnail_id(get_the_ID()),'fundraising-thumb');
								}else{
									$img = '<img src="'.get_template_directory_uri().'/images/no-image.jpg" alt="image 1">';
								}
							?>
                        	<?php echo $img; ?>
                            <figcaption><span><img class="overlay-img" src="<?php echo get_template_directory_uri(); ?>/images/btn-donate.svg" alt="#">
								<a href="<?php the_field('iraiser_website_link'); ?>"><span class="hover-button"><?php _e('Faire un don', 'ch_du_bo'); ?></span></a>
								</span></figcaption>
                        </figure>
                        <div class="content-wrap">
							<?php if($donationID != ''): ?>
								 <?php $donationData = ch_du_bo_get_donation_data($donationID); ?>
								 <span class="fundCollected"><?php echo __('Dons collectés','ch_du_bo').': '.$donationData['donation'].'&nbsp;'.$donationData['devise']; ?></span>
							<?php endif; ?>
                            <h3><?php the_title(); ?></h3>
                            <p><?php echo ch_du_bo_truncate(get_the_excerpt(),150); ?></p>
                            <a href="<?php the_field('iraiser_website_link'); ?>" class="btn underline"><?php _e('Faire un don','ch_du_bo'); ?></a>
                        </div>
                         <div class="overlay"><a href="<?php the_field('iraiser_website_link'); ?>"></a></div>
                        </div><!--/.fundraising-->
                    </div><!--/.col-md-4-->
                </li>
				<?php
			}
			/* Restore original Post Data */
			wp_reset_postdata();
			?>
							</ul>
						</div>
					</div>
				</section>
			<?php
		} else {
			// no posts found
		}
	}

	private function ch_du_bo_get_video_block(){
        $hideEmergency = get_sub_field('hide_during_emergency');
        if(!empty($hideEmergency)) {
            if(is_emergency()){ return; }
        };
		$imageID = get_sub_field('image');
		$img = wp_get_attachment_image_src($imageID,'full');
		?>
		<section class="section-video" style="background:url(<?php echo $img[0]; ?>) no-repeat center center">
	        <div class="container">
	            <div class="center wow fadeInDown">
                    <h3><?php the_sub_field('video_title_top'); ?></h3>
                    <h2><?php the_sub_field('video_title_bottom'); ?></h2>

                   <div class="embed-responsive embed-responsive-16by9">

                   <a  class="play_btn video" href="<?php the_sub_field('video_link'); ?>?fs=1&amp;autoplay=1"><span><?php _e('Voir la vidéo','ch_du_bo'); ?></span></a>

                   </div>
	            </div>
	        </div><!--/.container-->
	    </section><!--/.section videos-->
		<?php
	}

	private function ch_du_bo_get_how_it_works(){
        $hideEmergency = get_sub_field('hide_during_emergency');
        if(!empty($hideEmergency)) {
            if(is_emergency()){ return; }
        };
		if( have_rows('blocks') ): ?>
			<section class="section-howworks">
            	<div class="center wow fadeInDown">
          			<div class="container">
            			<div class="row">
			<?php while ( have_rows('blocks') ) : the_row();
			$icon = get_sub_field('icon');
			?>
				<a href="<?php the_sub_field('link'); ?>" class="">
				<div class="col-md-6 col-xs-6">
                    <div class="howworks-content">
                       <h3><?php echo do_shortcode(get_sub_field('title')); ?></h3>
                       <p><?php echo do_shortcode(get_sub_field('description')); ?></p>
					   <?php
							if($icon):
								$icons = glob(ABSPATH.'wp-content/uploads/ch/Picto_'.$icon.".*");
								if(!empty($icons)):
									$icon = str_replace(ABSPATH, site_url('/'), $icons[0]);
									?><figure><img src="<?php echo $icon; ?>" /></figure><?php
								endif;
							endif;
						?>
                       <span href="<?php the_sub_field('link'); ?>" class="btn underline"><?php the_sub_field('link_text'); ?></span>
                       </div>
               </div>
               </a>
			<?php endwhile; ?>
						</div>
					</div>
				</div>
			</section>
			<?php
		endif;
	}

	private function ch_du_bo_get_news_section(){
        $hideEmergency = get_sub_field('hide_during_emergency');
        if(!empty($hideEmergency)) {
            if(is_emergency()){ return; }
        };
		switch (CH_LANG_CODE) {
			case 'fr':
				$fbPageId = 'chainedubonheur';
				$catID = 376;
				break;
			case 'en':
				$fbPageId = 'swisssolidarity';
				$catID = 377;
				break;
			case 'it':
				$fbPageId = 'catenadellasolidarieta';
				$catID = 379;
				break;
			default:
				$fbPageId = 'glueckskette';
				$catID = 378;
				break;
		}
		/* $fbMan = new FB_Page_Manager($fbPageId);
		$fbPosts = $fbMan->get_latest_posts(2); */

		// WP_Query arguments
		$args = array (
			'post_type'              => array( 'post' ),
			'posts_per_page'         => '3',
			//'category__in'	    	 =>	array($catID)
		);

		// The Query
		$query = new WP_Query( $args );
		$fbPosts = array();
		// The Loop
		if ( $query->have_posts() ) {
			$i = 0;
			while ( $query->have_posts() ) {
				$query->the_post();
				$fbPosts[$i]['name'] = get_the_title();
				$fbPosts[$i]['link'] = get_permalink();
				$fbPosts[$i]['src']	= 'self';
				$fbPosts[$i]['currentObjectClass'] = 'open-here';
				$fbPosts[$i]['currentTarget'] = '';
				$socialLink = get_field('social_link');
				if($socialLink != ''){
					$fbPosts[$i]['src']	= 'social';
					$fbPosts[$i]['link'] = $socialLink;
					$fbPosts[$i]['name'] = '';
					$fbPosts[$i]['currentObjectClass'] = 'open-new';
				    $fbPosts[$i]['currentTarget'] = '_blank';
				}

				$openNewWindow = get_field('open_in_new_window');
				$newWindowLink = get_field('new_window_link');
				if($openNewWindow && $openNewWindow[0] == 'Yes'){
				    $fbPosts[$i]['link'] = $newWindowLink;
				    $fbPosts[$i]['currentObjectClass'] = 'open-new';
				    $fbPosts[$i]['currentTarget'] = '_blank';
				}

				if(has_post_thumbnail()){
					$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'fundraising-thumb');
					$fbPosts[$i]['full_picture'] = $img[0];
				}else{
					$fbPosts[$i]['full_picture'] = get_template_directory_uri().'/images/img-col3.jpg';
				}
				$fbPosts[$i]['created_time'] = get_the_time(DATE_ISO8601);
				$fbPosts[$i]['message'] = nl2br(get_the_excerpt());
				$fbPosts[$i]['id']	= get_the_ID();
				$fbPosts[$i]['date']	= get_the_time('d.m.Y');
				$i++;
			}
			wp_reset_postdata();
		}


		// WP_Query arguments
		/* $args = array (
			'post_type'              => array( 'post' ),
			'posts_per_page'         => '1',
			'tax_query' => array(
				array(
					'taxonomy' => 'category',
					'field'    => 'term_id',
					'terms'    => array( $catID ),
					'operator' => 'NOT IN',
				),
			),
		);
		//debug($args);
		// The Query
		$query = new WP_Query( $args );
		$wpPosts = array();
		// The Loop
		if ( $query->have_posts() ) {
			$i = 0;
			while ( $query->have_posts() ) {
				$query->the_post();
				global $post;
				//debug(get_the_category(), true);
				$wpPosts[$i]['name'] = get_the_title();
				$wpPosts[$i]['link'] = get_permalink();
				if(has_post_thumbnail()){
					$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
					$wpPosts[$i]['full_picture'] = $img[0];
				}else{
					$wpPosts[$i]['full_picture'] = get_template_directory_uri().'/images/img-col3.jpg';
				}
				$wpPosts[$i]['created_time'] = get_the_time(DATE_ISO8601);
				$wpPosts[$i]['message'] = get_the_excerpt();
				$wpPosts[$i]['id']	= get_the_ID();
				$wpPosts[$i]['src']	= 'self';
				$wpPosts[$i]['date']	= get_the_time('d.m.Y');
				$i++;
			}
			wp_reset_postdata();
		} */

		/* $allPosts = array();
		$allPosts = array_merge($fbPosts,$wpPosts); */
		$allPosts = $fbPosts;

		usort($allPosts, 'sortByCreatedTime');
/* if($_GET['karun']){
	debug($allPosts);
} */
		if(!empty($allPosts)){
			$count = 0;
			?>
			<section class="section-news">
		        <div class="container">
		            <div class="row">
			            <div class="center title">
				            <h3><?php the_sub_field('title'); ?></h3>
				        </div>
		                <div class="news-listing">
			                <ul class="mobile-carousel news_carousel">
			                <?php foreach($allPosts as $post):
							$iconDisp = '';
			                if($post['src'] == 'social'){
			                	$term = 'facebook';
			                	$iconClass = 'icon_fb';
			                	$img = $post['full_picture'];
								$icons = glob(ABSPATH.'wp-content/uploads/ch/category-'.$term.".*");
								if(empty($icons)){
									$icon = get_field('icon',"category_".$term);
									if($icon){
										$iconDisp = '<img src="'.$icon.'" />';
									}
								}else{
									$icon = str_replace(ABSPATH, site_url('/'), $icons[0]);
									$iconDisp = '<img src="'.$icon.'" />';
								}
			                }else{
			                	$terms = wp_get_post_terms($post['id'],'category');
								foreach($terms as $term){
									/* if($term->parent == 0){
										continue;
									} */
									$icons = glob(ABSPATH.'wp-content/uploads/ch/category-'.$term->slug.".*");
									if(empty($icons)){
										$icon = get_field('icon',"category_".$term->term_id);
										if($icon){
											$iconDisp = '<img src="'.$icon.'" />';
										}
									}else{
										$icon = str_replace(ABSPATH, site_url('/'), $icons[0]);
										$iconDisp = '<img src="'.$icon.'" />';
									}

									break;
								}
								if($iconDisp == ""){
									$iconDisp = '<img src="'.site_url('/').'wp-content/themes/ch_du_bo/images/icon-news.png">';
								}
			                	$iconClass = 'icon_news';
			                	if($post['full_picture'] != NULL):
			                		$img = ch_du_bo_img_resize($post['full_picture'],360,253);
			                	endif;
			                	//$img = $img[0];
			                }
			                ?>
			                	<li>
			                	<div class="col-md-4 col-sm-4 col-xs-12">
			                		<div class="feature_block item">
				                    	<div class="features center qwe">
				                    	<a href="<?php echo $post['link']; ?>" class="<?php echo $post['currentObjectClass']; ?>" target="<?php echo $post['currentTarget']; ?>">
				                         	<figure>
				                         	<div class="image_crop">
				                         		<?php if($img): ?>
						                        	<img src="<?php echo $img; ?>" alt="<?php echo $post['name']; ?>">
						                        <?php endif; ?>
						                        </div>
					                            <figcaption><span class="<?php echo $iconClass; ?>"><?php echo $iconDisp; ?></span></figcaption>
				                        	</figure>
				                        	<div class="content-wrap">
				                        		<span class="disasterdate"><?php echo $post['date']; ?></span>
				                        		<?php
				                        		if($post['name']){
				                        			echo "<h4>".$post['name']."</h4>";
				                        		}
				                        		?>
				                            	<p><?php echo ch_du_bo_truncate($post['message'],150); ?></p>
				                        	</div>
				                        	</a>
				                        </div><!--/.fundraising-->
			                    	</div><!--/.col-md-4-->
			                    </div>
			                    </li>
		                	<?php
		                		if($count == 2){
		                			break;
		                		}
		                		$count++; endforeach; ?>
			                </ul>
		               	</div>
		                <div class="center newsAll">
		                	<a class="btn border" href="<?php echo get_news_page_link(); ?>"><?php the_sub_field('plus_dactualites_text'); ?></a>
		                </div>
	            	</div>
        		</div>
        	</section>

			<?php
		}

		// Restore original Post Data
		wp_reset_postdata();
	}

	private function ch_du_bo_get_simple_block(){
        $hideEmergency = get_sub_field('hide_during_emergency');
        if(!empty($hideEmergency)) {
            if(is_emergency()){ return; }
        };
		$title = get_sub_field('title');
		$blocks = get_sub_field('blocks');
		$total = count($blocks);
		if($total == 2){
			$col = '6';
		}else{
			$col = '4';
		}
		if( have_rows('blocks') ):
			?>
			<section class="section-htmlfree">
				<div class="container">
					<div class="row">
						<div class="center title hidden-xs wow fadeInDown animated">
							<h3><?php echo $title; ?></h3>
						</div>
						<div class="wow fadeInDown">
							<?php while ( have_rows('blocks') ) : the_row();
								$background_image = get_sub_field('background_image');
								$mobile_background_color = get_sub_field('mobile_background_color');
								$title = get_sub_field('title');
								$link = get_sub_field('link_url');
								$link_title = get_sub_field('link_title');
								$link_open_in_new_tab = get_sub_field('link_open_in_new_tab');
								$link_class = '';
								if($link_open_in_new_tab == 'No'){
									$link_class = 'open-here';
								}
								$video_url = get_sub_field('video_url');
								$videoClass = '';
								if($video_url){
									$link = $video_url;
									$videoClass = 'video';
								}
							?>
							<div class="col-md-<?php echo $col; ?>">
								<figure>
									<div class="hidden-xs">
										<a href="<?php echo $link; ?>" class="<?php echo $videoClass; ?>"><img src="<?php echo $background_image; ?>" alt=""></a>
									</div>
									<?php if($link != ''): ?>
									<figcaption>
										<?php if($title != ''){ ?>
											<h2><?php echo $title; ?></h2>
										<?php } ?>
										<?php if($video_url){ ?>
											<a class="btn underline hidden-xs itemblk video" href="<?php echo $link; ?>">
												<span class="video"></span>
											</a>
										<?php } ?>
										<a class="btn underline itemred <?php echo $link_class.' '.$videoClass; ?>" href="<?php echo $link; ?>" style="background:<?php echo $mobile_background_color; ?>"><span><?php echo $link_title; ?></span></a>
									</figcaption>
									<?php endif;?>
								</figure>
							</div>
							<?php endwhile; ?>
						</div>
					</div>
				</div>
			</section>
			<?php
		endif;
	}

	private function ch_du_bo_get_html_free_area_section(){
        $hideEmergency = get_sub_field('hide_during_emergency');
        if(!empty($hideEmergency)) {
            if(is_emergency()){ return; }
        };
		$total = (int)get_sub_field('columns');

		$col_1_bg_color = get_sub_field('col_1_mobile_background_color');
		if($col_1_bg_color == ''){
			$col_1_bg_color = '#e5222f';
		}
		$col_2_bg_color = get_sub_field('col_2_mobile_background_color');
		if($col_2_bg_color == ''){
			$col_2_bg_color = '#3a3a3b';
		}
		$col_3_bg_color = get_sub_field('col_3_mobile_background_color');
		if($col_3_bg_color == ''){
			$col_3_bg_color = '#3a3a3b';
		}

		$col_1_title = get_sub_field('col_1_title');
		$col_1_link_url = get_sub_field('col_1_link_url');
		$col_1_open_new_tab = get_sub_field('col_1_link_open_in_new_tab');
		$col_1_class = '';
		if($col_1_open_new_tab == 'No'){
			$col_1_class = 'open-here';
		}

		$col_2_link_url = get_sub_field('col_2_link_url');
		$col_2_open_new_tab = get_sub_field('col_2_link_open_in_new_tab');
		$col_2_class = '';
		if($col_2_open_new_tab == 'No'){
			$col_2_class = 'open-here';
		}
		if($total == 2){
			$col = '6';
		}else{
			$col = '4';
		}
		?>
		<section class="section-htmlfree">
	        <div class="container">
	            <div class="row">
		            <div class="center title hidden-xs wow fadeInDown animated">
			            <h3><?php the_sub_field('title'); ?></h3>
			        </div>
		        	<div class="wow fadeInDown">
                     	<div class="col-md-4">

                     		<figure>
                            	<div class="hidden-xs"><a href="<?php echo $col_1_link_url; ?>"><img href="<?php echo $col_1_link_url; ?>" src="<?php the_sub_field('col_1_bg_image'); ?>" alt=""></a></div>
                            	<?php if($col_1_link_url != ''): ?>
	                           	<figcaption>
	                           		<?php if($col_1_title != ''){ ?>
										<h2><?php echo $col_1_title; ?></h2>
									<?php } ?>
	                            	<a class="btn underline itemred <?php echo $col_1_class; ?>" href="<?php echo $col_1_link_url; ?>" style="background:<?php echo $col_1_bg_color; ?>"><span><?php the_sub_field('col_1_link_title'); ?></span></a>
	                           	</figcaption>
	                            <?php endif;?>
                            </figure>
                     	</div>
                 	 	<div class="col-md-4">

                     		<figure>
                            	<div class="hidden-xs"><a href="<?php echo $col_2_link_url; ?>" ><img src="<?php the_sub_field('col_2_bg_image'); ?>" alt=""></a></div>
                            	<figcaption>
                            		<a class="btn underline itemblk <?php echo $col_2_class; ?>" href="<?php echo $col_2_link_url; ?>" style="background:<?php echo $col_2_bg_color; ?>"><span><?php the_sub_field('col_2_link_title'); ?></span></a></figcaption>
                            </figure>

                     	</div>
                     	<?php if($total == 3){ ?>
                     	<div class="col-md-4">

                     		<figure>
                            	<div class="hidden-xs"><a href="<?php the_sub_field('col_3_video_url'); ?>" class="video"><img src="<?php the_sub_field('col_3_bg_image'); ?>" alt=""></a></div>
                            	<figcaption>
                            		<a class="btn underline hidden-xs itemblk video" href="<?php the_sub_field('col_3_video_url'); ?>">
                                	<span class="video"></span></a>

                                	<a class="btn underline itemblk video" href="<?php the_sub_field('col_3_video_url'); ?>" style="background:<?php echo $col_3_bg_color; ?>">
                                	<span><?php the_sub_field('col_3_title'); ?></span></a>

                                </figcaption>
                            </figure>
                     	</div>
                     	<?php } ?>
	                </div><!--/.-->
				</div>
			</div>
		</section>
		<?php
	}

	private function ch_du_bo_get_swiss_solidarity_section(){
        $hideEmergency = get_sub_field('hide_during_emergency');
        if(!empty($hideEmergency)) {
            if(is_emergency()){ return; }
        };
		$bgImage = get_sub_field('background_image');
		$h3Title = get_sub_field('title_top_small');
		?>
		<section class="section-solidarity" style="background:url(<?php echo $bgImage['url']; ?>) no-repeat center center">
	        <div class="container">

	            <div class="center wow fadeInDown">
	            	<?php if($h3Title != ''): ?>
		            	<h3><?php echo $h3Title; ?></h3>
		            <?php endif; ?>
	                <h2><?php the_sub_field('title_bottom_big'); ?></h2>

	            </div>
	            <?php

				$totalRow = count(get_sub_field('factsheet_parts'));
	            if( have_rows('factsheet_parts') ): ?>
	            	<div class="partners">
		               	<div class="row">
		                	<ul>
		                	<?php while ( have_rows('factsheet_parts') ) : the_row();
		                		$smallText = get_sub_field('small_text');
								$icon = get_sub_field('icon');
		                	?>
		                		<li class="col-md-<?php echo 12/$totalRow; ?> col-xs-<?php echo 12/$totalRow; ?>">
									<?php
										if($icon):
											$icons = glob(ABSPATH.'wp-content/uploads/ch/Picto_'.$icon.".*");
											if(!empty($icons)):
												$icon = str_replace(ABSPATH, site_url('/'), $icons[0]);
												?><img src="<?php echo $icon; ?>" /><?php
											endif;
										endif;
									?>
									<h4><?php the_sub_field('value'); ?></h4>
		                		<?php if($smallText != ''): ?>
			                		<p><?php echo $smallText ?></p>
			                	<?php endif; ?>
		                		</li>
		                	<?php endwhile; ?>
		                	</ul>
		                </div>
		            </div>
	            <?php endif; ?>
	        </div><!--/.container-->
	    </section><!--/.swiss solidarity-->
		<?php
	}
}
