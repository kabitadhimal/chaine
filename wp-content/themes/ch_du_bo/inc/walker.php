<?php 
//Custom Walker Class
class Primary_Nav_Walker extends Walker_Nav_Menu{

    private $rightSubMenu = false;

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $wp_query;
       // print_r($item);
        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
 
        // Depth-dependent classes.
        $depth_classes = array(
            ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
            ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
            ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
            'menu-item-depth-' . $depth
        );        

        $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

 
        // Passed classes.
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $aClass = '';
        $attributes = '';
        if(in_array('menu-item-has-children',$item->classes)){
        	$classes[] = 'dropdown menu-large';
        	$aClass = ' dropdown-toggle ';
        	$attributes .= ' data-toggle="dropdown"';
        	$item->url = '#';
        }
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
 
        // Build HTML.
        if($depth == 0){
	        $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';
	    }
        $args->link_before='';
        static $parent = '';
        static $count = 0;
        if($parent != $item->menu_item_parent){
        	$parent = $item->menu_item_parent;
        	$count = 0;
        }
		$args->before = '';
		$args->after = '';
        // If it's 1st level child
 		if($depth == 1){ 		
 			// Manage the block menu with image for the 1st 3 menu item	
	        if($count < 3){
	        	$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
	        	ob_start(); ?>
	        	<div class="col-sm-6 col-md-3">
					<figure>
						<a href="<?php echo $item->url; ?>" <?php echo $attributes; ?>>
							<?php 
								// Use the nav menu image, if nav menu image is not available, use the featured image
								$thumb = get_the_post_thumbnail( $item->ID, 'megamenu-img' );
								if($thumb){
									echo $thumb;
								}else{
									$thumb = get_the_post_thumbnail( $item->object_id, 'megamenu-img' );
									if($thumb == ""){
										$thumb = '<img src="'.get_template_directory_uri().'/images/default-menu.jpg" alt="no image"/>';
									}
									echo $thumb;
								}
							?>
							<figcaption>
								<h3><?php echo apply_filters( 'ch_the_title', $item->title, $item->ID ); ?></h3>
								<p><?php echo $item->description; ?></p>
							</figcaption>
						</a>
					</figure>
				</div>
				<?php 
				$output .= ob_get_contents();
				ob_end_clean();
	        	$count++;
	        	// we don't need the right menu untill the count is less than 3
	        	$this->rightSubMenu = false;
	        	return;
	        }
	        $args->after = '';
	        // setting menu for the right section of the mega menu
	        if($count == 3){
	        	$output .= '<div class="col-sm-6 col-md-3"><div class="subMenu new_menu">';
	        	//$output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';
	        	// flag for the right menu
	        	$this->rightSubMenu = true; 
	        	$args->after = '<li>';
	        	$count++;
	        }
	        if($count == 4){
	        	$args->before ='<span id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';
	        	$args->after = '</span>';
	        }
 		}

        // Link attributes.
        $attributes .= ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ' class="menu-link '. $aClass . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';
 
        // Build HTML output and pass through the proper filter.
        $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
            $args->before,
            $attributes,
            $args->link_before,
             $item->title, //apply_filters( 'the_title',, $item->ID ),
            $args->link_after,
            $args->after
        );
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"dropdown-menu megamenu row sub-menu\">\n";
		$output .= "\n<li>\n<div class='row'>\n<div class=container>\n";
	}

	public function end_el( &$output, $item, $depth = 0, $args = array() ) {
		if($depth == 0){
			$output .= "</li>\n";
		}
	}

	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		if($this->rightSubMenu){
			$output .= "$indent</div>\n</div>\n";
		}
		$output .= "$indent</div>\n</div>\n</li>\n";
		$output .= "$indent</ul>\n";
	}
}


class Custom_Walker extends Walker_Nav_Menu{
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;
		if(in_array('menu-item-has-children', $classes)){
			 $classes[] = 'dropdown';
		}
		/**
		 * Filters the arguments for a single nav menu item.
		 *
		 * @since 4.4.0
		 *
		 * @param array  $args  An array of arguments.
		 * @param object $item  Menu item data object.
		 * @param int    $depth Depth of menu item. Used for padding.
		 */
		$args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

		/**
		 * Filters the CSS class(es) applied to a menu item's list item element.
		 *
		 * @since 3.0.0
		 * @since 4.1.0 The `$depth` parameter was added.
		 *
		 * @param array  $classes The CSS classes that are applied to the menu item's `<li>` element.
		 * @param object $item    The current menu item.
		 * @param array  $args    An array of wp_nav_menu() arguments.
		 * @param int    $depth   Depth of menu item. Used for padding.
		 */
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		/**
		 * Filters the ID applied to a menu item's list item element.
		 *
		 * @since 3.0.1
		 * @since 4.1.0 The `$depth` parameter was added.
		 *
		 * @param string $menu_id The ID that is applied to the menu item's `<li>` element.
		 * @param object $item    The current menu item.
		 * @param array  $args    An array of wp_nav_menu() arguments.
		 * @param int    $depth   Depth of menu item. Used for padding.
		 */
		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $class_names .'>';

		$atts = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
		$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
		$atts['href']   = ! empty( $item->url )        ? $item->url        : '';
		if(in_array('menu-item-has-children', $classes)){
			$atts['class']   = 'dropdown-toggle';
			$atts['data-toggle']   = 'dropdown';
			$atts['role']   = 'button';
			$atts['aria-haspopup']   = 'true';
			$atts['aria-expanded']   = 'false';
			$args->link_after = '<i class="fa fa-angle-down"></i>';
		}else{
			$args->link_after = '';
		}
		
		/**
		 * Filters the HTML attributes applied to a menu item's anchor element.
		 *
		 * @since 3.6.0
		 * @since 4.1.0 The `$depth` parameter was added.
		 *
		 * @param array $atts {
		 *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
		 *
		 *     @type string $title  Title attribute.
		 *     @type string $target Target attribute.
		 *     @type string $rel    The rel attribute.
		 *     @type string $href   The href attribute.
		 * }
		 * @param object $item  The current menu item.
		 * @param array  $args  An array of wp_nav_menu() arguments.
		 * @param int    $depth Depth of menu item. Used for padding.
		 */
		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		/** This filter is documented in wp-includes/post-template.php */
		$title = $item->title;

		/**
		 * Filters a menu item's title.
		 *
		 * @since 4.4.0
		 *
		 * @param string $title The menu item's title.
		 * @param object $item  The current menu item.
		 * @param array  $args  An array of wp_nav_menu() arguments.
		 * @param int    $depth Depth of menu item. Used for padding.
		 */
		$title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . $title . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		/**
		 * Filters a menu item's starting output.
		 *
		 * The menu item's starting output only includes `$args->before`, the opening `<a>`,
		 * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
		 * no filter for modifying the opening and closing `<li>` for a menu item.
		 *
		 * @since 3.0.0
		 *
		 * @param string $item_output The menu item's starting HTML output.
		 * @param object $item        Menu item data object.
		 * @param int    $depth       Depth of menu item. Used for padding.
		 * @param array  $args        An array of wp_nav_menu() arguments.
		 */
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"dropdown-menu\">\n";
	}
}