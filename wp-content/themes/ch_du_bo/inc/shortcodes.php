<?php
class Ch_Du_Bo_Shortcode {
	public static function icon( $atts, $content = "" ) {
		$atts = shortcode_atts( array(
			'icon' => 'facebook',
			'link' => '#'
		), $atts, 'ch_icon' );

		ob_start();
		?>
		<div class="social-link"><a href="<?php echo $atts['link']; ?>"><i class="fa fa-<?php echo $atts['icon']; ?>"></i></a></div>
		<?php
		$content = ob_get_contents();
		ob_end_clean();
		return $content;
	}

	public static function download( $atts, $content = "" ) {
		$atts = shortcode_atts( array(
			'link' => '#',
			'title' => __("Télécharger d'autres documents",'ch_du_bo')
		), $atts, 'ch_download' );

		ob_start();
		?>
		<a href="<?php echo $atts['link']; ?>" class="download" download><?php echo $atts['title']; ?></a><?php
		$content = ob_get_contents();
		ob_end_clean();
		return $content;
	}

	public static function audio_player($atts){
		$atts = shortcode_atts( array(
			'link' => '',
		), $atts, 'ch_audio_player' );

		ob_start();
		?>
		<div class="ipro-col__content"> 
          
			<!-- Custom html5 audio player -->
			<div class="ipro-audio__wrap js-audio-wrap">
				<audio controls preload="auto" class="ipro-audio__elem js-audio"> 
					<!-- <source src="audio/minions-clip.ogg" type="audio/ogg" /> -->
					<source src="<?php echo $atts['link']; ?>" type="audio/mp3">
				</audio>
			</div>
			<!-- /.#Custom html5 audio player -->
        </div>
		<?php
		$content = ob_get_contents();
		ob_end_clean();
		return $content;
	}


	public static function empty_area($atts){
		$atts = shortcode_atts( array(
			'space' => '10px',
		), $atts, 'ch_empty_area' );
		ob_start();
		?>
		<div style="margin: <?php echo $atts['space']; ?> 0;"></div>
		<?php
		$content = ob_get_contents();
		ob_end_clean();
		return $content;
	}


	public static function button($atts){
		$atts = shortcode_atts( array(
			'text' => 'Button',
			'link' => '#',
			'type' => 'black',
		), $atts, 'ch_button' );
		ob_start();
		?>
		<a class="btn <?php echo $atts['type']; ?>" href="<?php echo $atts['link']; ?>"><?php echo $atts['text']; ?></a>
		<?php
		$content = ob_get_contents();
		ob_end_clean();
		return $content;
	}
 }
 add_shortcode( 'ch_icon', array( 'Ch_Du_Bo_Shortcode', 'icon' ) );
 add_shortcode( 'ch_audio_player', array( 'Ch_Du_Bo_Shortcode', 'audio_player' ) );
 add_shortcode( 'ch_download', array( 'Ch_Du_Bo_Shortcode', 'download' ) );
 add_shortcode( 'ch_empty_area', array( 'Ch_Du_Bo_Shortcode', 'empty_area' ) );
 add_shortcode( 'ch_button', array( 'Ch_Du_Bo_Shortcode', 'button' ) );