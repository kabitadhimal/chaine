<?php
	$wp_customize->add_panel( 'ch_du_bo_social_setting_panel',
	    array(
	        'title'          => 'Footer Setting',
	        'description'    => 'Social Links, Partners logo for '.get_bloginfo().'',
	        'capability'     => 'manage_options',
	        'theme-supports' => '',
	        'priority'       => 11,
	    )
	);
	$wp_customize->add_section( 'ch_du_do_social_title_footer_section',
	    array(
	        'title'          => 'Social Title Footer',
	        'capability'     => 'manage_options',
	        'theme-supports' => '',
	        'priority'       => 12,
	        'panel'          => 'ch_du_bo_social_setting_panel',
	    )
	);

	foreach($langs as $lang){
		$wp_customize->add_setting('ch_du_do_social_title_footer_'.$lang['language_code'],array(
				'type'	=>	'theme_mod',
				'default'	=>	'',
				'capability'	=>	'manage_options',
				'transport'	=>	'refresh',
				'sanitize_callback'	=>	'sanitize_text_field'
			)
		);
		$wp_customize->add_control( new WP_Customize_Control(
		        $wp_customize,
		        'ch_du_do_social_title_footer_'.$lang['language_code'],
		        array(
		            'label'    => 'Title ('.strtoupper($lang['language_code']).')',
		            'section'  => 'ch_du_do_social_title_footer_section',
		            'settings' => 'ch_du_do_social_title_footer_'.$lang['language_code'],
		            'priority' => $priority,
		            'type'	=>	'text'
		        )
		) );
	}

	$priority = 10;
	foreach($langs as $lang){
		$wp_customize->add_section( 'ch_du_bo_section_social_'.$lang['language_code'],
		    array(
		        'title'          => 'Social Links for '.strtoupper($lang['language_code']),
		        'description'    => 'Social Links for '.strtoupper($lang['language_code']),
		        'capability'     => 'manage_options',
		        'theme-supports' => '',
		        'priority'       => 12,
		        'panel'          => 'ch_du_bo_social_setting_panel',
		    )
		);

		$wp_customize->add_setting('ch_du_bo_fb_link_'.$lang['language_code'],array(
				'type'	=>	'theme_mod',
				'default'	=>	'',
				'capability'	=>	'manage_options',
				'transport'	=>	'refresh',
				'sanitize_callback'	=>	'esc_url'
			)
		);
		$wp_customize->add_setting('ch_du_bo_tw_link_'.$lang['language_code'],array(
				'type'	=>	'theme_mod',
				'default'	=>	'',
				'capability'	=>	'manage_options',
				'transport'	=>	'refresh',
				'sanitize_callback'	=>	'esc_url'
			)
		);
		$wp_customize->add_setting('ch_du_bo_insta_link_'.$lang['language_code'],array(
				'type'	=>	'theme_mod',
				'default'	=>	'',
				'capability'	=>	'manage_options',
				'transport'	=>	'refresh',
				'sanitize_callback'	=>	'esc_url'
			)
		);
		$wp_customize->add_setting('ch_du_bo_youtube_link_'.$lang['language_code'],array(
				'type'	=>	'theme_mod',
				'default'	=>	'',
				'capability'	=>	'manage_options',
				'transport'	=>	'refresh',
				'sanitize_callback'	=>	'esc_url'
			)
		);
		$wp_customize->add_setting('ch_du_bo_lnkd_link_'.$lang['language_code'],array(
				'type'	=>	'theme_mod',
				'default'	=>	'',
				'capability'	=>	'manage_options',
				'transport'	=>	'refresh',
				'sanitize_callback'	=>	'esc_url'
			)
		);

		$wp_customize->add_control( new WP_Customize_Control(
		        $wp_customize,
		        'ch_du_bo_fb_link_'.$lang['language_code'],
		        array(
		            'label'    => 'Facebook Link ('.strtoupper($lang['language_code']).')',
		            'section'  => 'ch_du_bo_section_social_'.$lang['language_code'],
		            'settings' => 'ch_du_bo_fb_link_'.$lang['language_code'],
		            'priority' => $priority,
		            'type'	=>	'text'
		        )
		) );
		$wp_customize->add_control( new WP_Customize_Control(
		        $wp_customize,
		        'ch_du_bo_tw_link_'.$lang['language_code'],
		        array(
		            'label'    => 'Twitter Link ('.strtoupper($lang['language_code']).')',
		            'section'  => 'ch_du_bo_section_social_'.$lang['language_code'],
		            'settings' => 'ch_du_bo_tw_link_'.$lang['language_code'],
		            'priority' => $priority,
		            'type'	=>	'text'
		        )
		) );
		$wp_customize->add_control( new WP_Customize_Control(
		        $wp_customize,
		        'ch_du_bo_insta_link_'.$lang['language_code'],
		        array(
		            'label'    => 'Instagram Link ('.strtoupper($lang['language_code']).')',
		            'section'  => 'ch_du_bo_section_social_'.$lang['language_code'],
		            'settings' => 'ch_du_bo_insta_link_'.$lang['language_code'],
		            'priority' => $priority,
		            'type'	=>	'text'
		        )
		) );
		$wp_customize->add_control( new WP_Customize_Control(
		        $wp_customize,
		        'ch_du_bo_youtube_link_'.$lang['language_code'],
		        array(
		            'label'    => 'Youtube Link ('.strtoupper($lang['language_code']).')',
		            'section'  => 'ch_du_bo_section_social_'.$lang['language_code'],
		            'settings' => 'ch_du_bo_youtube_link_'.$lang['language_code'],
		            'priority' => $priority,
		            'type'	=>	'text'
		        )
		) );
		$wp_customize->add_control( new WP_Customize_Control(
		        $wp_customize,
		        'ch_du_bo_lnkd_link_'.$lang['language_code'],
		        array(
		            'label'    => 'LinkedIn Link ('.strtoupper($lang['language_code']).')',
		            'section'  => 'ch_du_bo_section_social_'.$lang['language_code'],
		            'settings' => 'ch_du_bo_lnkd_link_'.$lang['language_code'],
		            'priority' => $priority,
		            'type'	=>	'text'
		        )
		) );
		$priority++;
	}

	$priority = 10;
	foreach($langs as $lang){
		$wp_customize->add_section( 'ch_du_bo_section_footer_'.$lang['language_code'],
		    array(
		        'title'          => 'Footer Text for '.strtoupper($lang['language_code']),
		        'description'    => 'Footer Text for '.strtoupper($lang['language_code']),
		        'capability'     => 'manage_options',
		        'theme-supports' => '',
		        'priority'       => 12,
		        'panel'          => 'ch_du_bo_social_setting_panel',
		    )
		);

		$wp_customize->add_setting('ch_du_bo_footer_right_content_'.$lang['language_code'],array(
				'type'	=>	'theme_mod',
				'default'	=>	'',
				'capability'	=>	'manage_options',
				'transport'	=>	'refresh',
				'sanitize_callback'	=>	'sanitize_text_field'
			)
		);		

		$wp_customize->add_control( new WP_Customize_Control(
		        $wp_customize,
		        'ch_du_bo_footer_right_content_'.$lang['language_code'],
		        array(
		            'label'    => 'Footer Right Content ('.strtoupper($lang['language_code']).')',
		            'section'  => 'ch_du_bo_section_footer_'.$lang['language_code'],
		            'settings' => 'ch_du_bo_footer_right_content_'.$lang['language_code'],
		            'priority' => $priority,
		            'type'	=>	'text'
		        )
		) );

		$wp_customize->add_setting('ch_du_bo_footer_left_content_'.$lang['language_code'],array(
				'type'	=>	'theme_mod',
				'default'	=>	'',
				'capability'	=>	'manage_options',
				'transport'	=>	'refresh',
				'sanitize_callback'	=>	'esc_textarea'
			)
		);		

		$wp_customize->add_control( new WP_Customize_Control(
		        $wp_customize,
		        'ch_du_bo_footer_left_content_'.$lang['language_code'],
		        array(
		            'label'    => 'Footer Left Content ('.strtoupper($lang['language_code']).')',
		            'section'  => 'ch_du_bo_section_footer_'.$lang['language_code'],
		            'settings' => 'ch_du_bo_footer_left_content_'.$lang['language_code'],
		            'priority' => $priority,
		            'type'	=>	'text'
		        )
		) );		
		$priority++;
	}