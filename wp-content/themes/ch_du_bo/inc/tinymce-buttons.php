<?php

add_action( 'after_setup_theme', 'ch_du_bo_button_setup' );
 
if ( ! function_exists( 'ch_du_bo_button_setup' ) ) {
    function ch_du_bo_button_setup() {
 
        add_action( 'init', 'ch_du_bo_buttons' );
 
    }
}

if ( ! function_exists( 'ch_du_bo_buttons' ) ) {
    function ch_du_bo_buttons() {
        if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
            return;
        }
 
        if ( get_user_option( 'rich_editing' ) !== 'true' ) {
            return;
        }
 
        add_filter( 'mce_external_plugins', 'ch_du_bo_add_buttons' );
        add_filter( 'mce_buttons', 'ch_du_bo_register_buttons' );
    }
}

if ( ! function_exists( 'ch_du_bo_add_buttons' ) ) {
    function ch_du_bo_add_buttons( $plugin_array ) {
        $plugin_array['fa_icon_button'] = get_template_directory_uri().'/js/tinymce_buttons.js';
        $plugin_array['audio_player'] = get_template_directory_uri().'/js/tinymce_buttons.js';
        $plugin_array['ch_download'] = get_template_directory_uri().'/js/tinymce_buttons.js';
        $plugin_array['ch_button'] = get_template_directory_uri().'/js/tinymce_buttons.js';
        return $plugin_array;
    }
}

if ( ! function_exists( 'ch_du_bo_register_buttons' ) ) {
    function ch_du_bo_register_buttons( $buttons ) {
        array_push( $buttons, 'fa_icon_button' );
        array_push( $buttons, 'audio_player' );
        array_push( $buttons, 'ch_download' );
        array_push( $buttons, 'ch_button' );
        return $buttons;
    }
}

//add_action ( 'after_wp_tiny_mce', 'mytheme_tinymce_extra_vars' );
 
if ( !function_exists( 'mytheme_tinymce_extra_vars' ) ) {
	function mytheme_tinymce_extra_vars() { ?>
		<script type="text/javascript">
			var tinyMCE_object = <?php echo json_encode(
				array(
					'button_name' => esc_html__('Icon', 'ch_du_bo'),
					'button_title' => esc_html__('Icon with Link', 'ch_du_bo'),
				)
				);
			?>;
		</script><?php
	}
}