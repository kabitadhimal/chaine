﻿<?php
add_action('wp_footer','ch_du_bo_footer_scripts',99);
function ch_du_bo_footer_scripts(){
	$isNewsPage = is_page_template('page-templates/news-template.php');
	?>
<script type="text/javascript" defer onload=''>
	var paged = 2;
	var start = 7;
	var filterUsed = false;
	var catIds = [];
	var $grid, $gridSocial;
	var hideMoreBtn = false;
	var isActive = true;
	var isPreviousEventComplete = true;
	var isDataAvailable = true;
	var isComplete = true;
	var displayedRow = 1;
	var owlOptions;
	var owlstartPosition = 0;
	var position = 1;
	var currentTotal;

	jQuery(document).ready(function() {
		setTimeout(function(){
			if(jQuery('input[type="radio"]:checked').length == 1){
				jQuery('input[type="radio"]:checked').trigger('change');
			}else{
				console.log('No');
			}
		}, 2000);

		if(jQuery('.select').length > 0){
			jQuery('.select').niceSelect();
		}
		if(jQuery('.contact-listing li').length == 2){
			var lastBlock = jQuery('.contact-listing li').get(1);
		    var lastBlockDiv = jQuery(lastBlock).find('div.col-md-4');
		    jQuery(lastBlockDiv).removeClass('col-md-4');
		    jQuery(lastBlockDiv).addClass('col-md-8');
		}
    	jQuery('.more-news-ajax').click(function(e){
    		e.preventDefault();
    		if(!isDataAvailable){
    			return;
    		}
    		var thisBlock = jQuery(this).parents('.section-news').find('.news-list-ul');
    		var loader = jQuery(".page-overlay");
    		loader.show();
    		paged = thisBlock.attr('data-paged');
    		if(typeof paged === 'undefined'){
    			paged = 2;
    			thisBlock.attr('data-paged',3);
    		}
    		var ids = thisBlock.parents('.section-news.no-padding-btm').data('ids');
    		//console.log(ids);
    		jQuery.ajax({
			  method: "POST",
			  url: ch.ajax_url,
			  data: { action: "ajax_more_news", data: ids, paged: paged, lang: ch.site_lang }
			}).done(function( msg ) {
				msg = jQuery(msg);
				if(isActive){
					thisBlock.append(msg).masonry('appended', msg, 'reloadItems');	
					jQuery(msg).imagesLoaded().progress( function() {
						thisBlock.masonry('layout');
					});	
				}else{
					thisBlock.append(msg);
					var thisB = jQuery('.js-mobile-carousel');
					var genres = [];
					var content = '';
					
					thisB.trigger('destroy.owl.carousel');
					thisB.find('.owl-stage-outer').children().unwrap();
					thisB.removeClass("owl-center owl-loaded owl-text-select-on owl-carousel");

					thisB.find('.grid-item').each(function(){
						genres.push('<div class="grid-item feature_block col-md-4 col-sm-4 item wow fadeInDown">'+jQuery(this).html()+'</div>');
					});

					for (var i = 0; i < genres.length; i++) { 
						 content += "<div class=\"item\">" + genres[i] + "</div>"
					}

					thisB.html(content);
					owlstartPosition += 3; 
					position = owlstartPosition;
					owlOptions.startPosition = owlstartPosition;
					thisB.owlCarousel(owlOptions);
					currentTotal = jQuery('.js-mobile-carousel .owl-item').length;
					jQuery('.newsAll').addClass('hidden-xs');
				}
				
				if(msg == '' || hideMoreBtn === true){ 
					jQuery('.newsAll').addClass('hidden');
					loader.hide(); return; 
				}
			  	paged++;
			  	thisBlock.attr('data-paged',paged);
			  	loader.hide();
		  	});
    	});

    	var load_more_funds = function(){
    		if(ch.page_template != 'fund-raising.php'){
				return;
			}
    		var catIds = [];
			var selectedOptions = jQuery('.filtersel option:selected');
			selectedOptions.each(function(){
				catIds.push(jQuery(this).val());
				filterUsed = true;
			});
    		var loader = jQuery(".page-overlay");
    		loader.show();
    		var data = { action: "ajax_more_fund_list", paged: paged, start: start, lang: ch.site_lang };
    		if(filterUsed){
    			data.catIds = catIds;
    		}
    		jQuery.ajax({
				method: "POST",
				url: ch.ajax_url,
				data: data
			}).done(function( msg ) {
				//jQuery('.news-listing .masonry.grid').append(msg);
				msg = jQuery(msg);
				if(isActive){
					$grid.append(msg).masonry('appended', msg, 'reloadItems');
					jQuery(msg).imagesLoaded().progress( function() {
						$grid.masonry('layout');
					});
				}else{
					jQuery('.news-listing .masonry.grid').append(msg);
				}
				
				start = start + 6;
			  	paged++;
				loader.hide();
				isPreviousEventComplete = true;
		  	});
    	}

		var load_more_news = function(){
			if(ch.page_template != 'news-template.php'){
				return;
			}
			isComplete = false;
			var catIds = [];
			catIds = jQuery('.filter_menu input[type="radio"]:checked').val();    		
			filterUsed = true;
			var loader = jQuery(".page-overlay");
			loader.show();
			//just making 1 change to the start. Doing here to make sure no other scripts are affected using same variable.
			if(start == 7){
				start = 8;
			}
			var data = { action: "ajax_more_news_list", paged: paged, start: start, lang: ch.site_lang };
			if(filterUsed){
				data.catIds = catIds;
			}
			jQuery.ajax({
				method: "POST",
				url: ch.ajax_url,
				data: data
			}).done(function( msg ) {
				isComplete = true;
				msg = jQuery(msg);
				if(isActive){
					$grid.append(msg).masonry('appended', msg, 'reloadItems');
					jQuery(msg).imagesLoaded().progress( function() {
						$grid.masonry('layout');
					});					
				}else{
					jQuery('.grid').append(msg);
				}
				start = start + 6;
			  	paged++;
				loader.hide();
				isPreviousEventComplete = true;
		  	});
		}

		jQuery(window).scroll(function () {
			if (jQuery(document).height() - 700 <= jQuery(window).scrollTop() + jQuery(window).height()) {
			 
				if (isPreviousEventComplete && isDataAvailable && isComplete) {				
					isPreviousEventComplete = false;
					load_more_news();
					load_more_funds();
				}
			}
		});

    	jQuery('.more-news-btn').click(function(e){
    		e.preventDefault();
    		isPreviousEventComplete = false;
			load_more_news();
    	});

    	jQuery('.more-fund-btn').click(function(e){
    		e.preventDefault();
    		isPreviousEventComplete = false;
			load_more_funds();			
    	});


    	function checkAndAddHttpProtocol(url){
            if(url.indexOf("http://") == 0 ) return url;
            if(url.indexOf("https://") == 0 ) return url;
            return "https://"+ url;
        }
		
		
		jQuery('body').on('click','a',function(e) {
			var thisHref = jQuery(this).attr('href');

            if (thisHref.indexOf('mailto') == 0) {
                return;
            }

            if (thisHref.indexOf('tel') == 0) {
                return;
            }

             if(thisHref == 'javascript:void(0);'){
                return;
            }

			if(thisHref.indexOf('#') >= 0){
				return;
			}
			
			if (thisHref === '#') {
				return;
			}
			if(jQuery(this).hasClass('open-here')){
				return;
			}
			if(jQuery(this).hasClass('play_btn')){
				return;
			}
			if(jQuery(this).hasClass('video')){
				return;
			}


            //var a = new RegExp('/' + '(www.bonheur.ch|bonheur.ch|www.catena-della-solidarieta.ch|catena-della-solidarieta.ch|www.glueckskette.ch|glueckskette.ch|www.swiss-solidarity.org|swiss-solidarity.org|don.bonheur.ch|www.swisscom.ch|spenden.glueckskette.ch|donation.swiss-solidarity.org)' + '/');
            var a = new RegExp('/' + '(www.bonheur.ch|bonheur.ch|www.catena-della-solidarieta.ch|catena-della-solidarieta.ch|www.glueckskette.ch|glueckskette.ch|www.swiss-solidarity.org|swiss-solidarity.org)' + '/');
            if (!a.test(thisHref)) {
                if (thisHref != 'javascript:void(0);') {
                    //add http protocol if it is missing... this ususally comes from cms..
                    thisHref = checkAndAddHttpProtocol(thisHref);
                    e.preventDefault();
                    e.stopPropagation();
                    window.open(thisHref, '_blank');
                    return;
                }
            }

			/*
			var a = new RegExp('/' + '(www.bonheur.ch|bonheur.ch|www.catena-della-solidarieta.ch|catena-della-solidarieta.ch|www.glueckskette.ch|glueckskette.ch|www.swiss-solidarity.org|swiss-solidarity.org)' + '/');
			if (!a.test(thisHref)) {
				if (thisHref != 'javascript:void(0);') {					
					e.preventDefault();
					e.stopPropagation();
					window.open(thisHref, '_blank');
					return;
				}
			}
			*/
			
		});

		jQuery(function($) {
			mobileSlider($('.mobile-carousel'));
			mobileSlider($('.js-mobile-carousel'));
			
			var owlNextClick = function(){
				this.position++;
				if(this.position >= this.currentTotal){
					this.position = this.currentTotal;
				}
				console.log(this.position);
				if( (this.position)%3 == 0 ){
					jQuery('.'+this.btnClass).trigger('click');
				}
			}
			
			var owlPrevClick = function(){
				this.position--;
				console.log(this.position);
				if(this.position < 1){
					this.position = 1;
				}
			}
			
			function owlSliderMob(name) {
				this.name = name;
				this.position = 1;
				this.nextClick = owlNextClick;
				this.prevClick = owlPrevClick;
			}
			
			var socialSlider = new owlSliderMob('social');
			socialSlider.btnClass = 'show-more-social';
			socialSlider.currentTotal = jQuery('.social-News .item').length;
			
			var newsSlider = new owlSliderMob('news');
			newsSlider.btnClass = 'more-news-ajax';
			newsSlider.currentTotal = currentTotal;
			
			jQuery('body').on('click','.owl-next',function(){
				if(jQuery(this).parents('.social-News').length >= 1){
					socialSlider.nextClick();
				}else{
					newsSlider.nextClick();
				}
			});
			jQuery('body').on('click','.owl-prev',function(){
				if(jQuery(this).parents('.social-News').length >= 1){
					socialSlider.prevClick();
				}else{
					newsSlider.prevClick();
				}
			});
			jQuery('body').on('dragged.owl.carousel', '.js-mobile-carousel, .mobile-carousel' , function(e) {
					console.log('dragged');					
					var direction = e.relatedTarget.state.direction;
					if(jQuery(e.currentTarget).parents('.social-News').length >= 1){
						if(direction == 'left'){
							socialSlider.nextClick();
						}else{
							socialSlider.prevClick();
						}
					}else{
						if(direction == 'left'){
							newsSlider.nextClick();
						}else{
							newsSlider.prevClick();
						}
					}
			});
			function mobileSlider($item) {
				if($item.length === 0) return;
				$item.each(function() {
					var $self = $(this);
					var owl = jQuery($self);
				        owlOptions = {
				            //loop: false,
				            margin: 10,
							dots: false,
							nav: true,
				            responsive: {
				                0: {
				                    items: 1
				                }
				            }
				        };

				    if ( jQuery(window).width() < 768 ) {
				        var owlActive = owl.owlCarousel(owlOptions);
						currentTotal = jQuery('.js-mobile-carousel .owl-item').length;
				    } else {
				        owl.addClass('off');
				    }

				    jQuery(window).resize(function() {
				        if ( jQuery(window).width() < 768 ) {
				            if ( $self.hasClass('off') ) {
				                var owlActive = owl.owlCarousel(owlOptions);
								currentTotal = jQuery('.js-mobile-carousel .owl-item').length;
				                owl.removeClass('off');
				            }
				        } else {
				            if ( !$self.hasClass('off') ) {
				                owl.addClass('off').trigger('destroy.owl.carousel');
				                owl.find('.owl-stage-outer').children(':eq(0)').unwrap();
				            }
				        }
				    });
				});
			}		    
		});

		if(jQuery("#mailchilpCheckbox span.wpcf7-list-item-label").length > 0){
			jQuery("#mailchilpCheckbox span.wpcf7-list-item").append('<label for="c1" class="wpcf7-list-item-label">'+ch.newsletter_text+'</label>');
			jQuery("#mailchilpCheckbox span.wpcf7-list-item-label").remove();
		}


	});


	// Dropdown Menu Fade
	jQuery(document).ready(function(){
		jQuery('.feature_block').css({'cursor':'pointer'});
	    jQuery("#primary-menu .dropdown").hover(
	        function() { jQuery('.dropdown-menu', this).stop(true,true).fadeIn("fast");
	        },
	        function() { jQuery('.dropdown-menu', this).stop(true,true).fadeOut("fast");
	    });
	});

	/* jQuery('#contact-subject').change(function(){
		var subject = jQuery(this).find('option:selected').data('subject');
		if(subject == "Bénévolat"){
			var lang = '';
			if(ch.site_lang != "fr"){
				lang = "/"+ch.site_lang+"/";
			}
			window.location.replace(ch.site_url+lang+"vous-impliquer/devenir-benevole-2/");
		}
	}); */

	/* jQuery('.feature_block').click(function(e){
		var link = jQuery(this).find('a').attr('href');
		window.location.replace(link);
	}); */

	jQuery(document).ready(function() {

		if(jQuery('.fancybox').length > 0 ){
			jQuery('.fancybox').fancybox();
		}
		/*if(jQuery('#mc-embedded-subscribe-form').length > 0 ){
			jQuery("#mc-embedded-subscribe-form").submit(function(e){
				e.preventDefault();
				var em = jQuery('#mce-EMAIL').attr('value');
				if(em === ''){
					return;
				}
				var mailchimpURL = jQuery(this).attr('action');
				var splitURL = mailchimpURL.split('&');
				jQuery.fancybox({
					maxWidth	: 800,
					maxHeight	: 600,
					fitToView	: false,
					width		: '70%',
					height		: '70%',
					autoSize	: false,
					closeClick	: false,
					openEffect	: 'none',
					closeEffect	: 'none',
					title		: 'Subscribe to our mailing list',
					type		: 'iframe',
					href		: ch.ajax_url+'?action=mce_newsletter&email='+em+'&mce_url='+mailchimpURL+'&'+splitURL[1]
				});
			});
		}*/

		if(jQuery('.video').length > 0 ){
			jQuery('.play_btn.video').fancybox({
					fitToView	: true,
					width		: '90%',
					height		: '90%',
					autoSize	: false,
					closeClick	: false,
					openEffect	: 'none',
					closeEffect	: 'none',
					href		: jQuery('.play_btn.video').attr('href').replace("watch?v=", "embed/")
			});
			jQuery(".video").click(function() {		
				var replace1 = this.href.replace("watch?v=", "embed/");			
				var	regex = /([^\s]+)(&t=)/g;
				var m;
				if((m = regex.exec(replace1)) !== null){
					replace1 = m[1];
				}
				var replace2 = replace1.replace("&list", "?list");
				if(replace2.indexOf('?') <= 0){
					replace2 += '?autoplay=1';
				}else{
					replace2 += '&autoplay=1';
				}
				jQuery.fancybox({
					'padding'		: 0,
					'autoScale'		: false,
					'transitionIn'	: 'none',
					'transitionOut'	: 'none',
					'title'			: this.title,
					'fitToView'    	: false, //
					'maxWidth'     	: "90%", //
					'width'         : "90%",
					'height'        : "90%",
					//'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
					'href'			: replace2,
					'type'			: 'iframe',
					'iframe'			: {
						scrolling : 'auto',
						preload   : true
					}
				});

				return false;
			});
		}

		if(jQuery('.status_bar').length > 0){
			// grab the initial top offset of the navigation
		   	var stickyNavTop = jQuery('.status_bar').offset().top;

		   	// our function that decides weather the navigation bar should have "fixed" css position or not.
		   	var stickyNav = function(){
			    var scrollTop = jQuery(window).scrollTop(); // our current vertical position from the top

			    // if we've scrolled more than the navigation, change its position to fixed to stick to top,
			    // otherwise change it back to relative
			    if (scrollTop > stickyNavTop) {
			        //jQuery('.status_bar').addClass('sticky');					
					
			    } else {
			        //jQuery('.status_bar').removeClass('sticky');
			    }
			};

			stickyNav();
			// and run it again every time you scroll
			jQuery(window).scroll(function() {
				stickyNav();
			});
		}

		if(typeof jQuery('.filtersel').multiselect !== 'undefined'){
			jQuery('.filtersel').multiselect({
				onDropdownShown: function(event) {
					var sel = jQuery(event.target).find('input[value="select-all-value"]');
					var preSel = jQuery(event.target).find('li.active').length;
					if(!sel.parents('li').hasClass('active') && preSel === 0){
						//sel.trigger('click');
					}
			    },
				disableIfEmpty: true,
				onSelectAll: function() {
					this.options.onChange();
		        },
				onDeselectAll: function() {
					alert('onDeselectAll triggered!');
				},
				includeSelectAllOption: true,
				selectAllValue: 'select-all-value',
				selectAllJustVisible: true,
	            buttonText: function(options, select) {
	            	var title = select.data('title');
	                return title;
	            },
	            buttonTitle: function(options, select) {
	                var labels = [];
	                options.each(function () {
	                    labels.push(jQuery(this).text());
	                });
	                return labels.join(' - ');
	            },
	            onChange: function(option, checked, select) {
	            	jQuery('.open').removeClass('open');
					isComplete = false;
					isDataAvailable = true;
					if(isActive){
						$grid.masonry('destroy');
					}					
	            	catIds = [];
	            	paged = 2;
					start = 7;
					filterUsed = true;
	                var selectedOptions = jQuery('.filtersel option:selected');
	                var loader = jQuery('.news-loader');
					if( jQuery('.more-news-btn').length > 0 ){
						var pg = 'news';
					}else{
						var pg = 'fund';
					}
	                jQuery('.more-'+pg+'-btn').removeClass('hidden');
	                jQuery('.news-listing .masonry.grid').empty();
	                selectedOptions.each(function(){
	                	catIds.push(jQuery(this).val());
	                });
					jQuery('.btn-group button').removeClass('active');
					jQuery('li.active').each(function(){
						jQuery(this).parent('ul').prev().addClass('active');
					});
	                loader.show();
		    		jQuery.ajax({
						method: "POST",
						url: ch.ajax_url,
						data: { action: "ajax_"+pg+"_filter" , catIds: catIds, lang: ch.site_lang}
					}).done(function( msg ) {
						isComplete = true;
						$grid = jQuery('.grid').masonry({itemSelector: '.grid-item'});
						msg = jQuery(msg);
						$grid.append(msg).masonry('appended', msg);	
						jQuery(msg).imagesLoaded().progress( function() {
							$grid.masonry('layout');
						});				
						loader.hide();
				  	});
	            }
	        });
			
			jQuery('.deselectAll').on('click', function(e) {
				e.preventDefault();
				isDataAvailable = true;
				jQuery('input[type="radio"]:checked').each(function(){
					jQuery(this).prop('checked', false);
				});
				jQuery('.filtersel').multiselect('deselectAll', false);
				jQuery('.filtersel .multiselect-item').removeClass('active');
				jQuery('.multiselect').removeClass('active');
				catIds = [];
				paged = 1;
				start = 1;
				filterUsed = true;
				var selectedOptions = jQuery('.filtersel option:selected');
				var loader = jQuery('.news-loader');
				if( jQuery('.more-news-btn').length > 0 ){
					var pg = 'news';
				}else{
					var pg = 'fund';
				}
				//jQuery('.more-'+pg+'-btn').removeClass('hidden');
				
				if(isActive){
					$grid.masonry('destroy');
				}
				jQuery('.masonry.grid').empty();
				selectedOptions.each(function(){
					catIds.push(jQuery(this).val());
				});
				jQuery('.btn-group button').removeClass('active');
				jQuery('li.active').each(function(){
				jQuery(this).parent('ul').prev().addClass('active');
				});
				loader.show();
				jQuery.ajax({
					method: "POST",
					url: ch.ajax_url,
					data: { action: "ajax_"+pg+"_filter" , catIds: catIds, per_page: '-1'}
				}).done(function( msg ) {
					isDataAvailable = false;
					msg = jQuery(msg);
					jQuery('.masonry.grid').html(msg);
					if(isActive){
						jQuery(msg).imagesLoaded().progress( function() {
							$grid = jQuery('.masonry.grid').masonry({itemSelector: '.grid-item'});
							$grid.masonry('layout');
							loader.hide();						
						});
					}
					loader.hide();						
				});
			});
		}
		
		jQuery('.filter_menu input[type="radio"]').change(function(){
			isDataAvailable = true;
			isComplete = false;
			if(isActive){
				$grid.masonry('destroy');
			}			
			catIds = [];
			paged = 2;
			start = 8;
			filterUsed = true;
			var selectedOptions = jQuery('.filtersel option:selected');
			var loader = jQuery('.news-loader');
			if( jQuery('.more-news-btn').length > 0 ){
				var pg = 'news';
			}else{
				var pg = 'fund';
			}
			jQuery('.more-'+pg+'-btn').removeClass('hidden');
			jQuery('.grid').empty();
			/* selectedOptions.each(function(){
				catIds.push(jQuery(this).val());
			}); */
			catIds = jQuery(this).val();
			jQuery('.btn-group button').removeClass('active');
			jQuery('li.active').each(function(){
				jQuery(this).parent('ul').prev().addClass('active');
			});
			loader.show();
			jQuery.ajax({
				method: "POST",
				url: ch.ajax_url,
				data: { action: "ajax_"+pg+"_filter" , catIds: catIds, lang: ch.site_lang}
			}).done(function( msg ) {
				isComplete = true;
				msg = jQuery(msg);
				if(isActive){
					$grid = jQuery('.grid').masonry({itemSelector: '.grid-item'});				
					$grid.append(msg).masonry('appended', msg);	
					jQuery(msg).imagesLoaded().progress( function() {
						$grid.masonry('layout');
					});						
				}else{
					jQuery('.grid').append(msg);
				}			
				loader.hide();
			});
		});
		
		
		jQuery('#contact-subject').change(function(){
			var subject = jQuery('#contact-subject option:selected').attr('data-subject');
			var email = jQuery(this).val();
			jQuery('#sendTo').attr('value',email);
			jQuery('#subject').attr('value',subject);
		});

		jQuery('.wpcf7-submit').click(function(){
			var a = jQuery('#contact-subject').val();
			if(a == ''){
				jQuery("#contact-subject").css('border-color','red');
				jQuery(".subject").removeClass('hidden');
			}else{
				jQuery("#contact-subject").css('border-color','');
				jQuery(".subject").addClass('hidden');
			}
		});

	});
	
		
	//lazy loading for instagram in social block
	function getInstaFeedFromURL(){
		if(jQuery('[data-href*="instagram"]').length >=1 ){
			var countInsta = jQuery('[data-href*="instagram"]').length;
			jQuery('[data-href*="instagram"]').each(function(){
				var curBlock = jQuery(this);
				if(!curBlock.hasClass('row-'+displayedRow)){
					return;
				}
				var curInstaLink = jQuery(this).attr('data-href');
				jQuery.ajax({
					method: "POST",
					url: ch.ajax_url,
					dataType: 'json',
					data: { action: "import_instagram_links" , lang: ch.site_lang, link: curInstaLink}
				}).done(function( msg ) {
					curBlock.find('.image_crop img').attr('src',msg.data.images.standard_resolution.url);
					/* var d = new Date(msg.data.created_time * 1000);	
					var properDate = d.toLocaleDateString(); */
					var d = msg.data.created_time * 1000;	
					var a = d.split(/[^0-9]/);
					d=new Date (a[0],a[1],a[2],a[3],a[4],a[5] );
					/* var properDate = d.toLocaleDateString();
					properDate = properDate.replace(/\//g, "."); */
					var properDate = d.getDate()+'.'+( parseInt(d.getMonth()) + 1 )+'.'+d.getFullYear();
					curBlock.find('.disasterdate').text(properDate);
					curBlock.find('.insta_caption').text(msg.data.caption.text);
					curBlock.find('.loading').hide();
					
					if(isActive){
						$gridSocial.each(function(){
							jQuery(this).masonry('layout');
						});
					}					
				});
			});
		}
	}		
	
	//lazy loading for facebook in social block
	function getFBFeedFromURL(){
		if(jQuery('[data-href*="facebook"]').length >=1 ){
			var countFB = jQuery('[data-href*="facebook"]').length;			
			jQuery('[data-href*="facebook"]').each(function(){				
				var curBlock = jQuery(this);
				if(!curBlock.hasClass('row-'+displayedRow)){
					return;
				}
				var curFBLink = jQuery(this).attr('data-href');
				jQuery.ajax({
					method: "POST",
					url: ch.ajax_url,
					dataType: 'json',
					data: { action: "import_facebook_links" , lang: ch.site_lang, link: curFBLink}
				}).done(function( msg ) {
					curBlock.find('.image_crop img').attr('src',msg.full_picture);
					var d = msg.created_time;	
					var a = d.split(/[^0-9]/);
					d=new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
					console.log(d);
					/* var properDate = d.toLocaleDateString();
					properDate = properDate.replace(/\//g, "."); */
					var properDate = d.getDate()+'.'+( parseInt(d.getMonth()) + 1 )+'.'+d.getFullYear();
					curBlock.find('.disasterdate').text(properDate);
					curBlock.find('.insta_caption').text(msg.message);
					curBlock.find('.loading').hide();
					
					if(isActive){
						$gridSocial.each(function(){
							jQuery(this).masonry('layout');
						});
					}
				});
			});
		}
	}

	jQuery(window).load(function(){
		
		if(jQuery('.main-slider, .secondary-slider').length > 0){
			jQuery('.main-slider, .secondary-slider').owlCarousel({
				center: true,
				items:1,
				loop:true,
				nav: true,
				autoplay: true,
				lazyLoad: true,
				dots: true
			});
		}
		
		if(jQuery('.play_btn.video').length == 1){
			//jQuery('.play_btn.video').trigger('click');
		}
		
		///Added by ranjita
		if(jQuery(window).width() >= 768){
			
			if(jQuery('.social-News').length >= 1 ){
				if(typeof jQuery('.social-News .grid').masonry !== 'undefined'){
					$gridSocial = jQuery('.social-News .grid').masonry({
						itemSelector: '.grid-item',
						horizontalOrder: true
						
					});
				}
			}
		
			if(typeof jQuery('.grid').masonry !== 'undefined'){
				$grid = jQuery('.grid').masonry({
					// options...
					itemSelector: '.grid-item',
					horizontalOrder: true
					
					
				});
			}	
		}else{
			isActive = false;
		}
    	
		jQuery('.show-more-social').click(function(e){
			e.preventDefault();
			displayedRow++;
			var $found = jQuery('.row-'+displayedRow).length;
			if( $found > 0){
				getInstaFeedFromURL();
				getFBFeedFromURL();
				var $items = jQuery(jQuery('.row-'+displayedRow).removeClass('hidden'));
				if(isActive){
					$gridSocial.each(function(){
						jQuery(this).masonry('layout');
					});
				}				
				if($found < 3){
					jQuery('.show-more-social').addClass('hidden');
				}
			}else{
				jQuery('.show-more-social').addClass('hidden');
			}
			
			
		});
		
		getInstaFeedFromURL();
		getFBFeedFromURL();
		
		///*Added by ranjita
		
		
	});
	function disabledMasonary(){
		var $width = jQuery(window).width();
		if($width < 768){  
			if(isActive){
				$grid.masonry('destroy');				
			}  
			isActive = false;
		}else{
			//if(!isActive){
				$grid.masonry('destroy');
				$grid = jQuery('.grid').masonry({
				// options...
				itemSelector: '.grid-item',
				//columnWidth: 300
				});				
			//}
			isActive = true;
		}			 
	}
	
	jQuery(window).resize(function(){		
		setTimeout(function(){
			disabledMasonary();
		}, 1000);
	});
</script>
	<?php
}
