<?php
/** Import 10 Latest **/
add_action('wp','update_fb_posts');
add_action( 'wp_ajax_ajax_fb_import', 'update_fb_posts' );
add_action( 'wp_ajax_nopriv_ajax_fb_import', 'update_fb_posts' );
function update_fb_posts(){

    ini_set('max_execution_time', -1);
    ini_set('memory_limit','512M');


    if(!isset($_GET['import']) || $_GET['import'] != 'FB' || $_GET['pwd'] != 'a98d7'){
		return;
	}
	require_once(ABSPATH . 'wp-admin/includes/media.php');
    require_once(ABSPATH . 'wp-admin/includes/file.php');
    require_once(ABSPATH . 'wp-admin/includes/image.php');

    $lang = strtolower(CH_LANG_CODE);

	switch ($lang) {
		case 'fr':
			$fbPageId = 'chainedubonheur';
			$catID = 376;
			break;
		case 'en':
			$fbPageId = 'swisssolidarity';
			$catID = 377;
			break;
		case 'it':
			$fbPageId = 'catenadellasolidarieta';
			$catID = 379;
			break;
		default:
			$fbPageId = 'glueckskette';
			$catID = 378;
			break;
	}
    //$fbPageId = "chainedubonheur";
	$fbMan = new FB_Page_Manager($fbPageId);



	//$lang = 'it';
    $fbPagination = get_option("fb_pagination_{$lang}", []);
    $beforeCursor = $fbPagination['cursors']['before'];


    /*
    //clear
    delete_option( "fb_pagination_{$lang}" );
    $args = array(
        'cat'	=>	$catID,
        'posts_per_page'	=> -1
    );
    $query = new WP_Query($args);
    if($query->have_posts()){
        while($query->have_posts()){
            $query->the_post();
            //delete_post_media(get_the_ID());
            wp_delete_post(get_the_ID(),true);
        }
    }
    echo 'cleared';
    die();
    */


    //$beforeCursor  = 'Q2c4U1pXNTBYM0YxWlhKNVgzTjBiM0o1WDJsa0R5QXhOams0TURFeE5EWXdPVFk2TkRZAeU5ERTROREl4TXpRek1EQXpOamN5TUE4TVlYQnBYM04wYjNKNVgybGtEeDR4TmprNE1ERXhORFl3T1RaZAk1UQXhOVFV4TkRnMk5UUXpOREV3T1RjUEJIUnBiV1VHV3NUMktRRT0ZD';
    //$beforeCursor = '';
    $results = $fbMan->get_latest_posts_after(15, $beforeCursor);

	//echo '<pre>';

    $fbPosts = $results['data'];
    /*
	var_dump(count($results['data']));
	var_dump($results['paging']['cursors']['before']);
	var_dump($results['paging']);
    */
	update_option("fb_pagination_{$lang}", $results['paging'] );
	//var_dump(get_option("fb_pagination_{$lang}"));

	//update the pagination

    //var_dump($fbPosts);
	//$fbPagination = get_option('fb-pagination');
	//var_dump($fbPagination);

	//die();

	echo "<h3>FB Page: ".$fbPageId."</h3>";
	echo "<h4>FB count: ".count($fbPosts)."</h4>";


	ob_start();
	echo '<div><b>FB import: '.date("Y-m-d H:i:s").'</b></div>';
    echo '<pre>';
	var_dump("count", count($results['data']));
	var_dump("paging", $results['paging']);
	var_dump("before", $results['paging']['cursors']['before']);
	echo '</pre><hr/>';
	$log = ob_get_clean();

	error_log($log, 3, __DIR__.'/log_fb_import.html');

	/*
	$args = array(
		'cat'	=>	$catID,
		'posts_per_page'	=> -1
	);
	$query = new WP_Query($args);
	if($query->have_posts()){
		while($query->have_posts()){
			$query->the_post();
			delete_post_media(get_the_ID());
			wp_delete_post(get_the_ID(),true);
		}
	}
	*/

    $deletedFBPostsID = get_option('deletedFBPostsID', serialize(array()));
    $deletedFBPostsID = unserialize($deletedFBPostsID);

	//debug($fbPosts, true);
	if(!empty($fbPosts)){
		foreach($fbPosts as $fbPost){
			//debug($fbPost);
			// checking for trashed Facebook posts			//
            /*
			//$deletedFBPostsID = get_option('deletedFBPostsID', serialize(array()));
			//$deletedFBPostsID = unserialize($deletedFBPostsID);
			if(in_array($fbPost['id'], $deletedFBPostsID)){
				debug('Ignored '.$fbPost['id']);
				continue;
			}
            */

			// Create post object
			$my_post = array(
			  'post_title'    => wp_strip_all_tags( $fbPost['name'] ),
			  'post_content'  => $fbPost['message'],
			  'post_status'   => 'publish',
			  'post_author'   => 1,
			  'post_date'	  => date('Y-m-d H:i:s',strtotime($fbPost['created_time'])),
			  'post_category' => array( $catID ),
			  'meta_input'   => array(
			        'social_link' => $fbPost['permalink_url'] = '' ? $fbPost['permalink_url'] : $fbPost['link'],
			        'social_media' => 'Facebook',
			        'hero'	=>	'No',
			        'fbPostID'	=>	$fbPost['id']
			    ),
			);

			// Insert the post into the database
			$new_post_id = wp_insert_post( $my_post );
			set_featured_image(insert_post_media($new_post_id, $fbPost['full_picture']));
		}
	}
	die('Done');
}
function delete_post_media( $post_id ) {

	if(!isset($post_id)) return; // Will die in case you run a function like this: delete_post_media($post_id); if you will remove this line - ALL ATTACHMENTS WHO HAS A PARENT WILL BE DELETED PERMANENTLY!
	elseif($post_id == 0) return; // Will die in case you have 0 set. there's no page id called 0 :)
	elseif(is_array($post_id)) return; // Will die in case you place there an array of pages.

	else {

		$attachments = get_posts( array(
			'post_type'      => 'attachment',
			'posts_per_page' => -1,
			'post_status'    => 'any',
			'post_parent'    => $post_id
		) );

		foreach ( $attachments as $attachment ) {
			if ( false === wp_delete_attachment( $attachment->ID ) ) {
				// Log failure to delete attachment.
			}
		}
	}
}
function insert_post_media( $parent_post_id, $url){

    $tmp = download_url( $url );

    $post_id = $parent_post_id;
    $file_array = array();

    // Set variables for storage
    // fix file filename for query strings
    preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $url, $matches);
    $file_array['name'] = basename($matches[0]);
    $file_array['tmp_name'] = $tmp;

    // If error storing temporarily, unlink
    if ( is_wp_error( $tmp ) ) {
        @unlink($file_array['tmp_name']);
        $file_array['tmp_name'] = '';
    }

    // do the validation and storage stuff
    $id = media_handle_sideload( $file_array, $post_id, $desc );

    // If error storing permanently, unlink
    if ( is_wp_error($id) ) {
        @unlink($file_array['tmp_name']);
        $error_string = $id->get_error_message();
        echo '<div id="message" class="error">'.$url.'<p>' . $error_string . '</p></div>';
        return $id;
    }

    return $id;
    //$src = wp_get_attachment_url( $id );
}

function set_featured_image($id){
    $p = get_post($id);
    update_post_meta($p->post_parent,'_thumbnail_id',$id);
}

/*
add_action( 'wp_trash_post', 'fb_post_delete_flag' );
//add_action( 'before_delete_post', 'fb_post_delete_flag');
function fb_post_delete_flag($post_id){
	// get a meta value for source of the post
	$source = get_post_meta( $post_id, 'social_media', true);
	// check if source of the post is Facebook
	if($source === 'Facebook'){
		// get the facebook id of the post
		$fbPostID = get_post_meta( $post_id, 'fbPostID', true);
		// get previously saved trashed posts. Return value in serialize format
		$deletedFBPostsID = get_option('deletedFBPostsID', serialize(array()));
		// unserialize value to array
		$deletedFBPostsID = unserialize($deletedFBPostsID);
		// check if trashed post id already save in meta
		if(!in_array($fbPostID, $deletedFBPostsID)){
			// if not already saved, add new trashed id to the array
			$deletedFBPostsID[] = $fbPostID;
			// serialize variable after new id insertion
			$deletedFBPostsID = serialize($deletedFBPostsID);
			// update option
			update_option('deletedFBPostsID', $deletedFBPostsID);
		}
	}
}
*/

/*
add_action( 'untrash_post', 'fb_post_delete_flag_restore' );
function fb_post_delete_flag_restore($post_id){
	// get a meta value for source of the post
	$source = get_post_meta( $post_id, 'social_media', true);
	// check if source of the post is Facebook
	if($source === 'Facebook'){
		// get the facebook id of the post
		$fbPostID = get_post_meta( $post_id, 'fbPostID', true);
		// get previously saved trashed posts. Return value in serialize format
		$deletedFBPostsID = get_option('deletedFBPostsID', serialize(array()));
		// unserialize value to array
		$deletedFBPostsID = unserialize($deletedFBPostsID);
		// check if trashed post id already save in meta
		$foundKey = array_search($fbPostID, $deletedFBPostsID);
		if(isset($foundKey)){
			// remove if from meta
			unset($deletedFBPostsID[$foundKey]);
			// serialize variable after id removed
			$deletedFBPostsID = serialize($deletedFBPostsID);
			// update option
			update_option('deletedFBPostsID', $deletedFBPostsID);
		}
	}
}
*/
