<?php
require 'vendor/autoload.php';
require 'src/Instagram.php';

use MetzWeb\Instagram\Instagram;
class Instagram_Manager{
	const APP_ID = 'dcb088a497eb44089a94a4317d350a07';
	const APP_SECRET_ID = '329cf57f86f846b2a93bd522c4fd3fe8';
	private $instagram, $instagramID;

	function __construct(){
		$this->instagram = new Instagram(array(
			'apiKey' => self::APP_ID,
			'apiSecret' => self::APP_SECRET_ID,
			'apiCallback' => admin_url().'tools.php?page=instagram-posts-import' // must point to success.php
		));
	}
	
	function get_login_url(){
		$loginUrl = $this->instagram->getLoginUrl();
		return $loginUrl;
	}
	
	function authenticate($code){		
		$data = $this->instagram->getOAuthToken($code);
		
		if($data->user){			
			$userData = get_option('insta_code_'.CH_LANG_CODE);
			//if($userData->user->id != $data->user->id){
				update_option('insta_code_'.CH_LANG_CODE, $data);
			//}
		}else{
			wp_die($data->error_message);
		}		
	}
	
	function get_media($userData, $limit = 10){		
		$this->instagram->setAccessToken($userData);
		$data = $this->instagram->getUserMedia($userData->user->id, $limit);
		return $data;
	}
}