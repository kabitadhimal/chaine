<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Chaîne_du_Bonheur
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function ch_du_bo_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed ';
	}

	if ( (is_home() || is_front_page()) && !is_emergency()){
		$classes[] = 'homepage';
	}

	if ( (is_home() || is_front_page()) && is_emergency()){
		$classes[] = 'homeurgency';
	}

	if( is_page_template('page-templates/cms-template.php') ){
		$classes[] = 'cms-page';	
	}

	if(is_single() && !is_singular('fundraising-campaign')){		
		$classes[] = 'news-page';
	}


	return $classes;
}
add_filter( 'body_class', 'ch_du_bo_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function ch_du_bo_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', bloginfo( 'pingback_url' ), '">';
	}
}
add_action( 'wp_head', 'ch_du_bo_pingback_header' );
