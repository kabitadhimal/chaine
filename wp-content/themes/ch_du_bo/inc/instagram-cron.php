<?php
/** Import 10 Latest **/
/* add_action('wp','authenticate_instagram');
function authenticate_instagram(){
	if(isset($_GET['code'])){
		
		// grab OAuth callback code
		$code = $_GET['code'];
		
		$instaMan = new Instagram_Manager();
		
		$instaMan->authenticate($code);		
		
	}
} */
add_action('wp','get_insta_media');
add_action( 'wp_ajax_ajax_instagram_import', 'get_insta_media' );
add_action( 'wp_ajax_nopriv_ajax_instagram_import', 'get_insta_media' );
function get_insta_media(){
	if(!isset($_GET['import']) || $_GET['import'] != 'Instagram' || $_GET['pwd'] != 'a98d7'){
		return;
	}
	
	require_once(ABSPATH . 'wp-admin/includes/media.php');
	require_once(ABSPATH . 'wp-admin/includes/file.php');
	require_once(ABSPATH . 'wp-admin/includes/image.php');
	
	switch (CH_LANG_CODE) {
		case 'fr':
			$catID = 380;
			break;
		case 'en':
			$catID = 381;
			break;
		case 'it':
			$catID = 382;
			break;
		default:
			$catID = 383;
			break;
	}
	
	$userData = get_option('insta_code_'.CH_LANG_CODE);
	
	$instaMan = new Instagram_Manager();
	
	$media = $instaMan->get_media($userData, 10);
	
	$instaPosts = $media->data;
	
	echo "<h3>Instagram Manager: ".$userData->user->username."</h3>";
	
	$args = array(
		'cat'	=>	$catID,
		'posts_per_page'	=> -1
	);
	$query = new WP_Query($args);
	if($query->have_posts()){
		while($query->have_posts()){
			$query->the_post();
			delete_post_media(get_the_ID());
			wp_delete_post(get_the_ID(),true);
		}
	}
	
	if(!empty($instaPosts)){
		foreach($instaPosts as $instaPost){
			
			// checking for trashed Instagram posts
			/* $deletedInstaPostsID = get_option('deletedInstaPostsID', serialize(array()));
			$deletedInstaPostsID = unserialize($deletedInstaPostsID);
			if(in_array($instaPost['id'], $deletedInstaPostsID)){
				debug('Ignored '.$instaPost['id']);
				continue;
			} */
			
			// Create post object
			$my_post = array(
			  'post_title'    => wp_strip_all_tags( $instaPost->id ),
			  'post_content'  => $instaPost->caption->text,
			  'post_status'   => 'publish',
			  'post_author'   => 1,
			  'post_date'	  => date('Y-m-d H:i:s',strtotime(gmdate("Y-m-d\TH:i:s\Z", $instaPost->created_time))),
			  'post_category' => array( $catID ),
			  'meta_input'   => array(
					'social_link' => $instaPost->link,
					'social_media' => 'Instagram',
					'hero'	=>	'No',
					'instaPostID'	=>	$instaPost->id
				),
			);
			
			// Insert the post into the database
			$new_post_id = wp_insert_post( $my_post );
			set_featured_image(insert_post_media($new_post_id, $instaPost->images->standard_resolution->url));
		}
	}
	die('Done');
}

