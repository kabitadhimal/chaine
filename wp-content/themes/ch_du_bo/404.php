<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Chaîne_du_Bonheur
 */

get_header(); ?>
<section class="section-backlink">
        <div class="container">
           <div class="btnback">
					<h1 class="page-title"><?php esc_html_e( 'Cette page est introuvable.', 'ch_du_bo' ); ?></h1>
           </div>
      	</div>
  	</section>
	<section class="section-news no-bg">
        <div class="container">
            <div class="row">
            	<div class="col-md-12">
					<p><?php echo sprintf(__('Cliquez ici pour rejoindre la <a href="%s">page d\'accueil</a><br> Ou cliquez sur la page que vous cherchez directement dans le menu','ch_du_bo'),home_url()); ?></p>
					<a class="btn rounded red big" href="<?php echo home_url(); ?>"><?php _e('page d\'accueil','ch_du_bo'); ?><a>
				</div>
			</div><!-- .page-content -->
		</div><!-- .error-404 -->

	</section><!-- #main -->

<?php
get_footer();
