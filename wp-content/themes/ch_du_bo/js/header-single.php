<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NAEF_Prestige
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php

  $urlID = $_GET['id'];
  if(ICL_LANGUAGE_CODE == 'fr')
    $transID = icl_object_id($post->ID, 'post', true,'en');
  if(ICL_LANGUAGE_CODE == 'en')
    $transID = icl_object_id($post->ID, 'post', true,'fr');
?>

<?php /* if(is_page_template('single-louer.php')): ?>
    <?php $share_img = social_image($urlID,'location'); //echo $share_img; die; ?>

    <?php $social_details = social_details($urlID,'cp_rent'); ?>    
    <meta property="og:image" content="<?php echo $share_img; ?>" />
    <meta property="og:title" content="<?php if(ICL_LANGUAGE_CODE == 'fr') echo $social_details->nom_fr; else echo $social_details->nom_en; ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo get_permalink($post->ID)."/?id=".$urlID; ?>" />   
    <meta property="og:description" content="<?php if(ICL_LANGUAGE_CODE == 'fr') echo $social_details->description_fr; else echo $social_details->description_en; ?>" />
    <meta property="og:site_name" content="<?php echo bloginfo('name'); ?>" />
<?php endif; ?>

<?php if(is_page_template('single-vente.php')): ?>

    <?php $share_img = social_image($urlID,'vente'); //echo $share_img; die; ?>

    <?php $social_details = social_details($urlID,'cp_vente'); ?>

    <meta property="og:image" content="<?php echo $share_img; ?>" />
    <meta property="og:title" content="<?php if(ICL_LANGUAGE_CODE == 'fr') echo $social_details->nom_fr; else echo $social_details->nom_en; ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo get_permalink($post->ID)."?id=".$urlID; ?>" />
    <meta property="og:description" content="<?php if(ICL_LANGUAGE_CODE == 'fr') echo $social_details->description_fr; else echo $social_details->description_en; ?>" />
    <meta property="og:site_name" content="<?php echo bloginfo('name'); ?>" />
<?php endif; */?>


<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php /* <link rel="canonical" href="<?php echo get_permalink($post->ID)."?id=".$urlID; ?>"> */ ?>

<!--<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBJWsP-7wLOPo8Eqit4cbCRi000MkhIo60&amp;libraries=places&amp;language=<?php echo ICL_LANGUAGE_CODE; ?>" async defer></script>
<script src="http://apis.google.com/js/platform.js" async defer></script>-->

<?php wp_head(); ?>

<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcuE9XwQvHEj6bLErU741PAiN6qhlQwUA&amp;libraries=places&amp;language=<?php echo ICL_LANGUAGE_CODE; ?>" async defer></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>-->

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcuE9XwQvHEj6bLErU741PAiN6qhlQwUA&amp;libraries=places&amp;language=<?php echo ICL_LANGUAGE_CODE; ?>" ></script>
<script src="https://apis.google.com/js/platform.js" ></script>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-318522-7', 'auto');
  ga('send', 'pageview');</script>
</head>

<?php
	/*if(is_page_template('page-templates/home-page.php'))
		$classes = "naef-index-body naef-home-body";*/
 /* if(is_page_template('page-templates/rent-listing-page.php'))
    $classes = "naef-search-page-body";*/
?>

<body <?php body_class($classes); ?>>

<?php
  if( function_exists('icl_get_languages') )
  {
    $languages = icl_get_languages('skip_missing=' . intval($skip_missing) . '&orderby=code&order=desc');
  }
?>

<!-- Naef Promotion page structure -->

<div class="naef-main-wrapper naef-site-outer"> 
  
  <!-- Naef header -->

  <?php $logo = get_field('naef_prestige_logo', 'option'); ?>
  <header id="naef-masthead" class="naef-masthead masthead maef-main-header" role="banner">
    <div class="container-fluid naef-container">
      <div class="naef-header-row <?php if( is_page_template('single-vente.php') || is_page_template('single-louer.php') || is_singular('promotion') ) echo ' naef-header-row-sm'; ?>">
        <div class="row clearfix naef-table-layout">
          <div class="naef-table-middle-cell naef-logo-wrap naef-lg-3 naef-xs-12">
            <figure> <a href="<?php echo icl_get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/header-template/naef-logo.png" alt="" /></a> </figure>
          </div>
          <div class="naef-table-middle-cell naef-sitenav-wrap naef-lg-9 naef-xs-12 naef-static-pos">
            <div class="row clearfix naef-table-layout">
              <div class="naef-lg-10 naef-xs-12 naef-nav-wrap padding-zero naef-static-pos">
                <nav class="main-nav site-nav" role="navigation"> 
                  
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="neaf-navbar-header">
                    <button type="button" class="navbar-toggle collapsed nav-closed" data-toggle="collapse" data-target="" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span><span class="icon-bar"></span> </button>
                  </div>
                  <!-- #Brand and toggle get grouped for better mobile display --> 
                  
                  <!-- Site main menu -->
                  <div class="collapse navbar-collapse naef-align-left" id="neaf-navbar-collapse">                    

                    <?php
                      $nav_menu = array(
                      'theme_location'    => 'primary',
                      'container'         => 'div',
                      'container_class'   => 'table-layout nav-inner',
                      'menu_class'        => 'naef-nav naef-table-layout',
                      'walker'            => new Menu_With_Description()
                      );
                      wp_nav_menu( $nav_menu );

                    ?>

                    <div class="naef-mbl-header-content">
                      <div class="naef-mbl-lang-link-block">                      
                        <ul class="naef-nav" role="menu">
                        <li class="naef-mbl-backoffice-picto">
                            <a href="#"></a>                            
                            <?php
                              $nav_menu = array(
                              'theme_location'    => 'header_right',                 
                              'container_class'        => '"naef-rich-menu-wrap neaf-mega-menu-wrap'                 
                              );
                              wp_nav_menu( $nav_menu );
                            ?>                                                    
                          </li>
                          <li class="">
                          <br>
                            <a href="#"><?php echo __("Language: ",'naef').strtoupper(ICL_LANGUAGE_CODE); ?></a>
                            <div class="naef-rich-menu-wrap neaf-mega-menu-wrap">
                              <ul>
                               <?php
                                 global $post;
                                  $urlID = $_GET['id'];
                                  $iclID = "";

                                  if( ICL_LANGUAGE_CODE == "fr"){
                                    $iclID = icl_object_id($post->ID, 'post', false,'en');                            
                                    $posts = get_post($iclID);
                                    $postSLug = $posts->post_name;
                                    $item = '<li class="naef-lang-item"><a class="naef-lang-data" href="'.site_url()."/en/".$postSLug."/?id=".$urlID.'">EN</a></li>';
                                  }
                                  else
                                  {
                                    $iclID = icl_object_id($post->ID, 'post', false,'fr');                            
                                    $posts = get_post($iclID);
                                    $postSLug = $posts->post_name;
                                    $item = '<li class="naef-lang-item"><a class="naef-lang-data" href="'.site_url()."/".$postSLug."/?id=".$urlID.'">FR</a></li>';
                                  }
                                  echo $item;
                                  ?>                                
                              </ul>
                            </div>
                          </li>
                          
                        </ul>
                      </div>
                    </div>          
                    
                  </div>
                  <!-- #Site main menu -->                   
                </nav>
              </div>


              <div class="naef-lg-2 naef-xs-12 naef-rightside-wrap padding-zero">
                 <?php
                      /*$nav_menu = array(
                      'theme_location'    => 'header_right',                 
                      'menu_class'        => 'naef-lang-picto-list'                 
                      );
                      wp_nav_menu( $nav_menu );*/
                    ?>

                    <?php custom_nav_menu_for_header('header_right'); ?>
              </div>


          </div>
        </div>
      </div>
    </div>
   </div>
  </header>
  <!-- #Naef header --> 

