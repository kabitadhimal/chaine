(function() {
    tinymce.PluginManager.add('fa_icon_button', function( editor, url ) {
        editor.addButton( 'fa_icon_button', {
            text: 'Icon',
            icon: false,
            onclick: function() {
                editor.windowManager.open( {
                    title: 'Social Icons',
                    body: [
                        {
                            type: 'textbox',
                            name: 'icon_name',
                            //label: tinyMCE_object.image_title,
                            label  : 'Icon name',
                            value: '',
                            classes: 'my_input_image',
                            tooltip: 'Icons name from Font Awesome. For example: facebook, twitter, instagram, etc',
                        },
                        {
                            type   : 'textbox',
                            name   : 'link',
                            label  : 'URL',
                            values : '',
                            value : 'http://procab.ch' // Sets the default
                        }
                    ],
                    onsubmit: function( e ) {
                        editor.insertContent( '[ch_icon icon="' + e.data.icon_name + '" link="' + e.data.link + '"]');
                    }
                });
            },
        });
    });
    tinymce.PluginManager.add('audio_player', function( editor, url ) {
        editor.addButton( 'audio_player', {
            text: 'Audio',
            icon: false,
            onclick: function() {
                editor.windowManager.open( {
                    title: 'Audio Player',
                    body: [
                        {
                            type: 'textbox',
                            name: 'link',
                            //label: tinyMCE_object.image_title,
                            label  : 'Link',
                            value: '',
                            classes: 'my_input_image',
                            tooltip: 'Audio file\'s link',
                        },
                        {
                            type: 'button',
                            name: 'my_upload_button',
                            label: '',
                            text: 'Upload',
                            classes: 'my_upload_button',
                        },//new stuff!
                    ],
                    onsubmit: function( e ) {
                        editor.insertContent( '[ch_audio_player link="' + e.data.link + '"]');
                    }
                });
            },
        });
    });
    tinymce.PluginManager.add('ch_download', function( editor, url ) {
        editor.addButton( 'ch_download', {
            text: 'Download',
            icon: false,
            onclick: function() {
                editor.windowManager.open( {
                    title: 'Download',
                    body: [
                        {
                            type: 'textbox',
                            name: 'link',
                            //label: tinyMCE_object.image_title,
                            label  : 'Link',
                            value: '',
                            classes: 'my_input_image',
                            tooltip: 'Audio file\'s link',
                        },
                        {
                            type: 'button',
                            name: 'my_upload_button',
                            label: '',
                            text: 'Upload',
                            classes: 'my_upload_button',
                        },//new stuff!
                    ],
                    onsubmit: function( e ) {
                        editor.insertContent( '[ch_download link="' + e.data.link + '"]');
                    }
                });
            },
        });
    });
    tinymce.PluginManager.add('ch_button', function( editor, url ) {
        editor.addButton( 'ch_button', {
            text: 'Button',
            icon: false,
            onclick: function() {
                editor.windowManager.open( {
                    title: 'Button',
                    body: [
                        {
                            type: 'textbox',
                            name: 'link',
                            label  : 'Link',
                            value: '',
                            classes: 'my_input_image',
                            tooltip: 'Please enter a valid URL',
                        },
                        {
                            type: 'textbox',
                            name: 'text',
                            label  : 'Text',
                            value: '',
                            classes: 'button-text',
                            tooltip: 'Please enter text for the button',
                        },
						{//add type select
							type: 'listbox',
							name: 'color',
							label: 'Type',
							value: '',
							'values': [								
								{text: 'Rounded Black', value: 'rounded black'},
								{text: 'Rounded Red', value: 'rounded red'},
								{text: 'Red Underlined', value: 'underline'},
								{text: 'Black Underlined', value: 'underline black'},
								{text: 'Grey Underlined', value: 'underline grey'},
								{text: 'Red Bordered', value: 'border red'},
								{text: 'Grey Bordered', value: 'border'},
							],
							tooltip: 'Select the color of the button'
						}
                    ],
                    onsubmit: function( e ) {
                        editor.insertContent( '[ch_button link="' + e.data.link + '" text="'+ e.data.text +'" type="'+ e.data.color +'"]');
                    }
                });
            },
        });
    });
 
})();

jQuery(document).ready(function($){
    $(document).on('click', '.mce-my_upload_button', upload_image_tinymce);
 
    function upload_image_tinymce(e) {
        e.preventDefault();
        var $input_field = $('.mce-my_input_image');
        var custom_uploader = wp.media.frames.file_frame = wp.media({
            /*title: 'Add Audio File',
            button: {
                text: 'Add Audio File'
            },*/
            multiple: false
        });
        custom_uploader.on('select', function() {
            var attachment = custom_uploader.state().get('selection').first().toJSON();
            $input_field.val(attachment.url);
        });
        custom_uploader.open();
    }
});