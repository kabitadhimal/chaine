<?php
	get_header(); the_post();
	$openNewWindow = get_field('open_in_new_window');
	$newWindowLink = get_field('new_window_link');
	if($openNewWindow && $openNewWindow[0] == 'Yes'){
		global $wp_query;
		$wp_query->set_404();
		status_header( 404 );
		get_template_part( 404 ); exit();
	}
	$banner_img = get_field('banner_image');
	if($banner_img){
		$img = $banner_img['sizes']['slider-img'];
	}else{
		$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'slider-img');
		if($img){
			$blurClass = 'featured-banner';
			$img = $img[0];
		}else{
			$img = get_template_directory_uri().'/images/cms-img-landscape.jpg';
		}
	}
	$displayBanner = false;
	$title_top = get_field('title_top');
	if($title_top != ""){ $displayBanner = true; }
	$title_bottom = get_field('title_bottom');
	if($title_bottom != ""){ $displayBanner = true; }
	$short_description = get_field('short_description');
	if($short_description != ""){ $displayBanner = true; }
	$copyright_text = get_field('copyright_text');
	if($img && $displayBanner):
		if($blurClass == 'featured-banner'):
		?>
		<section class="main-banner no-margin featured-banner">
	       <div class="item" style="background:url(<?php echo $img; ?>) no-repeat center center"></div>
			<div class="sliderCaption">
				<div class="container">
			        <div class="row">
			        	<div class="col-md-8 pull-left">
			        		<?php if($title_top != '' || $title_bottom != ''): ?>
			                	<h1><?php echo do_shortcode($title_top.' '.$title_bottom); ?></h1>
			                <?php endif; ?>
			              	<h4><?php echo do_shortcode($short_description); ?></h4>
			           </div>
			        </div>
			    </div>
			</div>

		   <?php if($copyright_text != ''): ?>
			   <div class="copyright"><?php echo $copyright_text; ?></div>
		   <?php endif; ?>
	    </section><!--/.main-banner-->
		<?php else: ?>
			<section class="main-banner no-margin">
		       <div class="item" style="background:url(<?php echo $img; ?>) no-repeat center center">
		            <div class="sliderCaption">
		            	<div class="container">
			                <div class="row">
			                	<div class="col-md-8 pull-left">
			                		<?php if($title_top != '' || $title_bottom != ''): ?>
				                    	<h1><?php echo do_shortcode($title_top.' '.$title_bottom); ?></h1>
				                    <?php endif; ?>
			                      	<h4><?php echo do_shortcode($short_description); ?></h4>
			                   </div>
			                </div>
		                </div>
		            </div>
		       </div>
			   <?php if($copyright_text != ''): ?>
				   <div class="copyright"><?php echo $copyright_text; ?></div>
			   <?php endif; ?>
		    </section><!--/.main-banner-->
		<?php endif; ?>
	<?php
	endif;
$ch_du_bo_content = new Ch_du_bo_Fund_Content();
$content = $ch_du_bo_content->ch_du_bo_get_flexible_content();
echo $content;
get_footer();
