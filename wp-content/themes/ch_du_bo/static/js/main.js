jQuery(function($) {'use strict',

	

	//Initiat WOW JS
	new WOW().init();

	
	$(window).scroll(function() {
    if ($(this).scrollTop() > 1){  
        $('#header').addClass("sticky");
    }
    else{
        $('#header').removeClass("sticky");
    }
});
	
	$( document ).ready( function() {
		  jQuery('header .collapse nav').meanmenu();
		  
		});

	
	//goto top
	$('.gototop').click(function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $("body").offset().top
		}, 500);
	});	

	
});