<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Chaîne_du_Bonheur
 */

get_header(); ?>

	<section class="section-backlink">
        <div class="container">
           <div class="btnback">           		
				<header class="page-header">
					<?php
						the_archive_title( '<h1 class="page-title">', '</h1>' );
						the_archive_description( '<div class="archive-description">', '</div>' );
					?>
				</header><!-- .page-header -->
           </div>
      	</div>
  	</section>
   <section class="section-news">
        <div class="container">
            <div class="row">
                
            
                <div class="news-listing">
                 <ul class="mobile-carousel">
                  <?php

                    // The Loop
                    if ( have_posts() ) {
                      while ( have_posts() ) {
                        the_post();
                        get_template_part('template-parts/content','news-post');
                      }
                      the_posts_navigation();
                    }
                     ?>
                </ul>  
                 </div> <!--/.news-listing-->
                
                
            </div><!--/.row-->
        </div><!--/.container-->
        
   </section>

<?php
get_footer();
