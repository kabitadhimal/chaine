<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Chaîne_du_Bonheur
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">


    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/png">
    <?php wp_head(); ?>
    <style>
        .page-overlay {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: rgba(0, 0, 0, 0.5);
            display: none;
        }

        .chaine-preloader-wrap {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate3d(-50%, -50%, 0);
            text-align: center;
        }

        .chaine-preloader-loading-text {
            text-align: center;
            display: inline-block;
        }

        .neaf-preloader-wrap {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate3d(-50%, -50%, 0);
            text-align: center;
        }

        .clearfix:before,
        .clearfix:after {
            content: "";
            display: table;
            overflow: hidden;
            height: 0;
        }

        .clearfix:after {
            clear: both;
        }

        #fountainTextG {
            width: 174px;
            margin: auto;
        }

        .chaine-preloader-loading-text {
            text-align: center;
            display: inline-block
        }

        @-webkit-keyframes rotating /* Safari and Chrome */
        {
            from {
                -ms-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -webkit-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            to {
                -ms-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -webkit-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes rotating {
            from {
                -ms-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -webkit-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            to {
                -ms-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -webkit-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        .rotating {
            -webkit-animation: rotating 2s linear infinite;
            -moz-animation: rotating 2s linear infinite;
            -ms-animation: rotating 2s linear infinite;
            -o-animation: rotating 2s linear infinite;
            animation: rotating 2s linear infinite;
        }

        .chaine-preloader-logo {
            animation: rotating 2s infinite 0.5s;
        }

    </style>
    <style type="text/css">
        img.section-video-bg {
            width: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        .full.grid-item {
            width: 100%;
        }
    </style>
    <style>
        #cookieMsg {
            position: relative;
            padding: 10px 15px;
            background: #3a3a3b;
            color: #ffffff;
            font-size: 12px;
            text-align: center;
        }

        #cookieMsg p {
            margin-bottom: 0;
        }

        #cookieMsg a {
            color: #e5222f;
            transition: color 0.25s ease-in-out;
        }

        #cookieMsg a:hover {
            color: #ffffff;
        }

        #cookieMsg .close {
            position: absolute;
            top: 50%;
            right: 15px;
            opacity: 1;
            box-shadow: none;
            color: #ffffff;
            transform: translateY(-50%);
        }
    </style>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            var $winwidth = jQuery(window).width();
            jQuery("img.section-video-bg").attr({
                width: $winwidth
            });
            jQuery(window).bind("resize", function () {
                var $winwidth = jQuery(window).width();
                jQuery("img.section-video-bg").attr({
                    width: $winwidth
                });
            });
        });
    </script>
</head>
<?php
$emergency = (is_emergency() && is_front_page());
$logo = ch_du_bo_get_logo(CH_LANG_CODE, $emergency);
$media_link = esc_url(get_theme_mod('media_link_' . CH_LANG_CODE));
if (!$media_link) {
    $media_link = '#';
}
$login_sao = esc_url(get_theme_mod('login_sao'));
if (!$login_sao) {
    $login_sao = '#';
}
$login_partnerweb = esc_url(get_theme_mod('login_partnerweb'));
if (!$login_partnerweb) {
    $login_partnerweb = '#';
}
$contact = esc_url(get_theme_mod('contact_' . CH_LANG_CODE));
if (!$contact) {
    $contact = '#';
}
?>


<body <?php body_class(); ?>>
<?php do_action('ch_du_bo_body'); ?>
<div class="news-loader page-overlay " style="display: none;">
    <div class="chaine-preloader-wrap">
        <div class="chaine-preloader-logo">
            <img src="<?php echo get_template_directory_uri() . '/images/logo_chaine_loader.png'; ?>"
                 class="iajax-loader">
        </div>
    </div>
</div>


<div id="cookieMsg" style="display: none">

    <?php if (ICL_LANGUAGE_CODE == 'de'): ?>
        <p>Diese Website verwendet Cookies für Analysen, personalisierte Inhalte und Werbung. Indem Sie diese
            Website nutzen, erklären Sie sich mit dieser Verwendung einverstanden. <a href="https://www.glueckskette.ch/datenschutzerklaerung/"> Mehr dazu</a>
        </p>
        <button type="button" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    <?php endif; ?>

    <?php if (ICL_LANGUAGE_CODE == 'fr'): ?>

        <p>Ce site utilise des cookies pour l'analyse, ainsi que pour les contenus et les publicités personnalisés. En continuant à naviguer sur ce site, vous acceptez cette utilisation. <a
                href="https://www.bonheur.ch/politique-de-protection-des-donnees/">En savoir
                plus</a></p>
        <button type="button" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    <?php endif; ?>
    <?php if (ICL_LANGUAGE_CODE == 'it'): ?>

        <p>Questo sito utilizza cookie per analisi, contenuti personalizzati e pubblicità. Continuando a navigare
            questo sito, accetti tale utilizzo. <a href="https://www.catena-della-solidarieta.ch/dichiarazione-sulla-protezione-dei-dati/">Per saperne di
                più</a></p>
        <button type="button" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    <?php endif; ?>

    <?php if (ICL_LANGUAGE_CODE == 'en'): ?>
        <p>This site uses cookies for analytics, personalized content and ads. By continuing to browse this site,
            you agree to this use. <a href="https://www.swiss-solidarity.org/data-privacy-statement/">Find out more.</a>
        </p>
        <button type="button" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

    <?php endif; ?>

</div>
<script type="text/javascript">
    // localStorage.setItem('show_cookie_info', true);
    var value = localStorage.getItem('hide_cookie_info');
    if (value) {
        jQuery("#cookieMsg").remove();

    }
    else {
        jQuery("#cookieMsg").show();

    }

    jQuery('.close').click(function () {
        localStorage.setItem('hide_cookie_info', true);
        jQuery('#cookieMsg').remove();
    });
</script>


<header id="header">
    <nav class="navbar">
        <div class="container">
            <div class="row">

                <div class="col-md-3 navbar-header">
                    <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>" rel="home"><img
                            src="<?php echo $logo; ?>" alt="<?php bloginfo(); ?>"></a>
                </div>

                <div class="col-md-9 navbar-right">
                    <div class="top-bar clearfix hidden-xs">
                        <div class="accRt">
                            <ul class="lang">
                                <li><a target="_blank"
                                       href="<?php echo $media_link; ?>"><?php _e('Médias', 'ch_du_bo'); ?></a></li>
                                <li><?php language_dropdown(); ?></li>
                            </ul>
                            <ul class="account">
                                <li>
                                    <div class="dropdown">
                                        <a class="dropdown-toggle acc" type="button" data-toggle="dropdown" href="#"><i
                                                class="fa fa-angle-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a target="_blank"
                                                   href="<?php echo $login_partnerweb; ?>"><?php _e('Partnerweb', 'ch_du_bo'); ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li><a class="mail" href="<?php echo $contact; ?>"></a></li>
                            </ul>
                        </div>
                    </div><!--/.top-bar-->
                    <div class="navbar-collapse collapse">
                        <nav>
                            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'primary',
                                'menu_id' => 'primary-menu',
                                'container' => 'ul',
                                'menu_class' => 'nav navbar-nav',
                                'walker' => new Primary_Nav_Walker()
                            ));
                            ?>
                        </nav>
                    </div>
                    <a class="btnDonate"
                       href="<?php echo get_donate_link(); ?>"><?php _e('Faire un don', 'ch_du_bo'); ?></a>
                </div>
            </div>
        </div><!--/.container-->
    </nav><!--/nav-->
</header>
