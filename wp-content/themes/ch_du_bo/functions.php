<?php
/**
 * Chaîne du Bonheur functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Chaîne_du_Bonheur
 */

function debug($data, $die = false){
	echo "<pre>";
	var_dump($data);
	$backTrace = debug_backtrace();
	echo "Debug in Path: ".$backTrace[0]['file']." and Line Number: ".$backTrace[0]['line'];
	echo "</pre>";
	if($die){
		die;
	}
}

define('CH_LANG_CODE', defined('ICL_LANGUAGE_CODE') ? ICL_LANGUAGE_CODE : 'de' );

if ( ! function_exists( 'ch_du_bo_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ch_du_bo_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Chaîne du Bonheur, use a find and replace
	 * to change 'ch_du_bo' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'ch_du_bo', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'post-formats', array( 'image', 'gallery', 'video' ) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'ch_du_bo' ),
		'footer' => esc_html__( 'Footer', 'ch_du_bo' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'ch_du_bo_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'ch_du_bo_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ch_du_bo_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ch_du_bo_content_width', 640 );
}
add_action( 'after_setup_theme', 'ch_du_bo_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ch_du_bo_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'ch_du_bo' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'ch_du_bo' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'ch_du_bo_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ch_du_bo_scripts() {
	wp_enqueue_style( 'ch_du_bo-style', get_stylesheet_uri() );

	/*wp_enqueue_script( 'ch_du_bo-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'ch_du_bo-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}*/

	/* wp_enqueue_style( 'ch_du_bo-icomoon', get_template_directory_uri().'/css/icomoon.css',array(),date('Ym'),'');
	wp_enqueue_style( 'ch_du_bo-boostrap', get_template_directory_uri().'/css/bootstrap.min.css',array(),date('Ym'),'');
	wp_enqueue_style( 'ch_du_bo-font-awesome', get_template_directory_uri().'/css/font-awesome.min.css',array(),date('Ym'),'');
	wp_enqueue_style( 'ch_du_bo-animate', get_template_directory_uri().'/css/animate.min.css',array(),date('Ym'),'');
	wp_enqueue_style( 'ch_du_bo-main', get_template_directory_uri().'/css/main.css',array(),date('Ym'),'');
	wp_enqueue_style( 'ch_du_bo-owl-carousel', get_template_directory_uri().'/css/owl.carousel.css',array(),date('Ym'),'');
	wp_enqueue_style( 'ch_du_bo-owl-theme', get_template_directory_uri().'/css/owl.theme.css',array(),date('Ym'),'');
	wp_enqueue_style( 'ch_du_bo-responsive', get_template_directory_uri().'/css/responsive.css',array(),date('Ym'),'');

	wp_enqueue_style( 'ch_du_bo-meanmenu', get_template_directory_uri().'/css/meanmenu.css',array(),date('Ym'));
	
	wp_enqueue_style( 'ch_du_bo-slider-pro', get_template_directory_uri().'/css/slider-pro.min.css',array(),date('Ym'));
	wp_enqueue_style( 'ch_du_bo-slider-pro-examples', get_template_directory_uri().'/css/examples.css',array(),date('Ym')); */
	wp_enqueue_style( 'ch_du_bo-main-style', get_template_directory_uri().'/css/style.css',array(),date('Ym')); 
	
	

	if(!is_page_template('page-templates/news-template.php')) {
		
	}

	wp_enqueue_script( 'ch_du_bo-masonary2', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'), date('Ym'), true );
     //wp_enqueue_script( 'ch_du_bo-isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'), date('Ym'), false );
	 
		//wp_enqueue_style( 'ch_du_bo-multiselect', get_template_directory_uri().'/css/bootstrap-multiselect.css',array(),date('Ym'));
		wp_enqueue_script( 'ch_du_bo-multiselect', get_template_directory_uri() . '/js/bootstrap-multiselect.js', array('jquery'), date('Ym'), true );
		wp_enqueue_script( 'ch_du_bo-imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array('jquery'), date('Ym'), true );
	
	/* wp_enqueue_style( 'ch_du_bo-fancybox', get_template_directory_uri().'/css/jquery.fancybox.css',array(),date('Ym'));
	wp_enqueue_style( 'ch_du_bo-nice-select', get_template_directory_uri().'/css/nice-select.css',array(),date('Ym'));

	wp_enqueue_style( 'ch_du_bo-audio-video', get_template_directory_uri().'/css/audio-video.css',array(),date('Ym')); */



	wp_enqueue_script( 'ch_du_bo-fancybox', get_template_directory_uri() . '/js/jquery.fancybox.pack.js', array('jquery'), date('Ym'), true );
	wp_enqueue_script( 'ch_du_bo-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), date('Ym'), true );
	wp_enqueue_script( 'ch_du_bo-owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), date('Ym'), true );
	wp_enqueue_script( 'ch_du_bo-meanmenu', get_template_directory_uri() . '/js/jquery.meanmenu.js', array('jquery'), date('Ym'), true );
	wp_register_script( 'ch_du_bo-main', get_template_directory_uri() . '/js/main.js', array('jquery'), date('Ym'), true );
	//wp_register_script( 'ch_du_bo-mailchimp', '//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js', array('jquery'), date('Ym'), false );
	//wp_register_script( 'ch_du_bo-masonary', get_template_directory_uri() . '/js/masonry.pkgd.js', array('jquery'), date('Ym'), true );



	// Localize the script with new data
	$translation_array = array(
		'ajax_url' => admin_url('/admin-ajax.php'),
		'site_url' => site_url('/'),
		'site_lang' => CH_LANG_CODE,
		'page_template' => basename( get_page_template() ),
		'newsletter_text' => __("Je m'inscris à la newsletter", 'ch_du_bo')
	);
	wp_localize_script( 'ch_du_bo-main', 'ch', $translation_array );

	// Enqueued script with localized data.
	wp_enqueue_script( 'ch_du_bo-main' );
	wp_enqueue_script( 'ch_du_bo-wow', get_template_directory_uri() . '/js/wow.min.js', array('jquery'), date('Ym'), true );
	wp_enqueue_script( 'ch_du_bo-wow', get_template_directory_uri() . '/js/wow.min.js', array('jquery'), date('Ym'), true );
	wp_enqueue_script( 'ch_du_bo-nice-select', get_template_directory_uri() . '/js/jquery.nice-select.min.js', array('jquery'), date('Ym'), true );

	wp_enqueue_script( 'ch_du_bo-audio2', get_template_directory_uri() . '/js/audio-video.min.js', array('jquery'), date('Ym'), true );
	wp_enqueue_script( 'ch_du_bo-audio', get_template_directory_uri() . '/js/html5-audio.js', array('jquery'), date('Ym'), true );
	//wp_enqueue_script( 'ch_du_bo-modulo', get_template_directory_uri() . '/js/modulo-columns.js', array('jquery'), date('Ym'), true );
	//wp_enqueue_script( 'ch_du_bo-masonry.ordered', get_template_directory_uri() . '/js/masonry.ordered.js', array('jquery'), date('Ym'), true );
	
	
wp_enqueue_script( 'ch_du_bo-sliderPro-min', get_template_directory_uri() . '/js/slider/jquery.sliderPro.min.js', array('jquery'), date('Ym'), true );

}
add_action( 'wp_enqueue_scripts', 'ch_du_bo_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
require get_template_directory() . '/inc/emergency.php';
require get_template_directory() . '/inc/walker.php';
require get_template_directory() . '/inc/shortcodes.php';
require get_template_directory() . '/inc/footer-scripts.php';
require get_template_directory() . '/inc/cms-template-tags.php';
require get_template_directory() . '/inc/home-template-tags.php';
require get_template_directory() . '/inc/social/fb/functions.php';
require get_template_directory() . '/inc/social/twitter/functions.php';
require get_template_directory() . '/inc/social/instagram/functions.php';
require get_template_directory() . '/inc/tinymce-custom-styles.php';
require get_template_directory() . '/inc/tinymce-buttons.php';
require get_template_directory() . '/inc/ajax-functions.php';
require get_template_directory() . '/inc/fb-cron.php';
require get_template_directory() . '/inc/instagram-cron.php';
require get_template_directory() . '/inc/donation.php';
require get_template_directory() . '/inc/rss.php';

//Disabling Comments
function disable_comments_for_all($open,$post_id){
    return false;
}
add_filter( 'comments_open','disable_comments_for_all', 10, 2 );



add_image_size( 'megamenu-img', 268, 182, true );
add_image_size( 'author-img', 369, 236, true );
add_image_size( 'slider-img', 1903, 626, true );
add_image_size( 'fundraising-thumb', 360, 253, true );
add_image_size( 'hero-thumb', 770, 260, true );
add_image_size( 'testimonail-thumb', 534, 341, true );

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

}

function language_dropdown(){
	$langs = icl_get_languages('skip_missing=0&orderby=KEY&order=DIR&link_empty_to=str');
	
	?>
	<div class="langDrop dropdown">
		<a class="dropdown-toggle" type="button" data-toggle="dropdown" href="#"><?php echo strtoupper(CH_LANG_CODE); ?><i class="fa fa-angle-down"></i></a>
		<?php if(!empty($langs)){ ?>
			<ul class="dropdown-menu">
			<?php foreach($langs as $lang){
				if($lang['active'])
					continue;
				?>
		
				<li><a href="<?php echo $lang['missing']==1?'https://'.$lang['url']:$lang['url']; ?>"><?php echo strtoupper($lang['language_code']); ?></a></li>
			<?php } ?>
			</ul>
		<?php } ?>
	</div>
	<?php
}

function ch_du_bo_get_logo($lang, $emergency){
	if($emergency){
		$logoPath = ABSPATH.'wp-content/uploads/ch/logo-emergency-'.$lang.'.svg';
		if(!file_exists($logoPath)){
			$logo = esc_url( get_theme_mod( 'ch_du_bo_custom_emergency_logo_'.CH_LANG_CODE ));
			return $logo;
		}else{
			$logo = site_url('/').'wp-content/uploads/ch/logo-emergency-'.$lang.'.svg';
			return $logo;
		}

	}else{
		$logoPath = ABSPATH.'wp-content/uploads/ch/logo-'.$lang.'.svg';
		if(!file_exists($logoPath)){
			$logo = esc_url( get_theme_mod( 'ch_du_bo_custom_logo_'.CH_LANG_CODE ));
			return $logo;
		}else{
			$logo = site_url('/').'wp-content/uploads/ch/logo-'.$lang.'.svg';
			return $logo;
		}
	}
	return $logo;
}




// get the donate link according to language
function get_donate_link(){
	$link = esc_url( get_theme_mod( 'donate_link_'.CH_LANG_CODE ));
	if(!$link)
		$link = '#';
	return $link;
}

function custom_excerpt_length( $length ) {
	return 50;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function custom_excerpt_more( $more ) {
	return ' ';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );

function sortByCreatedTime($a, $b) {
	$a['created_time'] = ($a['created_time']);
	$b['created_time'] = ($a['created_time']);
    return $a['created_time'] + $b['created_time'];
}

/**
 * Function to resize image
 *
 * @param string $url
 * @param integer $width
 * @param integer $height
 * @return string
 */
function ch_du_bo_img_resize($url, $width='', $height='', $quality = 80) {
    $imgPath = pathinfo($url);
    $originalPath = str_replace(site_url('/'), ABSPATH, $url);

    if(file_exists($originalPath)){
        $requiredFileName = $imgPath['dirname'].'/'.$imgPath['filename'].'-'.$width.'x'.$height.'.'.$imgPath['extension'];
        $requiredPath = str_replace(site_url('/'), ABSPATH, $requiredFileName);

        if(!file_exists($requiredPath)){
            $image = wp_get_image_editor( $url );
            if ( ! is_wp_error( $image ) ) {
                $image->resize( $width, $height, true );
                $image->set_quality($quality);
                $image->save( $requiredPath );
            }else{
				return $url;
			}
        }
        return $requiredFileName;
    }else{
        return get_template_directory_uri().'/images/no-image.png';
    }
}

function get_news_page_link(){
	$post = get_field('news_page_link','option');
	return get_permalink( $post->ID );
}

add_filter( "wp_nav_menu_items", function($items, $args){
	if($args->theme_location != 'footer'){
		return $items;
	}
	$premenuitem = html_entity_decode(get_theme_mod('ch_du_bo_footer_left_content_'.CH_LANG_CODE));
	$postmenuitem = get_theme_mod('ch_du_bo_footer_right_content_'.CH_LANG_CODE);
	if($premenuitem)
		$addBefore = "<li>".$premenuitem."</li>";

	if($postmenuitem)
		$addAfter = '<li class="websiteby">'.html_entity_decode($postmenuitem).'</li>';

	$items = $addBefore."\n".$items."\n".$addAfter;
	//debug($items);
	return $items;
},10,2 );


add_action('rss2_item',function(){
	global $post;
	if(has_post_thumbnail($post->ID)){
		 $img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
		 echo "<custImg>".$img[0]."</custImg>";
	}
});

add_action( 'wp_import_insert_post', function($post_id, $original_post_ID, $postdata, $post){

},10,4);

function get_post_icon($post_id,$social = false){
		$iconDisp = '<img src="'.get_template_directory_uri().'/images/icon-news.png" />';
		$terms = wp_get_post_terms($post_id,'category');
		$cat = new WPSEO_Primary_Term('category', $post_id);
		$cat = $cat->get_primary_term();	

		if($cat){
			$term = get_term_by('id', $cat, 'category');
			$icons = glob(ABSPATH.'wp-content/uploads/ch/category-'.$term->slug.".*");
			if(empty($icons)){			
				$icon = get_field('icon',"category_".$term->term_id);
				if($icon){
					$iconDisp = '<img src="'.$icon.'" />';
				}
			}else{
				$icon = str_replace(ABSPATH, site_url('/'), $icons[0]);
				$iconDisp = '<img src="'.$icon.'" />';
			}
		}else{			
			if($social == 'Facebook' || $social == 'Instagram'){				
				if($social == 'Facebook'){
					$term = 'facebook';
				}else if($social == 'Instagram'){
					$term = 'instagram';
				}
				$icons = glob(ABSPATH.'wp-content/uploads/ch/category-'.$term.".*");
				if(empty($icons)){			
					$icon = get_field('icon',"category_".$term);
					if($icon){
						$iconDisp = '<img src="'.$icon.'" />';
					}
				}else{
					$icon = str_replace(ABSPATH, site_url('/'), $icons[0]);
					$iconDisp = '<img src="'.$icon.'" />';
				}
			}else{
				foreach($terms as $term){
					/* if($term->parent == 0){
						continue;
					} */
					$icons = glob(ABSPATH.'wp-content/uploads/ch/category-'.$term->slug.".*");
					if(empty($icons)){			
						$icon = get_field('icon',"category_".$term->term_id);
						if($icon){
							$iconDisp = '<img src="'.$icon.'" />';
						}
					}else{
						$icon = str_replace(ABSPATH, site_url('/'), $icons[0]);
						$iconDisp = '<img src="'.$icon.'" />';
					}
					
					break;
				}
			}			
		}
		
		return $iconDisp;
}

// hook into wpcf7_before_send_mail
add_action( 'wpcf7_before_send_mail', function($contact_form){

	$submission = WPCF7_Submission::get_instance();

	$posted_data = $submission->get_posted_data();

	$toEmail = $posted_data['to'];
	
	$civilite = str_replace('.','',$_POST['civilite']);
	
	$ville = '';
	if(isset($_POST['ville'])){
		$ville = $_POST['ville'];
	}
		
	if(isset($_POST['sendToMailchimp'])){
		$mailChimpID = get_theme_mod('ch_du_bo_mailchimp_list_id_'.CH_LANG_CODE);
		$sendToMailchimp = $_POST['sendToMailchimp'];
		switch(CH_LANG_CODE){
			case 'en':
				$action = 'swiss-solidarity';
				break;
			case 'fr':
				$action = 'bonheur';
				break;
			case 'it':
				$action = 'catena-della-solidarieta';
				break;
			default:
				$action = 'glueckskette';
				break;
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"http://".$action.".us3.list-manage.com/subscribe/post?u=a905c457c17f2185048d2218f&id=".$mailChimpID);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "MERGE0=".$_POST['email']."&MERGE5=".$civilite."&MERGE1=".$_POST['prenom']."&MERGE2=".$_POST['nom']."&MERGE3=".$ville);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);
		
		/* ob_start();
		var_dump($server_output);
		$msg = ob_get_contents();
		ob_end_clean();
		
		$fp = fopen(ABSPATH.'data-cf7-mailchimp.txt', 'w');
		fwrite($fp, $msg);
		fclose($fp); */
		

		curl_close ($ch);
	}	
	
	/* writing data to file for debuging */
	/* ob_start();
	print_r($_POST);
	$msg = ob_get_contents();
	ob_end_clean();
	
	$fp = fopen(ABSPATH.'data-cf7.txt', 'w');
	fwrite($fp, $msg);
	fclose($fp); */
	/* writing data to file for debuging */

	if($_POST['_wpcf7'] == '26870' || $_POST['_wpcf7'] == '26871'){
		return;
	}
	
	$mail = $contact_form->prop('mail');

	$mail['recipient'] = $_POST['to'];
	//$mail['recipient'] = 'karun.shakya45@gmail.com';

	$contact_form->set_properties( array('mail' => $mail) );
});

add_filter( 'wpcf7_validate_email*', 'custom_email_confirmation_validation_filter', 20, 2 );
add_filter( 'wpcf7_validate_email', 'custom_email_confirmation_validation_filter', 20, 2 );
 
function custom_email_confirmation_validation_filter( $result, $tag ) {
    $tag = new WPCF7_FormTag( $tag );
 //debug($tag->name);
    if ( 'to' == $tag->name ) {
        $to_email = isset( $_POST['to'] ) ? trim( $_POST['to'] ) : '';
 
        if ( $to_email == "" ) {
            $result->invalidate( $tag, __('Le sujet est requis.','ch_du_bo') );			
			//add_action('wpcf7_display_message','update_error_msg_for_subject', 999999, 2);
        }
    }
 
    return $result;
}

function update_error_msg_for_subject($messages, $status){
	if($status == 'validation_error')
		return __('Le sujet est requis.','ch_du_bo');
	else
		return $messages;
}

/**
 *
 * @param string $text String to truncate.
 * @param integer $length Length of returned string, including ellipsis.
 * @param string $ending Ending to be appended to the trimmed string.
 * @param boolean $exact If false, $text will not be cut mid-word
 * @param boolean $considerHtml If true, HTML tags would be handled correctly
 * @return string Trimmed string.
 */
function ch_du_bo_truncate($text, $length = 100, $ending = '...', $exact = false, $considerHtml = true) {
    if ($considerHtml) {
        // if the plain text is shorter than the maximum length, return the whole text
        if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
            return $text;
        }
        // splits all html-tags to scanable lines
        preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
        $total_length = strlen($ending);
        $open_tags = array();
        $truncate = '';
        foreach ($lines as $line_matchings) {
            // if there is any html-tag in this line, handle it and add it (uncounted) to the output
            if (!empty($line_matchings[1])) {
                // if it's an "empty element" with or without xhtml-conform closing slash
                if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                    // do nothing
                    // if tag is a closing tag
                } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                    // delete tag from $open_tags list
                    $pos = array_search($tag_matchings[1], $open_tags);
                    if ($pos !== false) {
                        unset($open_tags[$pos]);
                    }
                    // if tag is an opening tag
                } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                    // add tag to the beginning of $open_tags list
                    array_unshift($open_tags, strtolower($tag_matchings[1]));
                }
                // add html-tag to $truncate'd text
                $truncate .= $line_matchings[1];
            }
            // calculate the length of the plain text part of the line; handle entities as one character
            $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
            if ($total_length + $content_length > $length) {
                // the number of characters which are left
                $left = $length - $total_length;
                $entities_length = 0;
                // search for html entities
                if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                    // calculate the real length of all entities in the legal range
                    foreach ($entities[0] as $entity) {
                        if ($entity[1] + 1 - $entities_length <= $left) {
                            $left--;
                            $entities_length += strlen($entity[0]);
                        } else {
                            // no more characters left
                            break;
                        }
                    }
                }
                $truncate .= substr($line_matchings[2], 0, $left + $entities_length);
                // maximum lenght is reached, so get off the loop
                break;
            } else {
                $truncate .= $line_matchings[2];
                $total_length += $content_length;
            }
            // if the maximum length is reached, get off the loop
            if ($total_length >= $length) {
                break;
            }
        }
    } else {
        if (strlen($text) <= $length) {
            return $text;
        } else {
            $truncate = substr($text, 0, $length - strlen($ending));
        }
    }
    // if the words shouldn't be cut in the middle...
    if (!$exact) {
        // ...search the last occurance of a space...
        $spacepos = strrpos($truncate, ' ');
        if (isset($spacepos)) {
            // ...and cut the text in this position
            $truncate = substr($truncate, 0, $spacepos);
        }
    }
    // add the defined ending to the text
    $truncate .= $ending;
    if ($considerHtml) {
        // close all unclosed html-tags
        foreach ($open_tags as $tag) {
            $truncate .= '</' . $tag . '>';
        }
    }
    return $truncate;
}

add_filter( 'wp_nav_menu_items', function($nav_menu, $args){
	if($args->theme_location == 'primary'){
		$media_link = esc_url( get_theme_mod( 'media_link_'.CH_LANG_CODE ) );
		if(!$media_link){ $media_link = '#'; }
		$login_sao = esc_url( get_theme_mod( 'login_sao' ) );
		if(!$login_sao){ $login_sao = '#'; }
		$login_partnerweb = esc_url( get_theme_mod( 'login_partnerweb' ) );
		if(!$login_partnerweb){ $login_partnerweb = '#'; }

		ob_start();

		?>
				<li class="hidden-xs"><a href="<?php echo $media_link; ?>"><?php _e('Médias','ch_du_bo'); ?></a></li>
				<li class="hidden-xs"><?php language_dropdown(); ?></li>
				<li class="hidden-xs">
					<div class="dropdown">
						<a class="dropdown-toggle acc" type="button" data-toggle="dropdown" href="#"><span class="fa fa-user"></span><i class="fa fa-angle-down"></i></a>
						<ul class="dropdown-menu">


							<li><a target="_blank" href="<?php echo $login_partnerweb; ?>"><?php _e('Partnerweb','ch_du_bo'); ?></a></li>
						</ul>
					</div>
				</li>
				<li class="hidden-xs"><a class="acc" href="<?php echo home_url('/contact'); ?>"><span class="fa fa-envelope"></span></a></li>
		<?php

		$content = ob_get_contents();
		ob_end_clean();

		$nav_menu .= $content;
	}
	return $nav_menu;
}, 10, 2);

function get_mailchimp_subscription_form($listID){
	switch(CH_LANG_CODE){
		case 'en':
			$action = 'swiss-solidarity';
			break;
		case 'fr':
			$action = 'bonheur';
			break;
		case 'it':
			$action = 'catena-della-solidarieta';
			break;
		default:
			$action = 'glueckskette';
			break;
	}
	?>
	<div class="newsletter">
        <!-- Begin MailChimp Signup Form -->
		<h3><?php _e('Newsletter','ch_du_bo'); ?></h3>
		<p><?php _e('Nous vous informons régulièrement sur notre travail et l\'emploi des dons.','ch_du_bo'); ?></p>
        <div id="mc_embed_signup">
		<form action="//<?php echo $action; ?>.us3.list-manage.com/subscribe/post?u=a905c457c17f2185048d2218f&amp;id=<?php echo $listID; ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">

        <div class="mc-field-group">
            <input type="email" value="" name="EMAIL" placeholder="<?php _e('Votre e-mail','ch_du_bo'); ?>..." class="required email newsletter-form" id="mce-EMAIL">
            <input type="submit" value="<?php _e('GO','ch_du_bo'); ?>" name="subscribe" id="mc-embedded-subscribe" class="submit">
        </div>
            <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_bf1c1bee9948229a202de6939_<?php echo $listID; ?>" tabindex="-1" value=""></div>

            </div>
        </form>
        </div>
        <!--End mc_embed_signup-->
    </div>
	<?php
}


add_action('admin_menu', 'wpdocs_register_my_custom_submenu_page');

function wpdocs_register_my_custom_submenu_page() {
    add_submenu_page(
        'tools.php',
        'Facebook Posts Import',
        'Facebook Posts Import',
        'manage_options',
        'fb-posts-import',
        'ch_du_bo_fb_posts_import_callback' );
}

function ch_du_bo_fb_posts_import_callback() {
    ?>
	<div class="wrap"><div id="icon-tools" class="icon32"></div><h2>Facebook Posts Import</h2></div>
	<input id="fb-import" type="button" value="Import">
	<div class="loading" style="display:none">
	<h2>Please wait while we import Facebook Posts. Do not close or move away from this page.</h2>
	<img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif"></div>
	<div id="fbResponse"></div>
	<script>
	jQuery("#fb-import").click(function(){
		jQuery(this).attr('disabled','disabled');
		jQuery('.loading').show();
		jQuery.ajax({
			method: "GET",
			url: "<?php echo admin_url('/admin-ajax.php'); ?>",
			data: { action: "ajax_fb_import", import: "FB", pwd: "a98d7" }
		}).done(function( msg ) {
			jQuery("#fb-import").removeAttr('disabled');
			jQuery('#fbResponse').html(msg);
			jQuery('.loading').hide();
		});
	});
	</script>
	<?php
}

add_action('admin_menu', 'wpdocs_register_instagram_submenu_page');

function wpdocs_register_instagram_submenu_page() {
    add_submenu_page(
        'tools.php',
        'Instagram Posts Import',
        'Instagram Posts Import',
        'manage_options',
        'instagram-posts-import',
        'ch_du_bo_instagram_posts_import_callback' );
}

function ch_du_bo_instagram_posts_import_callback() {
    ?>
	<div class="wrap"><div id="icon-tools" class="icon32"></div><h2>Instagram Posts Import</h2></div>
	<?php
	// initialize class
	$instaMan = new Instagram_Manager();
	$loginUrl = $instaMan->get_login_url();
	?>
	<a class="login" href="<?php echo $loginUrl ?>">» New Authentication with Instagram</a><br>
	<?php 
		if(isset($_GET['code'])){
		
			// grab OAuth callback code
			$code = $_GET['code'];
			
			$instaMan = new Instagram_Manager();
			
			$instaMan->authenticate($code);		
			
			echo "Authentication Successful. Please use the Import button to import new posts.<br>";
		
		}
	?>
	<input id="instagram-import" type="button" value="Import">
	<div class="loading" style="display:none">
	<h2>Please wait while we import Instagram Posts. Do not close or move away from this page.</h2>
	<img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif"></div>
	<div id="instagramResponse"></div>
	<script>
	jQuery("#instagram-import").click(function(){
		jQuery(this).attr('disabled','disabled');
		jQuery('.loading').show();
		jQuery.ajax({
			method: "GET",
			url: "<?php echo admin_url('/admin-ajax.php'); ?>",
			data: { action: "ajax_instagram_import", import: "Instagram", pwd: "a98d7" }
		}).done(function( msg ) {
			jQuery("#instagram-import").removeAttr('disabled');
			jQuery('#instagramResponse').html(msg);
			jQuery('.loading').hide();
		});
	});
	</script>
	<?php
}

function _remove_script_version( $src ){
	$parts = explode( '?ver', $src );
	return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

// Defer Javascripts
// Defer jQuery Parsing using the HTML5 defer property
if (!(is_admin() )) {
    function defer_parsing_of_js ( $url ) {
        if ( FALSE === strpos( $url, '.js' ) ) return $url;
        if ( strpos( $url, 'jquery.js' ) ) return $url;
        // return "$url' defer ";
        return "$url' defer onload='";
    }
    add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );
}

add_action( 'init', 'create_fundraising_category' );

function create_fundraising_category() {
	register_taxonomy(
		'fundraising-category',
		'fundraising-campaign',
		array(
			'label' => __( 'Fundraising Categories' ),
			'rewrite' => array( 'slug' => 'fundraising-category' ),
			'hierarchical' => true,
		)
	);
}

function add_svg_to_upload_mimes( $upload_mimes ) {
	$upload_mimes['svg'] = 'image/svg+xml';
	$upload_mimes['svgz'] = 'image/svg+xml';
	return $upload_mimes;
}
add_filter( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );


add_filter('getimagesize_mimes_to_exts',function($type){
	$type['image/svg+xml'] = 'svg';
	$type['image/svg'] = 'svg';
	$type['application/octet-stream'] = 'svg';
	return $type;
});

/*
 * Adjust IDs of category based on WPML
 */
function lang_category_id($id){
	if(function_exists('icl_object_id')) {
		return icl_object_id($id,'category',true);
	} else {
		return $id;
	}
}
$id = lang_category_id(13);

/* Google analytics */
add_action('wp_head', function(){
	?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore (j,f);
	})(window,document,'script','dataLayer','GTM-KMP9MMS');</script>
	<!-- End Google Tag Manager -->
	<?php
	/* switch(CH_LANG_CODE){
		case 'fr':
			$gaCode = 'UA-40884908-2';
			break;
		case 'en':
			$gaCode = 'UA-40884908-1';
			break;
		case 'de':
			$gaCode = 'UA-40884908-3';
			break;
		case 'it':
			$gaCode = 'UA-40884908-4';
			break;
		default:
			$gaCode = 'UA-40884908-2';
	}
	?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $gaCode; ?>"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', '<?php echo $gaCode; ?>');
	</script>
	<?php */
});

/* Twitter analytics */
add_action('wp_head', function(){
	switch(CH_LANG_CODE){
		case 'fr':
			$twCode = 'nxiv0';
			break;
		case 'en':
			$twCode = 'nxn38';
			break;
		case 'de':
			$twCode = 'nxiv1';
			break;
		case 'it':
			$twCode = 'nxn3b';
			break;
		default:
			return;
			$twCode = 'nxiv0';
	}
	?>
	<!-- Twitter universal website tag code --> 
	<script> 
	!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments); 
	},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js', 
	a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script'); 
	// Insert Twitter Pixel ID and Standard Event data below 
	twq('init', '<?php echo $twCode; ?>'); 
	twq('track','PageView'); 
	</script> 
	<!-- End Twitter universal website tag code --> 
	<?php
});

/* Facebook Pixel Code */
add_action('wp_head', function(){
	?>
	<!-- Facebook Pixel Code --> 
<script> 
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod? 
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n; 
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0; 
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window, 
document,'script','https://connect.facebook.net/en_US/fbevents.js'); 
fbq('init', '896126720462747'); // Insert your pixel ID here. 
fbq('track', 'PageView'); 
</script> 
<noscript><img height="1" width="1" style="display:none" 
src="https://www.facebook.com/tr?id=896126720462747&ev=PageView&noscript=1 " 
/></noscript> 
<!-- DO NOT MODIFY --> 
<!-- End Facebook Pixel Code --> 
	<?php
});

/* Linkedin code */
add_action('wp_head', function(){
	?>
	<script type="text/javascript"> 
		_linkedin_data_partner_id = "54733"; 
	</script><script type="text/javascript"> 
	(function(){var s = document.getElementsByTagName("script" )[0]; 
	var b = document.createElement("script" ); 
	b.type = "text/javascript";b.async = true; 
	b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js "; 
	s.parentNode.insertBefore(b, s);})(); 
	</script> 
	<noscript> 
	<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=54733&fmt=gif " /> 
	</noscript> 
	<?php
});

/* googleadservices code */
add_action('ch_du_bo_body', function(){
	?>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMP9MMS"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<script type="text/javascript"> 
	/* <![CDATA[ */ 
	var google_conversion_id = 980124742; 
	var google_custom_params = window.google_tag_params; 
	var google_remarketing_only = true; 
	/* ]]> */ 
	</script> 
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"> 
	</script> 
	<noscript> 
	<div style="display:inline;"> 
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/980124742/?guid=ON&amp;script=0"/> 
	</div> 
	</noscript>
	<?php
});

/* add_filter ('wpml_translation_status', function($status){
	return false;
}); */

add_filter( 'cron_schedules', 'chaine_add_cron_interval' ); 
function chaine_add_cron_interval( $schedules ) {
    $schedules['thricedaily'] = array(
        'interval' => 8 * HOUR_IN_SECONDS,
        'display'  => esc_html__( 'Thrice Daily' ),
    );
 
    return $schedules;
}

if ( ! wp_next_scheduled( 'chaine_cron_hook' ) ) {
  wp_schedule_event( time(), 'thricedaily', 'chaine_cron_hook' );
}

add_action( 'chaine_cron_hook', 'clear_cache' );

function clear_cache() {
	if(function_exists('w3tc_flush_all')){
		w3tc_flush_all();
		$to = 'karun.shakya45@gmail.com';
		$subject = 'Chaine cron ran successfully';
		$body = 'Hello, We are pleased to inform you that cache for Bonheur.ch has been cleared.';
		$headers[] = 'From: Chaine CRON <bonheur@bonheur.ch>';
		$headers[] = 'Content-Type: text/html; charset=UTF-8';
		 
		wp_mail( $to, $subject, $body, $headers );		
	}
}

add_action('wp_enqueue_scripts', function(){
	remove_action('wp_enqueue_scripts','osc_add_dynamic_css',100);
});
/*
 * Registering Google Map API Key for acf
 */
/*function app_acf_init( $api ){
    $api['key'] = 'AIzaSyAv0MJAZzvRtZpZHEBdm-4I_5EfsNrygNQ';
    return $api;
}
add_filter('acf/fields/google_map/api', 'app_acf_init');*/
/*function app_acf_google_map_api( $api ){
    $api['key'] = "AIzaSyAv0MJAZzvRtZpZHEBdm-4I_5EfsNrygNQ";
    return $api;
}

add_filter('acf/fields/google_map/api', 'app_acf_google_map_api');*/

function app_acf_google_map_api( $api ){
    //$api['key'] = 'AIzaSyAv0MJAZzvRtZpZHEBdm-4I_5EfsNrygNQ';
    $api['key'] = 'AIzaSyAcGp_Miss7QiqDo_vzZTsTBk4UxEsaIvA';
    return $api;
}

add_filter('acf/fields/google_map/api', 'app_acf_google_map_api');