<?php
/*
  Template Name: Contact
*/
  get_header(); the_post();

  $banner_img = get_field('banner_image');
  $disable_banner = get_field('disable_banner');  
  $title_top = get_field('title_top');
  $title_bottom = get_field('title_bottom');
  $short_description = get_field('short_description');
  if(!$disable_banner):
  ?>
  <section class="main-banner no-margin">           
       <div class="item" style="background:url(<?php echo $banner_img['url']; ?>) no-repeat center center">
            <div class="sliderCaption">
              <div class="container">
                  <div class="row">
                      <?php if($title_top != '' || $title_bottom != ''): ?> 
                          <h1 class="fund-title"><?php echo $title_top.'<br>'.$title_bottom; ?></h1>
                        <?php endif; ?>
                          <p><?php echo $short_description; ?></p>  
                  </div>
                </div>            
            </div>               
       </div>   
    </section><!--/.main-banner-->
  <?php endif; ?>
    <section class="status_bar_removed contact-filter">
      <div class="container">
          <div class="row">
        <div class="formsubj"><h4><?php _e('Votre demande concerne','ch_du_bo'); ?> :</h4> 
                <div class="form-group form-control-sel">
                  <select class="form-control" id="contact-subject">
                    <option value=""><?php _e('Objet','ch_du_bo'); ?></option>
                    <option value="<?php _e('gschwendtner@bonheur.ch','ch_du_bo'); ?>" data-subject="<?php _e('Attestation de don','ch_du_bo'); ?>"><?php _e('Attestation de don','ch_du_bo'); ?></option>
                    <?php if(CH_LANG_CODE == 'fr' || CH_LANG_CODE == 'de'): ?>
                      <option value="<?php _e('benevole@bonheur.ch','ch_du_bo'); ?>" data-subject="<?php _e('Bénévolat','ch_du_bo'); ?>"><?php _e('Bénévolat','ch_du_bo'); ?></option>
                    <?php endif; ?>
                    <option value="<?php _e('info@bonheur.ch','ch_du_bo'); ?>" data-subject="<?php _e('Autre','ch_du_bo'); ?>"><?php _e('Autre','ch_du_bo'); ?></option>
                  </select>
                  <span role="alert" class="wpcf7-not-valid-tip subject hidden"><?php _e('Ce champ est obligatoire.', 'ch_du_bo'); ?></span>
                </div>
            </div>              
            </div>
        </div>
    </section><!--/.status bar-->

  <section class="section-contactform">
      <div class="container">
          <div class="row">
              <div class="contact-form">
              
              <?php 
        switch(ICL_LANGUAGE_CODE){
          case 'en':
            $contactId = 15278;
            break;
          case 'it':
            $contactId = 15276;
            break;
          case 'fr':
            $contactId = 480;
            break;
          case 'de':
            $contactId = 15277;
            break;
          default:
            $contactId = 15278;
        }
        echo do_shortcode('[contact-form-7 id="'.$contactId.'" title="Contact '.ICL_LANGUAGE_CODE.'"]'); ?>
              
            </div>
              
      
          </div>
      </div>
    
  </section><!--/.contact form-->

  <section class="section-news contact-list">
        <div class="container">
            <div class="row">
                <div class="center title wow fadeInDown animated">
                <h2><?php the_field('title'); ?></h2>          
            </div>
              <?php if( have_rows('places') ): ?>
                <div class="contact-listing">
                 <ul class="mobile-carousel">
                 <?php while ( have_rows('places') ) : the_row(); 
                 $address = get_sub_field('address');
                 $tel = get_sub_field('tel');
                 $fax = get_sub_field('fax');
                 $email = get_sub_field('email');
                 $email_text = get_sub_field('email_text');
                 ?>
                     <li> <div class="col-md-4 col-sm-4 item wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                      <div class="features center">
                         <figure>
                          <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $address['lat'].','.$address['lng']; ?>&size=370x260&zoom=12&markers=color:red%7C<?php echo $address['lat'].','.$address['lng']; ?>&key=AIzaSyD1XG_MSHSz2abftvO3doLBBdR8Qn-1S8o" alt="image 1">
                            <figcaption><a class="icon_news" href="http://maps.google.com/maps?&z=10&q=<?php echo $address['lat'].'+'.$address['lng']; ?>&ll=<?php echo $address['lat'].'+'.$address['lng']; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-pin.png" alt="#"></a></figcaption>
                        </figure>
                        <div class="content-wrap">
                           <h4><?php the_sub_field('place_title'); ?></h4>
                           <address>
                            <?php
                            $addressName = str_replace(',','<br>',$address['address']);
                            echo $addressName;
                            ?><br><br>
                            <?php if($tel != ''): ?>
                              <?php _e('Tél','ch_du_bo'); ?>.: <?php echo $tel; ?><br>
                          <?php endif; ?>
                          <?php if($fax != ''): ?>
                              Fax: <?php echo $fax; ?><br>
                            <?php endif; ?>
                           </address>
                            <a class="btn underline open-here" href="mailto:<?php echo $email; ?>"><?php echo $email_text; ?></a>
                        </div>
                       
                        </div><!--/.fundraising-->
                    </div><!--/.col-md-4-->
                    </li> <!--/.col-md-4-->
                 <?php endwhile; ?>
                  
                    
                </ul>  
                 </div> <!--/.news-listing-->
             <?php endif; ?>
                
                
                
            </div><!--/.row-->
        </div><!--/.container-->
        
   </section>
<?php get_footer(); ?>