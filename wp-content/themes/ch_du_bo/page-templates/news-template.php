<?php
	/*
	Template Name: News
	*/
	get_header(); the_post();
?>
<script src="<?=get_template_directory_uri() . '/js/isotope.pkgd.min.js';?>" defer></script>

	<style type="text/css">
		#header{
			position: inherit;
		}
		.mean-container .mean-bar {
			position: absolute!important;
		}
	</style>
	<?php

	$banner_img = get_field('banner_image');
	$disable_banner = get_field('disable_banner');	
	$title_top = get_field('title_top');
	$title_bottom = get_field('title_bottom');
	$short_description = get_field('short_description');
	$copyright_text = get_field('copyright_text');
	if(!$disable_banner):
	?>
	<section class="main-banner no-margin news-banner">
       <div class="item news-listing-banner" style="background:url(<?php echo $banner_img['url']; ?>) no-repeat center center">
            <div class="sliderCaption">
            	<div class="container">
	                <div class="row">
	                	<div class="col-md-6 pull-left">
	                		<?php if($title_top != '' || $title_bottom != ''): ?>
		                    	<h1><?php echo $title_top.'<br>'.$title_bottom; ?></h1>
		                    <?php endif; ?>
	                      	<h4><?php echo $short_description; ?></h4>
	                   </div>
	                </div>
                </div>
            </div>
       </div>
	   <?php if($copyright_text != ''): ?>
		   <div class="copyright"><?php echo $copyright_text; ?></div>
	   <?php endif; ?>
    </section><!--/.main-banner-->
	<?php endif; ?>
    <section class="status_bar section-filter section-filter-mobile">
		<div class="container">
		<div class="panel panel-default">
			<div class="xs-hidden panel-heading">
			  <h4 class="panel-title">
			  <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1"><?php _e('Filtrer par','ch_du_bo'); ?></a>
			  </h4>
			</div>
			<div id="collapse1" class="panel-collapse collapse" style="height: 0px;">
				<div class="panel-body">
					<div class="row">
					<div class="col-md-2"><h4><?php _e('Filtrer par','ch_du_bo'); ?></h4></div>
					<div class="col-md-8 col-sm-9">
					<?php
					$terms = get_terms( 'category', array(
						'hide_empty' => false,
						'parent'	 => 0,
						'meta_key'	 =>	'order',
						'orderby'	 => 'meta_value_num'
					) );
					if(!empty($terms)){
						?>
						<ul class="filter_menu">
						<?php
						$activeCat = NULL;
						if( isset($_GET['category']) ){
							$activeCat = $_GET['category'];
						}
						foreach ($terms as $term) {
							?>
							<li>
								<input type="radio" name="media_filter" value="<?php echo $term->term_id; ?>" id="<?php echo $term->slug; ?>"
								<?php
									if($activeCat == $term->slug){
										echo ' checked="CHECKED"';
									}
								?>
								/>
								<label for="<?php echo $term->slug; ?>"><?php echo $term->name; ?></label>
							</li>
							<?php
						}
						?>
						</ul>
						<?php
					}
					?>
					</div>
					<div class="col-md-2 col-sm-3"><a class="btn rounded pull-right deselectAll" href="#"><?php _e('Voir tous', 'ch_du_bo'); ?></a></div>
					</div>
				</div>
			</div>
		</div>
		</div>
    </section><!--/.status bar-->
    <section class="section-news">
        <div class="container">
            <div class="row">


                <div class="news-listing">
                 <div class="masonry grid">
                 	<?php
                 	// WP_Query arguments
					$args = array (
						'posts_per_page'         => '1',
						'orderby'       => 'date',
						'order'=> 'DESC',
						'meta_query'	=>	array(
							array(
								'key'     => 'hero',
								'value'   => 'Yes',
								'compare' => '!=',
							)
						)
					);

					if( isset($_GET['tag']) ){
						$tag_query = array('tag' => $_GET['tag']);
						$args = array_merge($args, $tag_query);
					}

					if( isset($_GET['category']) ){
						$tax_query = array(
							'tax_query'	=> array(
								array(
									'taxonomy' => 'category',
									'field'    => 'slug',
									'terms'    => array($_GET['category']),
									'operator' => 'IN'
								),
							)
						);
						$args = array_merge($args, $tax_query);
					}

					// The Query
					$query = new WP_Query( $args );
					$usedID = array();
					// The Loop
					if ( $query->have_posts() ) {
						while ( $query->have_posts() ) {
							$query->the_post();
							$usedID[] = get_the_ID();
							get_template_part('template-parts/content','news-post');
							break;
						}
					}

					// Restore original Post Data
					wp_reset_postdata();
                 	?>

                     <?php
                     	// WP_Query arguments
						$args = array (
							'posts_per_page'         => '1',
							'orderby'       => 'date',
						    'order'=> 'DESC',
							'meta_query'	=>	array(
								'relation'	=>	'AND',
								array(
									'key'     => 'hero',
									'value'   => 'Yes',
									'compare' => '=',
								)
							)
						);

						if( isset($_GET['tag']) ){
							$tag_query = array('tag' => $_GET['tag']);
							$args = array_merge($args, $tag_query);
						}

						if( isset($_GET['category']) ){
							$tax_query = array(
								'tax_query'	=> array(
									array(
										'taxonomy' => 'category',
										'field'    => 'slug',
										'terms'    => array($_GET['category']),
										'operator' => 'IN'
									),
								)
							);
							$args = array_merge($args, $tax_query);
						}

						// The Query
						$query = new WP_Query( $args );

						// The Loop
						if ( $query->have_posts() ) {
							while ( $query->have_posts() ) {
								$query->the_post();
								if(get_post_meta(get_the_ID(), 'hero', 1) !== 'Yes'){
									continue;
								}
								get_template_part('template-parts/content','news-post');
							}
						}

						// Restore original Post Data
						wp_reset_postdata();
                     ?>
                     <?php
                     	// WP_Query arguments
						$args = array (
							//'paged'                  => '1',
							'posts_per_page'         => '6',
							'offset'                 => '0',
							'order'=> 'DESC',
							'orderby'       => 'date',
							'post_status' => 'publish',
					     
							'post__not_in'			 => $usedID,
							'meta_query'	=>	array(
								array(
									'key'     => 'hero',
									'value'   => 'Yes',
									'compare' => '!=',
								)
							)
						);

						if( isset($_GET['tag']) ){
							$tag_query = array('tag' => $_GET['tag']);
							$args = array_merge($args, $tag_query);
						}

						if( isset($_GET['category']) ){
							$tax_query = array(
								'tax_query'	=> array(
									array(
										'taxonomy' => 'category',
										'field'    => 'slug',
										'terms'    => array($_GET['category']),
										'operator' => 'IN'
									),
								)
							);
							$args = array_merge($args, $tax_query);
						}

						// The Query
						$query = new WP_Query( $args );

						

						// The Loop
						if ( $query->have_posts() ) {
							while ( $query->have_posts() ) {
								$query->the_post();
								get_template_part('template-parts/content','news-post');
							}
						}

						// Restore original Post Data
						wp_reset_postdata();
                     ?>

                     </div> 

                 </div> <!--/.news-listing-->


                <div class="center newsAll hidden-xs">
                	<a class="btn rounded black more-news-btn hidden" href="#"><?php _e("Plus d'actualités","ch_du_bo"); ?></a>
                </div>


            </div><!--/.row-->
        </div><!--/.container-->

   </section>

<script defer>
	/*jQuery(document).ready(function() {
		var newsGrid = jQuery('.grid').isotope({
			isInitLayout: true,
			itemSelector: '.grid-item',
		});
		jQuery('.grid').imagesLoaded().progress(function() {
			jQuery('.grid').isotope('layout');
		});

		jQuery(".filter_menu input[name=media_filter]").change(function(){
			console.log(jQuery(this).val())
			newsGrid.isotope({filter: '.news-'+jQuery(this).val()})
		})
		jQuery('.deselectAll').click(function(event){
			event.preventDefault();
			newsGrid.isotope({filter: '*'})
		})
	});*/
</script>

	<?php

	//wp_enqueue_script( 'ch_du_bo-masonary' );
	//wp_enqueue_script( 'ch_du_bo-masonary2');

	get_footer();
?>
