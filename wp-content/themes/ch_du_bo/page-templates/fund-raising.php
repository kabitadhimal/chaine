<?php
/*
Template Name: Fund Raising
*/
get_header(); the_post();
?>
	<style type="text/css">
		#header{
			position: inherit;
		}
	</style>
	<?php
	$banner_img = get_field('banner_image');
	$disable_banner = get_field('disable_banner');	
	$img = array();
	if(is_array($banner_img)){
		$img[0] = $banner_img['url'];
	}else{
		$img = wp_get_attachment_image_src($banner_img,'full');
	}
	$title_top = get_field('title_top');
	$title_bottom = get_field('title_bottom');
	$short_description = get_field('short_description');
	$copyright_text = get_field('copyright_text');
	if(!$disable_banner):
	?>
	<section class="main-banner no-margin news-banner">
       <div class="item fund-banner" style="background:url(<?php echo $img[0]; ?>) no-repeat center center">
            <div class="sliderCaption">
            	<div class="container">
	                <div class="row">
	                	<div class="col-md-6 pull-left">
	                		<?php if($title_top != '' || $title_bottom != ''): ?>
		                    	<h1><?php echo $title_top.'<br>'.$title_bottom; ?></h1>
		                    <?php endif; ?>
	                      	<h4><?php echo $short_description; ?></h4>
	                   </div>
	                </div>
                </div>
            </div>
       </div>
	   <?php if($copyright_text != ''): ?>
		   <div class="copyright"><?php echo $copyright_text; ?></div>
	   <?php endif; ?>
    </section><!--/.main-banner-->
	<?php endif; ?>
    <section class="status_bar section-filter section-filter-mobile">
		<div class="container">
		<div class="panel panel-default">
			<div class="xs-hidden panel-heading">
			  <h4 class="panel-title">
			  <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1"><?php _e('Filtrer par','ch_du_bo'); ?></a>
			  </h4>
			</div>
			<div id="collapse1" class="panel-collapse collapse" style="height: 0px;">
				<div class="panel-body">
					<div class="row">
					<div class="col-md-2"><h4><?php _e('Filtrer par','ch_du_bo'); ?></h4></div>
					<div class="col-md-8">
					<?php
					$terms = get_terms( 'fundraising-category', array(
						'hide_empty' => false,
						'parent'	 => 0
					) );
					if(!empty($terms)){
						$activeCat = NULL;
						if( isset($_GET['category']) ){
							$activeCat = $_GET['category'];
						}
						foreach ($terms as $term) {
							?>
							<select class="filtersel" multiple="multiple" data-title="<?php echo $term->name; ?>">
								<?php
								$args = array(
									'hide_empty' => false,
									'parent'	 => $term->term_id
								);
								$yearArray = array('Période', 'Period', 'Zeitraum', 'Periodo');
								if(in_array($term->name, $yearArray)){
									$args['order']	= 'DESC';
								}
								$children = get_terms( 'fundraising-category', $args );
								if(!empty($children)): ?>
									<?php
									foreach ( $children as $child ) {
										//$term = get_term_by( 'id', $child, 'fundraising-category' );
										echo '<option value="'.$child->term_id.'" ';
										if($activeCat == $child->slug){
											echo "SELECTED ";
										}
										echo '>'.$child->name . '</option>';
									}
									?>
								<?php endif; ?>
							</select>
							<?php
						}
					}
					?>
					</div>
					<div class="col-md-2"><a class="btn rounded pull-right deselectAll" href="#"><?php _e('Voir tous', 'ch_du_bo'); ?></a></div>
					</div>
				</div>
			</div>
		</div>
		</div>
    </section>
    <!--/.status bar-->
    
    <section class="section-threeColumn section-fundraising fund-listing">
        <div class="container">
            <div class="row">


                <div class="news-listing">
                 <div class="masonry grid">
                     <?php
                     	// WP_Query arguments
						$args = array (
							'post_type'	=>	'fundraising-campaign',
							'posts_per_page'         => '6',
						);

						if( isset($_GET['category']) ){
							$tax_query = array(
								'tax_query'	=> array(
									array(
										'taxonomy' => 'fundraising-category',
										'field'    => 'slug',
										'terms'    => array($_GET['category']),
										'operator' => 'IN'
									),
								)
							);
							$args = array_merge($args, $tax_query);
						}

						// The Query
						$query = new WP_Query( $args );

						// The Loop
						if ( $query->have_posts() ) {
							while ( $query->have_posts() ) {
								$query->the_post();
								get_template_part('template-parts/content','fund');
							}
						}

						// Restore original Post Data
						wp_reset_postdata();
                     ?>

                     </div>

                 </div> <!--/.news-listing-->


                <div class="center newsAll hidden-xs">
                	<a class="btn rounded black more-fund-btn hidden" href="#"><?php _e('Voir plus','ch_du_bo'); ?></a>
                </div>


            </div><!--/.row-->
        </div><!--/.container-->

   </section>
	<?php

	//wp_enqueue_script( 'ch_du_bo-masonary' );
	//wp_enqueue_script( 'ch_du_bo-masonary2');

	get_footer();
?>
