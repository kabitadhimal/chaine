<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Chaîne_du_Bonheur
 */
 /*
?>
<section>
	<div id="example2" class="slider-pro">
		<div class="sp-slides">
			<div class="sp-slide">
				<a href="http://bqworks.com/slider-pro/images2/image1_large.jpg" class="open-here">
					<img class="sp-image" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"
						data-src="http://bqworks.com/slider-pro/images2/image1_medium.jpg"
						data-retina="http://bqworks.com/slider-pro/images2/image1_large.jpg"/>
				</a>
				<p class="sp-caption">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
			</div>

			<div class="sp-slide">
				<a href="http://bqworks.com/slider-pro/images2/image1_large.jpg" class="open-here">
					<img class="sp-image" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"
						data-src="http://bqworks.com/slider-pro/images2/image1_medium.jpg"
						data-retina="http://bqworks.com/slider-pro/images2/image1_large.jpg"/>
				</a>
			</div>

			<div class="sp-slide">
				<a href="http://bqworks.com/slider-pro/images2/image1_large.jpg" class="open-here">
					<img class="sp-image" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"
						data-src="http://bqworks.com/slider-pro/images2/image1_medium.jpg"
						data-retina="http://bqworks.com/slider-pro/images2/image1_large.jpg"/>
				</a>
			</div>
			<div class="sp-slide">
				<a href="http://bqworks.com/slider-pro/images2/image1_large.jpg" class="open-here">
					<img class="sp-image" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"
						data-src="http://bqworks.com/slider-pro/images2/image1_medium.jpg"
						data-retina="http://bqworks.com/slider-pro/images2/image1_large.jpg"/>
				</a>
			</div>
			<div class="sp-slide">
				<a href="http://bqworks.com/slider-pro/images2/image1_large.jpg" class="open-here">
					<img class="sp-image" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"
						data-src="http://bqworks.com/slider-pro/images2/image1_medium.jpg"
						data-retina="http://bqworks.com/slider-pro/images2/image1_large.jpg"/>
				</a>
			</div>
			<div class="sp-slide">
				<a href="http://bqworks.com/slider-pro/images2/image1_large.jpg" class="open-here">
					<img class="sp-image" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"
						data-src="http://bqworks.com/slider-pro/images2/image1_medium.jpg"
						data-retina="http://bqworks.com/slider-pro/images2/image1_large.jpg"/>
				</a>
			</div>
			<div class="sp-slide">
				<a href="http://bqworks.com/slider-pro/images2/image1_large.jpg" class="open-here">
					<img class="sp-image" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"
						data-src="http://bqworks.com/slider-pro/images2/image1_medium.jpg"
						data-retina="http://bqworks.com/slider-pro/images2/image1_large.jpg"/>
				</a>
			</div>
			<div class="sp-slide">
				<a href="http://bqworks.com/slider-pro/images2/image1_large.jpg" class="open-here">
					<img class="sp-image" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"
						data-src="http://bqworks.com/slider-pro/images2/image1_medium.jpg"
						data-retina="http://bqworks.com/slider-pro/images2/image1_large.jpg"/>
				</a>
			</div>

            <div class="sp-slide">
				<a href="http://bqworks.com/slider-pro/images2/image1_large.jpg" class="open-here">
					<img class="sp-image" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"
						data-src="http://bqworks.com/slider-pro/images2/image1_medium.jpg"
						data-retina="http://bqworks.com/slider-pro/images2/image1_large.jpg"/>
				</a>
			</div>
		</div>
    </div>
</section>
<?php
*/
$ch_du_bo_content = new Ch_du_bo_Content();
if(is_emergency() && (is_home() || is_front_page())){

	$background_image =  get_field('background_image');
	if($background_image){
		$img = $background_image['sizes']['slider-img'];
	}else{
		$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'slider-img');
		if($img){
			$blurClass = 'featured-banner';
			$img = $img[0];
		}else{
			$img = get_template_directory_uri().'/images/cms-img-landscape.jpg';
		}
	}
	$displayBanner = false;
	$banner_title_small =  get_field('banner_title_small');
	if($banner_title_small != ""){ $displayBanner = true; }
	$banner_title_big =  get_field('banner_title_big');
	if($banner_title_big != ""){ $displayBanner = true; }
	$banner_link =  get_field('banner_link');
	$banner_link_text =  get_field('banner_link_text');
	if($banner_link_text != ""){ $displayBanner = true; }
	$top_text =  get_field('top_text');
	if($top_text != ""){ $displayBanner = true; }
	$bottom_text =  get_field('bottom_text');
	if($bottom_text != ""){ $displayBanner = true; }
	$description =  get_field('description');
	if($description != ""){ $displayBanner = true; }
	$more_link =  get_field('more_link');
	if($more_text != ""){ $displayBanner = true; }
	$position = get_field('position');
	if($position == 'Left'){
		$class = 'content-left';
	}else if($position == 'Middle'){
		$class = 'pull-middle';
	}else if($position == 'Right'){
		$class = 'content-right';
	}
	$hide =  get_field('hide');
	$copyright_text =  get_field('copyright_text');
	if($img && $displayBanner):
		if($blurClass == 'featured-banner'):
	?>
		<section class="main-banner no-margin">
	           <div class="item" style="background:url(<?php echo $img; ?>) no-repeat center center;"></div>
	            <div class="sliderCaption">
	            	<div class="container">
		                <div class="row <?php echo $class; ?>">
		                		<?php if($banner_title_small): ?>
		                    	 <h2><?php echo $banner_title_small; ?></h2>
		                    	<?php endif; ?>
		                    	<?php if($banner_title_big): ?>
		                    	 <h1><?php echo $banner_title_big; ?></h1>
		                    	 <?php endif; ?>
		                      	<?php if($banner_link_text): ?>
									<a class="btnDonate open-here" href="<?php echo $banner_link; ?>"><?php echo $banner_link_text; ?></a>
								<?php endif; ?>
								 <?php if($hide == 'No'): ?>
			                   		 <h4><?php echo $top_text; ?></h4>
			                   		 <?php $days = get_emergency_activated_days(); ?>
									 <?php if($days <= 10 && $bottom_text != ''): ?>
			                         	<span class="nopart mgap"><?php echo $bottom_text; ?></span>
			                         <?php endif; ?>
		                         <?php endif; ?>

		                </div>
					</div>
	            </div>
		      <div class="itemCaption">
		      	<div class="container">
		      	<?php echo $description; ?>
		        <a href="<?php echo $more_link; ?>" class="btn underline"><?php echo $more_text; ?></a>
		        </div>
		      </div>
			  <?php if($copyright_text != ''): ?>
				  <div class="copyright"><?php echo $copyright_text; ?></div>
			  <?php endif; ?>
	    </section><!--/.main-slider-->
		<?php else: ?>
			<section class="main-banner no-margin">
		           <div class="item" style="background:url(<?php echo $img; ?>) no-repeat center center;">
		            <div class="sliderCaption">
		            	<div class="container">
			                <div class="row <?php echo $class; ?>">
			                		<?php if($banner_title_small): ?>
			                    	 <h2><?php echo $banner_title_small; ?></h2>
			                    	<?php endif; ?>
			                    	<?php if($banner_title_big): ?>
			                    	 <h1><?php echo $banner_title_big; ?></h1>
			                    	 <?php endif; ?>
			                      	<?php if($banner_link_text): ?>
										<a class="btnDonate open-here" href="<?php echo $banner_link; ?>"><?php echo $banner_link_text; ?></a>
									<?php endif; ?>
									 <?php if($hide == 'No'): ?>
				                   		 <h4><?php echo $top_text; ?></h4>
				                   		 <?php $days = get_emergency_activated_days(); ?>
										 <?php if($days <= 10 && $bottom_text != ''): ?>
				                         	<span class="nopart mgap"><?php echo $bottom_text; ?></span>
				                         <?php endif; ?>
			                         <?php endif; ?>

			                </div>
						</div>
		            </div>
		           </div>
			      <div class="itemCaption">
			      	<div class="container">
			      	<?php echo $description; ?>
			        <a href="<?php echo $more_link; ?>" class="btn underline"><?php echo $more_text; ?></a>
			        </div>
			      </div>
				  <?php if($copyright_text != ''): ?>
					  <div class="copyright"><?php echo $copyright_text; ?></div>
				  <?php endif; ?>
		    </section><!--/.main-slider-->
		<?php endif; ?>
		<?php
	endif;
}
$content = $ch_du_bo_content->ch_du_bo_get_flexible_content();
if($content):
	echo $content;
else:
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php /* if(is_page( 'Home' )): ?>
        <header class="entry-header">
            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </header><!-- .entry-header -->
    <?php endif; */ ?>
	<div class="entry-content">
		<?php
			the_content();
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'ch_du_bo' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'ch_du_bo' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->
<?php
endif;
?>
