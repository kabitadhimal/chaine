<?php
$socialLink = get_field('social_link');
$openNewWindow = get_field('open_in_new_window');
$newWindowLink = get_field('new_window_link');
//var_dump($openNewWindow, $newWindowLink);
$termIds = [];
$hero = get_field('hero');
if($hero == 'Yes'){
    $colClass = 8;
    $imgSize = 'hero-thumb';
    $charLimit = 250;
}else{
    $colClass = 4;
    $imgSize = 'fundraising-thumb';
    $charLimit = 150;
}

if($socialLink != ''){



    $socialMedia = get_field('social_media');
    if($socialMedia == 'Facebook'){
//        $fb = new FB_Page_Manager();
//        $data = $fb->get_post_by_url($socialLink);
//        $data['created_time'] = date('d.m.Y',strtotime($data['created_time']));
        $data['name'] = NULL;
        $data['link'] = $socialLink;
        if(has_post_thumbnail()){
            $img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),$imgSize);
            $data['full_picture'] = $img[0];
        }else{
            $data['full_picture'] = get_template_directory_uri().'/images/img-col3.jpg';
        }
        $data['created_time'] = get_the_time('d.m.Y');
        $data['message'] = get_the_excerpt();
        $data['id']	= get_the_ID();
        $data['src']	=	'Facebook';
        $class ="icon_fb";

        $socialParentCategoryID = lang_category_id(13);
        $socialID = 'news-'.$socialParentCategoryID;
        $termIds = [$socialID];

    }else{


        $socialParentCategoryID = lang_category_id(13);
        $socialID = 'news-'.$socialParentCategoryID;
        $termIds = [$socialID];
		if(has_post_thumbnail()){
            $img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),$imgSize);
			$img = $img[0];
        }else{
			$content = file_get_contents('https://api.instagram.com/oembed/?url='.$socialLink);
			$content =json_decode($content);
			$img = $content->thumbnail_url;
		}
		//debug($content);
		if(!$img){
			$img = get_sub_field('link').'media/?size=m';
		}
        $data = array(
            'full_picture'	=>	$img,
            'link'	=>	$socialLink,
            'created_time'	=>	get_the_time('d.m.Y'),
            'message'	=>	get_the_excerpt(),
            'id'	=>	'',
            'date'	=>	'',
            'name'	=>	'',
            'src'	=>	'Instagram',
        );
        $class="icon_news icon_insta";
    }
}else{
    $data['name'] = get_the_title();
    $data['link'] = get_permalink();
    if(has_post_thumbnail()){
        $img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),$imgSize);
        $data['full_picture'] = $img[0];
    }else{
        $data['full_picture'] = get_template_directory_uri().'/images/img-col3.jpg';
    }
    $data['created_time'] = get_the_time('d.m.Y');
    $data['message'] = get_the_excerpt();
    $data['id']	= get_the_ID();
    $data['src']	='WP';
    $class="icon_news";

   // $terms = get_terms
    //    for
    //$termIds[] = $term->ID;

    $terms = get_the_terms($data['id'],'category');
	if(!empty($terms)){
		foreach($terms as $term) {
			if(!empty($term->parent)) {
				$termIds[] = 'news-'.$term->parent;

			}else {
				$termIds[] = 'news-'.$term->term_id;

			}
		}
	}    

   // var_dump($termIds);
}
$currentPermalink = $data['link'];
$currentObjectClass = 'open-here';
$currentTarget = '';
if($openNewWindow && $openNewWindow[0] == 'Yes'){
    $currentPermalink = $newWindowLink;
    $currentObjectClass = 'open-new';
    $currentTarget = '_blank';
}
if(strpos($currentPermalink, site_url()) === false){
    $currentObjectClass = 'open-new';
}
?>

<div class="grid-item col-sm-6 col-md-<?php echo $colClass; ?> <?=join(' ', $termIds)?> item wowx xfadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
    <div class="features center">
        <a href="<?php echo $currentPermalink; ?>" class="<?php echo $currentObjectClass; ?>" target="<?php echo $currentTarget; ?>">
            <figure>
                <?php if($data['full_picture']): ?>
                    <img src="<?php echo $data['full_picture']; ?>" alt="<?php echo $data['name']; ?>">
                <?php endif; ?>
                <?php
                $icon = get_post_icon($data['id'],$data['src']);
                ?>
                <figcaption><span class="<?php echo $class; ?>" ><?php echo $icon; ?></span></figcaption>
            </figure>
            <div class="content-wrap">
                <span class="disasterdate"><?php echo $data['created_time']; ?></span>
                <h4><?php echo $data['name']; ?></h4>
                <?php echo ch_du_bo_truncate($data['message'],$charLimit); ?>
            </div>
        </a>

    </div><!--/.fundraising-->
</div><!--/.col-md-4-->
