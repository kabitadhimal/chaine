<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Chaîne_du_Bonheur
 */
$icon = get_template_directory_uri().'/images/btn-donate.svg';
$terms = wp_get_post_terms( get_the_ID(),'category' );
foreach($terms as $term){
	if($term->parent == 0){
		continue;
	}
	$icon = get_field('icon',"category_".$term->term_id);
	break;
}
?>


	<?php
 		$img = wp_get_attachment_image( get_post_thumbnail_id(  ), 'full' );
        if($img):
    ?>
        <figure class="center">
            <?php echo '<a href="'.get_permalink(  ).'">'.$img.'</a>'; ?>
            <figcaption><a href="<?php echo get_permalink(); ?>"><img src="<?php echo $icon; ?>" alt="#"></a></figcaption>
        </figure>
    <?php endif; ?>
<div class="content-wrap">
    <?php
        $donation = get_field('donation_amount');
        if($donation != ''):
    ?>
        <span class="fundCollected center"><?php _e('CHF','ch_du_bo'); ?> <?php echo $donation; ?> <?php _e('Dons collectés','ch_du_bo'); ?></span>
    <?php endif; ?>
    <h3><?php the_title(); ?></h3>
    <?php the_content(); ?>
</div>

<div class="media-footer clearfix">
	<div class="col-md-6">
        <?php
    		$telLink = get_field('telecharger_le_communique_link');
    		if( $telLink != ''):
    	?>
            <a class="btn underline" href="<?php echo $telLink['url']; ?>"><?php _e('Télécharger le communiqué','ch_du_bo'); ?></a>
        <?php endif; ?>
    </div>
    <div class="col-md-6 share_block">
        <a class="share pull-right" href="#"><?php _e('Partager','ch_du_bo'); ?></a>
        <div class="addthis_inline_share_toolbox"></div>
    </div>
</div>  <!--end of share-->
