<?php
$colClass = 4;
$imgSize = 'fundraising-thumb';
$charLimit = 150;

$data['name'] = get_the_title();
$data['link'] = get_permalink();
if(has_post_thumbnail()){
	$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),$imgSize);
	$data['full_picture'] = $img[0];
}else{
	$data['full_picture'] = get_template_directory_uri().'/images/img-col3.jpg';
}
$data['created_time'] = get_the_time('d.m.Y');
$data['message'] = get_the_excerpt();
$data['id']	= get_the_ID();
$data['src']	='WP';
$class="";
$donationID = get_field('funds_received',$data['id']);
?>

<div class="grid-item col-sm-6 col-md-<?php echo $colClass; ?> item">
	<div class="features center">
	<a href="<?php echo $data['link']; ?>">
	 <figure>
		<?php if($data['full_picture']): ?>
			<img src="<?php echo $data['full_picture']; ?>" alt="<?php echo $data['name']; ?>">
		<?php endif; ?>
		<figcaption><span class="<?php echo $class; ?>" ><img src="<?php echo get_template_directory_uri(); ?>/images/btn-donate.svg" alt="#"></span></figcaption>
	</figure>
	<div class="content-wrap">
		 <?php /* <span class="disasterdate"><?php echo $data['created_time']; ?></span> */ ?>
		 <?php if($donationID != ''): ?>
			 <?php $donationData = ch_du_bo_get_donation_data($donationID); ?>
			 <span class="fundCollected"><?php echo __('Dons collectés','ch_du_bo').': '.$donationData['donation'].'&nbsp;'.$donationData['devise']; ?></span>
		 <?php endif; ?>
		<h4><?php echo $data['name']; ?></h4>
		<?php echo ch_du_bo_truncate($data['message'],$charLimit); ?>
	</div>
   </a>
	</div><!--/.fundraising-->

</div><!--/.col-md-4-->
