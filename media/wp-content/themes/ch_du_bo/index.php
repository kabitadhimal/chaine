<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Chaîne_du_Bonheur
 */

get_header(); ?>

	<section class="section-two-col">
        <div class="container">
            <div class="row">
          
                	<div class="col-md-8">
                    	<div class="media-body">
                    	<div class="post-Listing">
                        	
                        <!-- <h3>Communiqués Associes</h3> -->
                        <?php

							// The Loop
							if ( have_posts() ) {
								?><ul class="postList row"><?php
								while ( have_posts() ) {
									the_post();
									$terms = wp_get_post_terms( get_the_ID(), 'category' );
									get_template_part('template-parts/content','media-post');
								}
								?></ul><?php
							} 

							// Restore original Post Data
							wp_reset_postdata();
                        ?>
                        </div>
                        <div class="center mediaAll hidden-xs">
		                	<a class="btn rounded black more-media-btn" href="#"><?php _e("Plus d'actualités", "ch_du_bo"); ?></a>
		                	<p style="display:none" class="news-loader"><img src="<?php echo get_template_directory_uri().'/images/ajax-loader.gif'; ?>" alt="Loader"></p>
		                </div>
                    
                    </div>
                    </div>
                    <div class="col-md-4">
                    
                    	<?php get_sidebar(); ?>
                    
                    </div>
                
            </div><!--/.row-->
        </div><!--/.container-->
        
   </section><!--/.section news-->

<?php
get_footer();
