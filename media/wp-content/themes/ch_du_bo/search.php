<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Chaîne_du_Bonheur
 */

get_header(); 
wp_enqueue_script('ch_du_bo-masonary2');
$backLink = $_SERVER['HTTP_REFERER'];
	if(!$backLink){
		$backLink = site_url();
	}
?>
	<section class="section-backlink">
        <div class="container">        
           <div class="btnback row">
           <div class="col-md-6">
           		<i class="fa fa-chevron-left"></i><a href="<?php echo $backLink; ?>"><?php _e('Retour', 'ch_du_bo'); ?></a> 
           </div>
           
           <div class="col-md-6">     
                <h4><?php printf( esc_html__( 'Résultats de recherche pour : %s', 'ch_du_bo' ), '<span>' . get_search_query() . '</span>' ); ?></h4> 
            </div>
                               
           </div>
      	</div>
  	</section>
	<section class="section-two-col">
        <div class="container">
            <div class="row">
          
                	<div class="col-md-8">
                    	<div class="media-body">
                    	<div class="post-Listing">
                        	
                        <!-- <h3>Communiqués Associes</h3> -->
                        <?php

							// The Loop
							if ( have_posts() ) {
								?>
								<ul class="postList grid row"><?php
								while ( have_posts() ) {
									the_post();
									$terms = wp_get_post_terms( get_the_ID(), 'category' );
									get_template_part('template-parts/content','media-post');
								}
								?></ul><?php
								the_posts_navigation();
							} 
                        ?>
                        </div>
                    
                    </div>
                    </div>
                    <div class="col-md-4">
                    
                    	<?php get_sidebar(); ?>
                    
                    </div>
                
            </div><!--/.row-->
        </div><!--/.container-->
        
   </section><!--/.section news-->

<?php
get_footer();
