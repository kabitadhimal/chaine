jQuery(function($) {'use strict',

	

	//Initiat WOW JS
	new WOW().init();

	
	$(window).scroll(function() {
    if ($(this).scrollTop() > 1){  
        $('#header').addClass("sticky");
    }
    else{
        $('#header').removeClass("sticky");
    }
});
	
	$( document ).ready( function() {
		  jQuery('header .collapse nav').meanmenu();
		  
		});

	
	//goto top
	$('.gototop').click(function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $("body").offset().top
		}, 500);
	});	

	
});

// Init placeholderHideSeek
placeholderHideSeek(jQuery('.form-control'));

// Function to hide and show form input field placeholder values
function placeholderHideSeek(input) {
    if(jQuery(input).length === 0) return;
    input.each( function(){
        var meInput = jQuery(this);
        var placeHolder = jQuery(meInput).attr('placeholder');
        jQuery(meInput).focusin(function(){
            jQuery(meInput).attr('placeholder','');
        });
        jQuery(meInput).focusout(function(){
            jQuery(meInput).attr('placeholder', placeHolder);
        });
    });
}


jQuery('#mailchilpCheckbox input[type="checkbox"]').attr('id','c1');


