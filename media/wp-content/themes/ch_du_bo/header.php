<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Chaîne_du_Bonheur
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/png">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
<style>
.page-overlay {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background-color: rgba(0,0,0,0.5);
    display: none;
}
.chaine-preloader-wrap {
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate3d(-50%, -50%, 0);
    text-align: center;
}
.chaine-preloader-loading-text {
    text-align: center;
    display: inline-block;
}
.neaf-preloader-wrap{position:fixed;top:50%;left:50%;transform:translate3d(-50%, -50%, 0);text-align:center;}

.clearfix:before,

.clearfix:after{content:"";display:table;overflow:hidden;height:0;}

.clearfix:after{clear:both;}

#fountainTextG{width:174px;margin:auto;}

.chaine-preloader-loading-text{text-align:center; display:inline-block}

@-webkit-keyframes rotating /* Safari and Chrome */ {
  from {
    -ms-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -webkit-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  to {
    -ms-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -webkit-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes rotating {
  from {
    -ms-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -webkit-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  to {
    -ms-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -webkit-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
.rotating {
  -webkit-animation: rotating 2s linear infinite;
  -moz-animation: rotating 2s linear infinite;
  -ms-animation: rotating 2s linear infinite;
  -o-animation: rotating 2s linear infinite;
  animation: rotating 2s linear infinite;
}

.chaine-preloader-logo {
	animation: rotating 2s infinite 0.5s;
}

</style>
</head>
<?php 
	$logo = ch_du_bo_get_logo(CH_LANG_CODE,$emergency);
	$contactLink = esc_url( get_theme_mod( 'contact_'.ICL_LANGUAGE_CODE ) );
	if(!$contactLink){ $contactLink = '#'; }
?>
<body <?php body_class(); ?>>
<div class="news-loader page-overlay " style="display: none;">
	<div class="chaine-preloader-wrap">
		<div class="chaine-preloader-logo">
			<img src="<?php echo get_template_directory_uri().'/images/logo_chaine_loader.png'; ?>" class="iajax-loader">
		</div>
	</div>
</div>
<header id="header">
	<nav class="navbar">
		<div class="container">
			<div class="row">
				<div class="col-md-2 col-sm-4 col-xs-5 navbar-header">
					<a class="navbar-brand" href="<?php echo site_url(); ?>" rel="home"><img src="<?php echo $logo; ?>" alt="<?php bloginfo(); ?>"></a>
				</div>
				<div class="col-md-10 col-sm-7 col-xs-6 navbar-right">	
					<div class="top-bar clearfix hidden-xs">
						<div class="accRt">
            <?php //echo $contactLink; ?>
							<ul class="mainsiteback">
								<li><a target="" class="" href="<?php echo PARENT_SITE_URL; ?>"><?php _e('Site principal','ch_du_bo'); ?></a></li>
							</ul>
							<ul class="lang">
								<li><?php language_dropdown(); ?></li>
							</ul>
						</div>
					</div><!--/.top-bar-->
					<!-- <div class="navbar-collapse collapse">
						<nav> -->    
							<?php 
								/*wp_nav_menu( array( 
									'theme_location' 	=>	'primary', 
									'menu_id' 			=>	'primary-menu',
									'container' 		=>	'ul',
									'menu_class'		=>	'nav navbar-nav',
									'walker'			=>	new Primary_Nav_Walker()
							) ); */
							?><!-- 
						</nav>
					</div> -->
				</div>   
			</div>   
		</div><!--/.container-->
	</nav><!--/nav-->
</header>
<?php
$backgroundImage = get_field('background_image','option');
$title = get_field('title','option');
$subTitle = get_field('subtitle','option');
?>
<section class="main-banner main-banner-media no-margin">
           
       <div class="item" style="background:url(<?php echo $backgroundImage['url']; ?>) no-repeat center center">
        <div class="sliderCaption">
        	<div class="container">
                <div class="row">
            		 <h1><?php echo $title; ?></h1>
            		 <?php if($subTitle != ''): ?>
               		 	<h4><?php echo $subTitle; ?></h4>
               		 <?php endif; ?>
                </div>
            </div>
        
        </div>		
       
       </div>
</section><!--/.main-slider-->