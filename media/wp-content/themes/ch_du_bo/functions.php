<?php
/**
 * Chaîne du Bonheur functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Chaîne_du_Bonheur
 */
define('COOKIE_NAME', 'filter_cookie');
function debug($data, $die = false){
	echo "<pre>";
	var_dump($data);
	echo "</pre>";
	if($die){
		die;
	}
}

define('CH_LANG_CODE', defined('ICL_LANGUAGE_CODE') ? ICL_LANGUAGE_CODE : 'de' );
switch(CH_LANG_CODE){
	case 'fr':
		define('PARENT_SITE_URL', 'https://www.bonheur.ch');
		break;
	case 'en':
		define('PARENT_SITE_URL', 'https://www.swiss-solidarity.org');
		break;
	case 'de':
		define('PARENT_SITE_URL', 'https://www.glueckskette.ch');
		break;
	case 'it':
		define('PARENT_SITE_URL', 'https://www.catena-della-solidarieta.ch');
		break;
	default:
		define('PARENT_SITE_URL', 'https://www.bonheur.ch');
		break;
}

if ( ! function_exists( 'ch_du_bo_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ch_du_bo_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Chaîne du Bonheur, use a find and replace
	 * to change 'ch_du_bo' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'ch_du_bo', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'post-formats', array( 'image', 'gallery', 'video' ) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'ch_du_bo' ),
		'footer' => esc_html__( 'Footer', 'ch_du_bo' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'ch_du_bo_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'ch_du_bo_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ch_du_bo_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ch_du_bo_content_width', 640 );
}
add_action( 'after_setup_theme', 'ch_du_bo_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ch_du_bo_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'ch_du_bo' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'ch_du_bo' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'ch_du_bo_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ch_du_bo_scripts() {
	wp_enqueue_style( 'ch_du_bo-style', get_stylesheet_uri() );

	/*wp_enqueue_script( 'ch_du_bo-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'ch_du_bo-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}*/

	wp_enqueue_style( 'ch_du_bo-boostrap', get_template_directory_uri().'/css/bootstrap.min.css',array(),date(Ym),'');
	wp_enqueue_style( 'ch_du_bo-font-awesome', get_template_directory_uri().'/css/font-awesome.min.css',array(),date(Ym),'');
	wp_enqueue_style( 'ch_du_bo-animate', get_template_directory_uri().'/css/animate.min.css',array(),date(Ym),'');
	wp_enqueue_style( 'ch_du_bo-main', get_template_directory_uri().'/css/main.css',array(),date(Ym),'');
	wp_enqueue_style( 'ch_du_bo-owl-carousel', get_template_directory_uri().'/css/owl.carousel.css',array(),date(Ym),'');
	wp_enqueue_style( 'ch_du_bo-owl-theme', get_template_directory_uri().'/css/owl.theme.css',array(),date(Ym),'');
	wp_enqueue_style( 'ch_du_bo-responsive', get_template_directory_uri().'/css/responsive.css',array(),date(Ym),'');
	wp_enqueue_style( 'ch_du_bo-meanmenu', get_template_directory_uri().'/css/meanmenu.css',array(),date(Ym));


	if(is_page_template('page-templates/home-template.php')){
		wp_enqueue_style( 'ch_du_bo-multiselect', get_template_directory_uri().'/css/bootstrap-multiselect.css',array(),date(Ym));
		wp_enqueue_script( 'ch_du_bo-multiselect', get_template_directory_uri() . '/js/bootstrap-multiselect.js', array('jquery'), date(Ym), true );
		wp_enqueue_script( 'ch_du_bo-imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array('jquery'), date('Ym'), true );
	}
	wp_enqueue_style( 'ch_du_bo-fancybox', get_template_directory_uri().'/css/jquery.fancybox.css',array(),date(Ym));
	wp_enqueue_style( 'ch_du_bo-nice-select', get_template_directory_uri().'/css/nice-select.css',array(),date(Ym));


	wp_enqueue_script( 'ch_du_bo-fancybox', get_template_directory_uri() . '/js/jquery.fancybox.pack.js', array('jquery'), date(Ym), true );
	wp_enqueue_script( 'ch_du_bo-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), date(Ym), true );
	wp_enqueue_script( 'ch_du_bo-owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), date(Ym), true );
	wp_enqueue_script( 'ch_du_bo-meanmenu', get_template_directory_uri() . '/js/jquery.meanmenu.js', array('jquery'), date(Ym), true );
	wp_register_script( 'ch_du_bo-main', get_template_directory_uri() . '/js/main.js', array('jquery'), date(Ym), true );

	wp_register_script( 'ch_du_bo-masonary2', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'), date(Ym), true );

	// Localize the script with new data
	$translation_array = array(
		'ajax_url' => admin_url('/admin-ajax.php'),
		'site_lang' => ICL_LANGUAGE_CODE,
		'latest_viewed_post' => isset($_COOKIE['latest_viewed_post']) ? $_COOKIE['latest_viewed_post'] : NULL,
		'backlink' => isset($_GET['backlink']) ? $_GET['backlink'] : NULL,
		'newsletterTitle' => __(' Recevez nos communiqués','ch_du_bo'),
	);
	wp_localize_script( 'ch_du_bo-main', 'ch', $translation_array );

	// Enqueued script with localized data.
	wp_enqueue_script( 'ch_du_bo-main' );
	wp_enqueue_script( 'ch_du_bo-wow', get_template_directory_uri() . '/js/wow.min.js', array('jquery'), date(Ym), true );
	wp_enqueue_script( 'ch_du_bo-nice-select', get_template_directory_uri() . '/js/jquery.nice-select.min.js', array('jquery'), date(Ym), true );

}
add_action( 'wp_enqueue_scripts', 'ch_du_bo_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
require get_template_directory() . '/inc/emergency.php';
require get_template_directory() . '/inc/walker.php';
require get_template_directory() . '/inc/shortcodes.php';
require get_template_directory() . '/inc/footer-scripts.php';
require get_template_directory() . '/inc/cms-template-tags.php';
require get_template_directory() . '/inc/fund-template-tags.php';
require get_template_directory() . '/inc/social/fb/functions.php';
require get_template_directory() . '/inc/social/twitter/functions.php';
require get_template_directory() . '/inc/tinymce-custom-styles.php';
require get_template_directory() . '/inc/tinymce-buttons.php';
require get_template_directory() . '/inc/ajax-functions.php';


/*if(!function_exists('icl_get_languages')){
	function icl_get_languages($args = array()){
		$lang = array(
			'language_code' => 'de',
			'active'	=> true,
			'url'	=> site_url()
		);
		return $lang;
	}
}

if(!function_exists('have_rows')){
	function have_rows($object){
		return;
	}
}*/

//Disabling Comments
function disable_comments_for_all($open,$post_id){
    return false;
}
add_filter( 'comments_open','disable_comments_for_all', 10, 2 );


add_action( 'after_setup_theme', 'media_theme_setup' );
function media_theme_setup() {
	add_image_size( 'megamenu-img', 268, 182, true );
	add_image_size( 'slider-img', 1903, 626, true );
	add_image_size( 'fundraising-thumb', 362, 254, true );
	add_image_size( 'hero-thumb', 770, 260, true );
}

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

}

function language_dropdown(){
	$langs = icl_get_languages('skip_missing=0&orderby=KEY&order=DIR&link_empty_to=str');
	?>
	<div class="langDrop dropdown">
		<a class="dropdown-toggle" type="button" data-toggle="dropdown" href="#"><?php echo strtoupper(CH_LANG_CODE); ?><i class="fa fa-angle-down"></i></a>
		<?php if(!empty($langs)){ ?>
			<ul class="dropdown-menu">
			<?php foreach($langs as $lang){
				if($lang['active'])
					continue;
				if($lang['url'] == 'str'){
					$lang['url'] = site_url('/');
				}
				?>
				<li><a href="<?php echo $lang['url']; ?>"><?php echo strtoupper($lang['language_code']); ?></a></li>
			<?php } ?>
			</ul>
		<?php } ?>
	</div>
	<?php
}

function ch_du_bo_get_logo($lang, $emergency){
	if($emergency){
		$logo = esc_url( get_theme_mod( 'ch_du_bo_custom_emergency_logo_'.CH_LANG_CODE ));
	}else{
		$logo = esc_url( get_theme_mod( 'ch_du_bo_custom_logo_'.CH_LANG_CODE ));
	}
	return $logo;
}




// get the donate link according to language
function get_donate_link(){
	$link = esc_url( get_theme_mod( 'donate_link_'.CH_LANG_CODE ));
	if(!$link)
		$link = '#';
	return $link;
}

function custom_excerpt_length( $length ) {
	return 15;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function custom_excerpt_more( $more ) {
	return ' ';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );

function sortByCreatedTime($a, $b) {
	$a['created_time'] = strtotime($a['created_time']);
	$b['created_time'] = strtotime($a['created_time']);
    return $a['created_time'] - $b['created_time'];
}

/**
 * Function to resize image
 *
 * @param string $url
 * @param integer $width
 * @param integer $height
 * @return string
 */
function ch_du_bo_img_resize($url, $width='', $height='', $quality = 80) {
    $imgPath = pathinfo($url);
    $originalPath = str_replace(site_url('/'), ABSPATH, $url);

    if(file_exists($originalPath)){
        $requiredFileName = $imgPath['dirname'].'/'.$imgPath['filename'].'-'.$width.'X'.$height.'.'.$imgPath['extension'];
        $requiredPath = str_replace(site_url('/'), ABSPATH, $requiredFileName);

        if(!file_exists($requiredPath)){
            $image = wp_get_image_editor( $url );
            if ( ! is_wp_error( $image ) ) {
                $image->resize( $width, $height, true );
                $image->set_quality($quality);
                $image->save( $requiredPath );
            }
        }
        return $requiredFileName;
    }else{
        return get_template_directory_uri().'/images/no-image.png';
    }
}

function get_news_page_link(){
	return get_permalink( get_option( 'page_for_posts' ) );
}


/* add_filter( "wp_nav_menu_footer-menu_items", function($items, $args){
	$addBefore = "<li>".get_theme_mod('ch_du_bo_footer_right_content_'.CH_LANG_CODE)."</li>";
	$addAfter = '<li class="websiteby">'.html_entity_decode(get_theme_mod('ch_du_bo_footer_left_content_'.CH_LANG_CODE)).'</li>';
	$items = $addBefore."\n".$items."\n".$addAfter;
	return $items;
},10,2 ); */
add_filter( "wp_nav_menu_items", function($items, $args){
	if($args->theme_location != 'footer'){
		return $items;
	}
	$premenuitem = html_entity_decode(get_theme_mod('ch_du_bo_footer_left_content_'.CH_LANG_CODE));
	$postmenuitem = get_theme_mod('ch_du_bo_footer_right_content_'.CH_LANG_CODE);
	if($premenuitem)
		$addAfter = "<li>".$premenuitem."</li>";

	if($postmenuitem)
		$addBefore = '<li class="websiteby">'.html_entity_decode($postmenuitem).'</li>';

	$items = $addBefore."\n".$items."\n".$addAfter;
	//debug($items);
	return $items;
},10,2 );


add_action('rss2_item',function(){
	global $post;
	if(has_post_thumbnail($post->ID)){
		 $img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
		 echo "<custImg>".$img[0]."</custImg>";
	}
});

add_action( 'wp_import_insert_post', function($post_id, $original_post_ID, $postdata, $post){

},10,4);

add_action( 'wp_ajax_ajax_more_news', 'ajax_more_news' );
add_action( 'wp_ajax_nopriv_ajax_more_news', 'ajax_more_news' );

function ajax_more_news(){
	$args = array(
		'post__in' => $_POST['data'],
		'ordeby'	=> 'post__in',
		'posts_per_page'	=>	3,
		'paged'		=> $_POST['paged']
	);
	// The Query
	$query = new WP_Query( $args );

	// The Loop
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			if(has_post_thumbnail()){
				$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'fundraising-thumb');
			}else{
				$img = array(get_template_directory_uri().'/images/news-default.jpg');
			}

			switch(get_post_format()){
				case 'standard':
					$icon = 'icon-news.png';
					break;
				case 'image':
					$icon = 'icon-camera.png';
					break;
				default:
					$icon = 'icon-news.png';
			}
			?>
			<li id="news-<?php the_ID(); ?>">
             	<div class="col-md-4 item wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                	<div class="features center">
                        <figure>
                        	<img src="<?php echo $img[0]; ?>" alt="image 4">
                            <figcaption><a class="icon_news" href="<?php the_permalink(); ?>" ><img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $icon; ?>" alt="#"></a></figcaption>
                        </figure>
                        <div class="content-wrap">
	                        <span class="disasterdate"><?php the_time('d.m.Y'); ?></span>
	                        <h4><?php the_title(); ?></h4>
                            <?php the_excerpt(); ?>
                    	</div>
                    </div><!--/.fundraising-->
                </div><!--/.col-md-4-->
        	</li> <!--/.col-md-4-->
			<?php
		}
	}
	// Restore original Post Data
	wp_reset_postdata();
	die;
}

add_action( 'wp_ajax_ajax_get_tw_feed', 'ajax_get_tw_feed' );
add_action( 'wp_ajax_nopriv_ajax_get_tw_feed', 'ajax_get_tw_feed' );
function ajax_get_tw_feed(){
	$statuses = get_twitter_account_feed($_POST['twID']);
	foreach($statuses as $status): ?>
	<div class="item">
	
			<?php if(ICL_LANGUAGE_CODE=='fr') :?>
			<p>« <?php echo $status->text; ?> »</p>
		<?php else : ?>
			<p>«<?php echo $status->text; ?>»</p>
		<?php endif; ?>
		
		<a href="https://twitter.com/<?php echo $status->user->screen_name; ?>" class="btn underline"><?php echo '@'.$status->user->screen_name; ?></a>
	</div>
	<?php endforeach; die;
}


add_action( 'wp_ajax_ajax_more_news_list', 'ajax_more_news_list' );
add_action( 'wp_ajax_nopriv_ajax_more_news_list', 'ajax_more_news_list' );
function ajax_more_news_list(){
	// WP_Query arguments
	$args = array (
		'paged'                  => $_POST['paged'],
		'posts_per_page'         => '6',
		'offset'                 => $_POST['start'],
		'meta_query'	=>	array(
			array(
				'key'     => 'hero',
				'value'   => 'Yes',
				'compare' => '!=',
			)
		)
	);

	if(isset($_POST['catIds'])){
		$tax_query = array(
			'tax_query' => array(
				array(
					'taxonomy' => 'category',
					'field'    => 'term_id',
					'terms'    => $_POST['catIds'],
					'operator' => 'IN',
				),
			)
		);
		$args = array_merge($args,$tax_query);
		/*echo "<pre>";
		print_r($args);
		echo "</pre>";*/
	}

	// The Query
	$query = new WP_Query( $args );

	if($query->post_count < 6){
		?>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery('.more-news-btn').addClass('hidden');
			});
		</script>
		<?php
	}
	// The Loop
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			get_template_part('template-parts/content','news-post');
		}
	}

	// Restore original Post Data
	wp_reset_postdata();
	die;
}

add_action( 'wp_ajax_ajax_more_media_list', 'ajax_more_media_list' );
add_action( 'wp_ajax_nopriv_ajax_more_media_list', 'ajax_more_media_list' );
function ajax_more_media_list(){
	global $sitepress;
    $sitepress->switch_lang($_POST['lang'], true);
	// WP_Query arguments
	$hero = get_field('select_hero_post','option');
	
	//setting up a cookie
	setcookie(COOKIE_NAME, serialize($_POST), time() + (120), "/"); // 86400 = 1 day
	
	$args = array (
		//'paged'                  => $_POST['paged'],
		'posts_per_page'         => '4',
		'offset'                 => $_POST['start'],
		'post__not_in'			 => array($hero->ID)
	);

	if(isset($_POST['catIds'])){
		$tax_query = array(
			'tax_query' => array(
				array(
					'taxonomy' => 'category',
					'field'    => 'term_id',
					'terms'    => $_POST['catIds'],
					'operator' => 'IN',
				),
			)
		);
		$args = array_merge($args,$tax_query);
	}

		//debug($args);
	// The Query
	$query = new WP_Query( $args );

	if($query->post_count < 4){
		?>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery('.more-media-btn').addClass('hidden');
			});
		</script>
		<?php
	}
	// The Loop
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			get_template_part('template-parts/content','media-post');
		}
	}

	// Restore original Post Data
	wp_reset_postdata();
	die;
}

add_action( 'wp_ajax_ajax_media_filter', 'ajax_media_filter' );
add_action( 'wp_ajax_nopriv_ajax_media_filter', 'ajax_media_filter' );
function ajax_media_filter(){
	if($_POST['lang']){
		global $sitepress;
		$sitepress->switch_lang($_POST['lang']);	
	}
	
	// WP_Query arguments
	$hero = get_field('select_hero_post','option');
	
	//setting up a cookie
	setcookie(COOKIE_NAME, serialize($_POST), time() + (120), "/"); // 86400 = 1 day
	
	$args = array (
		'posts_per_page'		 => '4',
		'post__not_in'			 => array($hero->ID),
		'post_status'			 => 'publish'
	);

	if(isset($_POST['catIds'])){				
		$taxQuery = array(
			'tax_query' => array(
				array(
					'taxonomy' => 'category',
					'field'    => 'term_id',
					'terms'    => $_POST['catIds'],
					'operator' => 'IN',
				),
			)
		);
		$args = array_merge($args, $taxQuery);
	}

	if(isset($_POST['per_page'])){
		$args['posts_per_page'] = $_POST['per_page'];
	}

	//debug($args); 
	// The Query
	$query = new WP_Query( $args );

	// The Loop
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			get_template_part('template-parts/content','media-post');
		}
	}
	
	if($query->max_num_pages <= 1){
		?>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				isDataAvailable = false;
				jQuery('.more-media-btn').addClass('hidden');
			});
		</script>
		<?php
	}

	// Restore original Post Data
	wp_reset_postdata();
	die;
}

function get_post_icon($format,$social = false){
	if($social == 'Facebook'){
		return '<i class="fa fa-facebook"></i>';
	}else if($social == 'Instagram'){
		return '<img src="'.get_template_directory_uri().'/images/icon-insta.png" alt="">';
	}else if($social == 'WP'){
		if($format == 'image'){
			return '<img src="'.get_template_directory_uri().'/images/icon-camera.png" alt="#">';
		}else{
			return '<img src="'.get_template_directory_uri().'/images/icon-news.png" alt="#">';
		}
	}
}

// hook into wpcf7_before_send_mail
add_action( 'wpcf7_before_send_mail', function($contact_form){

	$submission = WPCF7_Submission::get_instance();

	$posted_data = $submission->get_posted_data();

	$toEmail = $posted_data['to'];
	
	if($toEmail){
		$sendToMailchimp = $_POST['sendToMailchimp'];
		//var_dump($_POST);

		$mail = $contact_form->prop('mail');

		$mail['recipient'] = $toEmail;

		$contact_form->set_properties( array('mail' => $mail) );
	}	
});

/**
 *
 * @param string $text String to truncate.
 * @param integer $length Length of returned string, including ellipsis.
 * @param string $ending Ending to be appended to the trimmed string.
 * @param boolean $exact If false, $text will not be cut mid-word
 * @param boolean $considerHtml If true, HTML tags would be handled correctly
 * @return string Trimmed string.
 */
function ch_du_bo_truncate($text, $length = 100, $ending = '...', $exact = false, $considerHtml = true) {
    if ($considerHtml) {
        // if the plain text is shorter than the maximum length, return the whole text
        if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
            return $text;
        }
        // splits all html-tags to scanable lines
        preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
        $total_length = strlen($ending);
        $open_tags = array();
        $truncate = '';
        foreach ($lines as $line_matchings) {
            // if there is any html-tag in this line, handle it and add it (uncounted) to the output
            if (!empty($line_matchings[1])) {
                // if it's an "empty element" with or without xhtml-conform closing slash
                if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                    // do nothing
                    // if tag is a closing tag
                } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                    // delete tag from $open_tags list
                    $pos = array_search($tag_matchings[1], $open_tags);
                    if ($pos !== false) {
                        unset($open_tags[$pos]);
                    }
                    // if tag is an opening tag
                } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                    // add tag to the beginning of $open_tags list
                    array_unshift($open_tags, strtolower($tag_matchings[1]));
                }
                // add html-tag to $truncate'd text
                $truncate .= $line_matchings[1];
            }
            // calculate the length of the plain text part of the line; handle entities as one character
            $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
            if ($total_length + $content_length > $length) {
                // the number of characters which are left
                $left = $length - $total_length;
                $entities_length = 0;
                // search for html entities
                if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                    // calculate the real length of all entities in the legal range
                    foreach ($entities[0] as $entity) {
                        if ($entity[1] + 1 - $entities_length <= $left) {
                            $left--;
                            $entities_length += strlen($entity[0]);
                        } else {
                            // no more characters left
                            break;
                        }
                    }
                }
                $truncate .= substr($line_matchings[2], 0, $left + $entities_length);
                // maximum lenght is reached, so get off the loop
                break;
            } else {
                $truncate .= $line_matchings[2];
                $total_length += $content_length;
            }
            // if the maximum length is reached, get off the loop
            if ($total_length >= $length) {
                break;
            }
        }
    } else {
        if (strlen($text) <= $length) {
            return $text;
        } else {
            $truncate = substr($text, 0, $length - strlen($ending));
        }
    }
    // if the words shouldn't be cut in the middle...
    if (!$exact) {
        // ...search the last occurance of a space...
        $spacepos = strrpos($truncate, ' ');
        if (isset($spacepos)) {
            // ...and cut the text in this position
            $truncate = substr($truncate, 0, $spacepos);
        }
    }
    // add the defined ending to the text
    $truncate .= $ending;
    if ($considerHtml) {
        // close all unclosed html-tags
        foreach ($open_tags as $tag) {
            $truncate .= '</' . $tag . '>';
        }
    }
    return $truncate;
}

function get_mailchimp_subscription_form($listID){
	?>
	<div class="newsletter">
        <!-- Begin MailChimp Signup Form -->
		<h3>Newsletter</h3>
		<p><?php _e('Nous vous alerterons lors d’une catastrophe et vous tiendrons informés sur notre actualité.','ch_du_bo'); ?></p>
        <div id="mc_embed_signup">
		<form action="//bonheur.us3.list-manage.com/subscribe/post?u=a905c457c17f2185048d2218f&amp;id=<?php echo $listID; ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate mcForm" target="_blank">
            <div id="mc_embed_signup_scroll">

        <div class="mc-field-group">
            <input type="email" value="" name="EMAIL" placeholder="<?php _e('Votre e-mail','ch_du_bo'); ?>..." class="required email newsletter-form" id="mce-EMAIL" required>
            <input type="submit" value="GO" name="subscribe" id="mc-embedded-subscribe" class="submit" required>
        </div>
            <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_bf1c1bee9948229a202de6939_<?php echo $listID; ?>" tabindex="-1" value=""></div>

            </div>
        </form>
        </div>
        <!--End mc_embed_signup-->
    </div>
	<?php
}

if (!(is_admin() )) {
    function defer_parsing_of_js ( $url ) {
        if ( FALSE === strpos( $url, '.js' ) ) return $url;
        if ( strpos( $url, 'jquery.js' ) ) return $url;
        // return "$url' defer ";
        return "$url' defer onload='";
    }
    add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );
}


function next_prev_btn_class($attr){
	$attr .= 'class="btn black rounded"';
	return $attr;
}
add_filter('previous_posts_link_attributes', 'next_prev_btn_class');
add_filter('next_posts_link_attributes', 'next_prev_btn_class');

/* Google analytics */
add_action('wp_head', function(){
	switch(CH_LANG_CODE){
		case 'fr':
			$gaCode = 'UA-100214430-1';
			break;
		case 'en':
			$gaCode = 'UA-100214430-4';
			break;
		case 'de':
			$gaCode = 'UA-100214430-3';
			break;
		case 'it':
			$gaCode = 'UA-100214430-2';
			break;
		default:
			$gaCode = 'UA-100214430-3';
	}
	?>
	<script> 
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ 
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), 
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) 
	})(window,document,'script','https://www.google-analytics.com/analytics.js' ,'ga'); 
	ga('create', '<?php echo $gaCode; ?>', 'auto'); 
	ga('send', 'pageview'); 
	</script> 
	<?php
});

/* Twitter analytics */
add_action('wp_head', function(){
	switch(CH_LANG_CODE){
		case 'fr':
			$twCode = 'nxiv0';
			break;
		case 'en':
			$twCode = 'nxn38';
			break;
		case 'de':
			$twCode = 'nxiv1';
			break;
		case 'it':
			$twCode = 'nxn3b';
			break;
		default:
			return;
			$twCode = 'UA-40884908-2';
	}
	?>
	<!-- Twitter universal website tag code --> 
	<script> 
	!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments); 
	},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js', 
	a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script'); 
	// Insert Twitter Pixel ID and Standard Event data below 
	twq('init','<?php echo $twCode; ?>'); 
	twq('track','PageView'); 
	</script> 
	<!-- End Twitter universal website tag code --> 

	<?php
});

add_action('wp_footer', function(){
	?>
	<!-- New Live Media Server -->
	<?php
});

add_action('wp_head', 'new_rel_canonical', 5);
function new_rel_canonical(){
	if(is_front_page() || is_home()){
		remove_action( 'wp_head',	'rel_canonical');
		echo '<link rel="canonical" href="' . esc_url( 'https://medien.glueckskette.ch/' ) . '" />' . "\n";
	}
}