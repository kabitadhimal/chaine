<?php
/*
Template Name: CMS
*/
get_header(); the_post(); 
	$banner_img = get_field('banner_image');
	$title_top = get_field('title_top');
	$title_bottom = get_field('title_bottom');
	$short_description = get_field('short_description');
	?>
	<section class="main-banner no-margin">           
       <div class="item" style="background:url(<?php echo $banner_img['url']; ?>) no-repeat center center">
            <div class="sliderCaption">
            	<div class="container">
	                <div class="row">
	                	<div class="col-md-8 pull-left">	
	                		<?php if($title_top != '' || $title_bottom != ''): ?>	
		                    	<h1><?php echo $title_top.' '.$title_bottom; ?></h1>
		                    <?php endif; ?>
	                      	<h4><?php echo $short_description; ?></h4>						
	                   </div>                	
	                </div>
                </div>            
            </div>		           
       </div>   
    </section><!--/.main-banner-->
<?php
$ch_du_bo_content = new Ch_du_bo_Fund_Content();
$content = $ch_du_bo_content->ch_du_bo_get_flexible_content();
echo $content; 
get_footer();