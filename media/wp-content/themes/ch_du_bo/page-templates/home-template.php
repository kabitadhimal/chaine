<?php 
/*
	Template Name: Homepage
*/
if(isset($_COOKIE[COOKIE_NAME]) && isset($_GET['backlink'])){
	$filterParams = unserialize(stripcslashes($_COOKIE[COOKIE_NAME]));	
	setcookie(COOKIE_NAME, "", time() - 3600, "/");
}
get_header(); the_post();
$hero = get_field('select_hero_post','option');
$terms = wp_get_post_terms( $hero->ID, 'category' );
$hideTwi = '';
/* debug($_COOKIE[COOKIE_NAME]);
debug($filterParams); */
if(isset($filterParams) && is_array($filterParams)){
	$hideTwi = 'hidden';
	?>
	<script type="text/javascript">
		var filterUsed = true;
		var catIds = <?php echo isset($filterParams['catIds']) ? $filterParams['catIds'] : "false"; ?>;
		var paged = <?php echo isset($filterParams['paged']) ? $filterParams['paged'] + 1 : 2; ?>;
		var start = <?php echo isset($filterParams['start']) ? $filterParams['start'] + 4 : 4; ?>;
		var disableTwt = true;
	</script>
	<?php
}
?>
<script type="text/javascript">
	var heroCats = <?php echo json_encode($terms); ?>
</script>
<?php
if(!empty($terms)){
	$catIcon = get_field('icon',"category_".$term->term_id);
	if(!$catIcon){
		$catIcon = get_template_directory_uri().'/images/btn-donate.png';
	}
}else{
	$catIcon = get_template_directory_uri().'/images/btn-donate.png';
}

wp_enqueue_script( 'ch_du_bo-masonary');
wp_enqueue_script( 'ch_du_bo-masonary2');


?>
    
    <section class="status_bar section-filter">
    	<div class="container">
        	<div class="row">
			  	<div class="col-md-2"><h4><?php _e('Filtrer par','ch_du_bo'); ?></h4></div>          	
			 	<div class="col-md-8 col-sm-9">                   
			 		<?php 
			 		$terms = get_terms( 'category', array(
					    'hide_empty' => false,
					    /*'parent'	 => 0,
						'exclude'	=> array(1),*/
						'meta_key'	 =>	'order',
						'orderby'	 => 'meta_value_num'
					) );
					if(!empty($terms)){
						?>
						<ul class="filter_menu">
						<?php
						$activeCat = NULL;
						if( isset($_GET['category']) ){
							$activeCat = $_GET['category'];
						}
						foreach ($terms as $term) {
							?>
							<li>
								<input type="radio" name="media_filter" value="<?php echo $term->term_id; ?>" id="<?php echo $term->slug; ?>" 
								<?php
									if( ($filterParams['catIds'] == $term->term_id) || ($activeCat == $term->slug)){
										echo ' checked="CHECKED" ';
									}
								?>
								/>
								<label for="<?php echo $term->slug; ?>"><?php echo $term->name; ?></label>
							</li>
							<?php
						}
						?>
						</ul>
						<?php
					}
			 		?>
			   	</div>
				<div class="col-md-2 col-sm-3"><a class="btn rounded pull-right deselectAll" href="#"><?php _e('Voir tous', 'ch_du_bo'); ?></a></div>
				</div>           
	        </div>
        </div>  	
    </section><!--/.status bar-->
    
    <section class="section-two-col">
        <div class="container">
            <div class="row">
          
                	<div class="col-md-8">
                    	<div class="media-body">
						<?php 
							$heroHide = '';
							if(isset($filterParams) && $filterParams['catIds'] != $terms[0]->term_id){
								$heroHide = 'hidden';
							}
						?>
                        <div class="features center hero <?php echo $heroHide; ?>">
							<figure class="center">
								<?php 
									if(has_post_thumbnail($hero->ID)){
										$img = wp_get_attachment_image( get_post_thumbnail_id( $hero->ID ), 'hero-thumb' );
										//debug(wp_get_attachment_image_src( get_post_thumbnail_id( $hero->ID ), 'hero-thumb' ));
									}else{
										$img = '<img src="'.get_template_directory_uri().'/images/img-col3-hero.jpg">';
									}
									
									
									echo '<a href="'.get_permalink( $hero->ID ).'">'.$img.'</a>';
								?>
								<figcaption><a href="<?php echo get_permalink($hero->ID); ?>"><img src="<?php echo $catIcon; ?>" alt="#"></a></figcaption>
							</figure>
							<div class="content-wrap">
								<span class="fundCollected center">
								<?php 
									
									if(!empty($terms)){
										echo $terms[0]->name.' / ';
									}
								?>
								<?php echo date('d.m.Y', strtotime($hero->post_date)); ?>
								</span>
								<h3><?php echo $hero->post_title; ?></h3>
								<p><?php echo ch_du_bo_truncate(strip_tags($hero->post_content),350); ?></p>

							  
							</div>
							
							
						  
							<div class="media-footer clearfix">
								<div class="col-md-12">
									<a class="btn rounded black" href="<?php the_permalink($hero->ID); ?>"><?php _e('En savoir plus','ch_du_bo'); ?></a>                        
								</div>
								
								
								<div class="col-md-12">
									<?php
										$telLink = get_field('telecharger_le_communique_link', $hero->ID);
										if( $telLink != ''):
									?>
										<a class="btn underline" href="<?php echo $telLink['url']; ?>"><?php _e('Télécharger le communiqué','ch_du_bo'); ?></a>
									<?php endif; ?>
								</div>
							</div>  <!--end of share-->
						</div>  <!--end of features-->
                        <?php $twitName = get_field('twitter_account_name'); ?>
						<div class="slider-twitter <?php echo $hideTwi; ?>">
							<div class="slider-twitter-inner hidden">
							<?php if($twitName): ?>
								
									<figure class="slide-twit"><figcaption><a class="icon_tw" href="#"><i class="fa fa-twitter"></i></a></figcaption></figure>
									<div class="secondary-slider" data-tw-id="<?php echo $twitName; ?>"></div>
							
							<?php endif; ?>
							</div>
						</div>
                    	<div class="post-Listing">
                        	
                        <!-- <h3>Communiqués Associes</h3> -->
                        <?php
                        	// WP_Query arguments
							$args = array (
								'post_type'              => array( 'post' ),
								'posts_per_page'         => '4',
								'post__not_in'			 => array($hero->ID)	
							);
							
							if( isset($filterParams['catIds']) ){
								$args['cat'] = $filterParams['catIds'];
							}
							
							if( isset($filterParams['paged']) ){
								$args['posts_per_page'] = $filterParams['paged'] * 4;
							}

							// The Query
							$query = new WP_Query( $args );

							// The Loop
							if ( $query->have_posts() ) {
								?>

                                <ul class="postList grid row"><?php
								while ( $query->have_posts() ) {
									$query->the_post();
									$terms = wp_get_post_terms( get_the_ID(), 'category' );
									get_template_part('template-parts/content','media-post');
								}
								?></ul><?php
							} 

							// Restore original Post Data
							wp_reset_postdata();
                        ?>
                        </div> 

                         <div class="center mediaAll hidden-xs">
                            <a class="btn rounded black more-media-btn" href="#"><?php _e("Plus d'actualités", "ch_du_bo"); ?></a>
                            <p style="display:none" class="news-loader"><img src="<?php echo get_template_directory_uri().'/images/ajax-loader.gif'; ?>" alt="Loader"></p>
                        </div>                       
                    
                    </div>
                    </div>
                    <div class="col-md-4">
                    
                    	<?php get_sidebar(); ?>
                    
                    </div>
                     </div><!--/.row-->                   
                
           
        </div><!--/.container-->
        
   </section><!--/.section news-->



<?php
get_footer();