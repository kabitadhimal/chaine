<?php
	$wp_customize->add_section( 'custom_link_setting', array(
		'title'    => __( 'Links' ),
		'priority' => 15,
	) );
	foreach($langs as $lang){
		/*$wp_customize->add_setting( 'donate_link_'.$lang['language_code'], array(
			'default'           => '',
			'sanitize_callback' => 'esc_url',
		) );
		$wp_customize->add_control( 'ch_du_bo_donate_'.$lang['language_code'], array(
			'settings' => 'donate_link_'.$lang['language_code'],
			'label'    => __( 'Donate '.$lang['language_code'] ),
			'section'  => 'custom_link_setting',
			'type'     => 'text',
			'priority' => 10,
		) );*/

		$wp_customize->add_setting( 'contact_'.$lang['language_code'], array(
			'default'           => '',
			'sanitize_callback' => 'esc_url',
		) );
		$wp_customize->add_control( 'ch_du_bo_contact_link_'.$lang['language_code'], array(
			'settings' => 'contact_'.$lang['language_code'],
			'label'    => __( 'Contact '.$lang['language_code'] ),
			'section'  => 'custom_link_setting',
			'type'     => 'text',
			'priority' => 12,
		) );
	}