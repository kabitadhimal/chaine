<?php
// Add new styles to the TinyMCE "formats" menu dropdown
if ( ! function_exists( 'ch_du_bo_styles_dropdown' ) ) {
	function ch_du_bo_styles_dropdown( $settings ) {

		// Create array of new styles
		$new_styles = array(
			array(
				'title'	=> __( 'Custom Styles', 'ch_du_bo' ),
				'items'	=> array(
					array(
						'title'		=> __('Rounded Black Button','ch_du_bo'),
						'selector'	=> 'a',
						'classes'	=> 'btn rounded black'
					),
					array(
						'title'		=> __('Rounded Red Button','ch_du_bo'),
						'selector'	=> 'a',
						'classes'	=> 'btn rounded red'
					),
					array(
						'title'		=> __('Rounded Red Border Button','ch_du_bo'),
						'selector'	=> 'a',
						'classes'	=> 'btn border red'
					),
					array(
						'title'		=> __('Rounded Grey Border Button','ch_du_bo'),
						'selector'	=> 'a',
						'classes'	=> 'btn border'
					),
					array(
						'title'		=> __('Underlined Button','ch_du_bo'),
						'selector'	=> 'a',
						'classes'	=> 'btn underline',
					),
					array(
						'title'		=> __('Underlined Blank Button','ch_du_bo'),
						'selector'	=> 'a',
						'classes'	=> 'btn underline black',
					),
					array(
						'title'		=> __('Underlined Grey Button','ch_du_bo'),
						'selector'	=> 'a',
						'classes'	=> 'btn underline grey',
					),
				),
			),
		);

		// Merge old & new styles
		$settings['style_formats_merge'] = true;

		// Add new styles
		$settings['style_formats'] = json_encode( $new_styles );

		// Return New Settings
		return $settings;

	}
}
add_filter( 'tiny_mce_before_init', 'ch_du_bo_styles_dropdown' );