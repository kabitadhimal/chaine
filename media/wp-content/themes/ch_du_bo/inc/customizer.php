<?php
/**
 * Diocèse de Sion Theme Customizer.
 *
 * @package Chaîne_du_Bonheur
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */

function ch_du_bo_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if(function_exists('icl_get_languages')){
		$langs = icl_get_languages('skip_missing=0&orderby=KEY&order=DIR&link_empty_to=str');
	}else{
		$langs = array(CH_LANG_CODE);
	}

	/**** Settings ****/
	$wp_customize->add_setting('ch_du_bo_color_scheme',array(
			'type'	=>	'theme_mod',
			'default'	=>	'#e02c3f',
			'capability'	=>	'manage_options',
			'transport'	=>	'refresh',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);
	$wp_customize->add_setting('ch_du_bo_custom_css',array(
			'type'	=>	'theme_mod',
			'default'	=>	'',
			'capability'	=>	'manage_options',
			'transport'	=>	'refresh',
			'sanitize_callback'	=>	'',
		)
	);

	/* Logo Section */
	include 'customizer/logos.php';
	/* Logo Section */
	/* Links */
	include 'customizer/links.php';
	/* Links */
	/* Social Setting */
	include 'customizer/social-links.php';
	/* Social Setting */
	/* API Setting */
	include 'customizer/api.php';
	/* API Setting */

	$wp_customize->add_setting('ch_du_bo_google_api',array(
			'type'	=>	'theme_mod',
			'default'	=>	'',
			'capability'	=>	'manage_options',
			'transport'	=>	'refresh',
			'sanitize_callback'	=>	'sanitize_text_field'
		)
	);
	$wp_customize->add_setting('ch_du_bo_mailchimp_list_id_fr',array(
			'type'	=>	'theme_mod',
			'default'	=>	'',
			'capability'	=>	'manage_options',
			'transport'	=>	'refresh',
			'sanitize_callback'	=>	'sanitize_text_field'
		)
	);
	$wp_customize->add_setting('ch_du_bo_mailchimp_list_id_de',array(
			'type'	=>	'theme_mod',
			'default'	=>	'',
			'capability'	=>	'manage_options',
			'transport'	=>	'refresh',
			'sanitize_callback'	=>	'sanitize_text_field'
		)
	);

	/**** Contact Setting ****/
	$wp_customize->add_setting('ch_du_bo_email',array(
			'type'	=>	'theme_mod',
			'default'	=>	'',
			'capability'	=>	'manage_options',
			'transport'	=>	'refresh',
			'sanitize_callback'	=>	'sanitize_email'
		)
	);
	$wp_customize->add_setting('ch_du_bo_phone_number',array(
			'type'	=>	'theme_mod',
			'default'	=>	'',
			'capability'	=>	'manage_options',
			'transport'	=>	'refresh',
			'sanitize_callback'	=>	'sanitize_text_field'
		)
	);
	$wp_customize->add_setting('ch_du_bo_fax',array(
			'type'	=>	'theme_mod',
			'default'	=>	'',
			'capability'	=>	'manage_options',
			'transport'	=>	'refresh',
			'sanitize_callback'	=>	'sanitize_text_field'
		)
	);
	$wp_customize->add_setting('ch_du_bo_copyright',array(
			'type'	=>	'theme_mod',
			'default'	=>	'',
			'capability'	=>	'manage_options',
			'transport'	=>	'refresh',
			'sanitize_callback'	=>	''
		)
	);
	$wp_customize->add_setting('ch_du_bo_extra_info',array(
			'type'	=>	'theme_mod',
			'default'	=>	'',
			'capability'	=>	'manage_options',
			'transport'	=>	'refresh',
			'sanitize_callback'	=>	''
		)
	);
	$wp_customize->add_setting('ch_du_bo_news_subtitle',array(
			'type'	=>	'theme_mod',
			'default'	=>	'',
			'capability'	=>	'manage_options',
			'transport'	=>	'refresh',
			'sanitize_callback'	=>	''
		)
	);
	$wp_customize->add_setting('ch_du_bo_news_banner_image',array(
			'type'	=>	'theme_mod',
			'default'	=>	'',
			'capability'	=>	'manage_options',
			'transport'	=>	'refresh',
			'sanitize_callback'	=>	''
		)
	);

	/**** Sections ****/
	/*$wp_customize->add_section( 'ch_du_bo_section_color_scheme',
	    array(
	        'title'          => __('Farbe','ch_du_bo'),
	        'description'    => 'Color Scheme for '.get_bloginfo(),
	        'capability'     => 'manage_options',
	        'theme-supports' => '',
	        'priority'       => 10,
	        'panel'          => 'ch_du_bo_site_setting_panel',
	    )
	);*/
	$wp_customize->add_section( 'ch_du_bo_section_api',
	    array(
	        'title'          => 'APIs',
	        'description'    => 'Apis for '.get_bloginfo(),
	        'capability'     => 'manage_options',
	        'theme-supports' => '',
	        'priority'       => 11,
	        'panel'          => 'ch_du_bo_site_setting_panel',
	    )
	);

	/*$wp_customize->add_section( 'ch_du_bo_section_news',
	    array(
	        'title'          => 'Actualite',
	        'description'    => 'Actualite for '.get_bloginfo(),
	        'capability'     => 'manage_options',
	        'theme-supports' => '',
	        'priority'       => 13,
	        'panel'          => 'ch_du_bo_site_setting_panel',
	    )
	);*/
	$wp_customize->add_section( 'ch_du_bo_section_contact',
	    array(
	        'title'          => 'Contact',
	        'description'    => 'Contact for '.get_bloginfo(),
	        'capability'     => 'manage_options',
	        'theme-supports' => '',
	        'priority'       => 14,
	        'panel'          => 'ch_du_bo_site_setting_panel',
	    )
	);
	/*$wp_customize->add_section( 'ch_du_bo_section_footer',
	    array(
	        'title'          => 'Footer',
	        'description'    => 'Footer for '.get_bloginfo(),
	        'capability'     => 'manage_options',
	        'theme-supports' => '',
	        'priority'       => 15,
	        'panel'          => 'ch_du_bo_site_setting_panel',
	    )
	);*/

	/**** Panel ****/
	$wp_customize->add_panel( 'ch_du_bo_site_setting_panel',
	    array(
	        'title'          => 'Site Setting',
	        'description'    => 'Setting for '.get_bloginfo().'',
	        'capability'     => 'manage_options',
	        'theme-supports' => '',
	        'priority'       => 10,
	    )
	);

	$wp_customize->add_section( 'emergency_setting', array(
		'title'    => __( 'Emergency Setting' ),
		'priority' => 0,
	) );

	$wp_customize->add_setting( 'ch_emergency', array(
		'default'           => 0,
		'sanitize_callback' => 'absint',
	) );

	/**** Emergency Controls ****/
	$wp_customize->add_control( 'ch_du_bo_emergency', array(
		'settings' => 'ch_emergency',
		'label'    => __( 'In case of Emergency.' ),
		'section'  => 'emergency_setting',
		'type'     => 'checkbox',
		'priority' => 10,
	) );

	/*$wp_customize->add_control( new WP_Customize_Color_Control(
	        $wp_customize,
	        'ch_du_bo_color_scheme',
	        array(
	            'label'    => 'Color Scheme',
	            'section'  => 'ch_du_bo_section_color_scheme',
	            'settings' => 'ch_du_bo_color_scheme',
	            'priority' => 10,
	            'type'     => 'color',
	        )
	) );
	$wp_customize->add_control( new WP_Customize_Control(
	        $wp_customize,
	        'ch_du_bo_custom_css',
	        array(
	            'label'    => 'Custom Css',
	            'section'  => 'ch_du_bo_section_color_scheme',
	            'settings' => 'ch_du_bo_custom_css',
	            'priority' => 20,
	            'type'	=>	'textarea'
	        )
	) );*/

	$wp_customize->add_control( new WP_Customize_Control(
	        $wp_customize,
	        'ch_du_bo_google_api',
	        array(
	            'label'    => 'Google API key',
	            'section'  => 'ch_du_bo_section_api',
	            'settings' => 'ch_du_bo_google_api',
	            'priority' => 10,
	            'type'	=>	'text'
	        )
	) );
	$wp_customize->add_control( new WP_Customize_Control(
	        $wp_customize,
	        'ch_du_bo_mailchimp_list_id_fr',
	        array(
	            'label'    => 'MailChimp List ID FR',
	            'section'  => 'ch_du_bo_section_api',
	            'settings' => 'ch_du_bo_mailchimp_list_id_fr',
	            'priority' => 11,
	            'type'	=>	'text'
	        )
	) );
	$wp_customize->add_control( new WP_Customize_Control(
	        $wp_customize,
	        'ch_du_bo_mailchimp_list_id_de',
	        array(
	            'label'    => 'MailChimp List ID DE',
	            'section'  => 'ch_du_bo_section_api',
	            'settings' => 'ch_du_bo_mailchimp_list_id_de',
	            'priority' => 12,
	            'type'	=>	'text'
	        )
	) );
	
	
	foreach($langs as $lang){
		$wp_customize->add_setting('ch_du_bo_address_'.$lang['language_code'],array(
				'type'	=>	'theme_mod',
				'default'	=>	'',
				'capability'	=>	'manage_options',
				'transport'	=>	'refresh',
				'sanitize_callback'	=>	'',
			)
		);
		$wp_customize->add_control( new WP_Customize_Control(
				$wp_customize,
				'ch_du_bo_address_'.$lang['language_code'],
				array(
					'label'    => 'Address '.$lang['language_code'],
					'section'  => 'ch_du_bo_section_contact',
					'settings' => 'ch_du_bo_address_'.$lang['language_code'],
					'priority' => 10,
					'type'	=>	'textarea'
				)
		) );
	}
	
	/*$wp_customize->add_control( new WP_Customize_Control(
	        $wp_customize,
	        'ch_du_bo_email',
	        array(
	            'label'    => 'Email',
	            'section'  => 'ch_du_bo_section_contact',
	            'settings' => 'ch_du_bo_email',
	            'priority' => 11,
	            'type'	=>	'email'
	        )
	) );
	$wp_customize->add_control( new WP_Customize_Control(
	        $wp_customize,
	        'ch_du_bo_phone_number',
	        array(
	            'label'    => 'Phone Number',
	            'section'  => 'ch_du_bo_section_contact',
	            'settings' => 'ch_du_bo_phone_number',
	            'priority' => 11,
	            'type'	=>	'text'
	        )
	) );
	$wp_customize->add_control( new WP_Customize_Control(
	        $wp_customize,
	        'ch_du_bo_fax',
	        array(
	            'label'    => 'Fax',
	            'section'  => 'ch_du_bo_section_contact',
	            'settings' => 'ch_du_bo_fax',
	            'priority' => 12,
	            'type'	=>	'text'
	        )
	) );*/
	/*$wp_customize->add_control( new WP_Customize_Control(
	        $wp_customize,
	        'ch_du_bo_copyright',
	        array(
	            'label'    => 'Copyright',
	            'section'  => 'ch_du_bo_section_footer',
	            'settings' => 'ch_du_bo_copyright',
	            'priority' => 10,
	            'type'	=>	'text'
	        )
	) );
	$wp_customize->add_control( new WP_Customize_Control(
	        $wp_customize,
	        'ch_du_bo_extra_info',
	        array(
	            'label'    => 'Right Content',
	            'section'  => 'ch_du_bo_section_footer',
	            'settings' => 'ch_du_bo_extra_info',
	            'priority' => 10,
	            'type'	=>	'textarea'
	        )
	) );*/
	/*$wp_customize->add_control( new WP_Customize_Control(
	        $wp_customize,
	        'ch_du_bo_news_subtitle',
	        array(
	            'label'    => 'Subtitle',
	            'section'  => 'ch_du_bo_section_news',
	            'settings' => 'ch_du_bo_news_subtitle',
	            'priority' => 10,
	            'type'	=>	'text'
	        )
	) );
	$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'ch_du_bo_news_banner_image',
           array(
               'label'      => __( 'Upload a Banner', 'ch_du_bo' ),
               'section'    => 'ch_du_bo_section_news',
               'settings'   => 'ch_du_bo_news_banner_image',
               'priority' => 11,
               'button_labels'	=>	array(
               		'select'       => __( 'Select Banner' ),
					'change'       => __( 'Change Banner' ),
					'remove'       => __( 'Remove Banner' ),
					'default'      => __( 'Default' ),
					'placeholder'  => __( 'No Banner selected' ),
					'frame_title'  => __( 'Select Banner' ),
					'frame_button' => __( 'Choose Banner' ),
               	)
           )
       )
   	);*/
}
add_action( 'customize_register', 'ch_du_bo_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function ch_du_bo_customize_preview_js() {
	wp_enqueue_script( 'ch_du_bo_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'ch_du_bo_customize_preview_js' );
