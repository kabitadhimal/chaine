<?php 
// Check if site is in emergency mode
function is_emergency(){
	$em = get_theme_mod('ch_emergency');
	if($em){
		return true;
	}else{
		return false;
	}
}

/* Set the emergency start date */
function set_emergency_start_date(){
	if(!is_emergency()){
		return;
	}
	add_option('ch_du_bo_emergency_start_date',date('Y-m-d'));
}

/* Get the emergency start date. If date is not set, set date and get the emergency start date */
function get_emergency_start_date(){
	if(!is_emergency()){
		return;
	}
	$emergency_date = get_option('ch_du_bo_emergency_start_date');
	if(!$emergency_date){
		set_emergency_start_date();
		get_emergency_start_date();
		return;
	}else{
		return $emergency_date;
	}
}

/* Delete the emergency start date */
function delete_emergency_start_date(){
	if(is_emergency()){
		return;
	}
	delete_option('ch_du_bo_emergency_start_date');
}

/* Get date difference between today & emergency start date */
function get_emergency_activated_days(){
	if(!is_emergency()){
		return 0;
	}
	$date1=date_create($GLOBALS['emergency_date']);
	$date2=date_create(date('Y-m-d'));
	$diff=date_diff($date1,$date2);
	$days = $diff->format("%a");
	return $days;
}

/* Setting up emergency setting in head */
add_action('wp_head',function(){
	if(is_emergency()){
		$GLOBALS['emergency_date'] = get_emergency_start_date();
	}else{
		$GLOBALS['emergency_date'] = NULL;
		delete_emergency_start_date();
	}
});