<?php
add_action( 'wp_ajax_mce_newsletter', 'mce_newsletter' );
add_action( 'wp_ajax_nopriv_mce_newsletter', 'mce_newsletter' );
function mce_newsletter(){
	?>
	<!-- Begin MailChimp Signup Form -->
	<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
	<style type="text/css">
		#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
		/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
		   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
	</style>
	<div id="mc_embed_signup">
	<form action="<?php echo $_GET['mce_url']; ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	    <div id="mc_embed_signup_scroll">
		<h2><?php _e('Abonnez-vous à notre liste de diffusion','ch_du_bo'); ?></h2>
	<div class="indicates-required"><span class="asterisk">*</span> <?php _e('Indique obligatoire','ch_du_bo'); ?></div>
	<div class="mc-field-group">
		<label for="mce-EMAIL"><?php _e('Adresse électronique','ch_du_bo'); ?>  <span class="asterisk">*</span>
	</label>
		<input type="email" value="<?php echo $_GET['email']; ?>" name="EMAIL" class="required email" id="mce-EMAIL">
	</div>
	<div class="mc-field-group">
		<label for="mce-SALUTATION"><?php _e('Civilité','ch_du_bo'); ?>  <span class="asterisk">*</span>
	</label>
		<select name="SALUTATION" class="required" id="mce-SALUTATION">
		<option value=""></option>
		<option value="M">M</option>
	<option value="Mme">Mme</option>

		</select>
	</div>
	<div class="mc-field-group">
		<label for="mce-FNAME"><?php _e('Prénom','ch_du_bo'); ?>  <span class="asterisk">*</span>
	</label>
		<input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
	</div>
	<div class="mc-field-group">
		<label for="mce-LNAME"><?php _e('Nom de famille','ch_du_bo'); ?>  <span class="asterisk">*</span>
	</label>
		<input type="text" value="" name="LNAME" class="required" id="mce-LNAME">
	</div>
	<div class="mc-field-group">
		<label for="mce-CITY"><?php _e('Ville','ch_du_bo'); ?> </label>
		<input type="text" value="" name="CITY" class="" id="mce-CITY">
	</div>
	<div class="mc-field-group">
		<label for="mce-COUNTRY"><?php _e('Pays','ch_du_bo'); ?> </label>
		<input type="text" value="" name="COUNTRY" class="" id="mce-COUNTRY">
	</div>
		<div id="mce-responses" class="clear">
			<div class="response" id="mce-error-response" style="display:none"></div>
			<div class="response" id="mce-success-response" style="display:none"></div>
		</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
	    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_a905c457c17f2185048d2218f_763066042f" tabindex="-1" value=""></div>
	    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
	    </div>
	</form>
	</div>
	<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[5]='SALUTATION';ftypes[5]='dropdown';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='CITY';ftypes[3]='text';fnames[4]='COUNTRY';ftypes[4]='text'; /*
	 * Translated default messages for the $ validation plugin.
	 * Locale: FR
	 */
	$.extend($.validator.messages, {
	        required: "<?php _e('Ce champ est requis.','ch_du_bo'); ?>",
	        remote: "Veuillez remplir ce champ pour continuer.",
	        email: "<?php _e('Veuillez entrer une adresse email valide.','ch_du_bo'); ?>",
	        url: "Veuillez entrer une URL valide.",
	        date: "Veuillez entrer une date valide.",
	        dateISO: "Veuillez entrer une date valide (ISO).",
	        number: "Veuillez entrer un nombre valide.",
	        digits: "Veuillez entrer (seulement) une valeur numérique.",
	        creditcard: "Veuillez entrer un numéro de carte de crédit valide.",
	        equalTo: "Veuillez entrer une nouvelle fois la même valeur.",
	        accept: "Veuillez entrer une valeur avec une extension valide.",
	        maxlength: $.validator.format("Veuillez ne pas entrer plus de {0} caractères."),
	        minlength: $.validator.format("Veuillez entrer au moins {0} caractères."),
	        rangelength: $.validator.format("Veuillez entrer entre {0} et {1} caractères."),
	        range: $.validator.format("Veuillez entrer une valeur entre {0} et {1}."),
	        max: $.validator.format("Veuillez entrer une valeur inférieure ou égale à {0}."),
	        min: $.validator.format("Veuillez entrer une valeur supérieure ou égale à {0}.")
	});}(jQuery));var $mcj = jQuery.noConflict(true);</script>
	<!--End mc_embed_signup-->
	<?php
	die();
}
?>
