<?php

class Ch_du_bo_Content{
	public function ch_du_bo_get_flexible_content(){
		// check if the flexible content field has rows of data
		if( have_rows('flexible_content') ):

			ob_start();

			// loop through the rows of data
			while ( have_rows('flexible_content') ) : the_row();

				if( get_row_layout() == 'slider_block' ):

					$this->ch_du_bo_get_slider();

				elseif( get_row_layout() == 'fundraising_campaign_block' ):

					$this->ch_du_bo_get_fundraising_campaign_block();

				elseif( get_row_layout() == 'video_block' ):

					$this->ch_du_bo_get_video_block();

				elseif( get_row_layout() == 'how_it_works' ):

					$this->ch_du_bo_get_how_it_works();

				elseif( get_row_layout() == 'news_section' ):

					$this->ch_du_bo_get_news_section();

				elseif( get_row_layout() == 'html_free_area_section' ):

					$this->ch_du_bo_get_html_free_area_section();

				elseif( get_row_layout() == 'swiss_solidarity_section' ):

					$this->ch_du_bo_get_swiss_solidarity_section();

				endif;

			endwhile;

			$content = ob_get_contents();
			ob_end_clean();
			return $content;

		else:

			return false;

		endif;
	}

	private function ch_du_bo_get_slider(){
		if(is_emergency()){ return; }
		if( have_rows('slider') ):
		?>
		<section class="main-slider no-margin"  role="banner">
			<?php while ( have_rows('slider') ) : the_row(); 
				$imageID = get_sub_field('image');
				$image = wp_get_attachment_image_src($imageID,'slider-img');
				$title = get_sub_field('title');
				$content = get_sub_field('content');
				$link_text = get_sub_field('link_text');
				$url = get_sub_field('link') ? get_sub_field('link') : '#';
				$link_open_in_new_tab = get_sub_field('link_open_in_new_tab');
				$aClass = '';
				if($link_open_in_new_tab == 'No'){
					$aClass = 'open-here';
				}
				$position = get_sub_field('position');
				if($position == 'Left'){
					$class = 'content-left';
				}else if($position == 'Middle'){
					$class = 'pull-middle';
				}else if($position == 'Right'){
					$class = 'content-right';
				}
				?>
				<div class="item" style="background:url(<?php echo $image[0]; ?>) no-repeat center center">
		            <div class="sliderCaption">
		            	<div class="container">
			                <div class="row">
			                	<div class="col-md-6 <?php echo $class; ?>">
			                		<?php if($title): ?>
				                    	<span class="coutry"><?php echo $title; ?></span>
				                    <?php endif; ?>
			                    	<?php echo $content; ?>
									<a href="<?php echo $url; ?>" class="btn rounded <?php echo $aClass; ?>"><?php echo $link_text; ?></a>
			                    </div>
			                </div>
		                </div>
		            </div>
	           	</div>
           	<?php endwhile; ?>
		</section>
		<?php
		endif;
	}

	private function ch_du_bo_get_fundraising_campaign_block(){
		$fundIds = get_sub_field('fund_raising_posts');
		$query = new WP_Query( array( 'post_type' => 'fundraising-campaign', 'post__in' => $fundIds, 'orderby'	=>	'post__in' ) );
		if ( $query->have_posts() ) {
			?>			
			<section class="section-threeColumn section-fundraising">
    			<div class="container">
    				<div class="row">
    				<div class="center title wow fadeInDown animated hidden-xs">
			            <h3><?php the_sub_field('fund_raising_title'); ?></h3>          
			        </div>	
            			<ul class="mobile-carousel">
			<?php
			while ( $query->have_posts() ) { 
				$query->the_post();
				$fundReceived = get_field('funds_received');
				?>
				<li>
					<div class="col-md-4 col-xs-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    	<div class="features center">
                         <figure>
                         	<?php
                         		if(has_post_thumbnail()){
                         			$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'fundraising-thumb');
                         		}
                         	?>
                        	<img src="<?php echo $img[0]; ?>" alt="image 1">
                            <figcaption><a href="<?php the_field('iraiser_website_link'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/btn-donate.png" alt="#"><span><?php _e('Faire un don', 'ch_du_bo'); ?></span></a></figcaption>
                        </figure>
                        <div class="content-wrap">
                        	<?php if($fundReceived != ''): ?>
	                            <span class="fundCollected"><?php echo $fundReceived; ?></span>
	                        <?php endif; ?>
                            <h3><?php the_title(); ?></h3>
                            <p><?php the_excerpt(); ?></p>
                            <a class="btn underline" href="<?php the_field('iraiser_website_link'); ?>"><?php _e('Faire un don','ch_du_bo'); ?></a>
                        </div>
                         <div class="overlay"><a href="<?php the_field('iraiser_website_link'); ?>"></a></div>
                        </div><!--/.fundraising-->
                    </div><!--/.col-md-4-->
                </li>
				<?php
			}
			/* Restore original Post Data */
			wp_reset_postdata();
			?>
							</ul>
						</div>
					</div>
				</section>
			<?php
		} else {
			// no posts found
		}
	}

	private function ch_du_bo_get_video_block(){
		$imageID = get_sub_field('image');
		$img = wp_get_attachment_image_src($imageID,'full');
		?>
		<section class="section-video" style="background:url(<?php echo $img[0]; ?>) no-repeat center center">
	        <div class="container">
	            <div class="center wow fadeInDown">
                    <h3><?php the_sub_field('video_title_top'); ?></h3>
                    <h2><?php the_sub_field('video_title_bottom'); ?></h2>
                  
                   <div class="embed-responsive embed-responsive-16by9"> <a  class="play_btn video" href="<?php the_sub_field('video_link'); ?>?fs=1&amp;autoplay=1"><span><?php _e('Voir la vidéo', 'ch_du_bo'); ?></span></a></div>
	            </div>
	        </div><!--/.container-->
	    </section><!--/.section videos-->
		<?php
	}

	private function ch_du_bo_get_how_it_works(){
		if( have_rows('blocks') ): ?>
			<section class="section-howworks">	   
            	<div class="center wow fadeInDown">
          			<div class="container">  
            			<div class="row">
			<?php while ( have_rows('blocks') ) : the_row(); ?>
				<div class="col-md-6 col-xs-6">
                    <div class="howworks-content">
                       <h3><?php the_sub_field('title'); ?></h3>
                       <p><?php the_sub_field('description'); ?></p>
                       <?php if(get_sub_field('icon')): ?>
	                       <figure><a href="<?php the_sub_field('link'); ?>" class=""><img src="<?php echo get_sub_field('icon'); ?>" alt=""></a></figure>
	                   <?php endif; ?>
                       <a href="<?php the_sub_field('link'); ?>" class="btn underline"><?php the_sub_field('link_text'); ?></a>
                       </div>
               </div>	
			<?php endwhile; ?>
						</div>
					</div>
				</div>
			</section>
			<?php
		endif;
	}

	private function ch_du_bo_get_news_section(){
		switch (CH_LANG_CODE) {
			case 'fr':
				$fbPageId = 'chainedubonheur';
				break;
			case 'en':
				$fbPageId = 'swisssolidarity';
				break;
			case 'it':
				$fbPageId = 'catenadellasolidarieta';
				break;			
			default:
				$fbPageId = 'glueckskette';
				break;
		}
		$fbMan = new FB_Page_Manager($fbPageId);
		$fbPosts = $fbMan->get_latest_posts(2);
		
		// WP_Query arguments
		$args = array (
			'post_type'              => array( 'post' ),
			'posts_per_page'         => '3',
		);

		// The Query
		$query = new WP_Query( $args );
		$wpPosts = array();
		// The Loop
		if ( $query->have_posts() ) {
			$i = 0;
			while ( $query->have_posts() ) {
				$query->the_post();
				$wpPosts[$i]['name'] = get_the_title();
				$wpPosts[$i]['link'] = get_permalink();
				if(has_post_thumbnail()){
					$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
					$wpPosts[$i]['full_picture'] = $img[0];
				}else{
					$wpPosts[$i]['full_picture'] = get_template_directory_uri().'/images/img-col3.jpg';
				}
				$wpPosts[$i]['created_time'] = get_the_time(DATE_ISO8601);
				$wpPosts[$i]['message'] = get_the_excerpt();
				$wpPosts[$i]['id']	= get_the_ID();
				$wpPosts[$i]['src']	= 'self';
				$i++;
			}
		} else {
			// no posts found
		}

		$allPosts = array();
		$allPosts = array_merge($fbPosts,$wpPosts);

		usort($allPosts, 'sortByCreatedTime');

		if(!empty($allPosts)){
			$count = 0;
			?>
			<section class="section-news">
		        <div class="container">
		            <div class="row">
			            <div class="center title wow fadeInDown animated">
				            <h3><?php the_sub_field('title'); ?></h3>          
				        </div>		            
		                <div class="news-listing">
			                <ul class="mobile-carousel">
			                <?php foreach($allPosts as $post): 
			                if(!$post['src']){
			                	$iconDisp = '<i class="fa fa-facebook"></i>';
			                	$iconClass = 'icon_fb';
			                	$img = $post['full_picture'];
			                }else{
			                	$iconDisp = '<img src="'.get_template_directory_uri().'/images/icon-news.png" alt="#">';
			                	$iconClass = 'icon_news';
			                	if($post['full_picture'] != NULL):
			                		$img = ch_du_bo_img_resize($post['full_picture'],360,253);
			                	endif;
			                	//$img = $img[0];
			                }
			                ?>
			                	<li> 
			                		<div class="col-md-4 item wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
				                    	<div class="features center">
				                         	<figure>
				                         		<?php if($img): ?>
						                        	<img src="<?php echo $img; ?>" alt="<?php echo $post['name']; ?>">
						                        <?php endif; ?>
					                            <figcaption><a class="<?php echo $iconClass; ?>" href="<?php echo $post['link']; ?>" ><?php echo $iconDisp; ?></a></figcaption>
				                        	</figure>
				                        	<div class="content-wrap">
				                        		<?php
				                        		if($post['name']){
				                        			echo "<h4>".$post['name']."</h4>";
				                        		}
				                        		?>
				                            	<p><?php echo $post['message']; ?></p>
				                            
				                        	</div>				                       
				                        </div><!--/.fundraising-->
			                    	</div><!--/.col-md-4-->
			                    </li>
		                	<?php 
		                		if($count == 2){
		                			break;
		                		}
		                		$count++; endforeach; ?>
			                </ul>
		               	</div>		               	
		                <div class="center newsAll hidden-xs">
		                	<a class="btn border" href="<?php echo get_news_page_link(); ?>"><?php the_sub_field('plus_dactualites_text'); ?></a>
		                </div>
	            	</div>
        		</div>
        	</section>

			<?php
		}

		// Restore original Post Data
		wp_reset_postdata();
	}

	private function ch_du_bo_get_html_free_area_section(){
		$total = (int)get_sub_field('columns');

		$col_1_bg_color = get_sub_field('col_1_mobile_background_color');
		if($col_1_bg_color == ''){
			$col_1_bg_color = '#e5222f';
		}
		$col_2_bg_color = get_sub_field('col_2_mobile_background_color');
		if($col_2_bg_color == ''){
			$col_2_bg_color = '#3a3a3b';
		}
		$col_3_bg_color = get_sub_field('col_3_mobile_background_color');
		if($col_3_bg_color == ''){
			$col_3_bg_color = '#3a3a3b';
		}

		$col_1_link_url = get_sub_field('col_1_link_url');
		$col_1_open_new_tab = get_sub_field('col_1_link_open_in_new_tab');
		$col_1_class = '';
		if($col_1_open_new_tab == 'No'){
			$col_1_class = 'open-here';
		}

		$col_2_link_url = get_sub_field('col_2_link_url');
		$col_2_open_new_tab = get_sub_field('col_2_link_open_in_new_tab');
		$col_2_class = '';
		if($col_2_open_new_tab == 'No'){
			$col_2_class = 'open-here';
		}
		if($total == 2){
			$col = '6';
		}else{
			$col = '4';
		}
		?>
		<section class="section-htmlfree">
	        <div class="container">
	            <div class="row">
		            <div class="center title hidden-xs wow fadeInDown animated">
			            <h3><?php the_sub_field('title'); ?></h3>          
			        </div>
		        	<div class="wow fadeInDown">                  
                     	<div class="col-md-4">
                     		<figure>
                            	<div class="hidden-xs"><img src="<?php the_sub_field('col_1_bg_image'); ?>" alt=""></div>
                            	<?php if($col_1_link_url != ''): ?>
	                            	<figcaption>
	                            		<a class="btn underline itemred <?php echo $col_1_class; ?>" href="<?php echo $col_1_link_url; ?>" style="background:<?php echo $col_1_bg_color; ?>"><span><?php the_sub_field('col_1_link_title'); ?></span></a></figcaption>
	                            <?php endif;?>
                            </figure>
                     	</div>
                 	 	<div class="col-md-4">
                     		<figure>
                            	<div class="hidden-xs"><img src="<?php the_sub_field('col_2_bg_image'); ?>" alt=""></div>
                            	<figcaption>
                            		<a class="btn underline itemblk <?php echo $col_2_class; ?>" href="<?php echo $col_2_link_url; ?>" style="background:<?php echo $col_2_bg_color; ?>"><span><?php the_sub_field('col_2_link_title'); ?></span></a></figcaption>
                            </figure>
                     	</div>
                     	<?php if($total == 3){ ?>
                     	<div class="col-md-4">
                     		<figure>
                            	<div class="hidden-xs"><img src="<?php the_sub_field('col_3_bg_image'); ?>" alt=""></div>
                            	<figcaption>
                                	<a class="btn underline itemblk video" href="<?php the_sub_field('col_3_video_url'); ?>" style="background:<?php echo $col_3_bg_color; ?>"><span><?php the_sub_field('col_3_title'); ?></span></a>
                                </figcaption>
                            </figure>
                     	</div>   
                     	<?php } ?>
	                </div><!--/.-->
				</div>
			</div>
		</section>
		<?php
	}

	private function ch_du_bo_get_swiss_solidarity_section(){
		$bgImage = get_sub_field('background_image');
		$h3Title = get_sub_field('title_top_small');
		?>
		<section class="section-solidarity" style="background:url(<?php echo $bgImage['url']; ?>) no-repeat center center">
	        <div class="container">
	       
	            <div class="center wow fadeInDown">
	            	<?php if($h3Title != ''): ?>
		            	<h3><?php echo $h3Title; ?></h3>
		            <?php endif; ?>
	                <h2><?php the_sub_field('title_bottom_big'); ?></h2>
	                
	            </div>    
	            <?php 

				$totalRow = count(get_sub_field('factsheet_parts'));
	            if( have_rows('factsheet_parts') ): ?>
	            	<div class="partners">
		               	<div class="row"> 
		                	<ul>
		                	<?php while ( have_rows('factsheet_parts') ) : the_row(); 
		                		$smallText = get_sub_field('small_text');
		                	?>
		                		<li class="col-md-<?php echo 12/$totalRow; ?> col-xs-<?php echo 12/$totalRow; ?>"><img src="<?php the_sub_field('icon'); ?>"><h4><?php the_sub_field('value'); ?></h4>
		                		<?php if($smallText != ''): ?>
			                		<p><?php echo $smallText ?></p>
			                	<?php endif; ?>
		                		</li>
		                	<?php endwhile; ?>
		                	</ul>
		                </div>
		            </div>
	            <?php endif; ?>      
	        </div><!--/.container-->
	    </section><!--/.swiss solidarity-->
		<?php
	}
}