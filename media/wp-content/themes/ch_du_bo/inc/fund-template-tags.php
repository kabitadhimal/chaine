<?php

class Ch_du_bo_Fund_Content{
	public function ch_du_bo_get_flexible_content(){
		// check if the flexible content field has rows of data
		if( have_rows('flexible_content') ):

			ob_start();

				while ( have_rows('flexible_content') ) : the_row();

					if( get_row_layout() == 'banner' ):

						$this->ch_du_bo_get_banner();

					elseif( get_row_layout() == 'status_bar' ):

						$this->ch_du_bo_get_status_bar();

					elseif( get_row_layout() == 'description_section' ):

						$this->ch_du_bo_get_description_section();

					elseif( get_row_layout() == 'quote_section' ):

						$this->ch_du_bo_get_quote_section();

					elseif( get_row_layout() == 'key_figure_section' ):

						$this->ch_du_bo_get_key_figure_section();

					elseif( get_row_layout() == 'conclusion_section' ):

						$this->ch_du_bo_get_conclusion_section();

					elseif( get_row_layout() == 'testimonial_section' ):

						$this->ch_du_bo_get_testimonial_section();

					elseif( get_row_layout() == 'photo_gallery_section' ):

						$this->ch_du_bo_get_photo_gallery_section();

					elseif( get_row_layout() == 'latest_news_section' ):

						$this->ch_du_bo_get_latest_news_section();

					elseif( get_row_layout() == 'social_media_section' ):

						$this->ch_du_bo_get_social_media_section();

					elseif( get_row_layout() == 'video_block' ):

						$this->ch_du_bo_get_video_block();

					elseif( get_row_layout() == 'partners_section' ):

						$this->ch_du_bo_get_partners_section();

					elseif( get_row_layout() == 'two_column' ):

						$this->ch_du_bo_get_two_column();

					elseif( get_row_layout() == 'three_column' ):

						$this->ch_du_bo_get_three_column();

					elseif( get_row_layout() == 'reportage_news_or_social_posts' ):

						$this->ch_du_bo_get_reportage_news_or_social_posts();

					elseif( get_row_layout() == 'html_free_area_section' ):

						$this->ch_du_bo_get_html_free_area_section();

					elseif( get_row_layout() == 'team_member_partners_logos' ):

						$this->ch_du_bo_get_team_member_partners_logos();

					elseif( get_row_layout() == 'small_expandable_area' ):

						$this->ch_du_bo_get_small_expandable_area();

					elseif( get_row_layout() == 'simple_text_area' ):

						$this->ch_du_bo_get_simple_text_area();

					endif;

				endwhile;

				$content = ob_get_contents();
				ob_end_clean();
				return $content;

		else:

			return false;

		endif;
	}

	private function ch_du_bo_get_banner(){
		$bgImage = get_sub_field('background_image');
		$hideDonate = get_sub_field('hide_faire_un_don_button');
		$shortDescription = get_sub_field('short_description');
		$youtubeVideoLink = get_sub_field('youtube_video_link');
		?>
		<section class="main-banner no-margin">
           
           <div class="item" style="background:url(<?php echo $bgImage['url']; ?>) no-repeat center center">
            <div class="sliderCaption">
            	<div class="container">
                <div class="row">
                	<h2><?php the_sub_field('title'); ?></h2>
                	<?php if($shortDescription != ''): ?>
	                    <p><?php echo $shortDescription; ?></p>	
	                <?php endif; ?>
	                	<?php if($youtubeVideoLink != ''): ?>
							<a href="<?php echo $youtubeVideoLink; ?>" class="btn underline video"><?php _e('Voir la vidéo','ch_du_bo'); ?></a>
						<?php endif; ?>
						<?php if($hideDonate[0] != "Yes"): ?>
						 <a class="btnDonate open-here" href="<?php the_sub_field('faire_un_don_button_link'); ?>"><?php the_sub_field('faire_un_don_button_text'); ?></a>  
						<?php endif; ?>
                   
                	
                </div></div>
            
            </div>		
           
           </div>
           
           
      
    </section><!--/.main-banner-->
		<?php
	}

	private function ch_du_bo_get_status_bar(){
		$progress_bar = get_sub_field('progress_bar');
		$date_of_the_disaster = get_sub_field('date_of_the_disaster');
		$total_of_donation = get_sub_field('total_of_donation');
		?>
		<section class="status_bar">
	    	<div class="container">
	        	<div class="row">
	            	<div class="col-md-5">
	                	<div class="status"><span class="itemhide"><?php _e('Statut : en cours','ch_du_bo'); ?></span>
	                    <div class="progress">
	                          <div class="progress-bar progress-bar-urgence active" role="progressbar" style="width:25%">
	                            <?php the_sub_field('step_1'); ?>
	                          </div>
	                          <div class="progress-bar progress-bar-reconstruction" role="progressbar" style="width:40%">
	                            <?php the_sub_field('step_2'); ?>
	                          </div>
	                          <div class="progress-bar progress-bar-consolidation" role="progressbar" style="width:35%">
	                           <?php the_sub_field('step_3'); ?>
	                          </div>
	                        </div></div>
	                	
	                </div>
	                
	                <div class="col-md-7">
	                	<?php if($date_of_the_disaster): ?>
		                	<div class="col-md-6">
			                	<div class="dateDisaster">
			                    	<span class="donate itemhide"><?php _e('Date du séisme','ch_du_bp'); ?></span>
			                        <span class="date"><?php the_sub_field('date_of_the_disaster'); ?></span>
			                    </div>
		                    </div>
	                    <?php endif; ?>
	                    <?php if($total_of_donation): ?>
		                	<div class="col-md-6">	<div class="totalDonation">
		                    	<span class="total itemhide"><?php _e('Nombre de dons','ch_du_bp'); ?></span>
		                        <span class="amount"><?php echo $total_of_donation; ?></span>
		                    </div>
		                    </div>
		                <?php endif; ?>
	                </div>
	                
	                
	            </div>
	        </div>
	    	
	    </section><!--/.status bar-->
	    <?php if(!$stickyUsed): static $stickyUsed = true; ?>
	    <script type="text/javascript">
	    	jQuery(document).ready(function($) {
				// grab the initial top offset of the navigation 
			   	var stickyNavTop = $('.status_bar').offset().top;
			   	
			   	// our function that decides weather the navigation bar should have "fixed" css position or not.
			   	var stickyNav = function(){
				    var scrollTop = $(window).scrollTop(); // our current vertical position from the top
				         
				    // if we've scrolled more than the navigation, change its position to fixed to stick to top,
				    // otherwise change it back to relative
				    if (scrollTop > stickyNavTop) { 
				        $('.status_bar').addClass('sticky');
				    } else {
				        $('.status_bar').removeClass('sticky'); 
				    }
				};

				stickyNav();
				// and run it again every time you scroll
				$(window).scroll(function() {
					stickyNav();
				});
			});
	    </script>
		<?php endif;?>
		<?php
	}

	private function ch_du_bo_get_description_section(){
		$hideShare = get_sub_field('hide_share');
		$pdfLink = get_sub_field('pdf_link');
		$backgroundColor = get_sub_field('background_color');
		$bgColor = '#fff';
		static $shared = false;
		if($backgroundColor == 'Grey'){
			$bgColor = '#e1e1e1';
		}
		?>
		<section class="fund_desc wow fadeInDown animated" style="background: <?php echo $bgColor; ?>">
	    	<div class="container">
	        	<div class="row">
	    			
	                 <div class="col-md-6">
	                		<?php the_sub_field('left_column_text'); ?>
	                </div>
	                
	                <div class="col-md-6">
	                	<figure>
	                    	<?php the_sub_field('right_column_text'); ?>	                    
	                    </figure>
	                    <div class="downloadlnk">
	                    	<?php if($pdfLink != ''): ?>
		                    	<li class="col-md-6"><a download class="icon-download" href="<?php the_sub_field('pdf_link'); ?>"><img class="pull-left" src="<?php echo get_template_directory_uri(); ?>/images/icon-download.png" alt="download"><?php the_sub_field('pdf_title'); ?></a></li>
		                    <?php endif; ?>
	                    	<?php if(!$hideShare): ?>
		                        <li class="col-md-6 pull-right"><a class="share" href="#">Partager</a>
		                        <p>
		                        	<!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox"></div>
		                        	<?php if(!$shared): 
		                        		$shared = true;
		                        	?>
			                        	<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5819b0844233f71a"></script> 
			                        <?php endif; ?>
		                        </p></li>
		                    <?php endif; ?>
	                    </div>
	                
	                </div>
	          
	            </div>
	        </div>
	    	
	    </section><!--/.description section-->
		<?php
	}

	private function ch_du_bo_get_quote_section(){
		$authorImage = get_sub_field('image_of_author'); 
		?>
		<section class="secquote wow fadeInDown animated">
    	<div class="container">
    		<?php if($authorImage != ''): ?>
	       		<img src="<?php echo $authorImage['sizes']['thumbnail']; ?>" class="author-img alignleft" style="padding: 10px 20px;">
	       	<?php endif; ?>
        	<?php the_sub_field('text'); ?>
            <em><?php the_sub_field('author_name'); ?></em>
        </div>
    	
    </section><!--/.section quote-->
		<?php
	}

	private function ch_du_bo_get_key_figure_section(){
		$bgImage = get_sub_field('background_image'); 
		?>
		<section class="section-solidarity keyfigure" style="background:url(<?php echo $bgImage['url']; ?>) no-repeat center center">
	        <div class="container">
	        	
	            <div class="center wow fadeInDown">
	            	<h2><?php the_sub_field('title'); ?></h2>
	                <h1><?php the_sub_field('date'); ?></h1>
	                
	            </div>    

	            <?php if( have_rows('figures') ): ?>
	            	<div class="partners">
	               		<div class="row"> 
	                		<ul>
	                		<?php while ( have_rows('figures') ) : the_row(); ?>
	                			<li class="col-md-4 col-xs-4"><img src="<?php the_sub_field('icon'); ?>"><h4><?php the_sub_field('title'); ?></h4><p><?php the_sub_field('sub_title'); ?></p></li>
	                		<?php endwhile; ?>
	                		</ul>
	                	</div>
	                </div>
	            <?php endif; ?>     
	        </div><!--/.container-->
	    </section><!--/.swiss solidarity-->
		<?php
	}

	private function ch_du_bo_get_conclusion_section(){
		?>
		<section class="secquote">
	    	<div class="container">
	    		<?php the_sub_field('text'); ?>		
    		</div>    	
    	</section><!--/.section quote-->
		<?php
	}

	private function ch_du_bo_get_testimonial_section(){
		$background_color = get_sub_field('background_color');
		if($background_color == 'Grey'){
			$background_color = '#e1e1e1';
		}else{
			$background_color = '#fff';
		}
		$lTitle = get_sub_field('l_title');
		$rTitle = get_sub_field('r_title');
		$lImage = get_sub_field('l_picture_of_witness');
		$rImage = get_sub_field('r_picture_of_witness');
		$lDescription = get_sub_field('l_description');
		$rDescription = get_sub_field('r_description');
		?>
		<section class="sec-testimonail" style="background-color: <?php echo $background_color; ?>">
    		<div class="container">        	
    			<div class="title">
                	<h3><?php the_sub_field('title'); ?></h3>
                </div>
             	<div class="row">   
                    <div class="content">
                    
                    	<div class="col-md-6">
                        	<figure>
                            	<img src="<?php echo $lImage['url']; ?>" alt="">
                                <figcaption>
                                	<?php if($lTitle): ?>
	                                	<h3><?php echo $lTitle; ?></h3>
	                                <?php endif; ?>
                                    <?php echo $lDescription; ?>
                                
                                </figcaption>
                            
                            </figure>
                        
                        </div>
                    	<div class="col-md-6">
                        	<figure>
                            	<img src="<?php echo $rImage['url']; ?>" alt="">
                                <figcaption>
                                	<?php if($rTitle): ?>
	                                	<h3><?php echo $rTitle; ?></h3>
	                                <?php endif; ?>
                                    <?php echo $rDescription; ?>
                                </figcaption>
                            
                            </figure>
                        
                        </div>
                    </div>
              	</div>
        	</div>
        </section> <!--/.end of testimonial-->
		<?php
	}

	private function ch_du_bo_get_photo_gallery_section(){

		$images = get_sub_field('images');
		if( $images ): ?>
		    <section class="secondary-slider">
		        <?php foreach( $images as $image ): ?>
		            <div class="item" style="background:url(<?php echo $image['url']; ?>) no-repeat center center"><p><?php echo $image['caption']; ?></p></div>
		        <?php endforeach; ?>
		    </section><!--/.secondary-slider-->
		<?php endif; 
	}

	private function ch_du_bo_get_latest_news_section(){
		$ids = get_sub_field('choose_news');
		static $blockID = 1;
		?>
		<section class="section-news no-padding-btm" data-ids='<?php echo json_encode($ids); ?>'>
	        <div class="container">
	            <div class="row">
		            <div class="center title wow fadeInDown animated">
			            <h3><?php the_sub_field('title'); ?></h3>          
			        </div>
			        <?php 
			        	$args = array(
			        		'post__in' => $ids,
			        		'ordeby'	=> 'post__in',
			        		'posts_per_page'	=>	3			        		
			        	);
			        	
						// The Query
						$query = new WP_Query( $args );

						// The Loop
						if ( $query->have_posts() ) {
							?>
							<div class="news-listing">
	                  			<ul class="mobile-carousel news-list-ul">
							<?php
							while ( $query->have_posts() ) {
								$query->the_post();
								if(has_post_thumbnail()){
									$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'fundraising-thumb');
								}else{
									$img = array(get_template_directory_uri().'/images/news-default.jpg');
								}
								
								switch(get_post_format()){
									case 'standard':
										$icon = 'icon-news.png';
										break;
									case 'image':
										$icon = 'icon-camera.png';
										break;
									default:
										$icon = 'icon-news.png';
								}
								?>
								<li id="news-<?php the_ID(); ?>">
			                     	<div class="col-md-4 item wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
				                    	<div class="features center">
					                        <figure>
					                        	<img src="<?php echo $img[0]; ?>" alt="image 4">
					                            <figcaption><a class="icon_news" href="<?php the_permalink(); ?>" ><img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $icon; ?>" alt="#"></a></figcaption>
					                        </figure>
					                        <div class="content-wrap">
						                        <span class="disasterdate"><?php the_time('d.m.Y'); ?></span>
						                        <h4><?php the_title(); ?></h4>
					                            <?php the_excerpt(); ?>
				                        	</div>	                       
				                        </div><!--/.fundraising-->
				                    </div><!--/.col-md-4-->
		                    	</li> <!--/.col-md-4-->
								<?php
							}
							?>
								</ul>
							</div>
							<?php
						}
						// Restore original Post Data
						wp_reset_postdata();
			        ?>
	                <div class="center newsAll hidden-xs">
	                	<a class="btn border more-news-ajax" href="#">Voir plus</a>
	                	<p style="display:none" class="news-loader"><img src="<?php echo get_template_directory_uri().'/images/ajax-loader.gif'; ?>" alt="Loader"></p>
	                </div>
	                <?php if($blockID == 1): ?>
	                <script type="text/javascript">
	                	/*var paged = 2;
	                	jQuery('.more-news-ajax').click(function(e){
	                		e.preventDefault();
	                		var thisBlock = jQuery(this).parents('.section-news').find('.news-list-ul');
	                		paged = thisBlock.data('paged');
	                		if(typeof paged === 'undefined'){
	                			paged = 2;
	                			thisBlock.attr('data-paged',2);
	                		}
	                		var ids = thisBlock.parents('.section-news.no-padding-btm').data('ids');	                		
	                		console.log(ids);
	                		jQuery.ajax({
							  method: "POST",
							  url: ch.ajax_url,
							  data: { action: "ajax_more_news", data: ids, paged: paged }
							}).done(function( msg ) {
								thisBlock.append(msg);
							  	paged++;
							  	thisBlock.attr('data-paged',paged);
						  	});
	                	});*/
	                </script>
	                <?php endif; $blockID++; ?>
	            </div><!--/.row-->
	        </div><!--/.container-->
	        
	   </section><!--/.section news-->
		<?php
	}

	public function ch_du_bo_get_social_media_section(){
		?>
		<section class="section-news">
	        <div class="container">
	            <div class="row">
	            <div class="center title wow fadeInDown animated">
		            <h3>Sur les réseaux sociaux</h3>          
		        </div>
	            	<?php if(have_rows('social_links')): ?>
	            		<div class="news-listing social-News">
			                <ul class="mobile-carousel">
		            		<?php 
		            		$count = 0;
		            		$rowCount = 1;
		            		while ( have_rows('social_links') ) : the_row(); 
		            		if(get_sub_field('social_media') == 'Instagram'){
		            			$content = file_get_contents('https://api.instagram.com/oembed/?url='.get_sub_field('link'));
		            			$content =json_decode($content);
								$data = array(
									'full_picture'	=>	get_sub_field('link').'media/?size=m',
									'link'	=>	get_sub_field('link'),
									'created_time'	=>	'',
									'message'	=>	$content->title,
									'id'	=>	'',
									'date'	=>	'',
								);
								$class="icon_news icon_insta";
		            		}else{
		            			$fb = new FB_Page_Manager();
		            			$data = $fb->get_post_by_url(get_sub_field('link'));
		            			$data['date'] = date('d.m.Y',strtotime($data['created_time']));
		            			$class ="icon_fb";
		            		}
		            		$rowClass = '';
		            		if($count%3==0 && $count!=0){
		            			$rowCount++;		            			
		            		}
		            		if($rowCount > 1){
		            			$rowClass .= 'hidden ';
		            		}
		            		$rowClass .= 'row-'.$rowCount;

		            		?>
		            			<li class="<?php echo $rowClass; ?>"> 
		                      		<div class="col-md-4 item wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
				                    	<div class="features center">
				                         	<figure>
					                        	<img src="<?php echo $data['full_picture']; ?>" alt="image 7">
					                            <figcaption><a class="<?php echo $class; ?>" href="<?php echo $data['link']; ?>" >
					                            <?php if(get_sub_field('social_media') == 'Instagram'){ ?>
					                            	<img src="<?php echo get_template_directory_uri(); ?>/images/icon-insta.png" alt="<?php echo $data['title']; ?>"></a>
					                            <?php }else{ ?>
					                            	<i class="fa fa-facebook"></i>
					                            <?php } ?>
					                            

					                            </a></figcaption>
					                        </figure>
					                        <div class="content-wrap">
						                        <span class="disasterdate"><?php echo $data['date']; ?></span>
						                        <p><?php echo $data['message']; ?></p>			                            
					                        </div>		                       
				                        </div><!--/.fundraising-->
				                    </div><!--/.col-md-4-->
			                    </li>
		            		<?php $count++; endwhile; ?>
		            		</ul>
	            		</div>
            		<?php endif; ?>
	                
	                <div class="center newsAll hidden-xs">
	                	<a class="btn border show-more-social" href="#">Voir plus</a>
	                </div>   <!--end of link all-->
	                
	             <div class="col-md-12 slider-twitter"> 
	             <figure><figcaption><a class="icon_tw" href="#"><i class="fa fa-twitter"></i></a></figcaption></figure>
	             	<?php 
	             		$statuses = get_twitter_search_feed('refugee');	             		
	             	?>
	              <div class="secondary-slider ">
	              		<?php foreach($statuses as $status): ?>
	              		<div class="item">
	                    	<p>“ <?php echo $status->text; ?> ”</p>
	                        <a href="https://twitter.com/<?php echo $status->user->screen_name; ?>" class="btn underline"><?php echo '@'.$status->user->screen_name; ?></a>
	                    </div>
	                    <?php endforeach; ?>	              
	              </div>
	              </div>
	              
	                
	            </div><!--/.row-->
	        </div><!--/.container-->
	    </section><!--/#section news social-->
		<?php
	}

	public function ch_du_bo_get_video_block(){
		$imageID = get_sub_field('background_image');
		$img = wp_get_attachment_image_src($imageID,'full');
		?>
		<section class="section-video" style="background:url(<?php echo $img[0]; ?>) no-repeat center center">
	        <div class="container">
	            <div class="center wow fadeInDown">
                    <h1><?php the_sub_field('video_title'); ?></h1>                  
                   	<div class="embed-responsive embed-responsive-16by9">
	                   	<a class="play_btn video" href="<?php the_sub_field('video_link'); ?>?fs=1&amp;autoplay=1">
	                   		<span><?php echo get_sub_field('regarder_la_video_text'); ?></span>
	                   	</a>
                   	</div>
	            </div>
	        </div><!--/.container-->
	    </section><!--/.section videos-->

		<?php
	}

	public function ch_du_bo_get_partners_section(){
		$bgColor = get_sub_field('background_color');
		if($bgColor == 'Grey'){
			$bgColor = '#e1e1e1';
		}else{
			$bgColor = '#fff';
		}
		?>
		<section class="section-partners" style="background-color: <?php echo $bgColor; ?>">
		   	<div class="container">
	         	<div class="row">
		        	<div class="center title wow fadeInDown animated">
			            <h3><?php the_sub_field('title'); ?></h3>          
			        </div>		
			        <?php if(have_rows('partners')): ?>
			        	<ul class="center wow fadeInDown animated">
			        	<?php while(have_rows('partners')): the_row(); ?>
		        			<?php 
		        				$img = get_sub_field('logo'); 
		        				$link = get_sub_field('link'); 
		        				if($link == ''){ $link = '#'; }
		        			?>
				        	<li class="col-md-4 col-sm-6"><a href="<?php echo $link; ?>"><img src="<?php echo $img['url']; ?>" alt="heks eper"></a></li>
				        <?php endwhile; ?>
			        	</ul>
			        <?php endif; ?>
             	</div>
	        </div>
	   	</section> <!--/.section partners-->
		<?php
	}

	public function ch_du_bo_get_two_column(){ 

		?>
		<section class="section-imagetext  wow fadeInDown animated">
		    <div class="container">
		        <div class="row">
		        	<div class="col-md-6">
		            	<?php the_sub_field('left_column'); ?>
		            </div>
		            <div class="col-md-6">
			            <?php the_sub_field('right_column'); ?>
		            </div>
		        </div>
	        </div>
        </section> <!--end of left image right text-->
		<?php
	}

	public function ch_du_bo_get_three_column(){
		?>
		<section class="section-imagetext  wow fadeInDown animated">
		    <div class="container">
		        <div class="row">
		        	<div class="col-md-4">
		            	<?php the_sub_field('left_column'); ?>
		            </div>
		            <div class="col-md-4">
			            <?php the_sub_field('middle_column'); ?>
		            </div>
		            <div class="col-md-4">
			            <?php the_sub_field('right_column'); ?>
		            </div>
		        </div>
	        </div>
        </section> <!--end of left image right text-->
		<?php
	}

	public function ch_du_bo_get_reportage_news_or_social_posts(){
		$title = get_sub_field('title');
		$columns = get_sub_field('select_columns');
		$midCol = 4;
		if($columns == 2){
			$midCol = 8;
		}
		$leftPost = get_sub_field('left_post');     	
		$midPost = get_sub_field('mid_post');     	
		$rightPost = get_sub_field('right_post');     	

		?>
		<section class="section-news contact-list">
	        <div class="container">
	            <div class="row">
	                <div class="center title wow fadeInDown animated">
	                	<?php if($title != ''): ?>
				            <h2><?php echo $title; ?></h2>          
				        <?php endif; ?>
		        	</div>
	            
	                <div class="contact-listing">
                 	<ul class="mobile-carousel">
                 		<?php
	                 		// WP_Query arguments
							$args = array (
								'posts_per_page'         => '1',
								'post__in'	=> $leftPost
							);

							// The Query
							$query = new WP_Query( $args );

							// The Loop
							if ( $query->have_posts() ) {
								while ( $query->have_posts() ) {
									$query->the_post();
									get_template_part('template-parts/content','news-post');
								}
							}

							// Restore original Post Data
							wp_reset_postdata();
						?>
	                    
	                    <?php
	                 		// WP_Query arguments
							$args = array (
								'posts_per_page'         => '1',
								'post__in'	=> $midPost
							);

							// The Query
							$query = new WP_Query( $args );

							// The Loop
							if ( $query->have_posts() ) {
								while ( $query->have_posts() ) {
									$query->the_post();
									get_template_part('template-parts/content','news-post');
								}
							}

							// Restore original Post Data
							wp_reset_postdata();
						?>
	                    <?php if($columns == 3): ?>
	                    	<?php
		                 		// WP_Query arguments
								$args = array (
									'posts_per_page'         => '1',
									'post__in'	=> $rightPost
								);

								// The Query
								$query = new WP_Query( $args );

								// The Loop
								if ( $query->have_posts() ) {
									while ( $query->have_posts() ) {
										$query->the_post();
										get_template_part('template-parts/content','news-post');
									}
								}

								// Restore original Post Data
								wp_reset_postdata();
							?>
	                    <?php endif; ?>
	                      <!--/.col-md-4-->
	                  
	                    
	                </ul>  
	                 </div> <!--/.news-listing-->
	                
	                
	                
	            </div><!--/.row-->
	        </div><!--/.container-->
	        
	   </section> <!--section news-->
		<?php
	}

	private function ch_du_bo_get_html_free_area_section(){
		$total = (int)get_sub_field('columns');

		$col_1_bg_color = get_sub_field('col_1_mobile_background_color');
		if($col_1_bg_color == ''){
			$col_1_bg_color = '#e5222f';
		}
		$col_2_bg_color = get_sub_field('col_2_mobile_background_color');
		if($col_2_bg_color == ''){
			$col_2_bg_color = '#3a3a3b';
		}
		$col_3_bg_color = get_sub_field('col_3_mobile_background_color');
		if($col_3_bg_color == ''){
			$col_3_bg_color = '#3a3a3b';
		}

		$col_1_link_url = get_sub_field('col_1_link_url');
		$col_1_open_new_tab = get_sub_field('col_1_link_open_in_new_tab');
		$col_1_class = '';
		if($col_1_open_new_tab == 'No'){
			$col_1_class = 'open-here';
		}

		$col_2_link_url = get_sub_field('col_2_link_url');
		$col_2_open_new_tab = get_sub_field('col_2_link_open_in_new_tab');
		$col_2_class = '';
		if($col_2_open_new_tab == 'No'){
			$col_2_class = 'open-here';
		}
		if($total == 2){
			$col = '6';
		}else{
			$col = '4';
		}
		?>
		<section class="section-htmlfree">
	        <div class="container">
	            <div class="row">
		            <div class="center title hidden-xs wow fadeInDown animated">
			            <h3><?php the_sub_field('title'); ?></h3>          
			        </div>
		        	<div class="wow fadeInDown">                  
                     	<div class="col-md-4">
                     		<figure>
                            	<div class="hidden-xs"><img src="<?php the_sub_field('col_1_bg_image'); ?>" alt=""></div>
                            	<?php if($col_1_link_url != ''): ?>
	                            	<figcaption>
	                            		<a class="btn underline itemred <?php echo $col_1_class; ?>" href="<?php echo $col_1_link_url; ?>" style="background:<?php echo $col_1_bg_color; ?>"><span><?php the_sub_field('col_1_link_title'); ?></span></a></figcaption>
	                            <?php endif;?>
                            </figure>
                     	</div>
                 	 	<div class="col-md-4">
                     		<figure>
                            	<div class="hidden-xs"><img src="<?php the_sub_field('col_2_bg_image'); ?>" alt=""></div>
                            	<figcaption>
                            		<a class="btn underline itemblk <?php echo $col_2_class; ?>" href="<?php echo $col_2_link_url; ?>" style="background:<?php echo $col_2_bg_color; ?>"><span><?php the_sub_field('col_2_link_title'); ?></span></a></figcaption>
                            </figure>
                     	</div>
                     	<?php if($total == 3){ ?>
                     	<div class="col-md-4">
                     		<figure>
                            	<div class="hidden-xs"><img src="<?php the_sub_field('col_3_bg_image'); ?>" alt=""></div>
                            	<figcaption>
                                	<a class="btn underline itemblk video" href="<?php the_sub_field('col_3_video_url'); ?>" style="background:<?php echo $col_3_bg_color; ?>"><span><?php the_sub_field('col_3_title'); ?></span></a>
                                </figcaption>
                            </figure>
                     	</div>   
                     	<?php } ?>
	                </div><!--/.-->
				</div>
			</div>
		</section>
		<?php
	}

	private function ch_du_bo_get_team_member_partners_logos(){
		$backgroundColor = get_sub_field('background_color');
		$bgColor = '#fff';
		if($backgroundColor == 'Grey'){
			$bgColor = '#e1e1e1';
		}		
		if( have_rows('logos') ):
		?>
		<section class="section-contact" style="background: <?php echo $bgColor; ?>">
	    	<div class="container">
	        	<div class="row">
	            	<?php while ( have_rows('logos') ) : the_row(); 
	            	$img = get_sub_field('image');
	            	$description = get_sub_field('description');
	            	$link = get_sub_field('url_on_image');
	            	$open_link_in_new_tab = get_sub_field('open_link_in_new_tab');
	            	$class = '';
	            	if($open_link_in_new_tab == 'No'){
	            		$class = ' class="open-here" ';
	            	}
	            	?>
		                <div class="col-md-4">
		                	<div class="address">
		                    	<a href="<?php echo $link; ?>" <?php echo $class; ?>><img src="<?php echo $img['sizes']['fundraising-thumb']; ?>" alt="">
		                        <address>
		                        <?php echo $description; ?>		                        
		                        </address></a>
		                        
		                    </div>
		                </div>
					<?php endwhile; ?>
	            </div>	        
	        </div>	    
	    </section>
		<?php
		endif; 
	}

	private function ch_du_bo_get_small_expandable_area(){
		if(have_rows('contents')): ?>
		<section class="section-accrodion">
	    	<div class="container">	        
	        	<div class="panel-group" id="accordion">
		        	<?php 
		        	$counter = 0;
		        	while( have_rows('contents') ): the_row(); ?>
		        		<div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $counter; ?>">
						          <?php the_sub_field('title'); ?>
						        </a>
						      </h4>
						    </div>
						    <div id="collapse<?php echo $counter; ?>" class="panel-collapse collapse">
						      <div class="panel-body">
						        <?php the_sub_field('description'); ?>
						      </div>
						    </div>
					  </div>
		        	<?php $counter++; endwhile; ?>
	        	</div>
	        </div>
	    </section>
		<?php endif; 
	}

	private function ch_du_bo_get_simple_text_area(){
		?>
		<section class="section-simple-text">
	    	<div class="container">
	    		<?php the_sub_field('content'); ?>
	    	</div>
	    </section>
		<?php
	}
}