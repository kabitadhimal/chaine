<?php
add_action('wp_footer','ch_du_bo_footer_scripts',100); 
function ch_du_bo_footer_scripts(){
	?>
<script type="text/javascript">
	if(typeof paged == 'undefined')
		var paged = 2;
	if(typeof start == 'undefined')
		var start = 4;
	if(typeof filterUsed == 'undefined')
		var filterUsed = false;
	if(typeof catIds == 'undefined')
		var catIds = [];
	var data;
	var $grid = jQuery('.postList');
	var isDataAvailable = true;
	jQuery(document).ready(function() {

		jQuery('.select').niceSelect();

		if(jQuery('.contact-listing li').length == 2){
			var lastBlock = jQuery('.contact-listing li').get(1);
		    var lastBlockDiv = jQuery(lastBlock).find('div.col-md-4');
		    jQuery(lastBlockDiv).removeClass('col-md-4');
		    jQuery(lastBlockDiv).addClass('col-md-8');
		}
    	jQuery('.more-news-ajax').click(function(e){
    		e.preventDefault();
    		var thisBlock = jQuery(this).parents('.section-news').find('.news-list-ul');
    		var loader = jQuery(this).next();
    		loader.show();
    		paged = thisBlock.attr('data-paged');
    		if(typeof paged === 'undefined'){
    			paged = 2;
    			thisBlock.attr('data-paged',3);
    		}
    		var ids = thisBlock.parents('.section-news.no-padding-btm').data('ids');
    		//console.log(ids);
    		jQuery.ajax({
			  method: "POST",
			  url: ch.ajax_url,
			  data: { action: "ajax_more_news", data: ids, paged: paged }
			}).done(function( msg ) {
				if(msg == ''){ loader.hide(); return; }
				thisBlock.append(msg);
			  	paged++;
			  	thisBlock.attr('data-paged',paged);
			  	loader.hide();
		  	});
    	});

    	jQuery('.more-news-btn').click(function(e){
    		e.preventDefault();
    		console.log(filterUsed);
    		console.log(catIds);
    		var loader = jQuery(this).next();
    		loader.show();
    		var data = { action: "ajax_more_news_list", paged: paged, start: start };
    		if(filterUsed){
    			data.catIds = catIds;
    		}
    		jQuery.ajax({
				method: "POST",
				url: ch.ajax_url,
				data: data
			}).done(function( msg ) {
				jQuery('.news-listing ul').append(msg);
				start = start + 6;
			  	paged++;
			  	loader.hide();
		  	});
    	});
		
		jQuery('.filter_menu input[type="radio"]').change(function(){
			isDataAvailable = true;
			$grid.masonry('destroy');
			catIds = [];
        	paged = 2;
			start = 4;
			filterUsed = true;
            //var selectedOptions = jQuery('.filtersel option:selected');
            var loader = jQuery('.news-loader');
            jQuery('.more-media-btn').removeClass('hidden');
			jQuery('.hero, .slider-twitter').addClass('hidden');
            jQuery('.postList').empty();
            /*selectedOptions.each(function(){
            	catIds.push(jQuery(this).val());
            });*/
            catIds = jQuery(this).val();
            loader.show();
    		jQuery.ajax({
				method: "POST",
				url: ch.ajax_url,
				data: { action: "ajax_media_filter" , catIds: catIds, lang:ch.site_lang}
			}).done(function( msg ) {
				$grid = jQuery('.postList').masonry({itemSelector: '.grid-item',horizontalOrder: true});
				msg = jQuery(msg);
				$grid.append(msg).masonry('appended', msg);	
				jQuery(msg).imagesLoaded().progress( function() {
					$grid.masonry('layout');
				});
				//jQuery('.features.hero, .slider-twitter-inner').remove();
				
				for(var i = 0; i < heroCats.length; i++){
					if(heroCats[i].term_id == catIds){
						jQuery('.hero, .slider-twitter').removeClass('hidden');
						jQuery('.slider-twitter-inner').addClass('hidden');
					}
				}
				
			  	loader.hide();
		  	});
		});

    	jQuery('.more-media-btn').click(function(e){
    		e.preventDefault();
    		console.log(filterUsed);
    		console.log(catIds);
    		var loader = jQuery(this).next();
    		loader.show();
			/* if(start == 4){
				start = 3;
			} */
    		var data = { action: "ajax_more_media_list", paged: paged, start: start, lang:ch.site_lang };
    		if(filterUsed){
    			data.catIds = catIds;
    		}
    		jQuery.ajax({
				method: "POST",
				url: ch.ajax_url,
				data: data
			}).done(function( msg ) {
				msg = jQuery(msg);
				/* jQuery('.post-Listing .postList.grid').masonry('destroy');
					jQuery('.postList').masonry({
					// options...
					itemSelector: '.grid-item',
					//columnWidth: 300
				}); */
				setTimeout(function() {
					jQuery('.post-Listing .postList.grid').append(msg).masonry('appended', msg, 'reloadItems');
				}, 600);
				start = start + 4;
			  	paged++;
				/*$grid.masonry('destroy');
				$grid = jQuery('.postList').masonry({
					// options...
					itemSelector: '.grid-item',
					//columnWidth: 300
				});*/
			  	loader.hide();
		  	});
    	});

    	//var twID = jQuery('.secondary-slider').data('tw-id');
    	if(typeof twID !== 'undefined' && typeof(disableTwt) == 'undefined'){
	    	data = { action: "ajax_get_tw_feed", twID: twID };
	    	jQuery.ajax({
				method: "POST",
				url: ch.ajax_url,
				data: data
			}).done(function( msg ) {
				jQuery('.secondary-slider').html(msg);
				jQuery('.slider-twitter-inner').removeClass('hidden');
				jQuery('.secondary-slider').owlCarousel({
					center: true,
					items:1,
			        autoPlay: true,
					loop:true,
					margin:0,
					nav: true,
					dots: true,
					dragEndSpeed: 1000,
					navSpeed: 1500,
			        lazyLoad: true,
					fluidSpeed: 2000,
					dotsSpeed: 1500,
					responsiveClass: true,


				});
		  	});
    	}



    	var displayedRow = 1;
    	jQuery('.show-more-social').click(function(e){
    		e.preventDefault();
    		displayedRow++;
    		jQuery('.row-'+displayedRow).removeClass('hidden');
    	});
		jQuery('a').each(function() {
	        // Excluding hash in href
			switch(window.location.host) {
				case 'medias.bonheur.ch':
				case 'media.catena-della-solidarieta.ch':
				case 'medien.glueckskette.ch':
				case 'media.swiss-solidarity.org':
					return;
					break;
			}
	        if (this.href === '#') {
	            return;
	        }
	        if(jQuery(this).hasClass('open-here')){
	        	return;
	        }
	        if(jQuery(this).hasClass('play_btn')){
	        	return;
	        }
	        if(jQuery(this).hasClass('video')){
	        	return;
	        }
	        var a = new RegExp('/' + window.location.host + '/');
	        if (!a.test(this.href)) {
	            var href = jQuery(this).attr('href');
	            if (href != 'javascript:void(0);') {
	                jQuery(this).click(function(event) {
	                    event.preventDefault();
	                    event.stopPropagation();
	                    window.open(this.href, '_blank');
	                });
	            }
	        }
	    });

		jQuery('.main-slider').owlCarousel({
			center: true,
			items:1,
	        autoPlay: true,
			loop:true,
			margin:0,
			nav: true,
			dots: true,
			dragEndSpeed: 1000,
			navSpeed: 1500,
	        lazyLoad: true,
			fluidSpeed: 2000,
			dotsSpeed: 1500,
			responsiveClass: true,


		});

		jQuery(function() {
		    var owl = jQuery('.mobile-carousel'),
		        owlOptions = {
		            //loop: false,
		            margin: 10,
					dots: false,
					nav: true,
		            responsive: {
		                0: {
		                    items: 1
		                }
		            }
		        };

		    if ( jQuery(window).width() < 992 ) {
		        var owlActive = owl.owlCarousel(owlOptions);
		    } else {
		        owl.addClass('off');
		    }

		    jQuery(window).resize(function() {
		        if ( jQuery(window).width() < 992 ) {
		            if ( jQuery('.mobile-carousel').hasClass('off') ) {
		                var owlActive = owl.owlCarousel(owlOptions);
		                owl.removeClass('off');
		            }
		        } else {
		            if ( !jQuery('.mobile-carousel').hasClass('off') ) {
		                owl.addClass('off').trigger('destroy.owl.carousel');
		                owl.find('.owl-stage-outer').children(':eq(0)').unwrap();
		            }
		        }
		    });
		});

	});


	// Dropdown Menu Fade
	jQuery(document).ready(function(){
	    jQuery(".dropdown").hover(
	        function() { jQuery('.dropdown-menu', this).stop().fadeIn("fast");
	        },
	        function() { jQuery('.dropdown-menu', this).stop().fadeOut("fast");
	    });

		if(jQuery('.fancybox').length > 0 ){
			jQuery('.fancybox').fancybox();
		}
		if(jQuery('.mcForm').length > 0 ){
			jQuery(".mcForm").submit(function(e){
				e.preventDefault();
				var em = jQuery(this).find('#mce-EMAIL').attr('value');
				if(em === ''){
					return;
				}
				jQuery('.wpcf7-email').attr('value',em);
				var mailchimpURL = jQuery(this).attr('action');
				jQuery.fancybox({
					maxWidth	: 800,
					maxHeight	: 600,
					fitToView	: false,
					width		: '70%',
					height		: '70%',
					autoSize	: false,
					closeClick	: false,
					openEffect	: 'none',
					closeEffect	: 'none',
					title		: ch.newsletterTitle,
					type		: 'inline',
					//href		: ch.ajax_url+'?action=mce_newsletter&email='+em+'&mce_url='+mailchimpURL
					href		: "#hidden-content"
				});
			});
		}
	});

	jQuery(document).ready(function() {
		jQuery('.fancybox').fancybox();
		jQuery(".video").click(function() {
			jQuery.fancybox({
				'padding'		: 0,
				'autoScale'		: false,
				'transitionIn'	: 'none',
				'transitionOut'	: 'none',
				'title'			: this.title,
				'fitToView'    	: false, //
	   			'maxWidth'     	: "90%", //
				'width'         : "90%",
				'height'        : "90%",
				'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
				'type'			: 'swf',
				'swf'			: {
				'wmode'			: 'transparent',
			    'allowfullscreen'	: 'true'
				}
			});

			return false;
		});
		if(jQuery('.status_bar').length > 0){
			// grab the initial top offset of the navigation
		   	var stickyNavTop = jQuery('.status_bar').offset().top;

		   	// our function that decides weather the navigation bar should have "fixed" css position or not.
		   	var stickyNav = function(){
			    var scrollTop = jQuery(window).scrollTop(); // our current vertical position from the top

			    // if we've scrolled more than the navigation, change its position to fixed to stick to top,
			    // otherwise change it back to relative
			    if (scrollTop > stickyNavTop) {
			        jQuery('.status_bar').addClass('sticky');
			    } else {
			        jQuery('.status_bar').removeClass('sticky');
			    }
			};

			stickyNav();
			// and run it again every time you scroll
			jQuery(window).scroll(function() {
				stickyNav();
			});
		}

		if(typeof jQuery('.filtersel').multiselect !== 'undefined'){

			jQuery('.filtersel').multiselect({
				/* onDropdownShown: function(event) {
					var sel = jQuery(event.target).find('input[value="select-all-value"]');
					var preSel = jQuery(event.target).find('li.active').length;
					if(!sel.parents('li').hasClass('active') && preSel === 0){
						sel.trigger('click');
					}
			    },
				onSelectAll: function() {
					this.options.onChange();
		        },
				includeSelectAllOption: true,
				selectAllValue: 'select-all-value',
				selectAllJustVisible: true, */
	            buttonText: function(options, select) {
	            	var title = select.data('title');
	                return title;
	            },
	            buttonTitle: function(options, select) {
	                var labels = [];
	                options.each(function () {
	                    labels.push(jQuery(this).text());
	                });
	                return labels.join(' - ');
	            },
	            onChange: function(option, checked, select) {
	            	catIds = [];
	            	paged = 2;
					start = 4;
					filterUsed = true;
	                var selectedOptions = jQuery('.filtersel option:selected');
	                var loader = jQuery('.news-loader');
	                jQuery('.more-media-btn').removeClass('hidden');
	                jQuery('.postList').html('');
	                selectedOptions.each(function(){
	                	catIds.push(jQuery(this).val());
	                });
	                loader.show();
		    		jQuery.ajax({
						method: "POST",
						url: ch.ajax_url,
						data: { action: "ajax_media_filter" , catIds: catIds, lang:ch.site_lang}
					}).done(function( msg ) {
						msg = jQuery(msg);
						jQuery('.postList').append(msg).masonry('appended', msg, 'reloadItems');	
						jQuery(msg).imagesLoaded().progress( function() {
							jQuery('.postList').masonry('layout');
						});
						jQuery('.features.hero, .slider-twitter-inner').addClass('hidden');
					  	loader.hide();
						jQuery('.features.hero, .slider-twitter-inner').addClass('hidden');
					  	loader.hide();
				  	});
	            }
	        });

	        jQuery('.deselectAll').on('click', function(e) {
				e.preventDefault();
				isDataAvailable = true;
				jQuery('input[type="radio"]:checked').each(function(){
					jQuery(this).prop('checked', false);
				});
				jQuery('.filtersel').multiselect('deselectAll', false);
				jQuery('.filtersel .multiselect-item').removeClass('active');
				jQuery('.multiselect').removeClass('active');
				catIds = [];
				paged = 1;
				start = 1;
				filterUsed = true;
				var selectedOptions = jQuery('.filtersel option:selected');
				var loader = jQuery('.news-loader');
				$grid.masonry('destroy');
				jQuery('.postList').empty();
				selectedOptions.each(function(){
					catIds.push(jQuery(this).val());
				});
				jQuery('.btn-group button').removeClass('active');
				jQuery('li.active').each(function(){
				jQuery(this).parent('ul').prev().addClass('active');
				});
				loader.show();
				jQuery.ajax({
					method: "POST",
					url: ch.ajax_url,
					data: { action: "ajax_media_filter" , catIds: catIds, per_page: '-1', lang:ch.site_lang}
				}).done(function( msg ) {
					isDataAvailable = false;
					msg = jQuery(msg);
					//console.log(msg);
					jQuery('.postList').html(msg);
					jQuery(msg).imagesLoaded().progress( function() {
						$grid = jQuery('.postList').masonry({itemSelector: '.grid-item',horizontalOrder: true});
						loader.hide();						
					});
					jQuery('.hero, .slider-twitter').removeClass('hidden');
					jQuery('.slider-twitter-inner').addClass('hidden');
				});
			});
		}

		jQuery("[id^='media'] a").live('click',function(e){
		    e.preventDefault();
		    var url_location =jQuery(this).attr('href');
		    if (typeof history.pushState !== "undefined") {	
		        window.history.pushState("", "", '?backlink=1');
		    }
		    location.replace("<?php echo home_url('/'); ?>?backlink=1");
		    window.location = url_location;
		});

		jQuery('#contact-subject').change(function(){
			var subject = jQuery('#contact-subject option:selected').attr('data-subject');
			var email = jQuery(this).val();
			jQuery('#sendTo').attr('value',email);
			jQuery('#subject').attr('value',subject);
		});

	});
	jQuery(window).load(function() {
		///Added by ranjita
		if(typeof jQuery('.grid').masonry !== 'undefined'){
			$grid = $grid.masonry({
			itemSelector: '.grid-item',
			initLayout: false,
			horizontalOrder: true,
			}).on( 'layoutComplete',
			  function( ) {
				if(ch.latest_viewed_post && jQuery('#media'+ch.latest_viewed_post).length >= 1 && ch.backlink){
					jQuery('html, body').animate({
						scrollTop: jQuery( '#media'+ch.latest_viewed_post ).offset().top
					}, 500,function(){
						ch.latest_viewed_post = null;
					});
					
				}	
			  }
			).masonry();
		}
	});

</script>
	<?php
}
