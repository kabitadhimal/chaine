<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Chaîne_du_Bonheur
 */
$socialTitle = get_theme_mod('ch_du_do_social_title_footer_'.CH_LANG_CODE);

$fbLink = get_theme_mod('ch_du_bo_fb_link_'.CH_LANG_CODE);
$twLink = get_theme_mod('ch_du_bo_tw_link_'.CH_LANG_CODE);
$instaLink = get_theme_mod('ch_du_bo_insta_link_'.CH_LANG_CODE);
$youtubeLink = get_theme_mod('ch_du_bo_youtube_link_'.CH_LANG_CODE);
$linkedinLink = get_theme_mod('ch_du_bo_lnkd_link_'.CH_LANG_CODE);
$mailChimpID = get_theme_mod('ch_du_bo_mailchimp_list_id_'.CH_LANG_CODE);
?>

	<section class="section-footerbottom ">
    	<div class="container">
        <div class="row">

        <div class="postal-account col-md-12">
        <div class="wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="600ms">

             <div class="postal-content">
             	<?php echo get_theme_mod('ch_du_bo_address_'.CH_LANG_CODE); ?>
             </div>



            </div>
        </div>  <!--end of postal address-->




        <div class="col-md-12">
        <div class="footer-bottom">
        <div class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">

             <div class="footerbottom-content row">

                	<div class="col-md-6">
                     <div class="footerbottom-lt hidden-xs">
						<?php if($socialTitle): ?>
							<h3><?php echo $socialTitle; ?></h3>
						<?php endif; ?>
                       <ul class="social-share">
							<?php if($fbLink): ?>
								<li class="fb"><a href="<?php echo $fbLink; ?>"><i class="fa fa-facebook"></i></a></li>
							<?php endif; ?>
							<?php if($twLink): ?>
								<li class="tw"><a href="<?php echo $twLink; ?>"><i class="fa fa-twitter"></i></a></li>
							<?php endif; ?>
							<?php if($instaLink): ?>
								<li class="insta"><a href="<?php echo $instaLink; ?>"><i class="fa fa-instagram"></i></a></li>
							<?php endif; ?>
							<?php if($youtubeLink): ?>
								<li class="youtube"><a href="<?php echo $youtubeLink; ?>"><i class="fa fa-youtube-square"></i></a></li>
							<?php endif; ?>
							<?php if($linkedinLink): ?>
								<li class="link"><a href="<?php echo $linkedinLink; ?>"><i class="fa fa-linkedin"></i></a></li>
							<?php endif; ?>
                        </ul>
                         </div>
                    </div>

                    <div class="col-md-6">
                    	<?php get_mailchimp_subscription_form($mailChimpID); ?>
						<div style="display: none;" id="hidden-content">
							<?php 
                                switch(CH_LANG_CODE){
                                    case 'fr':
                                        echo do_shortcode('[contact-form-7 id="1865" title="Communiqué de presse FR"]'); 
                                        break;
                                    case 'de':
                                        echo do_shortcode('[contact-form-7 id="1868" title="Medienmitteilungen DE"]'); 
                                        break;
                                    case 'en':
                                        echo do_shortcode('[contact-form-7 id="1867" title="Media releases EN"]'); 
                                        break;
                                    case 'it':
                                        echo do_shortcode('[contact-form-7 id="1866" title="Comunicati stampa  IT"]'); 
                                        break;
                                    default:
                                        echo do_shortcode('[contact-form-7 id="1865" title="Communiqué de presse FR"]'); 
                                }
                                
                            ?>
						</div>

                    <ul class="social-share hidden-md hidden-lg">
                        <?php if($fbLink): ?>
							<li class="fb"><a href="<?php echo $fbLink; ?>"><i class="fa fa-facebook"></i></a></li>
						<?php endif; ?>
						<?php if($twLink): ?>
							<li class="tw"><a href="<?php echo $twLink; ?>"><i class="fa fa-twitter"></i></a></li>
						<?php endif; ?>
						<?php if($instaLink): ?>
							<li class="insta"><a href="<?php echo $instaLink; ?>"><i class="fa fa-instagram"></i></a></li>
						<?php endif; ?>
						<?php if($youtubeLink): ?>
							<li class="youtube"><a href="<?php echo $youtubeLink; ?>"><i class="fa fa-youtube-square"></i></a></li>
						<?php endif; ?>
						<?php if($linkedinLink): ?>
							<li class="link"><a href="<?php echo $linkedinLink; ?>"><i class="fa fa-linkedin"></i></a></li>
						<?php endif; ?>
                    </ul>

                    </div>

             </div>

              <div class="icon_fund"><img src="<?php echo get_template_directory_uri(); ?>/images/btn-donate-r.png" alt="#"></div>


            </div>
        </div>  <!--end of newsletter section-->
        </div>



        <div class="partnaires_logo">
        	<?php if( have_rows('parteners','option') ): $count = 1; ?>
        		<div class="col-md-7 col-sm-8">
        			<div class="partnaire_lt">
	        		<?php while ( have_rows('parteners','option') ) : the_row(); ?>
	        			<?php if($count == 3): ?>
	        				</div>
	        				</div>
	        				<div class="col-md-5 col-sm-4">
	        				<div class="partnaire_rt">
	        			<?php endif; ?>
	        			<a href="<?php echo get_sub_field('link','option'); ?>"><img src="<?php echo get_sub_field('logo','option'); ?>" alt="srg ssr"></a>
	        		<?php $count++; endwhile; ?>
	        		</div>
        		</div>
        	<?php endif; ?>

        </div>

        </div>

        </div>


    </section><!--/#bottom-->

    <footer class="section-footer">
        <div class="container">
        <div class="row">

         <?php
                wp_nav_menu( array(
                    'theme_location'  =>  'footer',
                    'container'       =>  'ul',
                    'menu_id'         =>  'menu-footer',
                    'container_id'        =>  '',
                    'menu_class'      =>  '',
                ) );
            ?>

        </div>





        </div>
    </footer><!--/#footer-->
<?php wp_footer(); ?>
</body>
</html>
