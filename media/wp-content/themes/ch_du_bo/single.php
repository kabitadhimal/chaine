<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Chaîne_du_Bonheur
 */
 
//setting up a cookie
setcookie('latest_viewed_post', get_the_ID(), time() + (120), "/"); // 86400 = 1 day
// getting cookie from previous filter
if(isset($_COOKIE[COOKIE_NAME])){
	$filterParams = unserialize(stripcslashes($_COOKIE[COOKIE_NAME]));
}
// current id
$currentPostID = get_the_ID();
// empty array to store other ids to get previous and next id of current post according to selected filter in homepage
$ids = array();
// default prev next links
$prevLink = get_previous_post_link('<i class="fa fa-chevron-left"></i>%link', __('Précédent','ch_du_bo'), TRUE);
$nextLink = get_next_post_link('%link<i class="fa fa-chevron-left"></i>', __('Suivant','ch_du_bo'), TRUE);
if($filterParams['catIds']){
	$args = array (
		'posts_per_page'         => '-1',
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'field'    => 'term_id',
				'terms'    => $filterParams['catIds'],
				'operator' => 'IN',
			),
		)
	);
	
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$ids[] = get_the_ID();
		}
		wp_reset_postdata();
	}
	$position = array_search(get_the_ID(), $ids);
	if((int)$position !== false){
		$prevPosition = $position - 1;
		$nextPosition = $position + 1;
		if($ids[$prevPosition]){
			$prevLink = '<i class="fa fa-chevron-left"></i><a href="'.get_permalink($ids[$prevPosition]).'">'.__('Précédent','ch_du_bo').'</a>';
		}else{
			$prevLink = '';
		}
		if($ids[$nextPosition]){
			$nextLink = '<a href="'.get_permalink($ids[$nextPosition]).'">'.__('Suivant','ch_du_bo').'<i class="fa fa-chevron-left"></i></a>';
		}else{
			$nextLink = '';
		}
		
		
	}
}
get_header(); 
	$backLink = site_url('/?backlink=1');
?>
	
    <section class="section-backlink">
        <div class="container">
          <div class="row clearfix post-nav-row">
            <div class="col-sm-4 col-xs-12">
              <div class="btnback">
               		<i class="fa fa-chevron-left"></i><a href="<?php echo $backLink; ?>"><?php _e('Retour', 'ch_du_bo'); ?></a> 
               </div>
            </div>
            <div class="col-sm-8 col-xs-12">
              <nav class="post-link-nav" role="nav">
                <ul class="post-link-menu clearfix text-right">
                  <li class="post-link-item post-link-item-prev"><?php echo $prevLink; ?></li>
                  <li class="post-link-item post-link-item-next"><?php echo $nextLink; ?></li>
                </ul>
              </nav>
            </div>
           </div>
      	</div>
  	</section>
	<section class="section-two-col">
        <div class="container">
            <div class="row">          
            	<div class="col-md-8">
                	<div class="media-body">

		<?php
		while ( have_posts() ) : the_post();

			
			?>
			<div class="features">
				<?php get_template_part( 'template-parts/content', get_post_format() ); ?>
          	</div>
          	<?php
			//the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			

		endwhile; // End of the loop.

    $terms = wp_get_post_terms( get_the_ID(), 'category' );
    $childTerms = array();
    if(!empty($terms)){
      foreach($terms as $term){
        if($term->parent != 0){
          $childTerms[] = $term->term_id;
        }
      }
    }

    if(!empty($childTerms)){
      $args = array(
          'post_type' => array('post'),
          'category__in'  => $childTerms,
          'posts_per_page'  => 2,
          'post__not_in'  => array(get_the_ID()),
          'orderby'       => 'rand'
      );

      $query = new WP_Query($args);

      if($query->have_posts()): ?>
        <div class="post-Listing">                          
          <h3><?php _e('Communiqués Associes','ch_du_bo'); ?></h3>            
          <ul class="postList row">
            <?php while($query->have_posts()): $query->the_post(); 
            get_template_part('template-parts/content','media-post');
            endwhile; ?>
          </ul>
        </div>
        <?php
      endif;
    }
		?>
					</div>
				</div>
				<div class="col-md-4">
          <?php get_sidebar(); ?>                    
        </div>
			</div>
		</div>
	</section>

      <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5819b0844233f71a"></script> 
<?php
get_footer();
