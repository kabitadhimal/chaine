<?php
$fbLink = get_theme_mod('ch_du_bo_fb_link_'.CH_LANG_CODE);
$twLink = get_theme_mod('ch_du_bo_tw_link_'.CH_LANG_CODE);
$instaLink = get_theme_mod('ch_du_bo_insta_link_'.CH_LANG_CODE);
$youtubeLink = get_theme_mod('ch_du_bo_youtube_link_'.CH_LANG_CODE);
$linkedinLink = get_theme_mod('ch_du_bo_lnkd_link_'.CH_LANG_CODE);
$mailChimpID = get_theme_mod('ch_du_bo_mailchimp_list_id_'.CH_LANG_CODE);

$id = icl_object_id(5, 'page', false,ICL_LANGUAGE_CODE);

?>
<aside class="widget-area" role="complementary">
    <section class="widget widget_contact">
    	<?php
    		$img = get_field('contact_image', $id);
    		$name = get_field('name', $id);
    		$job_title = get_field('job_title', $id);
    		$phone_number = get_field('phone_number', $id);
    		$email_address = get_field('email_address', $id);
    	?>
    	<?php if($img): ?>
        	<img src="<?php echo $img['sizes']['fundraising-thumb']; ?>" alt="">
        <?php endif; ?>
        <address>
        	<?php echo $name; ?> <br>
        	<?php echo $job_title; ?> <br>
            Tel: <a href="tel:<?php echo $phone_number; ?>"><?php echo $phone_number; ?></a> <br>
            E-Mail: <a href="mailto:<?php echo $email_address; ?>"><?php echo $email_address; ?></a>

        </address>

    </section>



           <section id="search-2" class="widget widget_email">
                <h4 class="widget-title"><?php _e('Recevez nos communiqués','ch_du_bo'); ?></h4>
                <div id="mc_embed_signup">
            		<form action="//bonheur.us3.list-manage.com/subscribe/post?u=a905c457c17f2185048d2218f&amp;id=<?php echo $mailChimpID; ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate mcForm" target="_blank">
                        <div id="mc_embed_signup_scroll">

                        <div class="mc-field-group">
                            <input type="email" value="" name="EMAIL" placeholder="<?php _e('Votre e-mail','ch_du_bo'); ?>..." class="required email newsletter-form" id="mce-EMAIL" required>
                            <input type="submit" value="GO" name="subscribe" id="mc-embedded-subscribe" class="submit">
                        </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_bf1c1bee9948229a202de6939_<?php echo $listID; ?>" tabindex="-1" value=""></div>

                        </div>
                    </form>
                </div>
                <!--End mc_embed_signup-->
            </section>

    <section  class="widget widget_search">
    	<h4 class="widget-title"><?php _e('Recherche','ch_du_bo'); ?></h4>
    	<?php get_search_form(true); ?>
    </section>

    <section  class="widget widget_search">
    	<h4 class="widget-title"><?php _e('Archives','ch_du_bo'); ?></h4>
    	<form  class="search-form" action="#">
      	  	<select class="select" onchange="document.location.href=this.options[this.selectedIndex].value;">
          		<?php wp_get_archives( array( 'type' => 'yearly', 'format'	=> 'option' ) ); ?>
          	</select>
        </form>

    </section>
	
	<?php
		include_once(ABSPATH.WPINC.'/feed.php');
		
		$feedPrefix = '';
		if(CH_LANG_CODE != 'fr'){
			$feedPrefix = '/'.CH_LANG_CODE;
		}
		
		$feedLink = str_replace('/media', $feedPrefix.'/feed', site_url());
		$rss = fetch_feed($feedLink);
		if( ! is_wp_error( $rss ) ) {
			$maxitems = $rss->get_item_quantity(3);

			$rss_items = $rss->get_items(0, $maxitems);
	?>
			<section  class="widget widget_archive">
			  <h4 class="widget-title"><?php _e('Articles recents','ch_du_bo'); ?></h4>
			  
				<ul>
					<?php foreach ( $rss_items as $item ) : ?>
						<li>
							<a href="<?php echo $item->get_permalink(); ?>"><span class="date"><?php echo $item->get_date('d.m.Y'); ?></span>
							<p><?php echo $item->get_title(); ?></p>
							</a>
						</li>

					<?php endforeach; ?>
				
				</ul>
			</section>
	<?php } ?>
	
	<?php /*
    $args = array(
		'posts_per_page'	=> 5
	);
	$query = new WP_Query($args);
	if($query->have_posts()):
    ?>
    <section  class="widget widget_archive">
      <h4 class="widget-title"><?php _e('Articles recents','ch_du_bo'); ?></h4>

      	<ul>
      		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
            	<li>
                	<a href="<?php echo get_permalink(); ?>"><span class="date"><?php echo get_the_time('d.m.Y'); ?></span>
					<p><?php echo get_the_title(); ?></p>
                    </a>
                </li>

        	<?php endwhile; wp_reset_postdata(); ?>

        </ul>


    </section>
    <?php
		endif;
	*/?>


    <?php
    $tagCloud = wp_tag_cloud( 'smallest=12&largest=12&format=list&link=view&echo=0' ); 
    if($tagCloud){
      ?>
        <section  class="widget widget_tags">
          <h4 class="widget-title">Tags</h4>
      <?php echo $tagCloud; ?>
        </section>
    <?php } ?>

    <section  class="widget widget_social">
        <h4 class="widget-title"><?php _e('Suivez-nous','ch_du_bo'); ?></h4>
        <ul class="social-share">
             <?php if($fbLink): ?>
                 <li class="fb"><a href="<?php echo $fbLink; ?>"><i class="fa fa-facebook"></i></a></li>
             <?php endif; ?>
             <?php if($twLink): ?>
                 <li class="tw"><a href="<?php echo $twLink; ?>"><i class="fa fa-twitter"></i></a></li>
             <?php endif; ?>
             <?php if($instaLink): ?>
                 <li class="insta"><a href="<?php echo $instaLink; ?>"><i class="fa fa-instagram"></i></a></li>
             <?php endif; ?>
             <?php if($youtubeLink): ?>
                 <li class="youtube"><a href="<?php echo $youtubeLink; ?>"><i class="fa fa-youtube-square"></i></a></li>
             <?php endif; ?>
             <?php if($linkedinLink): ?>
                 <li class="link"><a href="<?php echo $linkedinLink; ?>"><i class="fa fa-linkedin"></i></a></li>
             <?php endif; ?>
         </ul>
    </section>
</aside>
