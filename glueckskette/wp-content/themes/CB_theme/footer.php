                 
      
      </section><!-- container -->
  
	

	<div class="copy footer"> 
    <section class="container">
        <ul class="fnav-left">
        	<li><?php _e('Glückskette', 'CB_theme'); ?> &copy; 2014</li>
         	<li><a target="_blank" href="<?php _e('http://www.glueckskette.ch/de/disclaimer/rechtliche-hinweise.html', 'CB_theme'); ?>"><?php _e('Rechtliche Hinweise', 'CB_theme'); ?></a></li>
         	<li><a target="_blank" href="<?php _e('http://www.glueckskette.ch/de/disclaimer/impressum.html', 'CB_theme'); ?>"><?php _e('Impressum', 'CB_theme'); ?></a></li>
        </ul>
        
        <ul class="navbar-right">
        
                    <li><a target="_blank" href="<?php _e('https://www.facebook.com/glueckskette', 'CB_theme'); ?>"> <img src=" <?php echo get_template_directory_uri(); ?>/images/fb_r.png"/></a></li> 
                    <li><a target="_blank" href="<?php _e('https://twitter.com/glueckskette', 'CB_theme'); ?>"> <img src=" <?php echo get_template_directory_uri(); ?>/images/tw_r.png"/></a></li>
                    <li><a target="_blank" href="<?php _e('https://www.youtube.com/user/glueckskette', 'CB_theme'); ?>"> <img src=" <?php echo get_template_directory_uri(); ?>/images/yt_r.png"/></a></li> 
                    <li><a target="_blank" href="<?php _e('mailto:info@glueckskette.ch', 'CB_theme'); ?>"> <img src=" <?php echo get_template_directory_uri(); ?>/images/ml_r.png"/></a></li> 
                    <li><a target="_blank" href="<?php _e('http://www.glueckskette.ch/de/latest.xml', 'CB_theme'); ?>"> <img src=" <?php echo get_template_directory_uri(); ?>/images/rss_r.png"/></a></li> 
        </ul>
        
      </section>
    </div>
    
    <div class="logos footer">
    
        <section class="container">
        <ul class="fnav-left">
        <?php $templateURI =  get_template_directory_uri(); ?>
            <li><img src=" <?php echo $templateURI; ?>/images/srg.jpg"/></li> 
         </ul>
        
        <ul class="navbar-right">
                    <li><img src=" <?php echo $templateURI; ?>/images/swisscom.png"/></li>
                    <li><img src=" <?php echo $templateURI; ?>/images/Keystone.png"/></li> 

                   <?php printf( __('<li><img src="%1$s/images/rrr_DE.png"/></li>','CB_theme'), $templateURI); ?>
                   
                   
        </ul>

		</section>    
        
        <section class="container footer_last">
        <p class="left"><?php _e('Die Glückskette ist eine Stiftung, gegründet auf Initiative der SRG SSR', 'CB_theme'); ?></p>
        
        <p class="right"><?php _e('Partnerschaft & Zusammenarbeit', 'CB_theme'); ?></p>

		</section>          
        
        
    </div>



</div>

  

    <!-- Include all compiled plugins (below), or include individual files as needed -->

<?php wp_footer(); ?>
  </body>
</html>