
          <?php 
/*
Template Name: Finance
*/
		  
		  get_header(); 
		  
$args = array(
	'post_type' => 'Rapprot_fianance',
	'post_status' => 'publish',
	'posts_per_page' => '1');		  
		  
$new_query = new WP_Query( $args );
if ( $new_query->have_posts() ):
	
	while ( $new_query->have_posts() ): $new_query->the_post();
		  ?>
      
      
        <div class="home_content row">
            
            
       <div class="col-sm-3" id="sidenav">
        
            <div class="bs-docs-sidebar hidden-print" role="complementary">       
           <?php $file = get_field('pdf'); 
				
					 ?>              
               
               <div class="PdfDlWrap">        
                    <div class="cb_btn btn_dl"><a target="_blank" href="<?php echo $file["url"]; ?> "> <p><?php _e('Herunterladen', 'CB_theme'); ?></p> <div class="icon"></div></a></div>

                    
                    
               </div>
             </div>
         </div><!--
            
            --><section class="rapport_content col col-sm-9">

               <a target="_blank" href="<?php echo $file["url"]; ?> ">           
			 <div class="page template1 "> 
 		<?php
				$image = get_field('image');
				$url = $image['url'];
				
				$imageLogo = get_field('logo');
				$urlLogo = $imageLogo['url'];
				
				$title = get_field('titre');
				
				?>

					<div class=" col-sm-8 template1 full_img" style="
                    background: url(<?php echo $url; ?>) no-repeat center center;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;
                    background-size: cover;
                          ">
             
           
                      
                      </div>
                   
                   <div class="col-sm-4 redBar">
                     
                    <h1 style="font-size: 1.6em;" ><?php echo $title; ?></h1>
                    <img src="<?php echo $urlLogo; ?>" />      
                    
                      
                    </div> 
             
             
                 
             </div><!-- end page -->
                      </a>
			 
<?php			 
			 
			 endwhile;
			endif;

            ?>



           </section>    
           

        
        </div><!-- main_content -->
        
        <?php  get_footer(); ?>
        

