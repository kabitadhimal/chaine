<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
      <meta charset="<?php bloginfo('charset'); ?>">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
	  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php if ( is_page_template('page-rapport-financier.php') ) {
?>
 <title><?php _e('Jahresrechnung der Glückskette', 'CB_theme'); ?></title>

  <meta name="description" content="<?php _e('Die Jahresrechung der Glückskette ist als PDF verfügbar und kann als solches heruntergeladen werden.', 'CB_theme'); ?>">
<?php
} elseif(is_page_template('page-arch.php') ) {
?>
  <title><?php _e('Archiv der Jahresberichte und -rechungen der Glückskette', 'CB_theme'); ?></title>

  <meta name="description" content="<?php _e('Die früheren Jahresberichte und -rechungen der Glückskette sind als PDF verfügbar und können als solche heruntergeladen werden.', 'CB_theme'); ?>">
<?php    // Returns false when 'about.php' is not being used.
}else{
?>
  <title><?php _e('Jahresbericht der Glückskette', 'CB_theme'); ?></title>

  <meta name="description" content="<?php _e('Der Jahresbericht der Glückskette kann online gelesen (Desktop und Tablets) oder wie die Jahresrechnung als PDF heruntergeladen werden.', 'CB_theme'); ?>">	
<?php	
	}     ?> 
    
            
      <link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/images/favicon.ico" />
    
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      
  <?php wp_head(); ?>    
  
 <?php if ( !is_page_template('page-see_draft.php') ) {
  
  
  _e("<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-48635925-3', 'glueckskette.ch'); ga('send', 'pageview');</script>", 'CB_theme'); 
 }
  ?>  



  </head>
  
  <body data-spy="scroll" data-offset="300" data-target="#sidenav" <?php body_class(); ?> >
  
      <section class="container">
      
        <header class="header row">
            <section style="padding: 0; margin-bottom: 5px;" class="header col col-sm-3 col-md-2 col-lg-2">
           <a href="<?php get_option('home'); ?>/">  <img width="100%" height="auto" src=" <?php echo get_template_directory_uri(); ?>/images/<?php _e('CB_RVB_DE.png', 'CB_theme');?>"/> </a>
            </section>
              <section class="header col col-sm-6 col-md-7 col-lg-8">
            </section>
             <section class="header col col-sm-3 col-md-3 col-lg-2" style="padding:0;">
            	<div id="langues" class="col col-sm-12" style="padding:0;">
					<ul style="padding-left: 3px;">
                    	<li style="list-style:none; "><span class=" <?php _e('active', 'CB_theme');?>"><a href="http://jb.glueckskette.ch/">DE</a></span></li>
                        <li><span class=" <?php _e('fr_lang_menu', 'CB_theme');?>" ><a href="http://ra.bonheur.ch/">FR</a></span></li>
                        <li><span class=" <?php _e('en_lang_menu', 'CB_theme');?>"><a href="http://ar.swiss-solidarity.org/">EN</a></span></li>
                        <li><span class=" <?php _e('it_lang_menu', 'CB_theme');?>"><a href="http://ra.catena-della-solidarieta.ch/">IT</a></span></li>
                        <li style="float:right; list-style:none;"><span><a href="<?php _e('mailto:info@glueckskette.ch', 'CB_theme'); ?>"><?php _e('KONTAKT', 'CB_theme');?> </a></span></li>
                    </ul>
                </div>
                <div style="padding: 0;" class="col col-sm-12">
                
                <div style="text-align:center; ">
                 <div style="width:100%; margin-bottom:10px; margin-top:10px;" class="cb_btn"><a target="_blank" href="<?php _e('http://www.glueckskette.ch', 'CB_theme'); ?>"> <p><?php _e('Hauptseite', 'CB_theme'); ?> </p> <div class="icon"></div></a></div>
                </div>
                
                </div>
                
            </section>
            
     <div id="nav_affix">
            <nav class="navbar col col-sm-12" role="navigation">
                <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php 
					$defaults = array(

						'container'       => false,
						'menu_class'      => 'menu nav navbar-nav navbar-left',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					);
					
					wp_nav_menu( $defaults );
					
					?>
                    <ul class="nav navbar-nav navbar-right">
                    <li><a target="_blank" href="<?php _e('https://www.facebook.com/glueckskette', 'CB_theme'); ?>"> <img src=" <?php echo get_template_directory_uri(); ?>/images/fb.png"/></a></li> 
                    <li><a target="_blank" href="<?php _e('https://twitter.com/glueckskette', 'CB_theme'); ?>"> <img src=" <?php echo get_template_directory_uri(); ?>/images/tw.png"/></a></li>
                    <li><a target="_blank" href="<?php _e('https://www.youtube.com/user/glueckskette', 'CB_theme'); ?>"> <img src=" <?php echo get_template_directory_uri(); ?>/images/yt.png"/></a></li> 
                    <li><a target="_blank" href="<?php _e('mailto:info@glueckskette.ch', 'CB_theme'); ?>"> <img src=" <?php echo get_template_directory_uri(); ?>/images/ml.png"/></a></li> 
                    <li><a target="_blank" href="<?php _e('http://www.glueckskette.ch/de/latest.xml', 'CB_theme'); ?>"> <img src=" <?php echo get_template_directory_uri(); ?>/images/rss.png"/></a></li> 
                    </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
	  </div> 
</header><!-- header -->  