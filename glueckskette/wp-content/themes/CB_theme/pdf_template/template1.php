<style type="text/css">
<!--
table { vertical-align: top; }
tr    { vertical-align: top; }
td    { vertical-align: top; }
}
-->
</style>
<?php
	$title = get_sub_field('title');
	
	$image = get_sub_field('bg_image');
	$url = $image['url'];

	$logo = get_sub_field('logo');
	$urlLogo = $logo['url'];
?>

		<page backimg="<?php echo $url; ?>">
			

		<table style="border-collapse:collapse">
			<tr>
				<td style="height:251mm; width:100%;" colspan="2"></td>
			</tr>
			<tr style="background:#D9272E;">
				<td align="left" style="height:43mm; padding-left:7mm; width:60%; "><h1 style="color:#FFF; font-size:30px; font-weight:light; margin-top:21mm; margin-left:17mm; text-transform:uppercase;"><?php echo $title; ?></h1></td>
				<td align="right" style="height:43mm; width="40%""><img width="276" height="110" style="margin-right:7mm; margin-top:9mm;" src="<?php echo $urlLogo; ?>"/></td>
			</tr>
		</table>


	
	</page>