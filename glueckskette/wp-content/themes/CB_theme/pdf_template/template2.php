<?php
 	if(get_sub_field('img')){
		$get_img = true;
		$image = get_sub_field('img');
		$url = $image['url'];	
		
		}else{
			$get_img = false;
			}
		$title = get_sub_field('title'); 
		$content = get_sub_field('content'); 
		
		// Allow <p> and <a>
		$content = strip_tags($content, '<a><ul><li><p>');
			
	?>	
    
<style type="text/css">
<!--
ul
{
    background: #FFDDDD;
    border: solid 1px #FF0000;
}

ol
{
    background: #DDFFDD;
    border: solid 1px #00FF00;
}

ul li
{
    background: #DDFFAA;
    border: solid 1px #AAFF00;
}

ol li
{
    background: #AADDFF;
    border: solid 1px #00AAFF;
}
-->
</style>    	
		
<page>
    

<table style="border-collapse:collapse" style="width: 100%; height:100% text-align: left; font-size: 12pt;"  >
<?php if($get_img){ ?>
    <tr>
        <td style="width:100%;"><img style="width:210mm" width="210mm" src="<?php echo $url; ?>"/></td>
    </tr>	
	<?php } ?>


    <tr>
        <td style="width:100%; padding-left:15mm; padding-right:15mm; padding-top:15mm; font-size: 24pt; color:#D9272E"><?php echo $title; ?> </td>  	

    </tr>
    <tr>
        <td style="width:100%; padding-left:15mm; padding-right:15mm; padding-top:5mm"> <? echo $content; ?> </td>  	

    </tr>    
</table>



</page>




