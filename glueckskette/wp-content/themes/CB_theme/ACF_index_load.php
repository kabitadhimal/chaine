<?php 

	 // Our include  
	 define('WP_USE_THEMES', false);  
	 require_once('../../../wp-load.php'); 
		
		
	$postID = $_POST['postID'];
	$linkRef = $_POST['links_ref'];

	function slugify($str){
			if($str !== mb_convert_encoding( mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32') )
				$str = mb_convert_encoding($str, 'UTF-8', mb_detect_encoding($str));
			$str = htmlentities($str, ENT_NOQUOTES, 'UTF-8');
			$str = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\\1', $str);
			$str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
			$str = preg_replace(array('`[^a-z0-9]`i','`[-]+`'), '-', $str);
			$str = strtolower( trim($str, '-') );
			return $str;
	}

	
	
	$args = array(
   		'p'      => $postID
	);
	
$loop_index = new WP_Query( $args );		

if ( $loop_index->have_posts() ) : while ( $loop_index->have_posts() ) : $loop_index->the_post();

$newChoices = array();
$i1 = 0; 


while ( have_rows('index') ) : the_row();
	$i1++;
	
	$txt =  get_sub_field('primary_title_txt');
	
	$newChoices[ $i1 . '-' . slugify($txt)] = $i1 . '.' . $txt;
	
		if( have_rows('secondary_title') ):
		$i2 = 0;
			while ( have_rows('secondary_title') ) : the_row();
			$i2++;
		$txt = 	get_sub_field('secondary_title_text');
			
		$newChoices[ $i1 . '-'. $i2 . '-' . slugify($txt)] = $i1 . '.' . $i2 . '.' . $txt;
	
		if( have_rows('tercerary_title') ):
		$i3 = 0;
			while ( have_rows('tercerary_title') ) : the_row();
			$i3++;
		
		$txt = 	get_sub_field('tercerary title_text');
			
		$newChoices[ $i1 . '-'. $i2 . '-' . $i3.'-'. slugify($txt) ] = $i1 . '.' . $i2 . '.' . $i3 . '.' . $txt;
	
				endwhile;
			endif;
		
			endwhile;
		endif;


	endwhile;

endwhile; //end post loop
endif;

/*
---links_ref
$('#wp-link-wrap #most-recent-results ul').html('<li class="alternate"><input class="item-permalink" value="#none" type="hidden"><span class="item-title">cover</span><span class="item-info">test</span></li>');*/

if($linkRef === 'links_ref'){
		$iLink = 0 ;
	
		foreach( $newChoices as $key => $value ) {			
			$iLink++;
			
			if ($iLink % 2 == 0) {
				$liClass = 'alternate';
			}else{
				$liClass = '';
			}
			
				echo '<li class=" '.$liClass.' "><input class="item-permalink" value="#'.$key.'" type="hidden"><span class="item-title">'.$value.'</span></li>';
			
			}		
}else{

echo '<option value="cover">cover</option>';

foreach( $newChoices as $key => $value ) {			
	echo '<option value="' . $key . '">' . $value . '</option>';
}				
echo '<option value="footer">footer</option>';
		
		}


		
		

?>