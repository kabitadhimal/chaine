
          <?php 
/*
Template Name: myarchives
*/

	 get_header(); 
		 
$args = array(
	'post_type' => 'archives',
	'posts_per_page' => '-1',
	'meta_key'		=> 'year',
	'orderby'		=> 'meta_value_num',
	'order'			=> 'DESC'
	);		  
	
?>         <div class="home_content row">	 <?php	
		  
$arch_query = new WP_Query( $args );
if ( $arch_query->have_posts() ):

	while ( $arch_query->have_posts() ): $arch_query->the_post();
		  ?>
      
      


             
 		<?php

				
				$year = get_field('year');
				?>

            
		<section class="col col-sm-12">		             
             
	<h3 style="margin-top:40px; margin-bottom: 5px;
font-weight: bold;"><?php echo $year; ?> </h3></td> 
   
<?php
// check if the repeater field has rows of data
if( have_rows('archive') ):
	// loop through the rows of data
    while ( have_rows('archive') ) : the_row();
	
	if(get_sub_field('title')){
		
	
        // display a sub field value
		echo '<div class="row" style="border-top: 1pt solid rgb(0, 0, 0); position: relative;">'; 		
		 echo '<h4  style="margin-top: 15px; margin-bottom: 18px;  margin-left: 15px; font-weight: bold;font-size: 16px;">'. get_sub_field('title') . '</h4>';


				$file = get_sub_field('pdf'); 				
				$url = $file['url'];

?>

<div class="cb_btn btn_dl" style="position: absolute; right: 0px; top: 6px;">
	<a target="_blank" href="<?php echo $file["url"]; ?> "> <p><?php _e('Herunterladen', 'CB_theme'); ?></p><div class="icon"></div>
    
    </a>
</div> <?php
	echo '</div>';
	
	}
    endwhile; 
	
endif;
?>           
		   </section>	 
<?php			 
			      
			 endwhile;
			endif;

            ?>



       
           

        
        </div><!-- main_content -->
        
        <?php  get_footer(); ?>
        

