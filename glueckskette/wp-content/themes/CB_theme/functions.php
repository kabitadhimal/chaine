<?php 

function your_logout() {

wp_clear_auth_cookie();

}
add_action('wp_logout', 'your_logout');


function my_login_redirect( $redirect_to, $request, $user ) {

	  if ($user->ID != 0) {
        $user_info = get_userdata($user->ID);
            $primary_url = get_blogaddress_by_id($user_info->primary_blog) . 'wp-admin/edit.php';
            if ($primary_url) {
                wp_redirect($primary_url);
                die();
            
        }
    }
    return $redirect_to;

	 

}



add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );




add_action('pre_get_posts','alter_query');

function alter_query($query) {
	//gets the global query var object
	global $wp_query;
	//gets the front page id set in options
if ( !$query->is_main_query() )
		return;
			
      if(isset($_GET['post_id']))
			$query->set('p', $_GET['post_id']);
			
	//we remove the actions hooked on the '__after_loop' (post navigation)
	remove_all_actions ( '__after_loop');
}


add_action('after_setup_theme', 'my_theme_setup');
function my_theme_setup(){
    load_theme_textdomain('CB_theme', get_template_directory() . '/languages');
}


add_filter('show_admin_bar', '__return_false');
//deactivate autosave
add_action( 'admin_init', 'disable_autosave' );
function disable_autosave() {
        wp_deregister_script( 'autosave' );
		remove_action('all', 'wp_ajax_wp-link-ajax');

}


// Admin Bar Customisation
function mytheme_admin_bar_render() {
	
	
global $wp_admin_bar;
// Remove an existing link using its $id
// Here we remove the ‘Updates’ drop-down link
	if(!is_super_admin()){
		
		$wp_admin_bar->remove_menu('updates');
		$wp_admin_bar->remove_menu('comments');
		$wp_admin_bar->remove_menu('new-content');
		$wp_admin_bar->remove_menu('my-sites');
		$wp_admin_bar->remove_menu('wp-logo');		
	
	}

}

// Finally we add our hook function
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );




add_action( 'admin_init', 'my_remove_menu_pages' );


function my_remove_menu_pages() {

	if(!is_super_admin()){
	 
		//removing menu pages	
		remove_menu_page('edit-comments.php'); // Comments  
		remove_menu_page('upload.php'); // Comments  
		remove_menu_page('themes.php'); // Appearance
		remove_menu_page('users.php'); // Users
		remove_menu_page('tools.php'); // Tools
		remove_menu_page('plugins.php'); // Plugins
		remove_menu_page('edit.php?post_type=acf-field-group'); // ACF	
		  remove_menu_page('acf-options'); // ACF options	
		remove_menu_page('options-general.php'); // ACF options	
		
		
		
		remove_menu_page('edit.php?post_type=page'); // Page	
		


		remove_submenu_page( 'edit.php', 'edit-tags.php?taxonomy=category' ); // Clean posts cat
		remove_submenu_page( 'edit.php', 'edit-tags.php?taxonomy=post_tag' ); // Clean posts tax
	}
}


function change_post_menu_text() {
  global $menu;
  global $submenu;

  // Change menu item
  $menu[5][0] = 'Annual reports';

  // Change post submenu
}

add_action( 'admin_menu', 'change_post_menu_text' );


function change_post_type_labels() {
  global $wp_post_types;

  // Get the post labels
  $postLabels = $wp_post_types['post']->labels;
  $postLabels->name = 'Annual report';
  $postLabels->singular_name = 'Annual report';
  $postLabels->add_new = 'Add annual report';
  $postLabels->add_new_item = 'Add annual report';
  $postLabels->edit_item = 'Edit annual report';
  $postLabels->new_item = 'Annual reports';
  $postLabels->view_item = 'View annual reports';
  $postLabels->search_items = 'Search annual reports';
  $postLabels->not_found = 'No annual report found';
  $postLabels->not_found_in_trash = 'No annual report found in Trash';
}
add_action( 'init', 'change_post_type_labels' );

//custom post type
add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type( 'Rapprot_fianance',
		array(
			'labels' => array(
				'name' => 'Financial reports',
				'singular_name' => 'Financial raport'
			),
		'public' => true,
		'has_archive' => true,
		)
	);
	
	register_post_type( 'archives',
			array(
				'labels' => array(
					'name' => 'Archives',
					'singular_name' => 'archive'
				),
			'public' => true,
			'has_archive' => true,
			)
		);	
	
}



// This is the confirmation message that will appear.
$c_message = 'Are you sure you want to publish?';

function confirm_publish(){

global $c_message;
echo '<script type="text/javascript"><!--
var publish = document.getElementById("publish");
var $c_publish = "no";

if (publish !== null) publish.onclick = function(){

	if($c_publish == "no"){
		
		 var r = confirm("'.$c_message.'");
			if (r == true) {
				$c_publish = "yes";
			} else {
				return false;
			}			
			
		}

	
	
	
};
// --></script>';
}
add_action('admin_footer', 'confirm_publish');

//-------------



//preloader 

function loader_slh() { 

    global $post_type;

    if($post_type=='post'){ //only for the posts ?>


    <style>

    .loader_slh {

        position: fixed;

        left: 0px;

        top: 0px;

        width: 100%;

        height: 100%;

        z-index: 10000;

        background: url('<?php echo get_template_directory_uri() ?>/images/495.GIF') 50% 50% no-repeat rgb(249,249,249);

        /*text-indent:-9999px;*/



    }

    </style>

    <script>

           jQuery('<div class="loader_slh">loading</div>').prependTo('body');



			jQuery(window).load(function() {
		
				jQuery(".loader_slh").fadeOut("slow");
		
		});
        </script>

    <?

    

	}

}

add_action('admin_footer', 'loader_slh');



//remove internal linking 



	/* Remove Theme suports thumbnails  */

	remove_theme_support( 'post-thumbnails' ); 	
	
	// UNSET original sizes

	function paulund_remove_default_image_sizes( $sizes) {
    unset( $sizes['thumbnail']);
    unset( $sizes['medium']);
    unset( $sizes['large']);
     
    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'paulund_remove_default_image_sizes');	


function wpb_adding_scripts() {
 if (!is_admin()) {
		  wp_deregister_script('jquery');
		  wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', false, '1.11.0');
		  wp_enqueue_script('jquery');
			
			wp_register_script('bootstrap', get_template_directory_uri() . '/_/js/bootstrap.js', array('jquery'));
			wp_enqueue_script('bootstrap');

		  if ( !is_page_template('page-pdf.php') ) {
				wp_register_script('myScript', get_template_directory_uri() . '/_/js/myscript.js', array('jquery','bootstrap'));
				wp_enqueue_script('myScript'); 
		  }else{
				wp_register_script('pdfScript', get_template_directory_uri() . '/_/js/jspdf.js', array('jquery','bootstrap'));	
				wp_enqueue_script('pdfScript'); 			  
			  
			  }

	 }
}

function my_styles(){
	wp_register_style( 'bootstrapCSS', get_template_directory_uri() . '/_/css/bootstrap.css');
	wp_register_style( 'theme-css', get_stylesheet_uri() , array('bootstrapCSS'));
    wp_enqueue_style('theme-css');
}

add_action('wp_enqueue_scripts', 'my_styles');
add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' ); 


//nav

if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(
		array(
		  'primary' => 'Primary Header Nav'
		)
	);
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
     if( in_array('current-menu-item', $classes) ){
             $classes[] = 'active ';
     }
     return $classes;
}


	function renderACFfield( $field ) {
			if($field['_name'] == 'index_section'){
			echo '<div style="display:none;" class="fieldVal">'.$field['value'].'</div>'; 
				}
				
	}

add_filter( 'acf/render_field', 'renderACFfield', 12, 1 );


//include Admin script for ACF
 function my_acf_field_group_admin_enqueue_scripts(){
	 
    wp_enqueue_script( 'custom_admin_ACF_script', get_template_directory_uri() . '/_/js/ACF_admin.js', array('jquery'));

	 wp_localize_script( 'custom_admin_ACF_script', 'MyACF', array(
			'post_id' => $_GET['post'],
			'theme_directory' => get_template_directory_uri(),
			'site_url' =>   get_site_url(),
			'post_statu' => get_post_status(),
			'ajaxurl' => admin_url('admin-ajax.php')
		  ));	
	
	 } 
add_action('acf/input/admin_enqueue_scripts', 'my_acf_field_group_admin_enqueue_scripts');




//img size check


function limit_img_ajax(){
	
	$ptype = filter_input( INPUT_POST, 'ptype', FILTER_SANITIZE_STRING);
	$pid = filter_input( INPUT_POST, 'pid', FILTER_SANITIZE_STRING);
	$field_name = filter_input( INPUT_POST, 'field_name', FILTER_SANITIZE_STRING);
	

	
	if( 0 < ( $uid = get_current_user_id() ) )
			update_user_meta( $uid, 'my_last_acf_field', array( 'pid' => $pid, 'field_name' => $field_name, 'ptype' => $ptype ) );


	echo 'layout: ' . $field_name . 'field : ' . $pid . ' ptype : ' . $ptype ;

	die();	
	
}


//ajax


add_action( 'wp_ajax_customaction', 'limit_img_ajax' );

//prefileter img
function aj_handle_upload_prefilter($file){
	//user data
	$userID = get_current_user_id();
	$template_name = get_user_meta( $userID, 'my_last_acf_field', true ); 
		
	//img data
	$img = getimagesize($file['tmp_name']);
	$width = $img[0];
	$height = $img[1];	
	
		//template 1	
		if( $template_name['field_name'] == 'template1'){

			if( $template_name['pid'] == 'bg_image'){
			$size = array('width' => '794', 'height' => '1123');
				}elseif($template_name['pid'] == 'logo'){
			$size = array('width' => '276', 'height' => '110');
					}
			
			if ($width != $size['width'] || $height !=  $size['height'] )
				return array("error"=>"Image dimensions are incorrect. This image should be {$size['width']}px x {$size['height']}px . Uploaded image width is {$width}px x {$height}px");
			else
				return $file; 		
			
		//end template1
		}elseif($template_name['field_name'] == 'template2'){ 
				$size = array('width' => '794', 'height' => '393');
			if ($width != $size['width'] || $height !=  $size['height'] )
						return array("error"=>"Image dimensions are incorrect. This image should be {$size['width']}px x {$size['height']}px . Uploaded image width is {$width}px x {$height}px");
					else
						return $file; 
		//end template2				
		}elseif($template_name['field_name'] == 'template3'){ 
				$size = array('width' => '246', 'height' => '246');
			if ($width != $size['width'] || $height !=  $size['height'] )
						return array("error"=>"Image dimensions are incorrect. This image should be {$size['width']}px x {$size['height']}px . Uploaded image width is {$width}px x {$height}px");
					else
						return $file; 
		//end template3						
		}elseif($template_name['field_name'] == 'template4'){ 
				$size = array('width' => '794', 'height' => '1123');
			if ($width != $size['width'] || $height !=  $size['height'] )
						return array("error"=>"Image dimensions are incorrect. This image should be {$size['width']}px x {$size['height']}px . Uploaded image width is {$width}px x {$height}px");
					else
						return $file; 
		//end template					
				
		}elseif($template_name['field_name'] == 'template5'){ 
				$size = array('width' => '410', 'height' => '1038');
			if ($width != $size['width'] || $height !=  $size['height'] )
						return array("error"=>"Image dimensions are incorrect. This image should be {$size['width']}px x {$size['height']}px . Uploaded image width is {$width}px x {$height}px");
					else
						return $file; 
		//end template5						
				
		}elseif($template_name['field_name'] == 'template6'){ 
				$size = array('width' => '794', 'height' => '393');
			if ($width != $size['width'] || $height !=  $size['height'] )
						return array("error"=>"Image dimensions are incorrect. This image should be {$size['width']}px x {$size['height']}px . Uploaded image width is {$width}px x {$height}px");
					else
						return $file; 
		//end template6				
		}elseif($template_name['field_name'] == 'template7'){ 
				$size = array('width' => '150', 'height' => '55');
			if ($width != $size['width'] || $height !=  $size['height'] )
						return array("error"=>"Image dimensions are incorrect. This image should be {$size['width']}px x {$size['height']}px . Uploaded image width is {$width}px x {$height}px");
					else
						return $file; 
		//end template7				
		}elseif($template_name['field_name'] == 'template8'){ 
		
			if( $template_name['pid'] == ' logo_ssr'){
			$size = array('width' => '110', 'height' => '44');

			if ($width != $size['width'] || $height !=  $size['height'] )
						return array("error"=>"Image dimensions are incorrect. This image should be {$size['width']}px x {$size['height']}px . Uploaded image width is {$width}px x {$height}px");
					else
						return $file;			
			
			
				}elseif($template_name['pid'] == 'logo'){
			$size = array('width' => '276', 'height' => '110');
			if ($width != $size['width'] || $height !=  $size['height'] )
						return array("error"=>"Image dimensions are incorrect. This image should be {$size['width']}px x {$size['height']}px . Uploaded image width is {$width}px x {$height}px");
					else
						return $file;			
			
					}elseif($template_name['pid'] == 'logo_partenaires'){
			$size = array('width' => '232', 'height' => '102');
			if ($width != $size['width'] || $height !=  $size['height'] )
						return array("error"=>"Image dimensions are incorrect. This image should be {$size['width']}px x {$size['height']}px . Uploaded image width is {$width}px x {$height}px");
					else
						return $file;			
					}elseif($template_name['pid'] == 'logo_swiss_solidarity'){
			$size = array('width' => '150', 'height' => '55');
			if ($width != $size['width'] || $height !=  $size['height'] )
						return array("error"=>"Image dimensions are incorrect. This image should be {$size['width']}px x {$size['height']}px . Uploaded image width is {$width}px x {$height}px");
					else
						return $file;			
			
					}elseif($template_name['pid'] == 'image_qr'){
			$size = array('width' => '216', 'height' => '156');
			if ($width != $size['width'] || $height !=  $size['height'] )
						return array("error"=>"Image dimensions are incorrect. This image should be {$size['width']}px x {$size['height']}px . Uploaded image width is {$width}px x {$height}px");
					else
						return $file;			
					}elseif($template_name['pid'] == 'image'){
			$size = array('width' => '121', 'height' => '48');
			if ($width != $size['width'] || $height !=  $size['height'] )
						return array("error"=>"Image dimensions are incorrect. This image should be {$size['width']}px x {$size['height']}px . Uploaded image width is {$width}px x {$height}px");
					else
						return $file;			
					}elseif($template_name['pid'] == 'image-app'){
			$size = array('width' => '146', 'height' => '47');
			if ($width != $size['width'] || $height !=  $size['height'] )
						return array("error"=>"Image dimensions are incorrect. This image should be {$size['width']}px x {$size['height']}px . Uploaded image width is {$width}px x {$height}px");
					else
						return $file;			
					}
			
					else
						return $file; 
		//end template8			
		}elseif( $template_name['ptype'] == 'finance'){

			if( $template_name['pid'] == 'image'){
			$size = array('width' => '794', 'height' => '1123');
				}elseif($template_name['pid'] == 'logo'){
			$size = array('width' => '276', 'height' => '110');
					}
			
			if ($width != $size['width'] || $height !=  $size['height'] )
				return array("error"=>"Image dimensions are incorrect. This image should be {$size['width']}px x {$size['height']}px . Uploaded image width is {$width}px x {$height}px");
			else
				return $file;
				
		}else{//no template
		
		return $file; 
			
			}

	
	

	
	}
	
	add_filter('wp_handle_upload_prefilter','aj_handle_upload_prefilter',10);
	
	//admin bar
	

function ds_login_redirect( $redirect_to, $request_redirect_to, $user )
{
    if ($user->ID != 0) {
        $user_info = get_userdata($user->ID);
        if ($user_info->primary_blog) {
            $primary_url = get_blogaddress_by_id($user_info->primary_blog) . 'wp-admin/';
            if ($primary_url) {
                wp_redirect($primary_url);
                die();
            }
        }
    }
    return $redirect_to;
}
add_filter('login_redirect','ds_login_redirect', 100, 3);


?>
<?php

remove_action('wp_head', 'wp_generator');
