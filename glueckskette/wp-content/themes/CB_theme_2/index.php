
          <?php get_header();
	function slugify($str){
	    if($str !== mb_convert_encoding( mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32') )
	        $str = mb_convert_encoding($str, 'UTF-8', mb_detect_encoding($str));
	    $str = htmlentities($str, ENT_NOQUOTES, 'UTF-8');
	    $str = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\\1', $str);
	    $str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
	    $str = preg_replace(array('`[^a-z0-9]`i','`[-]+`'), '-', $str);
	    $str = strtolower( trim($str, '-') );
	    return $str;
}

		  ?>


        <div class="home_content row">


       <div class="col-sm-3" id="sidenav">

            <div class="bs-docs-sidebar hidden-print" role="complementary">
    <?php

           if( have_rows('index') ):
                    ?>
                <ul id="my_affix" class="nav bs-docs-sidenav affix">
                <li style="display:none !important; "> <a href="#cover"><?php _e('nach oben', 'CB_theme'); ?></a> </li>
                <?php
                        $i1 = 0;
                        // loop through the rows of data
                        while ( have_rows('index') ) : the_row();

                        $i1++;			?>
                      <li>
                      <?php $txt = get_sub_field('primary_title_txt'); ?>

                      <a href="#<?php echo $i1 . '-' . slugify($txt);?>"><?php echo $i1 . '. ' . $txt;?></a>
                        <?php	if( have_rows('secondary_title') ):
						        $i2 = 0;

                                        ?>
                                        <ul class="nav">
                                        <?php
                                        while ( have_rows('secondary_title') ) : the_row();
                                        $i2++;
                                        $txt = get_sub_field('secondary_title_text');

                                        ?>
                                        <li><a href="#<?php echo $i1 . '-'. $i2 . '-' . slugify($txt);?>"><?php echo $i1 . '.'. $i2 . '. ' . $txt;?></a>

                                                <?php	if( have_rows('tercerary_title') ):
														$i3 = 0;
                                                            ?>
                                                            <ul class="nav">
                                                            <?php

                                                            while ( have_rows('tercerary_title') ) : the_row();
                                                            $i3++;

                                                            $txt = get_sub_field('tercerary title_text');

                                                            ?>



                                                           <li><a href="#<?php echo $i1 . '-'. $i2 . '-' . $i3.'-'. slugify($txt);?>"><?php echo $i1 . '.'. $i2 . '.' . $i3.'. '. $txt;?></a></li>

                                                            <?php

                                                                            endwhile;
                                                                        ?>	</ul> <!--close tercierary ul --> <?php
                                                                        endif;
                                                           ?>
                                                                </li><!--close secondary li -->

                                                            <?php


                                                            endwhile; ?>

                                                            </ul><!--close secondary ul -->
                                                    <?php
                                                        endif;	?>

                                                    </li> <!--close primary li -->

                                            <?php
                                                // display a sub field value

                                            endwhile;

                                        endif;

                                    ?>






        		   <!--  <li style="display:none"> <a href="#footer">footer</a> </li> -->
                <li>

       <?php $file = get_field('pdf_file');?>

		<div class="PdfDlWrap">
			<div class="cb_btn btn_dl">
            	<a onclick=" ga('send', 'event', 'PDF', <?php _e("'Download pdf DE'", "CB_theme"); ?>, this.dataset.filename);" target="_blank" data-filename="<?php echo $file['filename'] ?>" href="<?php echo $file['url']; ?>">
                	<p><?php _e('Herunterladen', 'CB_theme'); ?></p> <div class="icon"></div>
                </a>
              </div>
         </div>



       </li>
                           <div class="indexBtn"></div>

                    </ul><!--close index UL -->


             </div>
         </div><!--

            --><section class="rapport_content col col-sm-9">

			<?php

            // check if the flexible content field has rows of data
            if( have_rows('select_layout') ):

                 // loop through the rows of data
                while ( have_rows('select_layout') ) : the_row();
				            $page_name = get_row_layout();
				?>

			 <div class="page <?php echo $page_name; ?> ">
				<div class="index_marker" id="<?php echo get_sub_field('index_section'); ?>"></div>
 		<?php
 				if( get_row_layout() == 'template1' ):
				$image = get_sub_field('bg_image');
				$url = $image['url'];

				$imageLogo = get_sub_field('logo');
				$urlLogo = $imageLogo['url'];

				$title = get_sub_field('title');

				?>

					<div class=" col-sm-8 template1 full_img" style="
                    background: url(<?php echo $url; ?>) no-repeat center center;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;
                    background-size: cover;
                          ">



                      </div>

                   <div class="col-sm-4 redBar">

                    <h1 ><?php echo $title; ?></h1>
                    <img src="<?php echo $urlLogo; ?>" />


                    </div>



					<?php
					endif;// end if template 1 -->

				// IF template 2 -->
                 if( get_row_layout() == 'template2' ): ?>
                 <?php
         	$image = get_sub_field('img');
					$url = $image['url'];

          $header_video = get_sub_field('header_video_check');
          $yt_video_url = get_sub_field('yt_video_url');

					$title = get_sub_field('title');
					$content = get_sub_field('content');


          $fact_box = get_sub_field('fact_box');

          $side_img_check = get_sub_field('side_img_check');


          if($header_video){

            ?>
            <div class="image place_holder">

              <?php


              preg_match('/src="(.+?)"/', $yt_video_url, $matches);
              $src = $matches[1];


                if($image){
                    $iframeStyle = 'display:none';

                    ?>
                    <img width="100%" height="auto" src="<?php echo $url; ?>" />

                    <?php

                }else{

                  $iframeStyle = '';

                }
              ?>




                    <?php

                      $params = array(
                          'rel'         => 0,
                          'controls'    => 1,
                          'hd'        => 1,
                          'autohide'    => 1,
                          'showinfo'    => 0

                      );
                      $new_src = add_query_arg($params, $src);
                      $yt_video_url = str_replace($src, $new_src, $yt_video_url);
                      $attributes = 'frameborder="0" style="'.$iframeStyle.'"';
                      $yt_video_url = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $yt_video_url);
                      echo $yt_video_url;

                    ?>
            </div>
            <?php
          }elseif($image){ ?>
                	<div class="image">
						              <img width="100%" height="auto" src="<?php echo $url; ?>" />
                 	</div>

                <?php } ?>

                 	<div class="content">
                   	<h3 > <?php echo $title; ?> </h3><br>
                 <?php
					echo $content;


			 ?>
             		</div> <!-- end content -->


                <?php
                $factBoxStyles = '';
                if($side_img_check){
                  $side_img_pos = get_sub_field('side_img_pos');
                  $side_img = get_sub_field('side_img');
                  $side_url = $side_img['url'];
                  $factBoxStyles = "width: 79%; display: inline-block; margin: 0px; vertical-align: middle;";

                  if($side_img_pos === 'left'){
                    echo '<div class="side_img">';

                      echo '<a class="image-popup-no-margins" href="'.$side_url.'">';
                        echo '<img class="side_img_el" src=" '.$side_url.' " />';
                      echo '</a>';

                    echo '</div>';
                  }



                }

				if( get_sub_field('fact_box_check') ){
				if( have_rows('fact_box') ):
					echo '<div class="fact_box" style="'.$factBoxStyles.'">';

					$ifact = 0;

					while ( have_rows('fact_box') ) : the_row();
					$ifact++;
					endwhile;

					$ifactCount = 0;




					while ( have_rows('fact_box') ) : the_row();
					$ifactCount++;
					if($ifact > 2 && $ifactCount === 1 || $ifactCount === 3 ){

						if($ifactCount === 1 ){ $colorCol = '#d2d3d3';
							echo '<div class="col col-sm-4" style="background:'.$colorCol.'">'; //2 col

						}else{
							$colorCol = 'transparent';
							echo '<div class="col col-sm-8" style="background:'.$colorCol.'">'; //10 col

							}

						}else{

							if($ifactCount === 1){ echo '<div class="single_col">';}//12 col

							}

							echo '<h4>'. get_sub_field('titre');
							if($ifactCount === 2){
								echo ' ' . get_sub_field('texte') . '</h4>';
								}else{
									echo '</h4>';
									echo '<p>' . get_sub_field('texte') . '</p>';

									}

						if($ifact === 1 && $ifactCount === 1){ echo '</div>';} ?> <!-- close single --> <?php

						if($ifact > 2 && $ifactCount === 2 || $ifactCount === 4 ){
						echo '</div>'; ?> <!-- close 6 col --> <?php
						}else{
							if($ifactCount === 2){ echo '</div>';}else{
							if($ifact === 3 && $ifactCount === 3){ echo '</div>';} ?> <!-- close 12 col --> <?php
						}
							 ?> <!-- close 12 col --> <?php
						};

					endwhile;



					echo '</div>'; ?> <!-- close fact --> <?php

				endif;
				?>

				<?php
				}

          if($side_img_check){
            if($side_img_pos === 'right'){
              echo '<div class="side_img">';

                echo '<a class="image-popup-no-margins" href="'.$side_url.'">';
                  echo '<img class="side_img_el" src=" '.$side_url.' " />';
                echo '</a>';

              echo '</div>';
            }
          }

				?>

        <?php //external link after factbox
  			if( get_sub_field('add_ext_link') ){
  				if( have_rows('lien_externe') ):
  					echo '<div style="text-align:center">';

  						while ( have_rows('lien_externe') ) : the_row();
  						echo ' <div class="cb_btn" style="margin-top:20px">  <a target="_blank" href="'. get_sub_field('lien') .'"> <p>'. get_sub_field('label') .'</p> <div class="icon"></div></a></div>';

  						endwhile;
  					echo '</div>';

  					endif;

  			}?>

                 <?php

                    endif; // end if template 2 -->

			 // IF template 3 -->
                 if( get_row_layout() == 'template3' ): ?>
                 <div>
                 <?php


                 	$image = get_sub_field('img');
					$url = $image['url'];

					$title = get_sub_field('title');
					$content = get_sub_field('content');  ?>


                 	<div class="content">
                    <h3  > <?php echo $title; ?> </h3><br>
                     <div class="<?php echo get_sub_field('img_right') ? 'image_right' : 'image'; ?> col col-sm-4" >
						<img src="<?php echo $url; ?>" />
                 	</div>



                 <?php
					echo $content;
			 ?>
             		</div> <!-- end content -->



                </div>
                 <?php

                    endif; // end if template 3 -->

 					// IF template 4 -->
                 if( get_row_layout() == 'template4' ): ?>
                 <?php
                 	$image = get_sub_field('img');
					$url = $image['url'];

					$title = get_sub_field('title');
					$content = get_sub_field('content');
					if($title ){ ?>
                    <h3 > <?php echo $title; ?> </h3>
                    <?php } ?>

					<div class="img_div">
					<img src="<?php echo $url; ?>" />
					</div>
                 	<?php
					if($content ){ ?>
                    <div class="content"> <?php echo $content; ?> </div>
                    <?php } ?>

                 <?php

                    endif; // end if template 4 -->

					// IF template 5 -->
                 if( get_row_layout() == 'template5' ): ?>
                 <div >
                 <?php
                 	$image = get_sub_field('img');
					$url = $image['url'];

					$title = get_sub_field('title');
					$content = get_sub_field('content');  ?>
                	<div class="image col-sm-6">
						<img width="100%" height="auto" src="<?php echo $url; ?>" />
                 	</div>


                <div class="content col-sm-6">

                    <h3  > <?php echo $title; ?> </h3><br>
                 <?php
					echo $content;
			 ?>
				</div><!-- end content -->


			<?php
			if( get_sub_field('add_ext_link') ){
			 	if( have_rows('lien_externe') ):
					echo '<div class="col-sm-6 col-md-12 col-lg-6" style="text-align:center">';

						while ( have_rows('lien_externe') ) : the_row();
						echo ' <div class="cb_btn"><a target="_blank" href="'. get_sub_field('lien') .'"> <p>'. get_sub_field('label') .'</p> <div class="icon"></div></a></div>';

						endwhile;
					echo '</div>';

					endif;
			}
					?>




                </div>
                 <?php

                    endif; // end if template 5 -->

					// IF template 6 -->
                 if( get_row_layout() == 'template6' ): ?>
                 <div>
                 <?php
                    $image = get_sub_field('img');
          					$url = $image['url'];

          					$title = get_sub_field('title');
          					$content = get_sub_field('content');

                    $side_img_check = get_sub_field('side_img_check');


					 ?>
                	<div class="image">
						              <img width="100%" height="auto" src="<?php echo $url; ?>" />
                 	</div>


				<div class="col col-sm-4">
                <?php
				if(get_sub_field('fact_box_check')){
					if( have_rows('fact_box') ):
						echo '<div class="fact_box">';

						$ifact = 0;

						while ( have_rows('fact_box') ) : the_row();
						$ifact++;
						endwhile;

						$ifactCount = 0;


						while ( have_rows('fact_box') ) : the_row();
						$ifactCount++;
						if($ifact > 2 && $ifactCount === 1 || $ifactCount === 3 ){

							if($ifactCount === 1 ){ $colorCol = '#d2d3d3';
								echo '<div  style="background:'.$colorCol.'">'; //col top

							}else{
								$colorCol = 'transparent';
								echo '<div style="background:'.$colorCol.'">'; //col bottom

								}

							}else{
								if($ifactCount === 1){ echo '<div class="single_col">';}		//12 col
								}

								echo '<h4>'. get_sub_field('titre') ;
								if($ifactCount === 2 && $ifact > 2 ){
								echo ' ' . get_sub_field('texte') .'</h4>';
								}else{
									echo '</h4>';
									echo '<p>'. get_sub_field('texte') .'</p>';
									}

							if($ifact > 2 && $ifactCount === 2 || $ifactCount === 4 ){
							echo '</div>'; //6 col
							}elseif($ifactCount === 3 && $ifact === 3){
								echo '</div>';		//12 col //close col
							}else{
								if($ifactCount === 2){ echo '</div>';}//12 col
								};

						endwhile;



						echo '</div>'; //close fact box
						endif;
				}

        if($side_img_check){
          $side_img = get_sub_field('side_img');
          $side_url = $side_img['url'];

            echo '<a class="image-popup-no-margins" href="'.$side_url.'">';
              echo '<img class="side_img_el" src=" '.$side_url.' " />';
            echo '</a>';


        }

				?>
				</div>





                <div class="col-sm-8 content">

                    <h3> <?php echo $title; ?> </h3><br>
                 <?php
					echo $content;
			 ?>

			<?php
			if( get_sub_field('add_ext_link') ){
				if( have_rows('lien_externe') ):
					echo '<div style="text-align:center">';

						while ( have_rows('lien_externe') ) : the_row();
						echo '<div class="cb_btn"> <a target="_blank" href="'. get_sub_field('lien') .'"> <p>'. get_sub_field('label') .'</p> <div class="icon"></div></a></div>';

						endwhile;
					echo '</div>';

					endif;
			}
					?>


				</div><!-- end content -->



                </div>
                 <?php

                    endif; // end if template 6 -->

					// IF template 7 -->
                 if( get_row_layout() == 'template7' ): ?>
                 <div >
                 <?php

					$title = get_sub_field('title');
					$content = get_sub_field('content');

					?>

                 <div class="content">

                    <h3  > <?php echo $title; ?> </h3><br>
					 <?php
                        echo $content;
                 ?>


                <?php
                if( have_rows('partenaire') ):
				    while ( have_rows('partenaire') ) : the_row();

					$image = get_sub_field('logo');
					$url = $image['url'];
					?>

                 <table class="partener" style="width: 100%;">
                  <tr>
                     <td class="col col-sm-3" style="text-align: center; vertical-align: middle;">
                    <?php if(get_sub_field('link')){ echo '<a target="_blank" href="'.get_sub_field('link').'" >';            } ?>
							<img width="auto" height="auto" src="<?php echo $url; ?>"  />
                     <?php if(get_sub_field('link')){ echo'</a>'; } ?>

                            </td>
                     <td class="description" style="font-size: 14px; line-height: 18px; vertical-align: middle; padding: 11px;">
                        <?php
                             echo get_sub_field('description');
                         ?>
                         </td>
                  </tr>
                </table>







					<?php
					endwhile;
				endif;
				?>
                </div><!-- end content -->

                </div>
                 <?php

                    endif; // end if template 7 -->



        					// IF template 8 -->
                 if( get_row_layout() == 'template8' ): ?>
                 <?php
                 	$logo_ssr = get_sub_field('logo_ssr');
					$url_ssr = $logo_ssr['url'];

					$logo_partner = get_sub_field('logo_partenaires');
					$url_partner = $logo_partner['url'];

					$img_qr = get_sub_field('image_qr');
					$url_qr = $img_qr['url'];

					$logo_swiss = get_sub_field('logo_swiss_solidarity');
					$url_sw = $logo_swiss['url'];

					$imp_head = get_sub_field('impressum_head');
					$imp_content = get_sub_field('texte_impressum');
					$adr_content = get_sub_field('texte_adresse');

					?>
                    <div class="col-sm-6">
                   		<h3><?php _e('IMPRESSUM', 'CB_theme'); ?></h3>
						<?php echo $imp_head;  ?>
            			<img src="<?php echo $url_ssr; ?>"/>
                       <p style="margin-top:20px;"> <?php _e('Partnerschaft & Zusammenarbeit', 'CB_theme'); ?>	</p>
            			<img src="<?php echo $url_partner; ?>"/>
						<?php echo $imp_content;  ?>
                    </div>
                    <div class="col-sm-6">
                    	<h3><?php _e('ADRESSE', 'CB_theme'); ?></h3>
						<?php echo $adr_content;  ?>
						<img style="margin-left: auto; margin-right: auto; display: block;" src="<?php echo $url_sw; ?>"/>



                    </div>
                    <div class="col-sm-4" style="clear: both;">

                    <img src="<?php echo $url_qr; ?>"/>


                    </div>
                    <div class="col-sm-8">
                    <p><?php _e('App erhältlich bei:', 'CB_theme'); ?></p>
                   <?php
					if( have_rows('btn_google') ):

						while ( have_rows('btn_google') ) : the_row();
							$image = get_sub_field('image');
							$url = $image['url'];
							$lien = get_sub_field('lien');

							?><a href="<?php echo $lien; ?>"><img src="<?php echo $url; ?>"/></a> <?php


						endwhile;

					endif;

					if( have_rows('boutton_app_store') ):
						while ( have_rows('boutton_app_store') ) : the_row();
							$image = get_sub_field('image-app');
							$url = $image['url'];
							$lien = get_sub_field('lien');

							?><a href="<?php echo $lien; ?>"><img src="<?php echo $url; ?>"/></a> <?php


						endwhile;
					endif; ?>



                    </div>





                 <?php

                    endif; // end if template 8 -->

                    // IF template 9 -->
                             if( get_row_layout() == 'template9' ): ?>
                               <div>
                               <?php

                               	$image = get_sub_field('img');
                      					$url = $image['url'];

                      					$title = get_sub_field('title');
                      					$content = get_sub_field('content');

                                ?>


                             	<div class="content">
                                    <h3> <?php echo $title; ?> </h3><br>

                                <?php


                                if( have_rows('photo_text_bloc') ):



                      						while ( have_rows('photo_text_bloc') ) : the_row();

                                    $sousTitre = get_sub_field('sous_titre');
                                    $photoPos = get_sub_field('photo_pos');

                                    $textClass = 'col col-sm-8';

                      							$image = get_sub_field('photo');
                      							$url = $image['url'];
                      							$content = get_sub_field('paragraphe');

                                    if(!$image){
                                      $textClass = 'col col-sm-12';
                                    }

                                    if($photoPos === 'left'){
                                      ?>

                                      <div class="row contentRow">
                                        <?php
                                        if($image){
                                          ?>
                                        <div class="col col-sm-4" >
                                          <img src="<?php echo $url; ?>" />
                                        </div>
                                        <?php
                                      }
                                        ?>
                                        <div class="<?php echo $textClass ?>">
                                          <?php
                                          if($sousTitre){
                                            echo '<h4>'.$sousTitre.'</h4>';
                                          }

                                          echo $content;
                                          ?>
                                        </div>
                                      </div>


                                      <?php
                                    }elseif ($photoPos === 'right') {
                                      ?>
                                      <div class="row contentRow">
                                        <div class="<?php echo $textClass ?>">
                                          <?php
                                          if($sousTitre){
                                            echo '<h4>'.$sousTitre.'</h4>';
                                          }
                                          echo $content;
                                          ?>
                                        </div>

                                        <?php
                                        if($image){
                                          ?>
                                        <div class="col col-sm-4" >
                                          <img src="<?php echo $url; ?>" />
                                        </div>
                                        <?php
                                      }
                                        ?>

                                      </div>
                                      <?php
                                    }



                      						endwhile;
                      					endif;

                                ?>

                           		</div> <!-- end content -->



                              </div>
                           <?php

                            endif; // end if template 9 -->

                              ?>



             </div><!-- end page -->






             <?php   endwhile;

            else :

                // no layouts found

            endif;

            ?>



           </section>


        </div><!-- main_content -->



        <?php  get_footer(); ?>


