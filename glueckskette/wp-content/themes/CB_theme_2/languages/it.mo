��    #      4  /   L           	  ,        H     P  9   e     �  �   �  I   6  f   �  �   �     k  
   x     �  	   �  	   �     �     �     �     �               &     3  1   @     r  7   �  A   �  (         0  %   Q  )   w     �     �  	   �  �  �     �	  B   �	  	   
     
  8   ,
     e
  m   s
  S   �
  v   5  y   �     &     @     P  	   Y  	   c  0   m     �     �     �     �     �     �     �  =   �  &   4  D   [  M   �  5   �     $  /   D  .   t     �  '   �     �                                                         	                                   "         
   !                  #                                   'Download pdf DE' <li><img src="%1$s/images/rrr_DE.png"/></li> ADRESSE App erhältlich bei: Archiv der Jahresberichte und -rechungen der Glückskette CB_RVB_DE.png Der Jahresbericht der Glückskette kann online gelesen (Desktop und Tablets) oder wie die Jahresrechnung als PDF heruntergeladen werden. Die Glückskette ist eine Stiftung, gegründet auf Initiative der SRG SSR Die Jahresrechung der Glückskette ist als PDF verfügbar und kann als solches heruntergeladen werden. Die früheren Jahresberichte und -rechungen der Glückskette sind als PDF verfügbar und können als solche heruntergeladen werden. Glückskette Hauptseite Herunterladen IMPRESSUM Impressum Jahresbericht der Glückskette Jahresrechnung der Glückskette KONTAKT Partnerschaft & Zusammenarbeit Rechtliche Hinweise active en_lang_menu fr_lang_menu ga('create', 'UA-48635925-3', 'glueckskette.ch'); http://www.glueckskette.ch http://www.glueckskette.ch/de/disclaimer/impressum.html http://www.glueckskette.ch/de/disclaimer/rechtliche-hinweise.html http://www.glueckskette.ch/de/latest.xml https://twitter.com/glueckskette https://www.facebook.com/glueckskette https://www.youtube.com/user/glueckskette it_lang_menu mailto:info@glueckskette.ch nach oben Project-Id-Version: CB trad
POT-Creation-Date: 2016-03-24 14:12+0100
PO-Revision-Date: 2016-03-24 14:12+0100
Last-Translator: lorenzo <print@options-it.ch>
Language-Team: fx <lp.options@gmail.com>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.8
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: _e;__
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
 'Download pdf IT' <li style="display:none;"><img src="%1$s/images/rrr_DE.png"/></li> INDIRIZZI Applicazione disponibile su: Archivi dei rapporti annuali e dei rendiconti finanziari CB_RVB_IT.png Il rapporto annuale può essere visualizzato sullo schermo (su computer o tablet) o scaricato in formato PDF.  La Catena della Solidarietà è una fondazione, creata su iniziativa della SRG SSR Il rendiconto finanziario della Catena della Solidarietà è disponibile in formato PDF e può anche essere scaricato. I rapporti annuali e i rendiconti finanziari precedenti sono disponibili in formato PDF e possono anche essere scaricati. Catena della Solidarietà Sito principale Download IMPRESSUM Impressum Rapporto annuale della Catena della Solidarietà Rendiconto finanziario CONTATTO Partenariato e collaborazione Indicazioni legali        ga('create', 'UA-48635925-4', 'catena-della-solidarieta.ch'); http://www.catena-della-solidarieta.ch http://www.catena-della-solidarieta.ch/it/disclaimer/impressum.html  http://www.catena-della-solidarieta.ch/it/disclaimer/indicazioni-legali.html  http://www.catena-della-solidarieta.ch/it/latest.xml  https://twitter.com/solidarieta https://www.facebook.com/catenadellasolidarieta https://www.youtube.com/user/CatenaSolidarieta active mailto:info@catena-della-solidarieta.ch torna su 