��    #      4  /   L           	  ,        H     P  9   e     �  �   �  I   6  f   �  �   �     k  
   x     �  	   �  	   �     �     �     �     �               &     3  1   @     r  7   �  A   �  (         0  %   Q  )   w     �     �  	   �  �  �     �	  ,   �	     �	     �	  ,   
     A
  �   O
  J   �
  r   1  �   �     0     C     R  	   `  	   j  '   t  (   �     �     �     �     �     �     �  ,        3  2   I  9   |  $   �  $   �  )      -   *     X     Z     q                                                         	                                   "         
   !                  #                                   'Download pdf DE' <li><img src="%1$s/images/rrr_DE.png"/></li> ADRESSE App erhältlich bei: Archiv der Jahresberichte und -rechungen der Glückskette CB_RVB_DE.png Der Jahresbericht der Glückskette kann online gelesen (Desktop und Tablets) oder wie die Jahresrechnung als PDF heruntergeladen werden. Die Glückskette ist eine Stiftung, gegründet auf Initiative der SRG SSR Die Jahresrechung der Glückskette ist als PDF verfügbar und kann als solches heruntergeladen werden. Die früheren Jahresberichte und -rechungen der Glückskette sind als PDF verfügbar und können als solche heruntergeladen werden. Glückskette Hauptseite Herunterladen IMPRESSUM Impressum Jahresbericht der Glückskette Jahresrechnung der Glückskette KONTAKT Partnerschaft & Zusammenarbeit Rechtliche Hinweise active en_lang_menu fr_lang_menu ga('create', 'UA-48635925-3', 'glueckskette.ch'); http://www.glueckskette.ch http://www.glueckskette.ch/de/disclaimer/impressum.html http://www.glueckskette.ch/de/disclaimer/rechtliche-hinweise.html http://www.glueckskette.ch/de/latest.xml https://twitter.com/glueckskette https://www.facebook.com/glueckskette https://www.youtube.com/user/glueckskette it_lang_menu mailto:info@glueckskette.ch nach oben Project-Id-Version: CB trad
POT-Creation-Date: 2016-03-24 14:12+0100
PO-Revision-Date: 2016-03-24 14:12+0100
Last-Translator: lorenzo <print@options-it.ch>
Language-Team: fx <lp.options@gmail.com>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.8
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: _e;__
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
 'Download pdf FR' <li><img src="%1$s/images/rrr_FR.png"/></li> ADRESSES Application disponible sur:  Archives des rapports et des comptes annuels CB_RVB_FR.png Le rapport annuel de la Chaîne du Bonheur peut être lu sur écran (ordinateur ou tablette) ou il peut être téléchargé comme les comptes annuels.  La Chaîne du Bonheur est une fondation issue d'une initiative de SRG SSR Les comptes annuels de la Chaîne du Bonheur sont disponibles en format PDF et peuvent aussi être téléchargés. Les rapports et comptes annuels précédents de la Chaîne du Bonheur sont disponibles en format PDF et peuvent aussi être téléchargés. Chaîne du Bonheur Site principal Télécharger IMPRESSUM Impressum Rapport annuel de la Chaîne du Bonheur Comptes annuels de la Chaîne du Bonheur CONTACT Partenariat & collaboration Mentions légales     active ga('create', 'UA-48635925-2', 'bonheur.ch'); http://www.bonheur.ch http://www.bonheur.ch/fr/disclaimer/impressum.html http://www.bonheur.ch/fr/disclaimer/mentions-legales.html http://www.bonheur.ch/fr/latest.xml  https://twitter.com/chainedubonheur   https://www.facebook.com/chainedubonheur https://www.youtube.com/user/ChaineduBonheur    mailto:info@bonheur.ch vers le haut 