��    #      4  /   L           	  ,        H     P  9   e     �  �   �  I   6  f   �  �   �     k  
   x     �  	   �  	   �     �     �     �     �               &     3  1   @     r  7   �  A   �  (         0  %   Q  )   w     �     �  	   �  �  �     �	  B   �	     
     
  &    
     G
  {   U
  \   �
  X   .  h   �     �  	                       $     C     b     j     �     �     �     �  6   �      �  :   �  @   2  -   s  #   �  )   �  ,   �                ?                                                         	                                   "         
   !                  #                                   'Download pdf DE' <li><img src="%1$s/images/rrr_DE.png"/></li> ADRESSE App erhältlich bei: Archiv der Jahresberichte und -rechungen der Glückskette CB_RVB_DE.png Der Jahresbericht der Glückskette kann online gelesen (Desktop und Tablets) oder wie die Jahresrechnung als PDF heruntergeladen werden. Die Glückskette ist eine Stiftung, gegründet auf Initiative der SRG SSR Die Jahresrechung der Glückskette ist als PDF verfügbar und kann als solches heruntergeladen werden. Die früheren Jahresberichte und -rechungen der Glückskette sind als PDF verfügbar und können als solche heruntergeladen werden. Glückskette Hauptseite Herunterladen IMPRESSUM Impressum Jahresbericht der Glückskette Jahresrechnung der Glückskette KONTAKT Partnerschaft & Zusammenarbeit Rechtliche Hinweise active en_lang_menu fr_lang_menu ga('create', 'UA-48635925-3', 'glueckskette.ch'); http://www.glueckskette.ch http://www.glueckskette.ch/de/disclaimer/impressum.html http://www.glueckskette.ch/de/disclaimer/rechtliche-hinweise.html http://www.glueckskette.ch/de/latest.xml https://twitter.com/glueckskette https://www.facebook.com/glueckskette https://www.youtube.com/user/glueckskette it_lang_menu mailto:info@glueckskette.ch nach oben Project-Id-Version: CB trad
POT-Creation-Date: 2016-03-24 17:40+0100
PO-Revision-Date: 2016-03-24 17:57+0100
Last-Translator: lorenzo <print@options-it.ch>
Language-Team: fx <lp.options@gmail.com>
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.8
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: _e;__
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
 'Download pdf EN' <li style="display:none;"><img src="%1$s/images/rrr_DE.png"/></li> ADDRESS App available on:  Archive of annual reports and accounts CB_RVB_EN.png The Swiss Solidarity Annual Report can be read onscreen (desktop or tablet computer) or downloaded like the annual accounts Swiss Solidarity is a foundation, started as an initiative of the Swiss public media SRG SSR The Swiss Solidarity Annual Report is available in PDF format and can also be downloaded Previous years’ Swiss Solidarity annual reports are available in PDF format and can also be downloaded Swiss Solidarity Main site Download IMPRINT Imprint Swiss Solidarity Annual Report Swiss Solidarity Annual Report CONTACT Partnership & collaboration Legal notices   active   ga('create', 'UA-48635925-1', 'swiss-solidarity.org'); http://www.swiss-solidarity.org/ http://www.swiss-solidarity.org/en/disclaimer/imprint.html http://www.swiss-solidarity.org/en/disclaimer/legal-notices.html http://www.swiss-solidarity.org/en/latest.xml https://twitter.com/swisssolidarity https://www.facebook.com/swisssolidarity  https://www.youtube.com/user/SwissSolidarity   mailto:info@swiss-solidarity.org back to top 